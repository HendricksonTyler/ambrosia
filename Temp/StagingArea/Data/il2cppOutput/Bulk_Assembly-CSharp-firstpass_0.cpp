﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityStandardAssets.Water.Displace
struct Displace_t357053105;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// System.String
struct String_t;
// UnityStandardAssets.Water.GerstnerDisplace
struct GerstnerDisplace_t4155590855;
// UnityStandardAssets.Water.MeshContainer
struct MeshContainer_t299039902;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityStandardAssets.Water.PlanarReflection
struct PlanarReflection_t2931543887;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Type
struct Type_t;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Object
struct Object_t1021602117;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>
struct Dictionary_2_t2882765492;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3417634846;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Skybox
struct Skybox_t2033495038;
// UnityStandardAssets.Water.SpecularLighting
struct SpecularLighting_t2824122729;
// UnityStandardAssets.Water.Water
struct Water_t1562849653;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>
struct Dictionary_2_t3541619047;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// UnityEngine.Renderer
struct Renderer_t257310565;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.FlareLayer
struct FlareLayer_t1985082419;
// UnityStandardAssets.Water.WaterBase
struct WaterBase_t4291487940;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityStandardAssets.Water.WaterTile
struct WaterTile_t1797616825;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t62501539;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Camera>
struct IEqualityComparer_1_t3697061051;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Camera,System.Boolean,System.Collections.DictionaryEntry>
struct Transform_1_t1346648271;
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Camera,UnityEngine.Camera,System.Collections.DictionaryEntry>
struct Transform_1_t858378736;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Reflection.MemberFilter
struct MemberFilter_t3405857066;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;

extern Il2CppCodeGenString* _stringLiteral327363994;
extern Il2CppCodeGenString* _stringLiteral245880664;
extern const uint32_t Displace_OnEnable_m438579546_MetadataUsageId;
extern const uint32_t Displace_OnDisable_m2084492719_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3918988257;
extern const uint32_t PlanarReflection__ctor_m1279159428_MetadataUsageId;
extern const RuntimeType* WaterBase_t4291487940_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* WaterBase_t4291487940_il2cpp_TypeInfo_var;
extern const uint32_t PlanarReflection_Start_m3592061564_MetadataUsageId;
extern const RuntimeType* Camera_t189460977_0_0_0_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern RuntimeClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisCamera_t189460977_m4200645945_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2319874103;
extern const uint32_t PlanarReflection_CreateReflectionCameraFor_m2967596404_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2515819385;
extern const uint32_t PlanarReflection_SetStandardCameraParameter_m1836612147_MetadataUsageId;
extern RuntimeClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern RuntimeClass* RenderTexture_t2666733923_il2cpp_TypeInfo_var;
extern const uint32_t PlanarReflection_CreateTextureFor_m3515003827_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t2882765492_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1695344951_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m2633323810_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m1418719199_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m3072793477_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m1469868346_RuntimeMethod_var;
extern const uint32_t PlanarReflection_RenderHelpCameras_m1380237535_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_Clear_m1038150726_RuntimeMethod_var;
extern const uint32_t PlanarReflection_LateUpdate_m557651377_MetadataUsageId;
extern const uint32_t PlanarReflection_WaterTileBeingRendered_m187266240_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1412582911;
extern Il2CppCodeGenString* _stringLiteral2466513744;
extern const uint32_t PlanarReflection_OnEnable_m1108670012_MetadataUsageId;
extern const uint32_t PlanarReflection_OnDisable_m239492945_MetadataUsageId;
extern const RuntimeType* Skybox_t2033495038_0_0_0_var;
extern RuntimeClass* Skybox_t2033495038_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern RuntimeClass* Matrix4x4_t2933234003_il2cpp_TypeInfo_var;
extern const uint32_t PlanarReflection_RenderReflectionFor_m2334755602_MetadataUsageId;
extern RuntimeClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t PlanarReflection_CalculateObliqueMatrix_m253681959_MetadataUsageId;
extern const uint32_t PlanarReflection_CameraSpacePlane_m1013868673_MetadataUsageId;
extern const uint32_t SpecularLighting_Start_m1391288300_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1565133518;
extern const uint32_t SpecularLighting_Update_m1227123071_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t3541619047_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1440922840_RuntimeMethod_var;
extern const uint32_t Water__ctor_m150162646_MetadataUsageId;
extern RuntimeClass* Water_t1562849653_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1174916207;
extern Il2CppCodeGenString* _stringLiteral2001222817;
extern const uint32_t Water_OnWillRenderObject_m1679173826_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m1484377914_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1539012380_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m2952023721_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1121419475_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1187418175_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m4188076919_RuntimeMethod_var;
extern const uint32_t Water_OnDisable_m3913667107_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3402504634;
extern Il2CppCodeGenString* _stringLiteral1816258960;
extern Il2CppCodeGenString* _stringLiteral3749796871;
extern Il2CppCodeGenString* _stringLiteral784172996;
extern const uint32_t Water_Update_m382650723_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisSkybox_t2033495038_m2860073064_RuntimeMethod_var;
extern const uint32_t Water_UpdateCameraModes_m282959606_MetadataUsageId;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m353229995_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisFlareLayer_t1985082419_m2043234002_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m2824272795_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral615419946;
extern Il2CppCodeGenString* _stringLiteral3365593692;
extern Il2CppCodeGenString* _stringLiteral378319007;
extern Il2CppCodeGenString* _stringLiteral648446824;
extern Il2CppCodeGenString* _stringLiteral2767343314;
extern const uint32_t Water_CreateWaterObjects_m3159049619_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2872901958;
extern Il2CppCodeGenString* _stringLiteral305787587;
extern Il2CppCodeGenString* _stringLiteral3079388997;
extern const uint32_t Water_FindHardwareWaterSupport_m3758364632_MetadataUsageId;
extern const uint32_t Water_CameraSpacePlane_m240838003_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1611267702;
extern Il2CppCodeGenString* _stringLiteral121570896;
extern const uint32_t WaterBase_UpdateShader_m3822819655_MetadataUsageId;
extern const uint32_t WaterBase_WaterTileBeingRendered_m2956654727_MetadataUsageId;
extern const uint32_t WaterBase_Update_m716995708_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisPlanarReflection_t2931543887_m147844090_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisWaterBase_t4291487940_m2297562155_RuntimeMethod_var;
extern const uint32_t WaterTile_AcquireComponents_m3172643562_MetadataUsageId;
extern const uint32_t WaterTile_OnWillRenderObject_m3692221246_MetadataUsageId;

struct Vector3U5BU5D_t1172311765;
struct TypeU5BU5D_t1664964607;
struct ObjectU5BU5D_t3614634134;


#ifndef U3CMODULEU3E_T3783534234_H
#define U3CMODULEU3E_T3783534234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534234 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534234_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_T2882765492_H
#define DICTIONARY_2_T2882765492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>
struct  Dictionary_2_t2882765492  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	CameraU5BU5D_t3079764780* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	BooleanU5BU5D_t3568034315* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___keySlots_6)); }
	inline CameraU5BU5D_t3079764780* get_keySlots_6() const { return ___keySlots_6; }
	inline CameraU5BU5D_t3079764780** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(CameraU5BU5D_t3079764780* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___valueSlots_7)); }
	inline BooleanU5BU5D_t3568034315* get_valueSlots_7() const { return ___valueSlots_7; }
	inline BooleanU5BU5D_t3568034315** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(BooleanU5BU5D_t3568034315* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2882765492_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1346648271 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2882765492_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1346648271 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1346648271 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1346648271 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2882765492_H
#ifndef DICTIONARY_2_T3541619047_H
#define DICTIONARY_2_T3541619047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>
struct  Dictionary_2_t3541619047  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	CameraU5BU5D_t3079764780* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	CameraU5BU5D_t3079764780* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___keySlots_6)); }
	inline CameraU5BU5D_t3079764780* get_keySlots_6() const { return ___keySlots_6; }
	inline CameraU5BU5D_t3079764780** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(CameraU5BU5D_t3079764780* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___valueSlots_7)); }
	inline CameraU5BU5D_t3079764780* get_valueSlots_7() const { return ___valueSlots_7; }
	inline CameraU5BU5D_t3079764780** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(CameraU5BU5D_t3079764780* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3541619047_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t858378736 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3541619047_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t858378736 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t858378736 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t858378736 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3541619047_H
#ifndef MESHCONTAINER_T299039902_H
#define MESHCONTAINER_T299039902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.MeshContainer
struct  MeshContainer_t299039902  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityStandardAssets.Water.MeshContainer::mesh
	Mesh_t1356156583 * ___mesh_0;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::vertices
	Vector3U5BU5D_t1172311765* ___vertices_1;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::normals
	Vector3U5BU5D_t1172311765* ___normals_2;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(MeshContainer_t299039902, ___mesh_0)); }
	inline Mesh_t1356156583 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t1356156583 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t1356156583 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_0), value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(MeshContainer_t299039902, ___vertices_1)); }
	inline Vector3U5BU5D_t1172311765* get_vertices_1() const { return ___vertices_1; }
	inline Vector3U5BU5D_t1172311765** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(Vector3U5BU5D_t1172311765* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_1), value);
	}

	inline static int32_t get_offset_of_normals_2() { return static_cast<int32_t>(offsetof(MeshContainer_t299039902, ___normals_2)); }
	inline Vector3U5BU5D_t1172311765* get_normals_2() const { return ___normals_2; }
	inline Vector3U5BU5D_t1172311765** get_address_of_normals_2() { return &___normals_2; }
	inline void set_normals_2(Vector3U5BU5D_t1172311765* value)
	{
		___normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___normals_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCONTAINER_T299039902_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef LAYERMASK_T3188175821_H
#define LAYERMASK_T3188175821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3188175821 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3188175821, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3188175821_H
#ifndef MATRIX4X4_T2933234003_H
#define MATRIX4X4_T2933234003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2933234003 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2933234003_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2933234003  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2933234003  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2933234003  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2933234003 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2933234003  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2933234003  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2933234003 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2933234003  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2933234003_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef DOUBLE_T4078015681_H
#define DOUBLE_T4078015681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t4078015681 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t4078015681, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T4078015681_H
#ifndef KEYVALUEPAIR_2_T1298964269_H
#define KEYVALUEPAIR_2_T1298964269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,UnityEngine.Camera>
struct  KeyValuePair_2_t1298964269 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Camera_t189460977 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Camera_t189460977 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1298964269, ___key_0)); }
	inline Camera_t189460977 * get_key_0() const { return ___key_0; }
	inline Camera_t189460977 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Camera_t189460977 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1298964269, ___value_1)); }
	inline Camera_t189460977 * get_value_1() const { return ___value_1; }
	inline Camera_t189460977 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Camera_t189460977 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1298964269_H
#ifndef KEYVALUEPAIR_2_T38854645_H
#define KEYVALUEPAIR_2_T38854645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t38854645 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t38854645, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t38854645, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T38854645_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef DEPTHTEXTUREMODE_T1156392273_H
#define DEPTHTEXTUREMODE_T1156392273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DepthTextureMode
struct  DepthTextureMode_t1156392273 
{
public:
	// System.Int32 UnityEngine.DepthTextureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DepthTextureMode_t1156392273, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHTEXTUREMODE_T1156392273_H
#ifndef RENDERINGPATH_T1538007819_H
#define RENDERINGPATH_T1538007819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderingPath
struct  RenderingPath_t1538007819 
{
public:
	// System.Int32 UnityEngine.RenderingPath::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderingPath_t1538007819, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERINGPATH_T1538007819_H
#ifndef ENUMERATOR_T566676453_H
#define ENUMERATOR_T566676453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,UnityEngine.Camera>
struct  Enumerator_t566676453 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3541619047 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1298964269  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t566676453, ___dictionary_0)); }
	inline Dictionary_2_t3541619047 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3541619047 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3541619047 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t566676453, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t566676453, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t566676453, ___current_3)); }
	inline KeyValuePair_2_t1298964269  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1298964269 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1298964269  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T566676453_H
#ifndef WATERMODE_T2303503718_H
#define WATERMODE_T2303503718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water/WaterMode
struct  WaterMode_t2303503718 
{
public:
	// System.Int32 UnityStandardAssets.Water.Water/WaterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaterMode_t2303503718, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERMODE_T2303503718_H
#ifndef ENUMERATOR_T3601534125_H
#define ENUMERATOR_T3601534125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t3601534125 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t2281509423 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t38854645  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___dictionary_0)); }
	inline Dictionary_2_t2281509423 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2281509423 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2281509423 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___current_3)); }
	inline KeyValuePair_2_t38854645  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t38854645 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t38854645  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3601534125_H
#ifndef HIDEFLAGS_T1434274199_H
#define HIDEFLAGS_T1434274199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t1434274199 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t1434274199, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T1434274199_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef CAMERACLEARFLAGS_T452084705_H
#define CAMERACLEARFLAGS_T452084705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t452084705 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t452084705, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T452084705_H
#ifndef BINDINGFLAGS_T1082350898_H
#define BINDINGFLAGS_T1082350898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1082350898 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1082350898, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1082350898_H
#ifndef RENDERTEXTUREFORMAT_T3360518468_H
#define RENDERTEXTUREFORMAT_T3360518468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t3360518468 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t3360518468, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T3360518468_H
#ifndef WATERQUALITY_T3485013746_H
#define WATERQUALITY_T3485013746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterQuality
struct  WaterQuality_t3485013746 
{
public:
	// System.Int32 UnityStandardAssets.Water.WaterQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaterQuality_t3485013746, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERQUALITY_T3485013746_H
#ifndef RUNTIMETYPEHANDLE_T2330101084_H
#define RUNTIMETYPEHANDLE_T2330101084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t2330101084 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t2330101084, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T2330101084_H
#ifndef SHADER_T2430389951_H
#define SHADER_T2430389951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_t2430389951  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T2430389951_H
#ifndef MESH_T1356156583_H
#define MESH_T1356156583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t1356156583  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T1356156583_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MATERIAL_T193706927_H
#define MATERIAL_T193706927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t193706927  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T193706927_H
#ifndef GAMEOBJECT_T1756533147_H
#define GAMEOBJECT_T1756533147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1756533147  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1756533147_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t2330101084  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t2330101084  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t2330101084 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t2330101084  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1664964607* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t3405857066 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t3405857066 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t3405857066 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1664964607* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1664964607* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t3405857066 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t3405857066 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t3405857066 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t3405857066 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t3405857066 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t3405857066 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef TEXTURE_T2243626319_H
#define TEXTURE_T2243626319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2243626319  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2243626319_H
#ifndef RENDERTEXTURE_T2666733923_H
#define RENDERTEXTURE_T2666733923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t2666733923  : public Texture_t2243626319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T2666733923_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef RENDERER_T257310565_H
#define RENDERER_T257310565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t257310565  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T257310565_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef SKYBOX_T2033495038_H
#define SKYBOX_T2033495038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Skybox
struct  Skybox_t2033495038  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKYBOX_T2033495038_H
#ifndef CAMERA_T189460977_H
#define CAMERA_T189460977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t189460977  : public Behaviour_t955675639
{
public:

public:
};

struct Camera_t189460977_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t834278767 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t834278767 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t834278767 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t834278767 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t834278767 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t834278767 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t834278767 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t834278767 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t834278767 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t834278767 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t834278767 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t834278767 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T189460977_H
#ifndef FLARELAYER_T1985082419_H
#define FLARELAYER_T1985082419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FlareLayer
struct  FlareLayer_t1985082419  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLARELAYER_T1985082419_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef WATERBASE_T4291487940_H
#define WATERBASE_T4291487940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterBase
struct  WaterBase_t4291487940  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material UnityStandardAssets.Water.WaterBase::sharedMaterial
	Material_t193706927 * ___sharedMaterial_2;
	// UnityStandardAssets.Water.WaterQuality UnityStandardAssets.Water.WaterBase::waterQuality
	int32_t ___waterQuality_3;
	// System.Boolean UnityStandardAssets.Water.WaterBase::edgeBlend
	bool ___edgeBlend_4;

public:
	inline static int32_t get_offset_of_sharedMaterial_2() { return static_cast<int32_t>(offsetof(WaterBase_t4291487940, ___sharedMaterial_2)); }
	inline Material_t193706927 * get_sharedMaterial_2() const { return ___sharedMaterial_2; }
	inline Material_t193706927 ** get_address_of_sharedMaterial_2() { return &___sharedMaterial_2; }
	inline void set_sharedMaterial_2(Material_t193706927 * value)
	{
		___sharedMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterial_2), value);
	}

	inline static int32_t get_offset_of_waterQuality_3() { return static_cast<int32_t>(offsetof(WaterBase_t4291487940, ___waterQuality_3)); }
	inline int32_t get_waterQuality_3() const { return ___waterQuality_3; }
	inline int32_t* get_address_of_waterQuality_3() { return &___waterQuality_3; }
	inline void set_waterQuality_3(int32_t value)
	{
		___waterQuality_3 = value;
	}

	inline static int32_t get_offset_of_edgeBlend_4() { return static_cast<int32_t>(offsetof(WaterBase_t4291487940, ___edgeBlend_4)); }
	inline bool get_edgeBlend_4() const { return ___edgeBlend_4; }
	inline bool* get_address_of_edgeBlend_4() { return &___edgeBlend_4; }
	inline void set_edgeBlend_4(bool value)
	{
		___edgeBlend_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERBASE_T4291487940_H
#ifndef DISPLACE_T357053105_H
#define DISPLACE_T357053105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Displace
struct  Displace_t357053105  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLACE_T357053105_H
#ifndef SPECULARLIGHTING_T2824122729_H
#define SPECULARLIGHTING_T2824122729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.SpecularLighting
struct  SpecularLighting_t2824122729  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform UnityStandardAssets.Water.SpecularLighting::specularLight
	Transform_t3275118058 * ___specularLight_2;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.SpecularLighting::m_WaterBase
	WaterBase_t4291487940 * ___m_WaterBase_3;

public:
	inline static int32_t get_offset_of_specularLight_2() { return static_cast<int32_t>(offsetof(SpecularLighting_t2824122729, ___specularLight_2)); }
	inline Transform_t3275118058 * get_specularLight_2() const { return ___specularLight_2; }
	inline Transform_t3275118058 ** get_address_of_specularLight_2() { return &___specularLight_2; }
	inline void set_specularLight_2(Transform_t3275118058 * value)
	{
		___specularLight_2 = value;
		Il2CppCodeGenWriteBarrier((&___specularLight_2), value);
	}

	inline static int32_t get_offset_of_m_WaterBase_3() { return static_cast<int32_t>(offsetof(SpecularLighting_t2824122729, ___m_WaterBase_3)); }
	inline WaterBase_t4291487940 * get_m_WaterBase_3() const { return ___m_WaterBase_3; }
	inline WaterBase_t4291487940 ** get_address_of_m_WaterBase_3() { return &___m_WaterBase_3; }
	inline void set_m_WaterBase_3(WaterBase_t4291487940 * value)
	{
		___m_WaterBase_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaterBase_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECULARLIGHTING_T2824122729_H
#ifndef WATERTILE_T1797616825_H
#define WATERTILE_T1797616825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterTile
struct  WaterTile_t1797616825  : public MonoBehaviour_t1158329972
{
public:
	// UnityStandardAssets.Water.PlanarReflection UnityStandardAssets.Water.WaterTile::reflection
	PlanarReflection_t2931543887 * ___reflection_2;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.WaterTile::waterBase
	WaterBase_t4291487940 * ___waterBase_3;

public:
	inline static int32_t get_offset_of_reflection_2() { return static_cast<int32_t>(offsetof(WaterTile_t1797616825, ___reflection_2)); }
	inline PlanarReflection_t2931543887 * get_reflection_2() const { return ___reflection_2; }
	inline PlanarReflection_t2931543887 ** get_address_of_reflection_2() { return &___reflection_2; }
	inline void set_reflection_2(PlanarReflection_t2931543887 * value)
	{
		___reflection_2 = value;
		Il2CppCodeGenWriteBarrier((&___reflection_2), value);
	}

	inline static int32_t get_offset_of_waterBase_3() { return static_cast<int32_t>(offsetof(WaterTile_t1797616825, ___waterBase_3)); }
	inline WaterBase_t4291487940 * get_waterBase_3() const { return ___waterBase_3; }
	inline WaterBase_t4291487940 ** get_address_of_waterBase_3() { return &___waterBase_3; }
	inline void set_waterBase_3(WaterBase_t4291487940 * value)
	{
		___waterBase_3 = value;
		Il2CppCodeGenWriteBarrier((&___waterBase_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERTILE_T1797616825_H
#ifndef WATER_T1562849653_H
#define WATER_T1562849653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water
struct  Water_t1562849653  : public MonoBehaviour_t1158329972
{
public:
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::waterMode
	int32_t ___waterMode_2;
	// System.Boolean UnityStandardAssets.Water.Water::disablePixelLights
	bool ___disablePixelLights_3;
	// System.Int32 UnityStandardAssets.Water.Water::textureSize
	int32_t ___textureSize_4;
	// System.Single UnityStandardAssets.Water.Water::clipPlaneOffset
	float ___clipPlaneOffset_5;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::reflectLayers
	LayerMask_t3188175821  ___reflectLayers_6;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::refractLayers
	LayerMask_t3188175821  ___refractLayers_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_ReflectionCameras
	Dictionary_2_t3541619047 * ___m_ReflectionCameras_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_RefractionCameras
	Dictionary_2_t3541619047 * ___m_RefractionCameras_9;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_ReflectionTexture
	RenderTexture_t2666733923 * ___m_ReflectionTexture_10;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_RefractionTexture
	RenderTexture_t2666733923 * ___m_RefractionTexture_11;
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::m_HardwareWaterSupport
	int32_t ___m_HardwareWaterSupport_12;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldReflectionTextureSize
	int32_t ___m_OldReflectionTextureSize_13;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldRefractionTextureSize
	int32_t ___m_OldRefractionTextureSize_14;

public:
	inline static int32_t get_offset_of_waterMode_2() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___waterMode_2)); }
	inline int32_t get_waterMode_2() const { return ___waterMode_2; }
	inline int32_t* get_address_of_waterMode_2() { return &___waterMode_2; }
	inline void set_waterMode_2(int32_t value)
	{
		___waterMode_2 = value;
	}

	inline static int32_t get_offset_of_disablePixelLights_3() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___disablePixelLights_3)); }
	inline bool get_disablePixelLights_3() const { return ___disablePixelLights_3; }
	inline bool* get_address_of_disablePixelLights_3() { return &___disablePixelLights_3; }
	inline void set_disablePixelLights_3(bool value)
	{
		___disablePixelLights_3 = value;
	}

	inline static int32_t get_offset_of_textureSize_4() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___textureSize_4)); }
	inline int32_t get_textureSize_4() const { return ___textureSize_4; }
	inline int32_t* get_address_of_textureSize_4() { return &___textureSize_4; }
	inline void set_textureSize_4(int32_t value)
	{
		___textureSize_4 = value;
	}

	inline static int32_t get_offset_of_clipPlaneOffset_5() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___clipPlaneOffset_5)); }
	inline float get_clipPlaneOffset_5() const { return ___clipPlaneOffset_5; }
	inline float* get_address_of_clipPlaneOffset_5() { return &___clipPlaneOffset_5; }
	inline void set_clipPlaneOffset_5(float value)
	{
		___clipPlaneOffset_5 = value;
	}

	inline static int32_t get_offset_of_reflectLayers_6() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___reflectLayers_6)); }
	inline LayerMask_t3188175821  get_reflectLayers_6() const { return ___reflectLayers_6; }
	inline LayerMask_t3188175821 * get_address_of_reflectLayers_6() { return &___reflectLayers_6; }
	inline void set_reflectLayers_6(LayerMask_t3188175821  value)
	{
		___reflectLayers_6 = value;
	}

	inline static int32_t get_offset_of_refractLayers_7() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___refractLayers_7)); }
	inline LayerMask_t3188175821  get_refractLayers_7() const { return ___refractLayers_7; }
	inline LayerMask_t3188175821 * get_address_of_refractLayers_7() { return &___refractLayers_7; }
	inline void set_refractLayers_7(LayerMask_t3188175821  value)
	{
		___refractLayers_7 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCameras_8() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_ReflectionCameras_8)); }
	inline Dictionary_2_t3541619047 * get_m_ReflectionCameras_8() const { return ___m_ReflectionCameras_8; }
	inline Dictionary_2_t3541619047 ** get_address_of_m_ReflectionCameras_8() { return &___m_ReflectionCameras_8; }
	inline void set_m_ReflectionCameras_8(Dictionary_2_t3541619047 * value)
	{
		___m_ReflectionCameras_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCameras_8), value);
	}

	inline static int32_t get_offset_of_m_RefractionCameras_9() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_RefractionCameras_9)); }
	inline Dictionary_2_t3541619047 * get_m_RefractionCameras_9() const { return ___m_RefractionCameras_9; }
	inline Dictionary_2_t3541619047 ** get_address_of_m_RefractionCameras_9() { return &___m_RefractionCameras_9; }
	inline void set_m_RefractionCameras_9(Dictionary_2_t3541619047 * value)
	{
		___m_RefractionCameras_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionCameras_9), value);
	}

	inline static int32_t get_offset_of_m_ReflectionTexture_10() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_ReflectionTexture_10)); }
	inline RenderTexture_t2666733923 * get_m_ReflectionTexture_10() const { return ___m_ReflectionTexture_10; }
	inline RenderTexture_t2666733923 ** get_address_of_m_ReflectionTexture_10() { return &___m_ReflectionTexture_10; }
	inline void set_m_ReflectionTexture_10(RenderTexture_t2666733923 * value)
	{
		___m_ReflectionTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionTexture_10), value);
	}

	inline static int32_t get_offset_of_m_RefractionTexture_11() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_RefractionTexture_11)); }
	inline RenderTexture_t2666733923 * get_m_RefractionTexture_11() const { return ___m_RefractionTexture_11; }
	inline RenderTexture_t2666733923 ** get_address_of_m_RefractionTexture_11() { return &___m_RefractionTexture_11; }
	inline void set_m_RefractionTexture_11(RenderTexture_t2666733923 * value)
	{
		___m_RefractionTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionTexture_11), value);
	}

	inline static int32_t get_offset_of_m_HardwareWaterSupport_12() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_HardwareWaterSupport_12)); }
	inline int32_t get_m_HardwareWaterSupport_12() const { return ___m_HardwareWaterSupport_12; }
	inline int32_t* get_address_of_m_HardwareWaterSupport_12() { return &___m_HardwareWaterSupport_12; }
	inline void set_m_HardwareWaterSupport_12(int32_t value)
	{
		___m_HardwareWaterSupport_12 = value;
	}

	inline static int32_t get_offset_of_m_OldReflectionTextureSize_13() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_OldReflectionTextureSize_13)); }
	inline int32_t get_m_OldReflectionTextureSize_13() const { return ___m_OldReflectionTextureSize_13; }
	inline int32_t* get_address_of_m_OldReflectionTextureSize_13() { return &___m_OldReflectionTextureSize_13; }
	inline void set_m_OldReflectionTextureSize_13(int32_t value)
	{
		___m_OldReflectionTextureSize_13 = value;
	}

	inline static int32_t get_offset_of_m_OldRefractionTextureSize_14() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_OldRefractionTextureSize_14)); }
	inline int32_t get_m_OldRefractionTextureSize_14() const { return ___m_OldRefractionTextureSize_14; }
	inline int32_t* get_address_of_m_OldRefractionTextureSize_14() { return &___m_OldRefractionTextureSize_14; }
	inline void set_m_OldRefractionTextureSize_14(int32_t value)
	{
		___m_OldRefractionTextureSize_14 = value;
	}
};

struct Water_t1562849653_StaticFields
{
public:
	// System.Boolean UnityStandardAssets.Water.Water::s_InsideWater
	bool ___s_InsideWater_15;

public:
	inline static int32_t get_offset_of_s_InsideWater_15() { return static_cast<int32_t>(offsetof(Water_t1562849653_StaticFields, ___s_InsideWater_15)); }
	inline bool get_s_InsideWater_15() const { return ___s_InsideWater_15; }
	inline bool* get_address_of_s_InsideWater_15() { return &___s_InsideWater_15; }
	inline void set_s_InsideWater_15(bool value)
	{
		___s_InsideWater_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATER_T1562849653_H
#ifndef PLANARREFLECTION_T2931543887_H
#define PLANARREFLECTION_T2931543887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.PlanarReflection
struct  PlanarReflection_t2931543887  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.LayerMask UnityStandardAssets.Water.PlanarReflection::reflectionMask
	LayerMask_t3188175821  ___reflectionMask_2;
	// System.Boolean UnityStandardAssets.Water.PlanarReflection::reflectSkybox
	bool ___reflectSkybox_3;
	// UnityEngine.Color UnityStandardAssets.Water.PlanarReflection::clearColor
	Color_t2020392075  ___clearColor_4;
	// System.String UnityStandardAssets.Water.PlanarReflection::reflectionSampler
	String_t* ___reflectionSampler_5;
	// System.Single UnityStandardAssets.Water.PlanarReflection::clipPlaneOffset
	float ___clipPlaneOffset_6;
	// UnityEngine.Vector3 UnityStandardAssets.Water.PlanarReflection::m_Oldpos
	Vector3_t2243707580  ___m_Oldpos_7;
	// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::m_ReflectionCamera
	Camera_t189460977 * ___m_ReflectionCamera_8;
	// UnityEngine.Material UnityStandardAssets.Water.PlanarReflection::m_SharedMaterial
	Material_t193706927 * ___m_SharedMaterial_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean> UnityStandardAssets.Water.PlanarReflection::m_HelperCameras
	Dictionary_2_t2882765492 * ___m_HelperCameras_10;

public:
	inline static int32_t get_offset_of_reflectionMask_2() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___reflectionMask_2)); }
	inline LayerMask_t3188175821  get_reflectionMask_2() const { return ___reflectionMask_2; }
	inline LayerMask_t3188175821 * get_address_of_reflectionMask_2() { return &___reflectionMask_2; }
	inline void set_reflectionMask_2(LayerMask_t3188175821  value)
	{
		___reflectionMask_2 = value;
	}

	inline static int32_t get_offset_of_reflectSkybox_3() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___reflectSkybox_3)); }
	inline bool get_reflectSkybox_3() const { return ___reflectSkybox_3; }
	inline bool* get_address_of_reflectSkybox_3() { return &___reflectSkybox_3; }
	inline void set_reflectSkybox_3(bool value)
	{
		___reflectSkybox_3 = value;
	}

	inline static int32_t get_offset_of_clearColor_4() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___clearColor_4)); }
	inline Color_t2020392075  get_clearColor_4() const { return ___clearColor_4; }
	inline Color_t2020392075 * get_address_of_clearColor_4() { return &___clearColor_4; }
	inline void set_clearColor_4(Color_t2020392075  value)
	{
		___clearColor_4 = value;
	}

	inline static int32_t get_offset_of_reflectionSampler_5() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___reflectionSampler_5)); }
	inline String_t* get_reflectionSampler_5() const { return ___reflectionSampler_5; }
	inline String_t** get_address_of_reflectionSampler_5() { return &___reflectionSampler_5; }
	inline void set_reflectionSampler_5(String_t* value)
	{
		___reflectionSampler_5 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionSampler_5), value);
	}

	inline static int32_t get_offset_of_clipPlaneOffset_6() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___clipPlaneOffset_6)); }
	inline float get_clipPlaneOffset_6() const { return ___clipPlaneOffset_6; }
	inline float* get_address_of_clipPlaneOffset_6() { return &___clipPlaneOffset_6; }
	inline void set_clipPlaneOffset_6(float value)
	{
		___clipPlaneOffset_6 = value;
	}

	inline static int32_t get_offset_of_m_Oldpos_7() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___m_Oldpos_7)); }
	inline Vector3_t2243707580  get_m_Oldpos_7() const { return ___m_Oldpos_7; }
	inline Vector3_t2243707580 * get_address_of_m_Oldpos_7() { return &___m_Oldpos_7; }
	inline void set_m_Oldpos_7(Vector3_t2243707580  value)
	{
		___m_Oldpos_7 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCamera_8() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___m_ReflectionCamera_8)); }
	inline Camera_t189460977 * get_m_ReflectionCamera_8() const { return ___m_ReflectionCamera_8; }
	inline Camera_t189460977 ** get_address_of_m_ReflectionCamera_8() { return &___m_ReflectionCamera_8; }
	inline void set_m_ReflectionCamera_8(Camera_t189460977 * value)
	{
		___m_ReflectionCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCamera_8), value);
	}

	inline static int32_t get_offset_of_m_SharedMaterial_9() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___m_SharedMaterial_9)); }
	inline Material_t193706927 * get_m_SharedMaterial_9() const { return ___m_SharedMaterial_9; }
	inline Material_t193706927 ** get_address_of_m_SharedMaterial_9() { return &___m_SharedMaterial_9; }
	inline void set_m_SharedMaterial_9(Material_t193706927 * value)
	{
		___m_SharedMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_SharedMaterial_9), value);
	}

	inline static int32_t get_offset_of_m_HelperCameras_10() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___m_HelperCameras_10)); }
	inline Dictionary_2_t2882765492 * get_m_HelperCameras_10() const { return ___m_HelperCameras_10; }
	inline Dictionary_2_t2882765492 ** get_address_of_m_HelperCameras_10() { return &___m_HelperCameras_10; }
	inline void set_m_HelperCameras_10(Dictionary_2_t2882765492 * value)
	{
		___m_HelperCameras_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_HelperCameras_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANARREFLECTION_T2931543887_H
#ifndef GERSTNERDISPLACE_T4155590855_H
#define GERSTNERDISPLACE_T4155590855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.GerstnerDisplace
struct  GerstnerDisplace_t4155590855  : public Displace_t357053105
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GERSTNERDISPLACE_T4155590855_H
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};
// System.Type[]
struct TypeU5BU5D_t1664964607  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C"  void Dictionary_2__ctor_m3420539152_gshared (Dictionary_2_t3417634846 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m2024407803_gshared (Dictionary_2_t3417634846 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m3435012856_gshared (Dictionary_2_t3417634846 * __this, RuntimeObject * p0, bool p1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(!0)
extern "C"  bool Dictionary_2_get_Item_m2522967366_gshared (Dictionary_2_t3417634846 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m353126835_gshared (Dictionary_2_t3417634846 * __this, RuntimeObject * p0, bool p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C"  void Dictionary_2_Clear_m3504688039_gshared (Dictionary_2_t3417634846 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m584589095_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2724124387_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3601534125  Dictionary_2_GetEnumerator_m3077639147_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m1091361971_gshared (Enumerator_t3601534125 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3349738440_gshared (Enumerator_t3601534125 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1905011127_gshared (Enumerator_t3601534125 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2325793156_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3975825838_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m1004257024_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1825328214 (MonoBehaviour_t1158329972 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m646668963 (Behaviour_t955675639 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Displace::OnEnable()
extern "C"  void Displace_OnEnable_m438579546 (Displace_t357053105 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Displace::OnDisable()
extern "C"  void Displace_OnDisable_m2084492719 (Displace_t357053105 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::EnableKeyword(System.String)
extern "C"  void Shader_EnableKeyword_m2347844001 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::DisableKeyword(System.String)
extern "C"  void Shader_DisableKeyword_m710761032 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Displace::.ctor()
extern "C"  void Displace__ctor_m767499238 (Displace_t357053105 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t1172311765* Mesh_get_vertices_m2446703813 (Mesh_t1356156583 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C"  Vector3U5BU5D_t1172311765* Mesh_get_normals_m3761590924 (Mesh_t1356156583 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C"  void Mesh_set_vertices_m512067060 (Mesh_t1356156583 * __this, Vector3U5BU5D_t1172311765* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
extern "C"  void Mesh_set_normals_m2778470027 (Mesh_t1356156583 * __this, Vector3U5BU5D_t1172311765* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_grey()
extern "C"  Color_t2020392075  Color_get_grey_m844259442 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m2159020946 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t3819376471 * GameObject_GetComponent_m724360550 (GameObject_t1756533147 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m3369637820 (Object_t1021602117 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m279709565 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m1757773010 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C"  void GameObject__ctor_m3412194074 (GameObject_t1756533147 * __this, String_t* p0, TypeU5BU5D_t1664964607* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C"  Component_t3819376471 * GameObject_AddComponent_m540062519 (GameObject_t1756533147 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t189460977_m4200645945(__this, method) ((  Camera_t189460977 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
extern "C"  void Camera_set_backgroundColor_m4178084823 (Camera_t189460977 * __this, Color_t2020392075  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
extern "C"  void Camera_set_clearFlags_m489868550 (Camera_t189460977 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.PlanarReflection::SetStandardCameraParameter(UnityEngine.Camera,UnityEngine.LayerMask)
extern "C"  void PlanarReflection_SetStandardCameraParameter_m1836612147 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, LayerMask_t3188175821  ___mask1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t2666733923 * Camera_get_targetTexture_m505846373 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityStandardAssets.Water.PlanarReflection::CreateTextureFor(UnityEngine.Camera)
extern "C"  RenderTexture_t2666733923 * PlanarReflection_CreateTextureFor_m3515003827 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
extern "C"  void Camera_set_targetTexture_m3042745048 (Camera_t189460977 * __this, RenderTexture_t2666733923 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C"  int32_t LayerMask_op_Implicit_m1604355880 (RuntimeObject * __this /* static, unused */, LayerMask_t3188175821  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C"  int32_t LayerMask_NameToLayer_m3213714080 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern "C"  void Camera_set_cullingMask_m2961679457 (Camera_t189460977 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2020392075  Color_get_black_m455352838 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m602406666 (Behaviour_t955675639 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelWidth()
extern "C"  int32_t Camera_get_pixelWidth_m2894134608 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m824771903 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelHeight()
extern "C"  int32_t Camera_get_pixelHeight_m1187016139 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void RenderTexture__ctor_m4010205058 (RenderTexture_t2666733923 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m3275307757 (Object_t1021602117 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::.ctor()
#define Dictionary_2__ctor_m1695344951(__this, method) ((  void (*) (Dictionary_2_t2882765492 *, const RuntimeMethod*))Dictionary_2__ctor_m3420539152_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m2633323810(__this, p0, method) ((  bool (*) (Dictionary_2_t2882765492 *, Camera_t189460977 *, const RuntimeMethod*))Dictionary_2_ContainsKey_m2024407803_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::Add(!0,!1)
#define Dictionary_2_Add_m1418719199(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2882765492 *, Camera_t189460977 *, bool, const RuntimeMethod*))Dictionary_2_Add_m3435012856_gshared)(__this, p0, p1, method)
// !1 System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::get_Item(!0)
#define Dictionary_2_get_Item_m3072793477(__this, p0, method) ((  bool (*) (Dictionary_2_t2882765492 *, Camera_t189460977 *, const RuntimeMethod*))Dictionary_2_get_Item_m2522967366_gshared)(__this, p0, method)
// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::CreateReflectionCameraFor(UnityEngine.Camera)
extern "C"  Camera_t189460977 * PlanarReflection_CreateReflectionCameraFor_m2967596404 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.PlanarReflection::RenderReflectionFor(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void PlanarReflection_RenderReflectionFor_m2334755602 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, Camera_t189460977 * ___reflectCamera1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m1469868346(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2882765492 *, Camera_t189460977 *, bool, const RuntimeMethod*))Dictionary_2_set_Item_m353126835_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>::Clear()
#define Dictionary_2_Clear_m1038150726(__this, method) ((  void (*) (Dictionary_2_t2882765492 *, const RuntimeMethod*))Dictionary_2_Clear_m3504688039_gshared)(__this, method)
// System.Void UnityStandardAssets.Water.PlanarReflection::RenderHelpCameras(UnityEngine.Camera)
extern "C"  void PlanarReflection_RenderHelpCameras_m1380237535 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___currentCam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m3642434134 (Material_t193706927 * __this, String_t* p0, Texture_t2243626319 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::HasProperty(System.String)
extern "C"  bool Material_HasProperty_m3932416560 (Material_t193706927 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.PlanarReflection::SaneCameraSettings(UnityEngine.Camera)
extern "C"  void PlanarReflection_SaneCameraSettings_m2023309855 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___helperCam0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t3819376471 * Component_GetComponent_m1409092944 (Component_t3819376471 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Skybox::get_material()
extern "C"  Material_t193706927 * Skybox_get_material_m3485302839 (Skybox_t2033495038 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Skybox::set_material(UnityEngine.Material)
extern "C"  void Skybox_set_material_m3485997388 (Skybox_t2033495038 * __this, Material_t193706927 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::set_invertCulling(System.Boolean)
extern "C"  void GL_set_invertCulling_m3669449627 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m3374354972 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t2243707580  Transform_get_eulerAngles_m2112588536 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1555724485 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m1566259983 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m2304215762 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2942701431 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t2243707580  Transform_get_up_m1990654328 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m827632793 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m3879980823 (Vector4_t2243707581 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern "C"  Matrix4x4_t2933234003  Matrix4x4_get_zero_m262690161 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t2933234003  PlanarReflection_CalculateReflectionMatrix_m577471045 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___reflectionMat0, Vector4_t2243707581  ___plane1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Matrix4x4_MultiplyPoint_m3620936054 (Matrix4x4_t2933234003 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_worldToCameraMatrix()
extern "C"  Matrix4x4_t2933234003  Camera_get_worldToCameraMatrix_m3671061738 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  Matrix4x4_t2933234003  Matrix4x4_op_Multiply_m3436371832 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  p0, Matrix4x4_t2933234003  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_worldToCameraMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_worldToCameraMatrix_m2710888845 (Camera_t189460977 * __this, Matrix4x4_t2933234003  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityStandardAssets.Water.PlanarReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t2243707581  PlanarReflection_CameraSpacePlane_m1013868673 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, Vector3_t2243707580  ___pos1, Vector3_t2243707580  ___normal2, float ___sideSign3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C"  Matrix4x4_t2933234003  Camera_get_projectionMatrix_m3387870255 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t2933234003  PlanarReflection_CalculateObliqueMatrix_m253681959 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___projection0, Vector4_t2243707581  ___clipPlane1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m1873250010 (Camera_t189460977 * __this, Matrix4x4_t2933234003  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::Render()
extern "C"  void Camera_Render_m2992476825 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
extern "C"  void Camera_set_depthTextureMode_m132643161 (Camera_t189460977 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_renderingPath(UnityEngine.RenderingPath)
extern "C"  void Camera_set_renderingPath_m3054370453 (Camera_t189460977 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C"  Matrix4x4_t2933234003  Matrix4x4_get_inverse_m1621049107 (Matrix4x4_t2933234003 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Water.PlanarReflection::Sgn(System.Single)
extern "C"  float PlanarReflection_Sgn_m2682791593 (RuntimeObject * __this /* static, unused */, float ___a0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Vector4_t2243707581  Matrix4x4_op_Multiply_m2168451752 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  p0, Vector4_t2243707581  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m962866688 (RuntimeObject * __this /* static, unused */, Vector4_t2243707581  p0, Vector4_t2243707581  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2243707581  Vector4_op_Multiply_m2550150317 (RuntimeObject * __this /* static, unused */, Vector4_t2243707581  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern "C"  float Matrix4x4_get_Item_m4261098696 (Matrix4x4_t2933234003 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m2591303557 (Matrix4x4_t2933234003 * __this, int32_t p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern "C"  float Vector4_get_Item_m2784900302 (Vector4_t2243707581 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m2498445460 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m394909128 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Matrix4x4_MultiplyVector_m4184387139 (Matrix4x4_t2933234003 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2243707580  Vector3_get_normalized_m1057036856 (Vector3_t2243707580 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t2243707580  Transform_get_forward_m2144220796 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t2243707581  Vector4_op_Implicit_m3079003974 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern "C"  void Material_SetVector_m3110165768 (Material_t193706927 * __this, String_t* p0, Vector4_t2243707581  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C"  LayerMask_t3188175821  LayerMask_op_Implicit_m1796475436 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::.ctor()
#define Dictionary_2__ctor_m1440922840(__this, method) ((  void (*) (Dictionary_2_t3541619047 *, const RuntimeMethod*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t257310565_m141370815(__this, method) ((  Renderer_t257310565 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t193706927 * Renderer_get_sharedMaterial_m3033033727 (Renderer_t257310565 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m1795429839 (Renderer_t257310565 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C"  Camera_t189460977 * Camera_get_current_m3769616944 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::FindHardwareWaterSupport()
extern "C"  int32_t Water_FindHardwareWaterSupport_m3758364632 (Water_t1562849653 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::GetWaterMode()
extern "C"  int32_t Water_GetWaterMode_m3222093059 (Water_t1562849653 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern "C"  void Water_CreateWaterObjects_m3159049619 (Water_t1562849653 * __this, Camera_t189460977 * ___currentCamera0, Camera_t189460977 ** ___reflectionCamera1, Camera_t189460977 ** ___refractionCamera2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_pixelLightCount()
extern "C"  int32_t QualitySettings_get_pixelLightCount_m1621733334 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)
extern "C"  void QualitySettings_set_pixelLightCount_m610294691 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Water_UpdateCameraModes_m282959606 (Water_t1562849653 * __this, Camera_t189460977 * ___src0, Camera_t189460977 * ___dest1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern "C"  void Water_CalculateReflectionMatrix_m4117333262 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003 * ___reflectionMat0, Vector4_t2243707581  ___plane1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityStandardAssets.Water.Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t2243707581  Water_CameraSpacePlane_m240838003 (Water_t1562849653 * __this, Camera_t189460977 * ___cam0, Vector3_t2243707580  ___pos1, Vector3_t2243707580  ___normal2, float ___sideSign3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::CalculateObliqueMatrix(UnityEngine.Vector4)
extern "C"  Matrix4x4_t2933234003  Camera_CalculateObliqueMatrix_m3128032212 (Camera_t189460977 * __this, Vector4_t2243707581  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_cullingMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_cullingMatrix_m908594059 (Camera_t189460977 * __this, Matrix4x4_t2933234003  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::get_value()
extern "C"  int32_t LayerMask_get_value_m2828952681 (LayerMask_t3188175821 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GL::get_invertCulling()
extern "C"  bool GL_get_invertCulling_m3538314322 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m2617026815 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m2824446320 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m201325964 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m1484377914(__this, method) ((  Enumerator_t566676453  (*) (Dictionary_2_t3541619047 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3077639147_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,UnityEngine.Camera>::get_Current()
#define Enumerator_get_Current_m1539012380(__this, method) ((  KeyValuePair_2_t1298964269  (*) (Enumerator_t566676453 *, const RuntimeMethod*))Enumerator_get_Current_m1091361971_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,UnityEngine.Camera>::get_Value()
#define KeyValuePair_2_get_Value_m2952023721(__this, method) ((  Camera_t189460977 * (*) (KeyValuePair_2_t1298964269 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1251901674_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,UnityEngine.Camera>::MoveNext()
#define Enumerator_MoveNext_m1121419475(__this, method) ((  bool (*) (Enumerator_t566676453 *, const RuntimeMethod*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,UnityEngine.Camera>::Dispose()
#define Enumerator_Dispose_m1187418175(__this, method) ((  void (*) (Enumerator_t566676453 *, const RuntimeMethod*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::Clear()
#define Dictionary_2_Clear_m4188076919(__this, method) ((  void (*) (Dictionary_2_t3541619047 *, const RuntimeMethod*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// UnityEngine.Vector4 UnityEngine.Material::GetVector(System.String)
extern "C"  Vector4_t2243707581  Material_GetVector_m2705903741 (Material_t193706927 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Material::GetFloat(System.String)
extern "C"  float Material_GetFloat_m1524931699 (Material_t193706927 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern "C"  float Time_get_timeSinceLevelLoad_m1272389819 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::IEEERemainder(System.Double,System.Double)
extern "C"  double Math_IEEERemainder_m340193149 (RuntimeObject * __this /* static, unused */, double p0, double p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2516226135 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m966754839 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Camera::get_backgroundColor()
extern "C"  Color_t2020392075  Camera_get_backgroundColor_m707399190 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Skybox>()
#define Component_GetComponent_TisSkybox_t2033495038_m2860073064(__this, method) ((  Skybox_t2033495038 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m2932657537 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
extern "C"  void Camera_set_farClipPlane_m3207261698 (Camera_t189460977 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m2824342520 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
extern "C"  void Camera_set_nearClipPlane_m143326947 (Camera_t189460977 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Camera::get_orthographic()
extern "C"  bool Camera_get_orthographic_m3688563802 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern "C"  void Camera_set_orthographic_m2016457201 (Camera_t189460977 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m1609126762 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C"  void Camera_set_fieldOfView_m4080277213 (Camera_t189460977 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_aspect()
extern "C"  float Camera_get_aspect_m972674900 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_aspect(System.Single)
extern "C"  void Camera_set_aspect_m3624679551 (Camera_t189460977 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m2553055249 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m4124508114 (Camera_t189460977 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m1193568599 (Object_t1021602117 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m1458854879 (Object_t1021602117 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_isPowerOfTwo(System.Boolean)
extern "C"  void RenderTexture_set_isPowerOfTwo_m554504365 (RenderTexture_t2666733923 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m353229995(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t3541619047 *, Camera_t189460977 *, Camera_t189460977 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3975825838_gshared)(__this, p0, p1, method)
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3881798623 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.FlareLayer>()
#define GameObject_AddComponent_TisFlareLayer_t1985082419_m2043234002(__this, method) ((  FlareLayer_t1985082419 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m2824272795(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3541619047 *, Camera_t189460977 *, Camera_t189460977 *, const RuntimeMethod*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.String UnityEngine.Material::GetTag(System.String,System.Boolean)
extern "C"  String_t* Material_GetTag_m3856861437 (Material_t193706927 * __this, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UnityEngine.Material::get_shader()
extern "C"  Shader_t2430389951 * Material_get_shader_m2582399212 (Material_t193706927 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::set_maximumLOD(System.Int32)
extern "C"  void Shader_set_maximumLOD_m1292647048 (Shader_t2430389951 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
extern "C"  bool SystemInfo_SupportsRenderTextureFormat_m1269175480 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m881971336 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
extern "C"  int32_t Camera_get_depthTextureMode_m4106494306 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern "C"  void WaterBase_UpdateShader_m3822819655 (WaterBase_t4291487940 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.WaterTile::AcquireComponents()
extern "C"  void WaterTile_AcquireComponents_m3172643562 (WaterTile_t1797616825 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m2752514051 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Water.PlanarReflection>()
#define Component_GetComponent_TisPlanarReflection_t2931543887_m147844090(__this, method) ((  PlanarReflection_t2931543887 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Water.WaterBase>()
#define Component_GetComponent_TisWaterBase_t4291487940_m2297562155(__this, method) ((  WaterBase_t4291487940 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void UnityStandardAssets.Water.PlanarReflection::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void PlanarReflection_WaterTileBeingRendered_m187266240 (PlanarReflection_t2931543887 * __this, Transform_t3275118058 * ___tr0, Camera_t189460977 * ___currentCam1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void WaterBase_WaterTileBeingRendered_m2956654727 (WaterBase_t4291487940 * __this, Transform_t3275118058 * ___tr0, Camera_t189460977 * ___currentCam1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Water.Displace::.ctor()
extern "C"  void Displace__ctor_m767499238 (Displace_t357053105 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Displace::Awake()
extern "C"  void Displace_Awake_m2658080107 (Displace_t357053105 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m646668963(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Displace_OnEnable_m438579546(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		Displace_OnDisable_m2084492719(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.Displace::OnEnable()
extern "C"  void Displace_OnEnable_m438579546 (Displace_t357053105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Displace_OnEnable_m438579546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_EnableKeyword_m2347844001(NULL /*static, unused*/, _stringLiteral327363994, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral245880664, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Displace::OnDisable()
extern "C"  void Displace_OnDisable_m2084492719 (Displace_t357053105 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Displace_OnDisable_m2084492719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_EnableKeyword_m2347844001(NULL /*static, unused*/, _stringLiteral245880664, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral327363994, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.GerstnerDisplace::.ctor()
extern "C"  void GerstnerDisplace__ctor_m1245399396 (GerstnerDisplace_t4155590855 * __this, const RuntimeMethod* method)
{
	{
		Displace__ctor_m767499238(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.MeshContainer::.ctor(UnityEngine.Mesh)
extern "C"  void MeshContainer__ctor_m553273975 (MeshContainer_t299039902 * __this, Mesh_t1356156583 * ___m0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_0 = ___m0;
		__this->set_mesh_0(L_0);
		Mesh_t1356156583 * L_1 = ___m0;
		Vector3U5BU5D_t1172311765* L_2 = Mesh_get_vertices_m2446703813(L_1, /*hidden argument*/NULL);
		__this->set_vertices_1(L_2);
		Mesh_t1356156583 * L_3 = ___m0;
		Vector3U5BU5D_t1172311765* L_4 = Mesh_get_normals_m3761590924(L_3, /*hidden argument*/NULL);
		__this->set_normals_2(L_4);
		return;
	}
}
// System.Void UnityStandardAssets.Water.MeshContainer::Update()
extern "C"  void MeshContainer_Update_m4249401906 (MeshContainer_t299039902 * __this, const RuntimeMethod* method)
{
	{
		Mesh_t1356156583 * L_0 = __this->get_mesh_0();
		Vector3U5BU5D_t1172311765* L_1 = __this->get_vertices_1();
		Mesh_set_vertices_m512067060(L_0, L_1, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_2 = __this->get_mesh_0();
		Vector3U5BU5D_t1172311765* L_3 = __this->get_normals_2();
		Mesh_set_normals_m2778470027(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::.ctor()
extern "C"  void PlanarReflection__ctor_m1279159428 (PlanarReflection_t2931543887 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection__ctor_m1279159428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0 = Color_get_grey_m844259442(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_clearColor_4(L_0);
		__this->set_reflectionSampler_5(_stringLiteral3918988257);
		__this->set_clipPlaneOffset_6((0.07f));
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::Start()
extern "C"  void PlanarReflection_Start_m3592061564 (PlanarReflection_t2931543887 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_Start_m3592061564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(WaterBase_t4291487940_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_2 = GameObject_GetComponent_m724360550(L_0, L_1, /*hidden argument*/NULL);
		Material_t193706927 * L_3 = ((WaterBase_t4291487940 *)CastclassClass((RuntimeObject*)L_2, WaterBase_t4291487940_il2cpp_TypeInfo_var))->get_sharedMaterial_2();
		__this->set_m_SharedMaterial_9(L_3);
		return;
	}
}
// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::CreateReflectionCameraFor(UnityEngine.Camera)
extern "C"  Camera_t189460977 * PlanarReflection_CreateReflectionCameraFor_m2967596404 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_CreateReflectionCameraFor_m2967596404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	Camera_t189460977 * V_2 = NULL;
	Camera_t189460977 * G_B6_0 = NULL;
	Camera_t189460977 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	Camera_t189460977 * G_B7_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		String_t* L_1 = Object_get_name_m3369637820(L_0, /*hidden argument*/NULL);
		Camera_t189460977 * L_2 = ___cam0;
		String_t* L_3 = Object_get_name_m3369637820(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m612901809(NULL /*static, unused*/, L_1, _stringLiteral2319874103, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		GameObject_t1756533147 * L_6 = GameObject_Find_m279709565(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GameObject_t1756533147 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_9 = V_0;
		TypeU5BU5D_t1664964607* L_10 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Camera_t189460977_0_0_0_var), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_11);
		GameObject_t1756533147 * L_12 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m3412194074(L_12, L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_0048:
	{
		GameObject_t1756533147 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Camera_t189460977_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_15 = GameObject_GetComponent_m724360550(L_13, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0073;
		}
	}
	{
		GameObject_t1756533147 * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Camera_t189460977_0_0_0_var), /*hidden argument*/NULL);
		GameObject_AddComponent_m540062519(L_17, L_18, /*hidden argument*/NULL);
	}

IL_0073:
	{
		GameObject_t1756533147 * L_19 = V_1;
		Camera_t189460977 * L_20 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_19, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_RuntimeMethod_var);
		V_2 = L_20;
		Camera_t189460977 * L_21 = V_2;
		Color_t2020392075  L_22 = __this->get_clearColor_4();
		Camera_set_backgroundColor_m4178084823(L_21, L_22, /*hidden argument*/NULL);
		Camera_t189460977 * L_23 = V_2;
		bool L_24 = __this->get_reflectSkybox_3();
		G_B5_0 = L_23;
		if (!L_24)
		{
			G_B6_0 = L_23;
			goto IL_0098;
		}
	}
	{
		G_B7_0 = 1;
		G_B7_1 = G_B5_0;
		goto IL_0099;
	}

IL_0098:
	{
		G_B7_0 = 2;
		G_B7_1 = G_B6_0;
	}

IL_0099:
	{
		Camera_set_clearFlags_m489868550(G_B7_1, G_B7_0, /*hidden argument*/NULL);
		Camera_t189460977 * L_25 = V_2;
		LayerMask_t3188175821  L_26 = __this->get_reflectionMask_2();
		PlanarReflection_SetStandardCameraParameter_m1836612147(__this, L_25, L_26, /*hidden argument*/NULL);
		Camera_t189460977 * L_27 = V_2;
		RenderTexture_t2666733923 * L_28 = Camera_get_targetTexture_m505846373(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_00c8;
		}
	}
	{
		Camera_t189460977 * L_30 = V_2;
		Camera_t189460977 * L_31 = ___cam0;
		RenderTexture_t2666733923 * L_32 = PlanarReflection_CreateTextureFor_m3515003827(__this, L_31, /*hidden argument*/NULL);
		Camera_set_targetTexture_m3042745048(L_30, L_32, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		Camera_t189460977 * L_33 = V_2;
		return L_33;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::SetStandardCameraParameter(UnityEngine.Camera,UnityEngine.LayerMask)
extern "C"  void PlanarReflection_SetStandardCameraParameter_m1836612147 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, LayerMask_t3188175821  ___mask1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_SetStandardCameraParameter_m1836612147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = ___cam0;
		LayerMask_t3188175821  L_1 = ___mask1;
		int32_t L_2 = LayerMask_op_Implicit_m1604355880(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_3 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral2515819385, /*hidden argument*/NULL);
		Camera_set_cullingMask_m2961679457(L_0, ((int32_t)((int32_t)L_2&(int32_t)((~((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_3&(int32_t)((int32_t)31))))))))), /*hidden argument*/NULL);
		Camera_t189460977 * L_4 = ___cam0;
		Color_t2020392075  L_5 = Color_get_black_m455352838(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_set_backgroundColor_m4178084823(L_4, L_5, /*hidden argument*/NULL);
		Camera_t189460977 * L_6 = ___cam0;
		Behaviour_set_enabled_m602406666(L_6, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RenderTexture UnityStandardAssets.Water.PlanarReflection::CreateTextureFor(UnityEngine.Camera)
extern "C"  RenderTexture_t2666733923 * PlanarReflection_CreateTextureFor_m3515003827 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_CreateTextureFor_m3515003827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderTexture_t2666733923 * V_0 = NULL;
	{
		Camera_t189460977 * L_0 = ___cam0;
		int32_t L_1 = Camera_get_pixelWidth_m2894134608(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_FloorToInt_m824771903(NULL /*static, unused*/, ((float)((float)(((float)((float)L_1)))*(float)(0.5f))), /*hidden argument*/NULL);
		Camera_t189460977 * L_3 = ___cam0;
		int32_t L_4 = Camera_get_pixelHeight_m1187016139(L_3, /*hidden argument*/NULL);
		int32_t L_5 = Mathf_FloorToInt_m824771903(NULL /*static, unused*/, ((float)((float)(((float)((float)L_4)))*(float)(0.5f))), /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_6 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4010205058(L_6, L_2, L_5, ((int32_t)24), /*hidden argument*/NULL);
		V_0 = L_6;
		RenderTexture_t2666733923 * L_7 = V_0;
		Object_set_hideFlags_m3275307757(L_7, ((int32_t)52), /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_8 = V_0;
		return L_8;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::RenderHelpCameras(UnityEngine.Camera)
extern "C"  void PlanarReflection_RenderHelpCameras_m1380237535 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___currentCam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_RenderHelpCameras_m1380237535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2882765492 * L_0 = __this->get_m_HelperCameras_10();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Dictionary_2_t2882765492 * L_1 = (Dictionary_2_t2882765492 *)il2cpp_codegen_object_new(Dictionary_2_t2882765492_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1695344951(L_1, /*hidden argument*/Dictionary_2__ctor_m1695344951_RuntimeMethod_var);
		__this->set_m_HelperCameras_10(L_1);
	}

IL_0016:
	{
		Dictionary_2_t2882765492 * L_2 = __this->get_m_HelperCameras_10();
		Camera_t189460977 * L_3 = ___currentCam0;
		bool L_4 = Dictionary_2_ContainsKey_m2633323810(L_2, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m2633323810_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		Dictionary_2_t2882765492 * L_5 = __this->get_m_HelperCameras_10();
		Camera_t189460977 * L_6 = ___currentCam0;
		Dictionary_2_Add_m1418719199(L_5, L_6, (bool)0, /*hidden argument*/Dictionary_2_Add_m1418719199_RuntimeMethod_var);
	}

IL_0034:
	{
		Dictionary_2_t2882765492 * L_7 = __this->get_m_HelperCameras_10();
		Camera_t189460977 * L_8 = ___currentCam0;
		bool L_9 = Dictionary_2_get_Item_m3072793477(L_7, L_8, /*hidden argument*/Dictionary_2_get_Item_m3072793477_RuntimeMethod_var);
		if (!L_9)
		{
			goto IL_0046;
		}
	}
	{
		return;
	}

IL_0046:
	{
		Camera_t189460977 * L_10 = __this->get_m_ReflectionCamera_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0063;
		}
	}
	{
		Camera_t189460977 * L_12 = ___currentCam0;
		Camera_t189460977 * L_13 = PlanarReflection_CreateReflectionCameraFor_m2967596404(__this, L_12, /*hidden argument*/NULL);
		__this->set_m_ReflectionCamera_8(L_13);
	}

IL_0063:
	{
		Camera_t189460977 * L_14 = ___currentCam0;
		Camera_t189460977 * L_15 = __this->get_m_ReflectionCamera_8();
		PlanarReflection_RenderReflectionFor_m2334755602(__this, L_14, L_15, /*hidden argument*/NULL);
		Dictionary_2_t2882765492 * L_16 = __this->get_m_HelperCameras_10();
		Camera_t189460977 * L_17 = ___currentCam0;
		Dictionary_2_set_Item_m1469868346(L_16, L_17, (bool)1, /*hidden argument*/Dictionary_2_set_Item_m1469868346_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::LateUpdate()
extern "C"  void PlanarReflection_LateUpdate_m557651377 (PlanarReflection_t2931543887 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_LateUpdate_m557651377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2882765492 * L_0 = __this->get_m_HelperCameras_10();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Dictionary_2_t2882765492 * L_1 = __this->get_m_HelperCameras_10();
		Dictionary_2_Clear_m1038150726(L_1, /*hidden argument*/Dictionary_2_Clear_m1038150726_RuntimeMethod_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void PlanarReflection_WaterTileBeingRendered_m187266240 (PlanarReflection_t2931543887 * __this, Transform_t3275118058 * ___tr0, Camera_t189460977 * ___currentCam1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_WaterTileBeingRendered_m187266240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = ___currentCam1;
		PlanarReflection_RenderHelpCameras_m1380237535(__this, L_0, /*hidden argument*/NULL);
		Camera_t189460977 * L_1 = __this->get_m_ReflectionCamera_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0043;
		}
	}
	{
		Material_t193706927 * L_3 = __this->get_m_SharedMaterial_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		Material_t193706927 * L_5 = __this->get_m_SharedMaterial_9();
		String_t* L_6 = __this->get_reflectionSampler_5();
		Camera_t189460977 * L_7 = __this->get_m_ReflectionCamera_8();
		RenderTexture_t2666733923 * L_8 = Camera_get_targetTexture_m505846373(L_7, /*hidden argument*/NULL);
		Material_SetTexture_m3642434134(L_5, L_6, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::OnEnable()
extern "C"  void PlanarReflection_OnEnable_m1108670012 (PlanarReflection_t2931543887 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_OnEnable_m1108670012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_EnableKeyword_m2347844001(NULL /*static, unused*/, _stringLiteral1412582911, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral2466513744, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::OnDisable()
extern "C"  void PlanarReflection_OnDisable_m239492945 (PlanarReflection_t2931543887 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_OnDisable_m239492945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Shader_EnableKeyword_m2347844001(NULL /*static, unused*/, _stringLiteral2466513744, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral1412582911, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::RenderReflectionFor(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void PlanarReflection_RenderReflectionFor_m2334755602 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, Camera_t189460977 * ___reflectCamera1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_RenderReflectionFor_m2334755602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Skybox_t2033495038 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector4_t2243707581  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Matrix4x4_t2933234003  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector4_t2243707581  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Matrix4x4_t2933234003  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Camera_t189460977 * G_B7_0 = NULL;
	Camera_t189460977 * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	Camera_t189460977 * G_B8_1 = NULL;
	{
		Camera_t189460977 * L_0 = ___reflectCamera1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Material_t193706927 * L_2 = __this->get_m_SharedMaterial_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		Material_t193706927 * L_4 = __this->get_m_SharedMaterial_9();
		String_t* L_5 = __this->get_reflectionSampler_5();
		bool L_6 = Material_HasProperty_m3932416560(L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0033;
		}
	}
	{
		return;
	}

IL_0033:
	{
		Camera_t189460977 * L_7 = ___reflectCamera1;
		LayerMask_t3188175821  L_8 = __this->get_reflectionMask_2();
		int32_t L_9 = LayerMask_op_Implicit_m1604355880(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		int32_t L_10 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral2515819385, /*hidden argument*/NULL);
		Camera_set_cullingMask_m2961679457(L_7, ((int32_t)((int32_t)L_9&(int32_t)((~((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)31))))))))), /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = ___reflectCamera1;
		PlanarReflection_SaneCameraSettings_m2023309855(__this, L_11, /*hidden argument*/NULL);
		Camera_t189460977 * L_12 = ___reflectCamera1;
		Color_t2020392075  L_13 = __this->get_clearColor_4();
		Camera_set_backgroundColor_m4178084823(L_12, L_13, /*hidden argument*/NULL);
		Camera_t189460977 * L_14 = ___reflectCamera1;
		bool L_15 = __this->get_reflectSkybox_3();
		G_B6_0 = L_14;
		if (!L_15)
		{
			G_B7_0 = L_14;
			goto IL_007a;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		goto IL_007b;
	}

IL_007a:
	{
		G_B8_0 = 2;
		G_B8_1 = G_B7_0;
	}

IL_007b:
	{
		Camera_set_clearFlags_m489868550(G_B8_1, G_B8_0, /*hidden argument*/NULL);
		bool L_16 = __this->get_reflectSkybox_3();
		if (!L_16)
		{
			goto IL_010b;
		}
	}
	{
		Camera_t189460977 * L_17 = ___cam0;
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m2159020946(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Skybox_t2033495038_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_20 = GameObject_GetComponent_m724360550(L_18, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_010b;
		}
	}
	{
		Camera_t189460977 * L_22 = ___reflectCamera1;
		GameObject_t1756533147 * L_23 = Component_get_gameObject_m2159020946(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Skybox_t2033495038_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_25 = GameObject_GetComponent_m724360550(L_23, L_24, /*hidden argument*/NULL);
		V_0 = ((Skybox_t2033495038 *)CastclassSealed((RuntimeObject*)L_25, Skybox_t2033495038_il2cpp_TypeInfo_var));
		Skybox_t2033495038 * L_26 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00eb;
		}
	}
	{
		Camera_t189460977 * L_28 = ___reflectCamera1;
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m2159020946(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_30 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Skybox_t2033495038_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_31 = GameObject_AddComponent_m540062519(L_29, L_30, /*hidden argument*/NULL);
		V_0 = ((Skybox_t2033495038 *)CastclassSealed((RuntimeObject*)L_31, Skybox_t2033495038_il2cpp_TypeInfo_var));
	}

IL_00eb:
	{
		Skybox_t2033495038 * L_32 = V_0;
		Camera_t189460977 * L_33 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Skybox_t2033495038_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_35 = Component_GetComponent_m1409092944(L_33, L_34, /*hidden argument*/NULL);
		Material_t193706927 * L_36 = Skybox_get_material_m3485302839(((Skybox_t2033495038 *)CastclassSealed((RuntimeObject*)L_35, Skybox_t2033495038_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Skybox_set_material_m3485997388(L_32, L_36, /*hidden argument*/NULL);
	}

IL_010b:
	{
		GL_set_invertCulling_m3669449627(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_37 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		V_1 = L_37;
		Camera_t189460977 * L_38 = ___cam0;
		Transform_t3275118058 * L_39 = Component_get_transform_m3374354972(L_38, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = Transform_get_eulerAngles_m2112588536(L_39, /*hidden argument*/NULL);
		V_2 = L_40;
		Camera_t189460977 * L_41 = ___reflectCamera1;
		Transform_t3275118058 * L_42 = Component_get_transform_m3374354972(L_41, /*hidden argument*/NULL);
		float L_43 = (&V_2)->get_x_1();
		float L_44 = (&V_2)->get_y_2();
		float L_45 = (&V_2)->get_z_3();
		Vector3_t2243707580  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Vector3__ctor_m1555724485((&L_46), ((-L_43)), L_44, L_45, /*hidden argument*/NULL);
		Transform_set_eulerAngles_m1566259983(L_42, L_46, /*hidden argument*/NULL);
		Camera_t189460977 * L_47 = ___reflectCamera1;
		Transform_t3275118058 * L_48 = Component_get_transform_m3374354972(L_47, /*hidden argument*/NULL);
		Camera_t189460977 * L_49 = ___cam0;
		Transform_t3275118058 * L_50 = Component_get_transform_m3374354972(L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = Transform_get_position_m2304215762(L_50, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_48, L_51, /*hidden argument*/NULL);
		Transform_t3275118058 * L_52 = V_1;
		Transform_t3275118058 * L_53 = Component_get_transform_m3374354972(L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = Transform_get_position_m2304215762(L_53, /*hidden argument*/NULL);
		V_3 = L_54;
		Transform_t3275118058 * L_55 = V_1;
		Vector3_t2243707580  L_56 = Transform_get_position_m2304215762(L_55, /*hidden argument*/NULL);
		V_4 = L_56;
		float L_57 = (&V_4)->get_y_2();
		(&V_3)->set_y_2(L_57);
		Transform_t3275118058 * L_58 = V_1;
		Transform_t3275118058 * L_59 = Component_get_transform_m3374354972(L_58, /*hidden argument*/NULL);
		Vector3_t2243707580  L_60 = Transform_get_up_m1990654328(L_59, /*hidden argument*/NULL);
		V_5 = L_60;
		Vector3_t2243707580  L_61 = V_5;
		Vector3_t2243707580  L_62 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		float L_63 = Vector3_Dot_m827632793(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		float L_64 = __this->get_clipPlaneOffset_6();
		V_6 = ((float)((float)((-L_63))-(float)L_64));
		float L_65 = (&V_5)->get_x_1();
		float L_66 = (&V_5)->get_y_2();
		float L_67 = (&V_5)->get_z_3();
		float L_68 = V_6;
		Vector4__ctor_m3879980823((&V_7), L_65, L_66, L_67, L_68, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t2933234003_il2cpp_TypeInfo_var);
		Matrix4x4_t2933234003  L_69 = Matrix4x4_get_zero_m262690161(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = L_69;
		Matrix4x4_t2933234003  L_70 = V_8;
		Vector4_t2243707581  L_71 = V_7;
		Matrix4x4_t2933234003  L_72 = PlanarReflection_CalculateReflectionMatrix_m577471045(NULL /*static, unused*/, L_70, L_71, /*hidden argument*/NULL);
		V_8 = L_72;
		Camera_t189460977 * L_73 = ___cam0;
		Transform_t3275118058 * L_74 = Component_get_transform_m3374354972(L_73, /*hidden argument*/NULL);
		Vector3_t2243707580  L_75 = Transform_get_position_m2304215762(L_74, /*hidden argument*/NULL);
		__this->set_m_Oldpos_7(L_75);
		Vector3_t2243707580  L_76 = __this->get_m_Oldpos_7();
		Vector3_t2243707580  L_77 = Matrix4x4_MultiplyPoint_m3620936054((&V_8), L_76, /*hidden argument*/NULL);
		V_9 = L_77;
		Camera_t189460977 * L_78 = ___reflectCamera1;
		Camera_t189460977 * L_79 = ___cam0;
		Matrix4x4_t2933234003  L_80 = Camera_get_worldToCameraMatrix_m3671061738(L_79, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_81 = V_8;
		Matrix4x4_t2933234003  L_82 = Matrix4x4_op_Multiply_m3436371832(NULL /*static, unused*/, L_80, L_81, /*hidden argument*/NULL);
		Camera_set_worldToCameraMatrix_m2710888845(L_78, L_82, /*hidden argument*/NULL);
		Camera_t189460977 * L_83 = ___reflectCamera1;
		Vector3_t2243707580  L_84 = V_3;
		Vector3_t2243707580  L_85 = V_5;
		Vector4_t2243707581  L_86 = PlanarReflection_CameraSpacePlane_m1013868673(__this, L_83, L_84, L_85, (1.0f), /*hidden argument*/NULL);
		V_10 = L_86;
		Camera_t189460977 * L_87 = ___cam0;
		Matrix4x4_t2933234003  L_88 = Camera_get_projectionMatrix_m3387870255(L_87, /*hidden argument*/NULL);
		V_11 = L_88;
		Matrix4x4_t2933234003  L_89 = V_11;
		Vector4_t2243707581  L_90 = V_10;
		Matrix4x4_t2933234003  L_91 = PlanarReflection_CalculateObliqueMatrix_m253681959(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
		V_11 = L_91;
		Camera_t189460977 * L_92 = ___reflectCamera1;
		Matrix4x4_t2933234003  L_93 = V_11;
		Camera_set_projectionMatrix_m1873250010(L_92, L_93, /*hidden argument*/NULL);
		Camera_t189460977 * L_94 = ___reflectCamera1;
		Transform_t3275118058 * L_95 = Component_get_transform_m3374354972(L_94, /*hidden argument*/NULL);
		Vector3_t2243707580  L_96 = V_9;
		Transform_set_position_m2942701431(L_95, L_96, /*hidden argument*/NULL);
		Camera_t189460977 * L_97 = ___cam0;
		Transform_t3275118058 * L_98 = Component_get_transform_m3374354972(L_97, /*hidden argument*/NULL);
		Vector3_t2243707580  L_99 = Transform_get_eulerAngles_m2112588536(L_98, /*hidden argument*/NULL);
		V_12 = L_99;
		Camera_t189460977 * L_100 = ___reflectCamera1;
		Transform_t3275118058 * L_101 = Component_get_transform_m3374354972(L_100, /*hidden argument*/NULL);
		float L_102 = (&V_12)->get_x_1();
		float L_103 = (&V_12)->get_y_2();
		float L_104 = (&V_12)->get_z_3();
		Vector3_t2243707580  L_105;
		memset(&L_105, 0, sizeof(L_105));
		Vector3__ctor_m1555724485((&L_105), ((-L_102)), L_103, L_104, /*hidden argument*/NULL);
		Transform_set_eulerAngles_m1566259983(L_101, L_105, /*hidden argument*/NULL);
		Camera_t189460977 * L_106 = ___reflectCamera1;
		Camera_Render_m2992476825(L_106, /*hidden argument*/NULL);
		GL_set_invertCulling_m3669449627(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.PlanarReflection::SaneCameraSettings(UnityEngine.Camera)
extern "C"  void PlanarReflection_SaneCameraSettings_m2023309855 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___helperCam0, const RuntimeMethod* method)
{
	{
		Camera_t189460977 * L_0 = ___helperCam0;
		Camera_set_depthTextureMode_m132643161(L_0, 0, /*hidden argument*/NULL);
		Camera_t189460977 * L_1 = ___helperCam0;
		Color_t2020392075  L_2 = Color_get_black_m455352838(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_set_backgroundColor_m4178084823(L_1, L_2, /*hidden argument*/NULL);
		Camera_t189460977 * L_3 = ___helperCam0;
		Camera_set_clearFlags_m489868550(L_3, 2, /*hidden argument*/NULL);
		Camera_t189460977 * L_4 = ___helperCam0;
		Camera_set_renderingPath_m3054370453(L_4, 1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t2933234003  PlanarReflection_CalculateObliqueMatrix_m253681959 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___projection0, Vector4_t2243707581  ___clipPlane1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_CalculateObliqueMatrix_m253681959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t2243707581  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Matrix4x4_t2933234003  L_0 = Matrix4x4_get_inverse_m1621049107((&___projection0), /*hidden argument*/NULL);
		float L_1 = (&___clipPlane1)->get_x_1();
		float L_2 = PlanarReflection_Sgn_m2682791593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		float L_3 = (&___clipPlane1)->get_y_2();
		float L_4 = PlanarReflection_Sgn_m2682791593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector4_t2243707581  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m3879980823((&L_5), L_2, L_4, (1.0f), (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t2933234003_il2cpp_TypeInfo_var);
		Vector4_t2243707581  L_6 = Matrix4x4_op_Multiply_m2168451752(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector4_t2243707581  L_7 = ___clipPlane1;
		Vector4_t2243707581  L_8 = ___clipPlane1;
		Vector4_t2243707581  L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2243707581_il2cpp_TypeInfo_var);
		float L_10 = Vector4_Dot_m962866688(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector4_t2243707581  L_11 = Vector4_op_Multiply_m2550150317(NULL /*static, unused*/, L_7, ((float)((float)(2.0f)/(float)L_10)), /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = (&V_1)->get_x_1();
		float L_13 = Matrix4x4_get_Item_m4261098696((&___projection0), 3, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m2591303557((&___projection0), 2, ((float)((float)L_12-(float)L_13)), /*hidden argument*/NULL);
		float L_14 = (&V_1)->get_y_2();
		float L_15 = Matrix4x4_get_Item_m4261098696((&___projection0), 7, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m2591303557((&___projection0), 6, ((float)((float)L_14-(float)L_15)), /*hidden argument*/NULL);
		float L_16 = (&V_1)->get_z_3();
		float L_17 = Matrix4x4_get_Item_m4261098696((&___projection0), ((int32_t)11), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m2591303557((&___projection0), ((int32_t)10), ((float)((float)L_16-(float)L_17)), /*hidden argument*/NULL);
		float L_18 = (&V_1)->get_w_4();
		float L_19 = Matrix4x4_get_Item_m4261098696((&___projection0), ((int32_t)15), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m2591303557((&___projection0), ((int32_t)14), ((float)((float)L_18-(float)L_19)), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_20 = ___projection0;
		return L_20;
	}
}
// UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t2933234003  PlanarReflection_CalculateReflectionMatrix_m577471045 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___reflectionMat0, Vector4_t2243707581  ___plane1, const RuntimeMethod* method)
{
	{
		float L_0 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		float L_1 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m00_0(((float)((float)(1.0f)-(float)((float)((float)((float)((float)(2.0f)*(float)L_0))*(float)L_1)))));
		float L_2 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		float L_3 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m01_4(((float)((float)((float)((float)(-2.0f)*(float)L_2))*(float)L_3)));
		float L_4 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		float L_5 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m02_8(((float)((float)((float)((float)(-2.0f)*(float)L_4))*(float)L_5)));
		float L_6 = Vector4_get_Item_m2784900302((&___plane1), 3, /*hidden argument*/NULL);
		float L_7 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m03_12(((float)((float)((float)((float)(-2.0f)*(float)L_6))*(float)L_7)));
		float L_8 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		float L_9 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m10_1(((float)((float)((float)((float)(-2.0f)*(float)L_8))*(float)L_9)));
		float L_10 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		float L_11 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m11_5(((float)((float)(1.0f)-(float)((float)((float)((float)((float)(2.0f)*(float)L_10))*(float)L_11)))));
		float L_12 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		float L_13 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m12_9(((float)((float)((float)((float)(-2.0f)*(float)L_12))*(float)L_13)));
		float L_14 = Vector4_get_Item_m2784900302((&___plane1), 3, /*hidden argument*/NULL);
		float L_15 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m13_13(((float)((float)((float)((float)(-2.0f)*(float)L_14))*(float)L_15)));
		float L_16 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		float L_17 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m20_2(((float)((float)((float)((float)(-2.0f)*(float)L_16))*(float)L_17)));
		float L_18 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		float L_19 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m21_6(((float)((float)((float)((float)(-2.0f)*(float)L_18))*(float)L_19)));
		float L_20 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		float L_21 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m22_10(((float)((float)(1.0f)-(float)((float)((float)((float)((float)(2.0f)*(float)L_20))*(float)L_21)))));
		float L_22 = Vector4_get_Item_m2784900302((&___plane1), 3, /*hidden argument*/NULL);
		float L_23 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		(&___reflectionMat0)->set_m23_14(((float)((float)((float)((float)(-2.0f)*(float)L_22))*(float)L_23)));
		(&___reflectionMat0)->set_m30_3((0.0f));
		(&___reflectionMat0)->set_m31_7((0.0f));
		(&___reflectionMat0)->set_m32_11((0.0f));
		(&___reflectionMat0)->set_m33_15((1.0f));
		Matrix4x4_t2933234003  L_24 = ___reflectionMat0;
		return L_24;
	}
}
// System.Single UnityStandardAssets.Water.PlanarReflection::Sgn(System.Single)
extern "C"  float PlanarReflection_Sgn_m2682791593 (RuntimeObject * __this /* static, unused */, float ___a0, const RuntimeMethod* method)
{
	{
		float L_0 = ___a0;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (1.0f);
	}

IL_0011:
	{
		float L_1 = ___a0;
		if ((!(((float)L_1) < ((float)(0.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (-1.0f);
	}

IL_0022:
	{
		return (0.0f);
	}
}
// UnityEngine.Vector4 UnityStandardAssets.Water.PlanarReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t2243707581  PlanarReflection_CameraSpacePlane_m1013868673 (PlanarReflection_t2931543887 * __this, Camera_t189460977 * ___cam0, Vector3_t2243707580  ___pos1, Vector3_t2243707580  ___normal2, float ___sideSign3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlanarReflection_CameraSpacePlane_m1013868673_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t2933234003  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector3_t2243707580  L_0 = ___pos1;
		Vector3_t2243707580  L_1 = ___normal2;
		float L_2 = __this->get_clipPlaneOffset_6();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Camera_t189460977 * L_5 = ___cam0;
		Matrix4x4_t2933234003  L_6 = Camera_get_worldToCameraMatrix_m3671061738(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t2243707580  L_7 = V_0;
		Vector3_t2243707580  L_8 = Matrix4x4_MultiplyPoint_m3620936054((&V_1), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3_t2243707580  L_9 = ___normal2;
		Vector3_t2243707580  L_10 = Matrix4x4_MultiplyVector_m4184387139((&V_1), L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Vector3_t2243707580  L_11 = Vector3_get_normalized_m1057036856((&V_4), /*hidden argument*/NULL);
		float L_12 = ___sideSign3;
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_x_1();
		float L_15 = (&V_3)->get_y_2();
		float L_16 = (&V_3)->get_z_3();
		Vector3_t2243707580  L_17 = V_2;
		Vector3_t2243707580  L_18 = V_3;
		float L_19 = Vector3_Dot_m827632793(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector4_t2243707581  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector4__ctor_m3879980823((&L_20), L_14, L_15, L_16, ((-L_19)), /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void UnityStandardAssets.Water.SpecularLighting::.ctor()
extern "C"  void SpecularLighting__ctor_m1185691240 (SpecularLighting_t2824122729 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.SpecularLighting::Start()
extern "C"  void SpecularLighting_Start_m1391288300 (SpecularLighting_t2824122729 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpecularLighting_Start_m1391288300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(WaterBase_t4291487940_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_2 = GameObject_GetComponent_m724360550(L_0, L_1, /*hidden argument*/NULL);
		__this->set_m_WaterBase_3(((WaterBase_t4291487940 *)CastclassClass((RuntimeObject*)L_2, WaterBase_t4291487940_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityStandardAssets.Water.SpecularLighting::Update()
extern "C"  void SpecularLighting_Update_m1227123071 (SpecularLighting_t2824122729 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpecularLighting_Update_m1227123071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WaterBase_t4291487940 * L_0 = __this->get_m_WaterBase_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0030;
		}
	}
	{
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(WaterBase_t4291487940_0_0_0_var), /*hidden argument*/NULL);
		Component_t3819376471 * L_4 = GameObject_GetComponent_m724360550(L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_WaterBase_3(((WaterBase_t4291487940 *)CastclassClass((RuntimeObject*)L_4, WaterBase_t4291487940_il2cpp_TypeInfo_var)));
	}

IL_0030:
	{
		Transform_t3275118058 * L_5 = __this->get_specularLight_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_007f;
		}
	}
	{
		WaterBase_t4291487940 * L_7 = __this->get_m_WaterBase_3();
		Material_t193706927 * L_8 = L_7->get_sharedMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007f;
		}
	}
	{
		WaterBase_t4291487940 * L_10 = __this->get_m_WaterBase_3();
		Material_t193706927 * L_11 = L_10->get_sharedMaterial_2();
		Transform_t3275118058 * L_12 = __this->get_specularLight_2();
		Transform_t3275118058 * L_13 = Component_get_transform_m3374354972(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Transform_get_forward_m2144220796(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2243707581_il2cpp_TypeInfo_var);
		Vector4_t2243707581  L_15 = Vector4_op_Implicit_m3079003974(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Material_SetVector_m3110165768(L_11, _stringLiteral1565133518, L_15, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::.ctor()
extern "C"  void Water__ctor_m150162646 (Water_t1562849653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water__ctor_m150162646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_waterMode_2(2);
		__this->set_disablePixelLights_3((bool)1);
		__this->set_textureSize_4(((int32_t)256));
		__this->set_clipPlaneOffset_5((0.07f));
		LayerMask_t3188175821  L_0 = LayerMask_op_Implicit_m1796475436(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		__this->set_reflectLayers_6(L_0);
		LayerMask_t3188175821  L_1 = LayerMask_op_Implicit_m1796475436(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		__this->set_refractLayers_7(L_1);
		Dictionary_2_t3541619047 * L_2 = (Dictionary_2_t3541619047 *)il2cpp_codegen_object_new(Dictionary_2_t3541619047_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1440922840(L_2, /*hidden argument*/Dictionary_2__ctor_m1440922840_RuntimeMethod_var);
		__this->set_m_ReflectionCameras_8(L_2);
		Dictionary_2_t3541619047 * L_3 = (Dictionary_2_t3541619047 *)il2cpp_codegen_object_new(Dictionary_2_t3541619047_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1440922840(L_3, /*hidden argument*/Dictionary_2__ctor_m1440922840_RuntimeMethod_var);
		__this->set_m_RefractionCameras_9(L_3);
		__this->set_m_HardwareWaterSupport_12(2);
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::OnWillRenderObject()
extern "C"  void Water_OnWillRenderObject_m1679173826 (Water_t1562849653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_OnWillRenderObject_m1679173826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t189460977 * V_0 = NULL;
	int32_t V_1 = 0;
	Camera_t189460977 * V_2 = NULL;
	Camera_t189460977 * V_3 = NULL;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	Vector4_t2243707581  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Matrix4x4_t2933234003  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector4_t2243707581  V_12;
	memset(&V_12, 0, sizeof(V_12));
	bool V_13 = false;
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector4_t2243707581  V_15;
	memset(&V_15, 0, sizeof(V_15));
	{
		bool L_0 = Behaviour_get_enabled_m646668963(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t257310565 * L_1 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t257310565 * L_3 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var);
		Material_t193706927 * L_4 = Renderer_get_sharedMaterial_m3033033727(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t257310565 * L_6 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var);
		bool L_7 = Renderer_get_enabled_m1795429839(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0041;
		}
	}

IL_0040:
	{
		return;
	}

IL_0041:
	{
		Camera_t189460977 * L_8 = Camera_get_current_m3769616944(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_8;
		Camera_t189460977 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0053;
		}
	}
	{
		return;
	}

IL_0053:
	{
		bool L_11 = ((Water_t1562849653_StaticFields*)il2cpp_codegen_static_fields_for(Water_t1562849653_il2cpp_TypeInfo_var))->get_s_InsideWater_15();
		if (!L_11)
		{
			goto IL_005e;
		}
	}
	{
		return;
	}

IL_005e:
	{
		((Water_t1562849653_StaticFields*)il2cpp_codegen_static_fields_for(Water_t1562849653_il2cpp_TypeInfo_var))->set_s_InsideWater_15((bool)1);
		int32_t L_12 = Water_FindHardwareWaterSupport_m3758364632(__this, /*hidden argument*/NULL);
		__this->set_m_HardwareWaterSupport_12(L_12);
		int32_t L_13 = Water_GetWaterMode_m3222093059(__this, /*hidden argument*/NULL);
		V_1 = L_13;
		Camera_t189460977 * L_14 = V_0;
		Water_CreateWaterObjects_m3159049619(__this, L_14, (&V_2), (&V_3), /*hidden argument*/NULL);
		Transform_t3275118058 * L_15 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Transform_get_position_m2304215762(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		Transform_t3275118058 * L_17 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Transform_get_up_m1990654328(L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		int32_t L_19 = QualitySettings_get_pixelLightCount_m1621733334(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_19;
		bool L_20 = __this->get_disablePixelLights_3();
		if (!L_20)
		{
			goto IL_00b4;
		}
	}
	{
		QualitySettings_set_pixelLightCount_m610294691(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		Camera_t189460977 * L_21 = V_0;
		Camera_t189460977 * L_22 = V_2;
		Water_UpdateCameraModes_m282959606(__this, L_21, L_22, /*hidden argument*/NULL);
		Camera_t189460977 * L_23 = V_0;
		Camera_t189460977 * L_24 = V_3;
		Water_UpdateCameraModes_m282959606(__this, L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_1;
		if ((((int32_t)L_25) < ((int32_t)1)))
		{
			goto IL_0214;
		}
	}
	{
		Vector3_t2243707580  L_26 = V_5;
		Vector3_t2243707580  L_27 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		float L_28 = Vector3_Dot_m827632793(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		float L_29 = __this->get_clipPlaneOffset_5();
		V_7 = ((float)((float)((-L_28))-(float)L_29));
		float L_30 = (&V_5)->get_x_1();
		float L_31 = (&V_5)->get_y_2();
		float L_32 = (&V_5)->get_z_3();
		float L_33 = V_7;
		Vector4__ctor_m3879980823((&V_8), L_30, L_31, L_32, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t2933234003_il2cpp_TypeInfo_var);
		Matrix4x4_t2933234003  L_34 = Matrix4x4_get_zero_m262690161(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_34;
		Vector4_t2243707581  L_35 = V_8;
		Water_CalculateReflectionMatrix_m4117333262(NULL /*static, unused*/, (&V_9), L_35, /*hidden argument*/NULL);
		Camera_t189460977 * L_36 = V_0;
		Transform_t3275118058 * L_37 = Component_get_transform_m3374354972(L_36, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = Transform_get_position_m2304215762(L_37, /*hidden argument*/NULL);
		V_10 = L_38;
		Vector3_t2243707580  L_39 = V_10;
		Vector3_t2243707580  L_40 = Matrix4x4_MultiplyPoint_m3620936054((&V_9), L_39, /*hidden argument*/NULL);
		V_11 = L_40;
		Camera_t189460977 * L_41 = V_2;
		Camera_t189460977 * L_42 = V_0;
		Matrix4x4_t2933234003  L_43 = Camera_get_worldToCameraMatrix_m3671061738(L_42, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_44 = V_9;
		Matrix4x4_t2933234003  L_45 = Matrix4x4_op_Multiply_m3436371832(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		Camera_set_worldToCameraMatrix_m2710888845(L_41, L_45, /*hidden argument*/NULL);
		Camera_t189460977 * L_46 = V_2;
		Vector3_t2243707580  L_47 = V_4;
		Vector3_t2243707580  L_48 = V_5;
		Vector4_t2243707581  L_49 = Water_CameraSpacePlane_m240838003(__this, L_46, L_47, L_48, (1.0f), /*hidden argument*/NULL);
		V_12 = L_49;
		Camera_t189460977 * L_50 = V_2;
		Camera_t189460977 * L_51 = V_0;
		Vector4_t2243707581  L_52 = V_12;
		Matrix4x4_t2933234003  L_53 = Camera_CalculateObliqueMatrix_m3128032212(L_51, L_52, /*hidden argument*/NULL);
		Camera_set_projectionMatrix_m1873250010(L_50, L_53, /*hidden argument*/NULL);
		Camera_t189460977 * L_54 = V_2;
		Camera_t189460977 * L_55 = V_0;
		Matrix4x4_t2933234003  L_56 = Camera_get_projectionMatrix_m3387870255(L_55, /*hidden argument*/NULL);
		Camera_t189460977 * L_57 = V_0;
		Matrix4x4_t2933234003  L_58 = Camera_get_worldToCameraMatrix_m3671061738(L_57, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_59 = Matrix4x4_op_Multiply_m3436371832(NULL /*static, unused*/, L_56, L_58, /*hidden argument*/NULL);
		Camera_set_cullingMatrix_m908594059(L_54, L_59, /*hidden argument*/NULL);
		Camera_t189460977 * L_60 = V_2;
		LayerMask_t3188175821 * L_61 = __this->get_address_of_reflectLayers_6();
		int32_t L_62 = LayerMask_get_value_m2828952681(L_61, /*hidden argument*/NULL);
		Camera_set_cullingMask_m2961679457(L_60, ((int32_t)((int32_t)((int32_t)-17)&(int32_t)L_62)), /*hidden argument*/NULL);
		Camera_t189460977 * L_63 = V_2;
		RenderTexture_t2666733923 * L_64 = __this->get_m_ReflectionTexture_10();
		Camera_set_targetTexture_m3042745048(L_63, L_64, /*hidden argument*/NULL);
		bool L_65 = GL_get_invertCulling_m3538314322(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_13 = L_65;
		bool L_66 = V_13;
		GL_set_invertCulling_m3669449627(NULL /*static, unused*/, (bool)((((int32_t)L_66) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Camera_t189460977 * L_67 = V_2;
		Transform_t3275118058 * L_68 = Component_get_transform_m3374354972(L_67, /*hidden argument*/NULL);
		Vector3_t2243707580  L_69 = V_11;
		Transform_set_position_m2942701431(L_68, L_69, /*hidden argument*/NULL);
		Camera_t189460977 * L_70 = V_0;
		Transform_t3275118058 * L_71 = Component_get_transform_m3374354972(L_70, /*hidden argument*/NULL);
		Vector3_t2243707580  L_72 = Transform_get_eulerAngles_m2112588536(L_71, /*hidden argument*/NULL);
		V_14 = L_72;
		Camera_t189460977 * L_73 = V_2;
		Transform_t3275118058 * L_74 = Component_get_transform_m3374354972(L_73, /*hidden argument*/NULL);
		float L_75 = (&V_14)->get_x_1();
		float L_76 = (&V_14)->get_y_2();
		float L_77 = (&V_14)->get_z_3();
		Vector3_t2243707580  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Vector3__ctor_m1555724485((&L_78), ((-L_75)), L_76, L_77, /*hidden argument*/NULL);
		Transform_set_eulerAngles_m1566259983(L_74, L_78, /*hidden argument*/NULL);
		Camera_t189460977 * L_79 = V_2;
		Camera_Render_m2992476825(L_79, /*hidden argument*/NULL);
		Camera_t189460977 * L_80 = V_2;
		Transform_t3275118058 * L_81 = Component_get_transform_m3374354972(L_80, /*hidden argument*/NULL);
		Vector3_t2243707580  L_82 = V_10;
		Transform_set_position_m2942701431(L_81, L_82, /*hidden argument*/NULL);
		bool L_83 = V_13;
		GL_set_invertCulling_m3669449627(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		Renderer_t257310565 * L_84 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var);
		Material_t193706927 * L_85 = Renderer_get_sharedMaterial_m3033033727(L_84, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_86 = __this->get_m_ReflectionTexture_10();
		Material_SetTexture_m3642434134(L_85, _stringLiteral3918988257, L_86, /*hidden argument*/NULL);
	}

IL_0214:
	{
		int32_t L_87 = V_1;
		if ((((int32_t)L_87) < ((int32_t)2)))
		{
			goto IL_02cb;
		}
	}
	{
		Camera_t189460977 * L_88 = V_3;
		Camera_t189460977 * L_89 = V_0;
		Matrix4x4_t2933234003  L_90 = Camera_get_worldToCameraMatrix_m3671061738(L_89, /*hidden argument*/NULL);
		Camera_set_worldToCameraMatrix_m2710888845(L_88, L_90, /*hidden argument*/NULL);
		Camera_t189460977 * L_91 = V_3;
		Vector3_t2243707580  L_92 = V_4;
		Vector3_t2243707580  L_93 = V_5;
		Vector4_t2243707581  L_94 = Water_CameraSpacePlane_m240838003(__this, L_91, L_92, L_93, (-1.0f), /*hidden argument*/NULL);
		V_15 = L_94;
		Camera_t189460977 * L_95 = V_3;
		Camera_t189460977 * L_96 = V_0;
		Vector4_t2243707581  L_97 = V_15;
		Matrix4x4_t2933234003  L_98 = Camera_CalculateObliqueMatrix_m3128032212(L_96, L_97, /*hidden argument*/NULL);
		Camera_set_projectionMatrix_m1873250010(L_95, L_98, /*hidden argument*/NULL);
		Camera_t189460977 * L_99 = V_3;
		Camera_t189460977 * L_100 = V_0;
		Matrix4x4_t2933234003  L_101 = Camera_get_projectionMatrix_m3387870255(L_100, /*hidden argument*/NULL);
		Camera_t189460977 * L_102 = V_0;
		Matrix4x4_t2933234003  L_103 = Camera_get_worldToCameraMatrix_m3671061738(L_102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t2933234003_il2cpp_TypeInfo_var);
		Matrix4x4_t2933234003  L_104 = Matrix4x4_op_Multiply_m3436371832(NULL /*static, unused*/, L_101, L_103, /*hidden argument*/NULL);
		Camera_set_cullingMatrix_m908594059(L_99, L_104, /*hidden argument*/NULL);
		Camera_t189460977 * L_105 = V_3;
		LayerMask_t3188175821 * L_106 = __this->get_address_of_refractLayers_7();
		int32_t L_107 = LayerMask_get_value_m2828952681(L_106, /*hidden argument*/NULL);
		Camera_set_cullingMask_m2961679457(L_105, ((int32_t)((int32_t)((int32_t)-17)&(int32_t)L_107)), /*hidden argument*/NULL);
		Camera_t189460977 * L_108 = V_3;
		RenderTexture_t2666733923 * L_109 = __this->get_m_RefractionTexture_11();
		Camera_set_targetTexture_m3042745048(L_108, L_109, /*hidden argument*/NULL);
		Camera_t189460977 * L_110 = V_3;
		Transform_t3275118058 * L_111 = Component_get_transform_m3374354972(L_110, /*hidden argument*/NULL);
		Camera_t189460977 * L_112 = V_0;
		Transform_t3275118058 * L_113 = Component_get_transform_m3374354972(L_112, /*hidden argument*/NULL);
		Vector3_t2243707580  L_114 = Transform_get_position_m2304215762(L_113, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_111, L_114, /*hidden argument*/NULL);
		Camera_t189460977 * L_115 = V_3;
		Transform_t3275118058 * L_116 = Component_get_transform_m3374354972(L_115, /*hidden argument*/NULL);
		Camera_t189460977 * L_117 = V_0;
		Transform_t3275118058 * L_118 = Component_get_transform_m3374354972(L_117, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_119 = Transform_get_rotation_m2617026815(L_118, /*hidden argument*/NULL);
		Transform_set_rotation_m2824446320(L_116, L_119, /*hidden argument*/NULL);
		Camera_t189460977 * L_120 = V_3;
		Camera_Render_m2992476825(L_120, /*hidden argument*/NULL);
		Renderer_t257310565 * L_121 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var);
		Material_t193706927 * L_122 = Renderer_get_sharedMaterial_m3033033727(L_121, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_123 = __this->get_m_RefractionTexture_11();
		Material_SetTexture_m3642434134(L_122, _stringLiteral1174916207, L_123, /*hidden argument*/NULL);
	}

IL_02cb:
	{
		bool L_124 = __this->get_disablePixelLights_3();
		if (!L_124)
		{
			goto IL_02dd;
		}
	}
	{
		int32_t L_125 = V_6;
		QualitySettings_set_pixelLightCount_m610294691(NULL /*static, unused*/, L_125, /*hidden argument*/NULL);
	}

IL_02dd:
	{
		int32_t L_126 = V_1;
		if (!L_126)
		{
			goto IL_02f6;
		}
	}
	{
		int32_t L_127 = V_1;
		if ((((int32_t)L_127) == ((int32_t)1)))
		{
			goto IL_0319;
		}
	}
	{
		int32_t L_128 = V_1;
		if ((((int32_t)L_128) == ((int32_t)2)))
		{
			goto IL_033c;
		}
	}
	{
		goto IL_035f;
	}

IL_02f6:
	{
		Shader_EnableKeyword_m2347844001(NULL /*static, unused*/, _stringLiteral2466513744, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral1412582911, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral2001222817, /*hidden argument*/NULL);
		goto IL_035f;
	}

IL_0319:
	{
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral2466513744, /*hidden argument*/NULL);
		Shader_EnableKeyword_m2347844001(NULL /*static, unused*/, _stringLiteral1412582911, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral2001222817, /*hidden argument*/NULL);
		goto IL_035f;
	}

IL_033c:
	{
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral2466513744, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral1412582911, /*hidden argument*/NULL);
		Shader_EnableKeyword_m2347844001(NULL /*static, unused*/, _stringLiteral2001222817, /*hidden argument*/NULL);
		goto IL_035f;
	}

IL_035f:
	{
		((Water_t1562849653_StaticFields*)il2cpp_codegen_static_fields_for(Water_t1562849653_il2cpp_TypeInfo_var))->set_s_InsideWater_15((bool)0);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::OnDisable()
extern "C"  void Water_OnDisable_m3913667107 (Water_t1562849653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_OnDisable_m3913667107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1298964269  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t566676453  V_1;
	memset(&V_1, 0, sizeof(V_1));
	KeyValuePair_2_t1298964269  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t566676453  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RenderTexture_t2666733923 * L_0 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		RenderTexture_t2666733923 * L_2 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m201325964(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_m_ReflectionTexture_10((RenderTexture_t2666733923 *)NULL);
	}

IL_0022:
	{
		RenderTexture_t2666733923 * L_3 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		RenderTexture_t2666733923 * L_5 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m201325964(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_m_RefractionTexture_11((RenderTexture_t2666733923 *)NULL);
	}

IL_0044:
	{
		Dictionary_2_t3541619047 * L_6 = __this->get_m_ReflectionCameras_8();
		Enumerator_t566676453  L_7 = Dictionary_2_GetEnumerator_m1484377914(L_6, /*hidden argument*/Dictionary_2_GetEnumerator_m1484377914_RuntimeMethod_var);
		V_1 = L_7;
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006e;
		}

IL_0055:
		{
			KeyValuePair_2_t1298964269  L_8 = Enumerator_get_Current_m1539012380((&V_1), /*hidden argument*/Enumerator_get_Current_m1539012380_RuntimeMethod_var);
			V_0 = L_8;
			Camera_t189460977 * L_9 = KeyValuePair_2_get_Value_m2952023721((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m2952023721_RuntimeMethod_var);
			GameObject_t1756533147 * L_10 = Component_get_gameObject_m2159020946(L_9, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_DestroyImmediate_m201325964(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		}

IL_006e:
		{
			bool L_11 = Enumerator_MoveNext_m1121419475((&V_1), /*hidden argument*/Enumerator_MoveNext_m1121419475_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0055;
			}
		}

IL_007a:
		{
			IL2CPP_LEAVE(0x8D, FINALLY_007f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007f;
	}

FINALLY_007f:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1187418175((&V_1), /*hidden argument*/Enumerator_Dispose_m1187418175_RuntimeMethod_var);
		IL2CPP_END_FINALLY(127)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(127)
	{
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008d:
	{
		Dictionary_2_t3541619047 * L_12 = __this->get_m_ReflectionCameras_8();
		Dictionary_2_Clear_m4188076919(L_12, /*hidden argument*/Dictionary_2_Clear_m4188076919_RuntimeMethod_var);
		Dictionary_2_t3541619047 * L_13 = __this->get_m_RefractionCameras_9();
		Enumerator_t566676453  L_14 = Dictionary_2_GetEnumerator_m1484377914(L_13, /*hidden argument*/Dictionary_2_GetEnumerator_m1484377914_RuntimeMethod_var);
		V_3 = L_14;
	}

IL_00a4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c2;
		}

IL_00a9:
		{
			KeyValuePair_2_t1298964269  L_15 = Enumerator_get_Current_m1539012380((&V_3), /*hidden argument*/Enumerator_get_Current_m1539012380_RuntimeMethod_var);
			V_2 = L_15;
			Camera_t189460977 * L_16 = KeyValuePair_2_get_Value_m2952023721((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m2952023721_RuntimeMethod_var);
			GameObject_t1756533147 * L_17 = Component_get_gameObject_m2159020946(L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_DestroyImmediate_m201325964(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		}

IL_00c2:
		{
			bool L_18 = Enumerator_MoveNext_m1121419475((&V_3), /*hidden argument*/Enumerator_MoveNext_m1121419475_RuntimeMethod_var);
			if (L_18)
			{
				goto IL_00a9;
			}
		}

IL_00ce:
		{
			IL2CPP_LEAVE(0xE1, FINALLY_00d3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00d3;
	}

FINALLY_00d3:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1187418175((&V_3), /*hidden argument*/Enumerator_Dispose_m1187418175_RuntimeMethod_var);
		IL2CPP_END_FINALLY(211)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(211)
	{
		IL2CPP_JUMP_TBL(0xE1, IL_00e1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e1:
	{
		Dictionary_2_t3541619047 * L_19 = __this->get_m_RefractionCameras_9();
		Dictionary_2_Clear_m4188076919(L_19, /*hidden argument*/Dictionary_2_Clear_m4188076919_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::Update()
extern "C"  void Water_Update_m382650723 (Water_t1562849653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_Update_m382650723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t193706927 * V_0 = NULL;
	Vector4_t2243707581  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector4_t2243707581  V_3;
	memset(&V_3, 0, sizeof(V_3));
	double V_4 = 0.0;
	Vector4_t2243707581  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Renderer_t257310565 * L_0 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Renderer_t257310565 * L_2 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var);
		Material_t193706927 * L_3 = Renderer_get_sharedMaterial_m3033033727(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Material_t193706927 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		Material_t193706927 * L_6 = V_0;
		Vector4_t2243707581  L_7 = Material_GetVector_m2705903741(L_6, _stringLiteral3402504634, /*hidden argument*/NULL);
		V_1 = L_7;
		Material_t193706927 * L_8 = V_0;
		float L_9 = Material_GetFloat_m1524931699(L_8, _stringLiteral1816258960, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = V_2;
		float L_11 = V_2;
		float L_12 = V_2;
		float L_13 = V_2;
		Vector4__ctor_m3879980823((&V_3), L_10, L_11, ((float)((float)L_12*(float)(0.4f))), ((float)((float)L_13*(float)(0.45f))), /*hidden argument*/NULL);
		float L_14 = Time_get_timeSinceLevelLoad_m1272389819(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = ((double)((double)(((double)((double)L_14)))/(double)(20.0)));
		float L_15 = (&V_1)->get_x_1();
		float L_16 = (&V_3)->get_x_1();
		double L_17 = V_4;
		double L_18 = Math_IEEERemainder_m340193149(NULL /*static, unused*/, ((double)((double)(((double)((double)((float)((float)L_15*(float)L_16)))))*(double)L_17)), (1.0), /*hidden argument*/NULL);
		float L_19 = (&V_1)->get_y_2();
		float L_20 = (&V_3)->get_y_2();
		double L_21 = V_4;
		double L_22 = Math_IEEERemainder_m340193149(NULL /*static, unused*/, ((double)((double)(((double)((double)((float)((float)L_19*(float)L_20)))))*(double)L_21)), (1.0), /*hidden argument*/NULL);
		float L_23 = (&V_1)->get_z_3();
		float L_24 = (&V_3)->get_z_3();
		double L_25 = V_4;
		double L_26 = Math_IEEERemainder_m340193149(NULL /*static, unused*/, ((double)((double)(((double)((double)((float)((float)L_23*(float)L_24)))))*(double)L_25)), (1.0), /*hidden argument*/NULL);
		float L_27 = (&V_1)->get_w_4();
		float L_28 = (&V_3)->get_w_4();
		double L_29 = V_4;
		double L_30 = Math_IEEERemainder_m340193149(NULL /*static, unused*/, ((double)((double)(((double)((double)((float)((float)L_27*(float)L_28)))))*(double)L_29)), (1.0), /*hidden argument*/NULL);
		Vector4__ctor_m3879980823((&V_5), (((float)((float)L_18))), (((float)((float)L_22))), (((float)((float)L_26))), (((float)((float)L_30))), /*hidden argument*/NULL);
		Material_t193706927 * L_31 = V_0;
		Vector4_t2243707581  L_32 = V_5;
		Material_SetVector_m3110165768(L_31, _stringLiteral3749796871, L_32, /*hidden argument*/NULL);
		Material_t193706927 * L_33 = V_0;
		Vector4_t2243707581  L_34 = V_3;
		Material_SetVector_m3110165768(L_33, _stringLiteral784172996, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Water_UpdateCameraModes_m282959606 (Water_t1562849653 * __this, Camera_t189460977 * ___src0, Camera_t189460977 * ___dest1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_UpdateCameraModes_m282959606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Skybox_t2033495038 * V_0 = NULL;
	Skybox_t2033495038 * V_1 = NULL;
	{
		Camera_t189460977 * L_0 = ___dest1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Camera_t189460977 * L_2 = ___dest1;
		Camera_t189460977 * L_3 = ___src0;
		int32_t L_4 = Camera_get_clearFlags_m966754839(L_3, /*hidden argument*/NULL);
		Camera_set_clearFlags_m489868550(L_2, L_4, /*hidden argument*/NULL);
		Camera_t189460977 * L_5 = ___dest1;
		Camera_t189460977 * L_6 = ___src0;
		Color_t2020392075  L_7 = Camera_get_backgroundColor_m707399190(L_6, /*hidden argument*/NULL);
		Camera_set_backgroundColor_m4178084823(L_5, L_7, /*hidden argument*/NULL);
		Camera_t189460977 * L_8 = ___src0;
		int32_t L_9 = Camera_get_clearFlags_m966754839(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0079;
		}
	}
	{
		Camera_t189460977 * L_10 = ___src0;
		Skybox_t2033495038 * L_11 = Component_GetComponent_TisSkybox_t2033495038_m2860073064(L_10, /*hidden argument*/Component_GetComponent_TisSkybox_t2033495038_m2860073064_RuntimeMethod_var);
		V_0 = L_11;
		Camera_t189460977 * L_12 = ___dest1;
		Skybox_t2033495038 * L_13 = Component_GetComponent_TisSkybox_t2033495038_m2860073064(L_12, /*hidden argument*/Component_GetComponent_TisSkybox_t2033495038_m2860073064_RuntimeMethod_var);
		V_1 = L_13;
		Skybox_t2033495038 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_005a;
		}
	}
	{
		Skybox_t2033495038 * L_16 = V_0;
		Material_t193706927 * L_17 = Skybox_get_material_m3485302839(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0066;
		}
	}

IL_005a:
	{
		Skybox_t2033495038 * L_19 = V_1;
		Behaviour_set_enabled_m602406666(L_19, (bool)0, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_0066:
	{
		Skybox_t2033495038 * L_20 = V_1;
		Behaviour_set_enabled_m602406666(L_20, (bool)1, /*hidden argument*/NULL);
		Skybox_t2033495038 * L_21 = V_1;
		Skybox_t2033495038 * L_22 = V_0;
		Material_t193706927 * L_23 = Skybox_get_material_m3485302839(L_22, /*hidden argument*/NULL);
		Skybox_set_material_m3485997388(L_21, L_23, /*hidden argument*/NULL);
	}

IL_0079:
	{
		Camera_t189460977 * L_24 = ___dest1;
		Camera_t189460977 * L_25 = ___src0;
		float L_26 = Camera_get_farClipPlane_m2932657537(L_25, /*hidden argument*/NULL);
		Camera_set_farClipPlane_m3207261698(L_24, L_26, /*hidden argument*/NULL);
		Camera_t189460977 * L_27 = ___dest1;
		Camera_t189460977 * L_28 = ___src0;
		float L_29 = Camera_get_nearClipPlane_m2824342520(L_28, /*hidden argument*/NULL);
		Camera_set_nearClipPlane_m143326947(L_27, L_29, /*hidden argument*/NULL);
		Camera_t189460977 * L_30 = ___dest1;
		Camera_t189460977 * L_31 = ___src0;
		bool L_32 = Camera_get_orthographic_m3688563802(L_31, /*hidden argument*/NULL);
		Camera_set_orthographic_m2016457201(L_30, L_32, /*hidden argument*/NULL);
		Camera_t189460977 * L_33 = ___dest1;
		Camera_t189460977 * L_34 = ___src0;
		float L_35 = Camera_get_fieldOfView_m1609126762(L_34, /*hidden argument*/NULL);
		Camera_set_fieldOfView_m4080277213(L_33, L_35, /*hidden argument*/NULL);
		Camera_t189460977 * L_36 = ___dest1;
		Camera_t189460977 * L_37 = ___src0;
		float L_38 = Camera_get_aspect_m972674900(L_37, /*hidden argument*/NULL);
		Camera_set_aspect_m3624679551(L_36, L_38, /*hidden argument*/NULL);
		Camera_t189460977 * L_39 = ___dest1;
		Camera_t189460977 * L_40 = ___src0;
		float L_41 = Camera_get_orthographicSize_m2553055249(L_40, /*hidden argument*/NULL);
		Camera_set_orthographicSize_m4124508114(L_39, L_41, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern "C"  void Water_CreateWaterObjects_m3159049619 (Water_t1562849653 * __this, Camera_t189460977 * ___currentCamera0, Camera_t189460977 ** ___reflectionCamera1, Camera_t189460977 ** ___refractionCamera2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_CreateWaterObjects_m3159049619_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	{
		int32_t L_0 = Water_GetWaterMode_m3222093059(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t189460977 ** L_1 = ___reflectionCamera1;
		*((RuntimeObject **)(L_1)) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_1), (RuntimeObject *)NULL);
		Camera_t189460977 ** L_2 = ___refractionCamera2;
		*((RuntimeObject **)(L_2)) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_2), (RuntimeObject *)NULL);
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)1)))
		{
			goto IL_0186;
		}
	}
	{
		RenderTexture_t2666733923 * L_4 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_6 = __this->get_m_OldReflectionTextureSize_13();
		int32_t L_7 = __this->get_textureSize_4();
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_00ae;
		}
	}

IL_0035:
	{
		RenderTexture_t2666733923 * L_8 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		RenderTexture_t2666733923 * L_10 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m201325964(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0050:
	{
		int32_t L_11 = __this->get_textureSize_4();
		int32_t L_12 = __this->get_textureSize_4();
		RenderTexture_t2666733923 * L_13 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4010205058(L_13, L_11, L_12, ((int32_t)16), /*hidden argument*/NULL);
		__this->set_m_ReflectionTexture_10(L_13);
		RenderTexture_t2666733923 * L_14 = __this->get_m_ReflectionTexture_10();
		int32_t L_15 = Object_GetInstanceID_m1193568599(__this, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral615419946, L_17, /*hidden argument*/NULL);
		Object_set_name_m1458854879(L_14, L_18, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_19 = __this->get_m_ReflectionTexture_10();
		RenderTexture_set_isPowerOfTwo_m554504365(L_19, (bool)1, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_20 = __this->get_m_ReflectionTexture_10();
		Object_set_hideFlags_m3275307757(L_20, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_21 = __this->get_textureSize_4();
		__this->set_m_OldReflectionTextureSize_13(L_21);
	}

IL_00ae:
	{
		Dictionary_2_t3541619047 * L_22 = __this->get_m_ReflectionCameras_8();
		Camera_t189460977 * L_23 = ___currentCamera0;
		Camera_t189460977 ** L_24 = ___reflectionCamera1;
		Dictionary_2_TryGetValue_m353229995(L_22, L_23, L_24, /*hidden argument*/Dictionary_2_TryGetValue_m353229995_RuntimeMethod_var);
		Camera_t189460977 ** L_25 = ___reflectionCamera1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, (*((Camera_t189460977 **)L_25)), /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_0186;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_27 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		ArrayElementTypeCheck (L_27, _stringLiteral3365593692);
		(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral3365593692);
		ObjectU5BU5D_t3614634134* L_28 = L_27;
		int32_t L_29 = Object_GetInstanceID_m1193568599(__this, /*hidden argument*/NULL);
		int32_t L_30 = L_29;
		RuntimeObject * L_31 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_30);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_31);
		ObjectU5BU5D_t3614634134* L_32 = L_28;
		ArrayElementTypeCheck (L_32, _stringLiteral378319007);
		(L_32)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral378319007);
		ObjectU5BU5D_t3614634134* L_33 = L_32;
		Camera_t189460977 * L_34 = ___currentCamera0;
		int32_t L_35 = Object_GetInstanceID_m1193568599(L_34, /*hidden argument*/NULL);
		int32_t L_36 = L_35;
		RuntimeObject * L_37 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_36);
		ArrayElementTypeCheck (L_33, L_37);
		(L_33)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Concat_m3881798623(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_39 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_40 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Camera_t189460977_0_0_0_var), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_39, L_40);
		(L_39)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_40);
		TypeU5BU5D_t1664964607* L_41 = L_39;
		Type_t * L_42 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Skybox_t2033495038_0_0_0_var), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_41, L_42);
		(L_41)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_42);
		GameObject_t1756533147 * L_43 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m3412194074(L_43, L_38, L_41, /*hidden argument*/NULL);
		V_1 = L_43;
		Camera_t189460977 ** L_44 = ___reflectionCamera1;
		GameObject_t1756533147 * L_45 = V_1;
		Camera_t189460977 * L_46 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_45, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_RuntimeMethod_var);
		*((RuntimeObject **)(L_44)) = (RuntimeObject *)L_46;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_44), (RuntimeObject *)L_46);
		Camera_t189460977 ** L_47 = ___reflectionCamera1;
		Behaviour_set_enabled_m602406666((*((Camera_t189460977 **)L_47)), (bool)0, /*hidden argument*/NULL);
		Camera_t189460977 ** L_48 = ___reflectionCamera1;
		Transform_t3275118058 * L_49 = Component_get_transform_m3374354972((*((Camera_t189460977 **)L_48)), /*hidden argument*/NULL);
		Transform_t3275118058 * L_50 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = Transform_get_position_m2304215762(L_50, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_49, L_51, /*hidden argument*/NULL);
		Camera_t189460977 ** L_52 = ___reflectionCamera1;
		Transform_t3275118058 * L_53 = Component_get_transform_m3374354972((*((Camera_t189460977 **)L_52)), /*hidden argument*/NULL);
		Transform_t3275118058 * L_54 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_55 = Transform_get_rotation_m2617026815(L_54, /*hidden argument*/NULL);
		Transform_set_rotation_m2824446320(L_53, L_55, /*hidden argument*/NULL);
		Camera_t189460977 ** L_56 = ___reflectionCamera1;
		GameObject_t1756533147 * L_57 = Component_get_gameObject_m2159020946((*((Camera_t189460977 **)L_56)), /*hidden argument*/NULL);
		GameObject_AddComponent_TisFlareLayer_t1985082419_m2043234002(L_57, /*hidden argument*/GameObject_AddComponent_TisFlareLayer_t1985082419_m2043234002_RuntimeMethod_var);
		GameObject_t1756533147 * L_58 = V_1;
		Object_set_hideFlags_m3275307757(L_58, ((int32_t)61), /*hidden argument*/NULL);
		Dictionary_2_t3541619047 * L_59 = __this->get_m_ReflectionCameras_8();
		Camera_t189460977 * L_60 = ___currentCamera0;
		Camera_t189460977 ** L_61 = ___reflectionCamera1;
		Dictionary_2_set_Item_m2824272795(L_59, L_60, (*((Camera_t189460977 **)L_61)), /*hidden argument*/Dictionary_2_set_Item_m2824272795_RuntimeMethod_var);
	}

IL_0186:
	{
		int32_t L_62 = V_0;
		if ((((int32_t)L_62) < ((int32_t)2)))
		{
			goto IL_02ff;
		}
	}
	{
		RenderTexture_t2666733923 * L_63 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_64 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01ae;
		}
	}
	{
		int32_t L_65 = __this->get_m_OldRefractionTextureSize_14();
		int32_t L_66 = __this->get_textureSize_4();
		if ((((int32_t)L_65) == ((int32_t)L_66)))
		{
			goto IL_0227;
		}
	}

IL_01ae:
	{
		RenderTexture_t2666733923 * L_67 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_68 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01c9;
		}
	}
	{
		RenderTexture_t2666733923 * L_69 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m201325964(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
	}

IL_01c9:
	{
		int32_t L_70 = __this->get_textureSize_4();
		int32_t L_71 = __this->get_textureSize_4();
		RenderTexture_t2666733923 * L_72 = (RenderTexture_t2666733923 *)il2cpp_codegen_object_new(RenderTexture_t2666733923_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m4010205058(L_72, L_70, L_71, ((int32_t)16), /*hidden argument*/NULL);
		__this->set_m_RefractionTexture_11(L_72);
		RenderTexture_t2666733923 * L_73 = __this->get_m_RefractionTexture_11();
		int32_t L_74 = Object_GetInstanceID_m1193568599(__this, /*hidden argument*/NULL);
		int32_t L_75 = L_74;
		RuntimeObject * L_76 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_75);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral648446824, L_76, /*hidden argument*/NULL);
		Object_set_name_m1458854879(L_73, L_77, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_78 = __this->get_m_RefractionTexture_11();
		RenderTexture_set_isPowerOfTwo_m554504365(L_78, (bool)1, /*hidden argument*/NULL);
		RenderTexture_t2666733923 * L_79 = __this->get_m_RefractionTexture_11();
		Object_set_hideFlags_m3275307757(L_79, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_80 = __this->get_textureSize_4();
		__this->set_m_OldRefractionTextureSize_14(L_80);
	}

IL_0227:
	{
		Dictionary_2_t3541619047 * L_81 = __this->get_m_RefractionCameras_9();
		Camera_t189460977 * L_82 = ___currentCamera0;
		Camera_t189460977 ** L_83 = ___refractionCamera2;
		Dictionary_2_TryGetValue_m353229995(L_81, L_82, L_83, /*hidden argument*/Dictionary_2_TryGetValue_m353229995_RuntimeMethod_var);
		Camera_t189460977 ** L_84 = ___refractionCamera2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_85 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, (*((Camera_t189460977 **)L_84)), /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_02ff;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_86 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		ArrayElementTypeCheck (L_86, _stringLiteral2767343314);
		(L_86)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2767343314);
		ObjectU5BU5D_t3614634134* L_87 = L_86;
		int32_t L_88 = Object_GetInstanceID_m1193568599(__this, /*hidden argument*/NULL);
		int32_t L_89 = L_88;
		RuntimeObject * L_90 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_89);
		ArrayElementTypeCheck (L_87, L_90);
		(L_87)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_90);
		ObjectU5BU5D_t3614634134* L_91 = L_87;
		ArrayElementTypeCheck (L_91, _stringLiteral378319007);
		(L_91)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral378319007);
		ObjectU5BU5D_t3614634134* L_92 = L_91;
		Camera_t189460977 * L_93 = ___currentCamera0;
		int32_t L_94 = Object_GetInstanceID_m1193568599(L_93, /*hidden argument*/NULL);
		int32_t L_95 = L_94;
		RuntimeObject * L_96 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_95);
		ArrayElementTypeCheck (L_92, L_96);
		(L_92)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_96);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = String_Concat_m3881798623(NULL /*static, unused*/, L_92, /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_98 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_99 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Camera_t189460977_0_0_0_var), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_98, L_99);
		(L_98)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_99);
		TypeU5BU5D_t1664964607* L_100 = L_98;
		Type_t * L_101 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Skybox_t2033495038_0_0_0_var), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_100, L_101);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_101);
		GameObject_t1756533147 * L_102 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m3412194074(L_102, L_97, L_100, /*hidden argument*/NULL);
		V_2 = L_102;
		Camera_t189460977 ** L_103 = ___refractionCamera2;
		GameObject_t1756533147 * L_104 = V_2;
		Camera_t189460977 * L_105 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_104, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_RuntimeMethod_var);
		*((RuntimeObject **)(L_103)) = (RuntimeObject *)L_105;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_103), (RuntimeObject *)L_105);
		Camera_t189460977 ** L_106 = ___refractionCamera2;
		Behaviour_set_enabled_m602406666((*((Camera_t189460977 **)L_106)), (bool)0, /*hidden argument*/NULL);
		Camera_t189460977 ** L_107 = ___refractionCamera2;
		Transform_t3275118058 * L_108 = Component_get_transform_m3374354972((*((Camera_t189460977 **)L_107)), /*hidden argument*/NULL);
		Transform_t3275118058 * L_109 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_110 = Transform_get_position_m2304215762(L_109, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_108, L_110, /*hidden argument*/NULL);
		Camera_t189460977 ** L_111 = ___refractionCamera2;
		Transform_t3275118058 * L_112 = Component_get_transform_m3374354972((*((Camera_t189460977 **)L_111)), /*hidden argument*/NULL);
		Transform_t3275118058 * L_113 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_114 = Transform_get_rotation_m2617026815(L_113, /*hidden argument*/NULL);
		Transform_set_rotation_m2824446320(L_112, L_114, /*hidden argument*/NULL);
		Camera_t189460977 ** L_115 = ___refractionCamera2;
		GameObject_t1756533147 * L_116 = Component_get_gameObject_m2159020946((*((Camera_t189460977 **)L_115)), /*hidden argument*/NULL);
		GameObject_AddComponent_TisFlareLayer_t1985082419_m2043234002(L_116, /*hidden argument*/GameObject_AddComponent_TisFlareLayer_t1985082419_m2043234002_RuntimeMethod_var);
		GameObject_t1756533147 * L_117 = V_2;
		Object_set_hideFlags_m3275307757(L_117, ((int32_t)61), /*hidden argument*/NULL);
		Dictionary_2_t3541619047 * L_118 = __this->get_m_RefractionCameras_9();
		Camera_t189460977 * L_119 = ___currentCamera0;
		Camera_t189460977 ** L_120 = ___refractionCamera2;
		Dictionary_2_set_Item_m2824272795(L_118, L_119, (*((Camera_t189460977 **)L_120)), /*hidden argument*/Dictionary_2_set_Item_m2824272795_RuntimeMethod_var);
	}

IL_02ff:
	{
		return;
	}
}
// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::GetWaterMode()
extern "C"  int32_t Water_GetWaterMode_m3222093059 (Water_t1562849653 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_HardwareWaterSupport_12();
		int32_t L_1 = __this->get_waterMode_2();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = __this->get_m_HardwareWaterSupport_12();
		return L_2;
	}

IL_0018:
	{
		int32_t L_3 = __this->get_waterMode_2();
		return L_3;
	}
}
// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::FindHardwareWaterSupport()
extern "C"  int32_t Water_FindHardwareWaterSupport_m3758364632 (Water_t1562849653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_FindHardwareWaterSupport_m3758364632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t193706927 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Renderer_t257310565 * L_0 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0012:
	{
		Renderer_t257310565 * L_2 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_RuntimeMethod_var);
		Material_t193706927 * L_3 = Renderer_get_sharedMaterial_m3033033727(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Material_t193706927 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002b;
		}
	}
	{
		return (int32_t)(0);
	}

IL_002b:
	{
		Material_t193706927 * L_6 = V_0;
		String_t* L_7 = Material_GetTag_m3856861437(L_6, _stringLiteral2872901958, (bool)0, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral305787587, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004a;
		}
	}
	{
		return (int32_t)(2);
	}

IL_004a:
	{
		String_t* L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, _stringLiteral3079388997, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005c;
		}
	}
	{
		return (int32_t)(1);
	}

IL_005c:
	{
		return (int32_t)(0);
	}
}
// UnityEngine.Vector4 UnityStandardAssets.Water.Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t2243707581  Water_CameraSpacePlane_m240838003 (Water_t1562849653 * __this, Camera_t189460977 * ___cam0, Vector3_t2243707580  ___pos1, Vector3_t2243707580  ___normal2, float ___sideSign3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_CameraSpacePlane_m240838003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t2933234003  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector3_t2243707580  L_0 = ___pos1;
		Vector3_t2243707580  L_1 = ___normal2;
		float L_2 = __this->get_clipPlaneOffset_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Camera_t189460977 * L_5 = ___cam0;
		Matrix4x4_t2933234003  L_6 = Camera_get_worldToCameraMatrix_m3671061738(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t2243707580  L_7 = V_0;
		Vector3_t2243707580  L_8 = Matrix4x4_MultiplyPoint_m3620936054((&V_1), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3_t2243707580  L_9 = ___normal2;
		Vector3_t2243707580  L_10 = Matrix4x4_MultiplyVector_m4184387139((&V_1), L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Vector3_t2243707580  L_11 = Vector3_get_normalized_m1057036856((&V_4), /*hidden argument*/NULL);
		float L_12 = ___sideSign3;
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_x_1();
		float L_15 = (&V_3)->get_y_2();
		float L_16 = (&V_3)->get_z_3();
		Vector3_t2243707580  L_17 = V_2;
		Vector3_t2243707580  L_18 = V_3;
		float L_19 = Vector3_Dot_m827632793(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector4_t2243707581  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector4__ctor_m3879980823((&L_20), L_14, L_15, L_16, ((-L_19)), /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void UnityStandardAssets.Water.Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern "C"  void Water_CalculateReflectionMatrix_m4117333262 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003 * ___reflectionMat0, Vector4_t2243707581  ___plane1, const RuntimeMethod* method)
{
	{
		Matrix4x4_t2933234003 * L_0 = ___reflectionMat0;
		float L_1 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		float L_2 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		L_0->set_m00_0(((float)((float)(1.0f)-(float)((float)((float)((float)((float)(2.0f)*(float)L_1))*(float)L_2)))));
		Matrix4x4_t2933234003 * L_3 = ___reflectionMat0;
		float L_4 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		float L_5 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		L_3->set_m01_4(((float)((float)((float)((float)(-2.0f)*(float)L_4))*(float)L_5)));
		Matrix4x4_t2933234003 * L_6 = ___reflectionMat0;
		float L_7 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		float L_8 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		L_6->set_m02_8(((float)((float)((float)((float)(-2.0f)*(float)L_7))*(float)L_8)));
		Matrix4x4_t2933234003 * L_9 = ___reflectionMat0;
		float L_10 = Vector4_get_Item_m2784900302((&___plane1), 3, /*hidden argument*/NULL);
		float L_11 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		L_9->set_m03_12(((float)((float)((float)((float)(-2.0f)*(float)L_10))*(float)L_11)));
		Matrix4x4_t2933234003 * L_12 = ___reflectionMat0;
		float L_13 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		float L_14 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		L_12->set_m10_1(((float)((float)((float)((float)(-2.0f)*(float)L_13))*(float)L_14)));
		Matrix4x4_t2933234003 * L_15 = ___reflectionMat0;
		float L_16 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		float L_17 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		L_15->set_m11_5(((float)((float)(1.0f)-(float)((float)((float)((float)((float)(2.0f)*(float)L_16))*(float)L_17)))));
		Matrix4x4_t2933234003 * L_18 = ___reflectionMat0;
		float L_19 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		float L_20 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		L_18->set_m12_9(((float)((float)((float)((float)(-2.0f)*(float)L_19))*(float)L_20)));
		Matrix4x4_t2933234003 * L_21 = ___reflectionMat0;
		float L_22 = Vector4_get_Item_m2784900302((&___plane1), 3, /*hidden argument*/NULL);
		float L_23 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		L_21->set_m13_13(((float)((float)((float)((float)(-2.0f)*(float)L_22))*(float)L_23)));
		Matrix4x4_t2933234003 * L_24 = ___reflectionMat0;
		float L_25 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		float L_26 = Vector4_get_Item_m2784900302((&___plane1), 0, /*hidden argument*/NULL);
		L_24->set_m20_2(((float)((float)((float)((float)(-2.0f)*(float)L_25))*(float)L_26)));
		Matrix4x4_t2933234003 * L_27 = ___reflectionMat0;
		float L_28 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		float L_29 = Vector4_get_Item_m2784900302((&___plane1), 1, /*hidden argument*/NULL);
		L_27->set_m21_6(((float)((float)((float)((float)(-2.0f)*(float)L_28))*(float)L_29)));
		Matrix4x4_t2933234003 * L_30 = ___reflectionMat0;
		float L_31 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		float L_32 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		L_30->set_m22_10(((float)((float)(1.0f)-(float)((float)((float)((float)((float)(2.0f)*(float)L_31))*(float)L_32)))));
		Matrix4x4_t2933234003 * L_33 = ___reflectionMat0;
		float L_34 = Vector4_get_Item_m2784900302((&___plane1), 3, /*hidden argument*/NULL);
		float L_35 = Vector4_get_Item_m2784900302((&___plane1), 2, /*hidden argument*/NULL);
		L_33->set_m23_14(((float)((float)((float)((float)(-2.0f)*(float)L_34))*(float)L_35)));
		Matrix4x4_t2933234003 * L_36 = ___reflectionMat0;
		L_36->set_m30_3((0.0f));
		Matrix4x4_t2933234003 * L_37 = ___reflectionMat0;
		L_37->set_m31_7((0.0f));
		Matrix4x4_t2933234003 * L_38 = ___reflectionMat0;
		L_38->set_m32_11((0.0f));
		Matrix4x4_t2933234003 * L_39 = ___reflectionMat0;
		L_39->set_m33_15((1.0f));
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::.ctor()
extern "C"  void WaterBase__ctor_m1259312559 (WaterBase_t4291487940 * __this, const RuntimeMethod* method)
{
	{
		__this->set_waterQuality_3(2);
		__this->set_edgeBlend_4((bool)1);
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern "C"  void WaterBase_UpdateShader_m3822819655 (WaterBase_t4291487940 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterBase_UpdateShader_m3822819655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_waterQuality_3();
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0026;
		}
	}
	{
		Material_t193706927 * L_1 = __this->get_sharedMaterial_2();
		Shader_t2430389951 * L_2 = Material_get_shader_m2582399212(L_1, /*hidden argument*/NULL);
		Shader_set_maximumLOD_m1292647048(L_2, ((int32_t)501), /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_0026:
	{
		int32_t L_3 = __this->get_waterQuality_3();
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		Material_t193706927 * L_4 = __this->get_sharedMaterial_2();
		Shader_t2430389951 * L_5 = Material_get_shader_m2582399212(L_4, /*hidden argument*/NULL);
		Shader_set_maximumLOD_m1292647048(L_5, ((int32_t)301), /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_004c:
	{
		Material_t193706927 * L_6 = __this->get_sharedMaterial_2();
		Shader_t2430389951 * L_7 = Material_get_shader_m2582399212(L_6, /*hidden argument*/NULL);
		Shader_set_maximumLOD_m1292647048(L_7, ((int32_t)201), /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_8 = SystemInfo_SupportsRenderTextureFormat_m1269175480(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0073;
		}
	}
	{
		__this->set_edgeBlend_4((bool)0);
	}

IL_0073:
	{
		bool L_9 = __this->get_edgeBlend_4();
		if (!L_9)
		{
			goto IL_00b8;
		}
	}
	{
		Shader_EnableKeyword_m2347844001(NULL /*static, unused*/, _stringLiteral1611267702, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral121570896, /*hidden argument*/NULL);
		Camera_t189460977 * L_10 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00b3;
		}
	}
	{
		Camera_t189460977 * L_12 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_13 = L_12;
		int32_t L_14 = Camera_get_depthTextureMode_m4106494306(L_13, /*hidden argument*/NULL);
		Camera_set_depthTextureMode_m132643161(L_13, ((int32_t)((int32_t)L_14|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_00b3:
	{
		goto IL_00cc;
	}

IL_00b8:
	{
		Shader_EnableKeyword_m2347844001(NULL /*static, unused*/, _stringLiteral121570896, /*hidden argument*/NULL);
		Shader_DisableKeyword_m710761032(NULL /*static, unused*/, _stringLiteral1611267702, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void WaterBase_WaterTileBeingRendered_m2956654727 (WaterBase_t4291487940 * __this, Transform_t3275118058 * ___tr0, Camera_t189460977 * ___currentCam1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterBase_WaterTileBeingRendered_m2956654727_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = ___currentCam1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		bool L_2 = __this->get_edgeBlend_4();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Camera_t189460977 * L_3 = ___currentCam1;
		Camera_t189460977 * L_4 = L_3;
		int32_t L_5 = Camera_get_depthTextureMode_m4106494306(L_4, /*hidden argument*/NULL);
		Camera_set_depthTextureMode_m132643161(L_4, ((int32_t)((int32_t)L_5|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterBase::Update()
extern "C"  void WaterBase_Update_m716995708 (WaterBase_t4291487940 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterBase_Update_m716995708_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_sharedMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		WaterBase_UpdateShader_m3822819655(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::.ctor()
extern "C"  void WaterTile__ctor_m458154778 (WaterTile_t1797616825 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::Start()
extern "C"  void WaterTile_Start_m1830566142 (WaterTile_t1797616825 * __this, const RuntimeMethod* method)
{
	{
		WaterTile_AcquireComponents_m3172643562(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::AcquireComponents()
extern "C"  void WaterTile_AcquireComponents_m3172643562 (WaterTile_t1797616825 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterTile_AcquireComponents_m3172643562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlanarReflection_t2931543887 * L_0 = __this->get_reflection_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		Transform_t3275118058 * L_2 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Transform_get_parent_m2752514051(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		Transform_t3275118058 * L_5 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Transform_get_parent_m2752514051(L_5, /*hidden argument*/NULL);
		PlanarReflection_t2931543887 * L_7 = Component_GetComponent_TisPlanarReflection_t2931543887_m147844090(L_6, /*hidden argument*/Component_GetComponent_TisPlanarReflection_t2931543887_m147844090_RuntimeMethod_var);
		__this->set_reflection_2(L_7);
		goto IL_0051;
	}

IL_0040:
	{
		Transform_t3275118058 * L_8 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		PlanarReflection_t2931543887 * L_9 = Component_GetComponent_TisPlanarReflection_t2931543887_m147844090(L_8, /*hidden argument*/Component_GetComponent_TisPlanarReflection_t2931543887_m147844090_RuntimeMethod_var);
		__this->set_reflection_2(L_9);
	}

IL_0051:
	{
		WaterBase_t4291487940 * L_10 = __this->get_waterBase_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00a2;
		}
	}
	{
		Transform_t3275118058 * L_12 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = Transform_get_parent_m2752514051(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		Transform_t3275118058 * L_15 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_16 = Transform_get_parent_m2752514051(L_15, /*hidden argument*/NULL);
		WaterBase_t4291487940 * L_17 = Component_GetComponent_TisWaterBase_t4291487940_m2297562155(L_16, /*hidden argument*/Component_GetComponent_TisWaterBase_t4291487940_m2297562155_RuntimeMethod_var);
		__this->set_waterBase_3(L_17);
		goto IL_00a2;
	}

IL_0091:
	{
		Transform_t3275118058 * L_18 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		WaterBase_t4291487940 * L_19 = Component_GetComponent_TisWaterBase_t4291487940_m2297562155(L_18, /*hidden argument*/Component_GetComponent_TisWaterBase_t4291487940_m2297562155_RuntimeMethod_var);
		__this->set_waterBase_3(L_19);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Water.WaterTile::OnWillRenderObject()
extern "C"  void WaterTile_OnWillRenderObject_m3692221246 (WaterTile_t1797616825 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterTile_OnWillRenderObject_m3692221246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlanarReflection_t2931543887 * L_0 = __this->get_reflection_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		PlanarReflection_t2931543887 * L_2 = __this->get_reflection_2();
		Transform_t3275118058 * L_3 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Camera_t189460977 * L_4 = Camera_get_current_m3769616944(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlanarReflection_WaterTileBeingRendered_m187266240(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		WaterBase_t4291487940 * L_5 = __this->get_waterBase_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		WaterBase_t4291487940 * L_7 = __this->get_waterBase_3();
		Transform_t3275118058 * L_8 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Camera_t189460977 * L_9 = Camera_get_current_m3769616944(NULL /*static, unused*/, /*hidden argument*/NULL);
		WaterBase_WaterTileBeingRendered_m2956654727(L_7, L_8, L_9, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
