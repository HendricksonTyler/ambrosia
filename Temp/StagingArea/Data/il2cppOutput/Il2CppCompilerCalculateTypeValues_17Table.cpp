﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t3928470916;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t3435657708;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t2260664863;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t2213949596;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t3246763936;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t1074830945;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t1976155184;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_t701624289;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2020713228;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t2262346586;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t890994926;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// MainWeapon
struct MainWeapon_t1214762455;
// SubWeapon
struct SubWeapon_t1081861670;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Collider
struct Collider_t3497673348;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1612828713;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;
// Ship
struct Ship_t1116303770;
// Player
struct Player_t1147783557;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// Game
struct Game_t1600141214;
// UIController
struct UIController_t2029583246;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// Enemy
struct Enemy_t1088811588;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t2761625415;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>
struct Dictionary_2_t2882765492;
// UnityStandardAssets.Water.WaterBase
struct WaterBase_t4291487940;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>
struct Dictionary_2_t3541619047;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityStandardAssets.Water.PlanarReflection
struct PlanarReflection_t2931543887;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t2719087314;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;




#ifndef U3CMODULEU3E_T3783534234_H
#define U3CMODULEU3E_T3783534234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534234 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534234_H
#ifndef U3CMODULEU3E_T3783534233_H
#define U3CMODULEU3E_T3783534233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534233 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534233_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef REFLECTIONMETHODSCACHE_T3343836395_H
#define REFLECTIONMETHODSCACHE_T3343836395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t3343836395  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t3928470916 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t3435657708 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t2260664863 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t2213949596 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t3246763936 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t1074830945 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___raycast3D_0)); }
	inline Raycast3DCallback_t3928470916 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t3928470916 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t3928470916 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t3435657708 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t3435657708 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t3435657708 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___raycast2D_2)); }
	inline Raycast2DCallback_t2260664863 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t2260664863 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t2260664863 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t2213949596 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t2213949596 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t2213949596 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t3246763936 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t3246763936 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t3246763936 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t1074830945 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t1074830945 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t1074830945 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t3343836395_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t3343836395 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t3343836395 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t3343836395 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t3343836395 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T3343836395_H
#ifndef LAYOUTUTILITY_T4076838048_H
#define LAYOUTUTILITY_T4076838048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t4076838048  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t4076838048_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache0
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache1
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache2
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache3
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache4
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache5
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache6
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache7
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T4076838048_H
#ifndef LAYOUTREBUILDER_T2155218138_H
#define LAYOUTREBUILDER_T2155218138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t2155218138  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t3349966182 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138, ___m_ToRebuild_0)); }
	inline RectTransform_t3349966182 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t3349966182 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t3349966182 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t2155218138_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_t701624289 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mg$cache0
	ReapplyDrivenProperties_t2020713228 * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache0
	Predicate_1_t2262346586 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache1
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache2
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache3
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache4
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_t701624289 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_t701624289 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_t701624289 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t2020713228 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t2020713228 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t2020713228 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2262346586 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2262346586 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2262346586 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T2155218138_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3228926346_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3228926346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t3349966182 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___rectTransform_0)); }
	inline RectTransform_t3349966182 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3349966182 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3349966182 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3228926346_H
#ifndef MESHCONTAINER_T299039902_H
#define MESHCONTAINER_T299039902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.MeshContainer
struct  MeshContainer_t299039902  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityStandardAssets.Water.MeshContainer::mesh
	Mesh_t1356156583 * ___mesh_0;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::vertices
	Vector3U5BU5D_t1172311765* ___vertices_1;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::normals
	Vector3U5BU5D_t1172311765* ___normals_2;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(MeshContainer_t299039902, ___mesh_0)); }
	inline Mesh_t1356156583 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t1356156583 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t1356156583 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_0), value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(MeshContainer_t299039902, ___vertices_1)); }
	inline Vector3U5BU5D_t1172311765* get_vertices_1() const { return ___vertices_1; }
	inline Vector3U5BU5D_t1172311765** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(Vector3U5BU5D_t1172311765* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_1), value);
	}

	inline static int32_t get_offset_of_normals_2() { return static_cast<int32_t>(offsetof(MeshContainer_t299039902, ___normals_2)); }
	inline Vector3U5BU5D_t1172311765* get_normals_2() const { return ___normals_2; }
	inline Vector3U5BU5D_t1172311765** get_address_of_normals_2() { return &___normals_2; }
	inline void set_normals_2(Vector3U5BU5D_t1172311765* value)
	{
		___normals_2 = value;
		Il2CppCodeGenWriteBarrier((&___normals_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCONTAINER_T299039902_H
#ifndef ENEMY_T1088811588_H
#define ENEMY_T1088811588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enemy
struct  Enemy_t1088811588  : public RuntimeObject
{
public:
	// System.Single Enemy::health
	float ___health_0;
	// System.Int32 Enemy::attackStrength
	int32_t ___attackStrength_1;
	// System.Single Enemy::attackDelay
	float ___attackDelay_2;
	// System.Boolean Enemy::attacking
	bool ___attacking_3;

public:
	inline static int32_t get_offset_of_health_0() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___health_0)); }
	inline float get_health_0() const { return ___health_0; }
	inline float* get_address_of_health_0() { return &___health_0; }
	inline void set_health_0(float value)
	{
		___health_0 = value;
	}

	inline static int32_t get_offset_of_attackStrength_1() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___attackStrength_1)); }
	inline int32_t get_attackStrength_1() const { return ___attackStrength_1; }
	inline int32_t* get_address_of_attackStrength_1() { return &___attackStrength_1; }
	inline void set_attackStrength_1(int32_t value)
	{
		___attackStrength_1 = value;
	}

	inline static int32_t get_offset_of_attackDelay_2() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___attackDelay_2)); }
	inline float get_attackDelay_2() const { return ___attackDelay_2; }
	inline float* get_address_of_attackDelay_2() { return &___attackDelay_2; }
	inline void set_attackDelay_2(float value)
	{
		___attackDelay_2 = value;
	}

	inline static int32_t get_offset_of_attacking_3() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___attacking_3)); }
	inline bool get_attacking_3() const { return ___attacking_3; }
	inline bool* get_address_of_attacking_3() { return &___attacking_3; }
	inline void set_attacking_3(bool value)
	{
		___attacking_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMY_T1088811588_H
#ifndef PLAYER_T1147783557_H
#define PLAYER_T1147783557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t1147783557  : public RuntimeObject
{
public:
	// MainWeapon Player::blenderBlaster
	MainWeapon_t1214762455 * ___blenderBlaster_0;
	// SubWeapon Player::omNomBomb
	SubWeapon_t1081861670 * ___omNomBomb_1;
	// System.Single Player::speed
	float ___speed_2;
	// System.Single Player::walkingSpeed
	float ___walkingSpeed_3;
	// System.Single Player::sprintingSpeed
	float ___sprintingSpeed_4;
	// System.Single Player::jumpHeight
	float ___jumpHeight_5;
	// System.Single Player::regenDelay
	float ___regenDelay_6;
	// System.Single Player::health
	float ___health_7;
	// System.Int32 Player::healthRegen
	int32_t ___healthRegen_8;
	// System.Boolean Player::partHeld
	bool ___partHeld_9;

public:
	inline static int32_t get_offset_of_blenderBlaster_0() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___blenderBlaster_0)); }
	inline MainWeapon_t1214762455 * get_blenderBlaster_0() const { return ___blenderBlaster_0; }
	inline MainWeapon_t1214762455 ** get_address_of_blenderBlaster_0() { return &___blenderBlaster_0; }
	inline void set_blenderBlaster_0(MainWeapon_t1214762455 * value)
	{
		___blenderBlaster_0 = value;
		Il2CppCodeGenWriteBarrier((&___blenderBlaster_0), value);
	}

	inline static int32_t get_offset_of_omNomBomb_1() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___omNomBomb_1)); }
	inline SubWeapon_t1081861670 * get_omNomBomb_1() const { return ___omNomBomb_1; }
	inline SubWeapon_t1081861670 ** get_address_of_omNomBomb_1() { return &___omNomBomb_1; }
	inline void set_omNomBomb_1(SubWeapon_t1081861670 * value)
	{
		___omNomBomb_1 = value;
		Il2CppCodeGenWriteBarrier((&___omNomBomb_1), value);
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_walkingSpeed_3() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___walkingSpeed_3)); }
	inline float get_walkingSpeed_3() const { return ___walkingSpeed_3; }
	inline float* get_address_of_walkingSpeed_3() { return &___walkingSpeed_3; }
	inline void set_walkingSpeed_3(float value)
	{
		___walkingSpeed_3 = value;
	}

	inline static int32_t get_offset_of_sprintingSpeed_4() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___sprintingSpeed_4)); }
	inline float get_sprintingSpeed_4() const { return ___sprintingSpeed_4; }
	inline float* get_address_of_sprintingSpeed_4() { return &___sprintingSpeed_4; }
	inline void set_sprintingSpeed_4(float value)
	{
		___sprintingSpeed_4 = value;
	}

	inline static int32_t get_offset_of_jumpHeight_5() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___jumpHeight_5)); }
	inline float get_jumpHeight_5() const { return ___jumpHeight_5; }
	inline float* get_address_of_jumpHeight_5() { return &___jumpHeight_5; }
	inline void set_jumpHeight_5(float value)
	{
		___jumpHeight_5 = value;
	}

	inline static int32_t get_offset_of_regenDelay_6() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___regenDelay_6)); }
	inline float get_regenDelay_6() const { return ___regenDelay_6; }
	inline float* get_address_of_regenDelay_6() { return &___regenDelay_6; }
	inline void set_regenDelay_6(float value)
	{
		___regenDelay_6 = value;
	}

	inline static int32_t get_offset_of_health_7() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___health_7)); }
	inline float get_health_7() const { return ___health_7; }
	inline float* get_address_of_health_7() { return &___health_7; }
	inline void set_health_7(float value)
	{
		___health_7 = value;
	}

	inline static int32_t get_offset_of_healthRegen_8() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___healthRegen_8)); }
	inline int32_t get_healthRegen_8() const { return ___healthRegen_8; }
	inline int32_t* get_address_of_healthRegen_8() { return &___healthRegen_8; }
	inline void set_healthRegen_8(int32_t value)
	{
		___healthRegen_8 = value;
	}

	inline static int32_t get_offset_of_partHeld_9() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___partHeld_9)); }
	inline bool get_partHeld_9() const { return ___partHeld_9; }
	inline bool* get_address_of_partHeld_9() { return &___partHeld_9; }
	inline void set_partHeld_9(bool value)
	{
		___partHeld_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T1147783557_H
#ifndef BASEVERTEXEFFECT_T2504093552_H
#define BASEVERTEXEFFECT_T2504093552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2504093552  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2504093552_H
#ifndef SHIP_T1116303770_H
#define SHIP_T1116303770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ship
struct  Ship_t1116303770  : public RuntimeObject
{
public:
	// System.Int32 Ship::health
	int32_t ___health_0;

public:
	inline static int32_t get_offset_of_health_0() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___health_0)); }
	inline int32_t get_health_0() const { return ___health_0; }
	inline int32_t* get_address_of_health_0() { return &___health_0; }
	inline void set_health_0(int32_t value)
	{
		___health_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIP_T1116303770_H
#ifndef UICONTROLLER_T2029583246_H
#define UICONTROLLER_T2029583246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController
struct  UIController_t2029583246  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image UIController::damageFrame
	Image_t2042527209 * ___damageFrame_1;
	// UnityEngine.UI.Image UIController::ammoBar
	Image_t2042527209 * ___ammoBar_2;
	// UnityEngine.UI.Image UIController::rechargingBar
	Image_t2042527209 * ___rechargingBar_3;
	// UnityEngine.GameObject UIController::omNombBombIcon
	GameObject_t1756533147 * ___omNombBombIcon_4;
	// UnityEngine.UI.Text UIController::waveCounter
	Text_t356221433 * ___waveCounter_5;
	// UnityEngine.UI.Text UIController::timer
	Text_t356221433 * ___timer_6;
	// UnityEngine.UI.Text UIController::partsCollected
	Text_t356221433 * ___partsCollected_7;
	// UnityEngine.UI.Text UIController::message
	Text_t356221433 * ___message_8;
	// UnityEngine.UI.Text UIController::messageDescription
	Text_t356221433 * ___messageDescription_9;
	// UnityEngine.UI.Text UIController::subMessage
	Text_t356221433 * ___subMessage_10;
	// UnityEngine.GameObject UIController::partIcon
	GameObject_t1756533147 * ___partIcon_11;
	// UnityEngine.GameObject UIController::deathCam
	GameObject_t1756533147 * ___deathCam_12;
	// System.Single UIController::shipAttackedDelay
	float ___shipAttackedDelay_13;
	// System.Boolean UIController::freezeUI
	bool ___freezeUI_14;

public:
	inline static int32_t get_offset_of_damageFrame_1() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___damageFrame_1)); }
	inline Image_t2042527209 * get_damageFrame_1() const { return ___damageFrame_1; }
	inline Image_t2042527209 ** get_address_of_damageFrame_1() { return &___damageFrame_1; }
	inline void set_damageFrame_1(Image_t2042527209 * value)
	{
		___damageFrame_1 = value;
		Il2CppCodeGenWriteBarrier((&___damageFrame_1), value);
	}

	inline static int32_t get_offset_of_ammoBar_2() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___ammoBar_2)); }
	inline Image_t2042527209 * get_ammoBar_2() const { return ___ammoBar_2; }
	inline Image_t2042527209 ** get_address_of_ammoBar_2() { return &___ammoBar_2; }
	inline void set_ammoBar_2(Image_t2042527209 * value)
	{
		___ammoBar_2 = value;
		Il2CppCodeGenWriteBarrier((&___ammoBar_2), value);
	}

	inline static int32_t get_offset_of_rechargingBar_3() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___rechargingBar_3)); }
	inline Image_t2042527209 * get_rechargingBar_3() const { return ___rechargingBar_3; }
	inline Image_t2042527209 ** get_address_of_rechargingBar_3() { return &___rechargingBar_3; }
	inline void set_rechargingBar_3(Image_t2042527209 * value)
	{
		___rechargingBar_3 = value;
		Il2CppCodeGenWriteBarrier((&___rechargingBar_3), value);
	}

	inline static int32_t get_offset_of_omNombBombIcon_4() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___omNombBombIcon_4)); }
	inline GameObject_t1756533147 * get_omNombBombIcon_4() const { return ___omNombBombIcon_4; }
	inline GameObject_t1756533147 ** get_address_of_omNombBombIcon_4() { return &___omNombBombIcon_4; }
	inline void set_omNombBombIcon_4(GameObject_t1756533147 * value)
	{
		___omNombBombIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&___omNombBombIcon_4), value);
	}

	inline static int32_t get_offset_of_waveCounter_5() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___waveCounter_5)); }
	inline Text_t356221433 * get_waveCounter_5() const { return ___waveCounter_5; }
	inline Text_t356221433 ** get_address_of_waveCounter_5() { return &___waveCounter_5; }
	inline void set_waveCounter_5(Text_t356221433 * value)
	{
		___waveCounter_5 = value;
		Il2CppCodeGenWriteBarrier((&___waveCounter_5), value);
	}

	inline static int32_t get_offset_of_timer_6() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___timer_6)); }
	inline Text_t356221433 * get_timer_6() const { return ___timer_6; }
	inline Text_t356221433 ** get_address_of_timer_6() { return &___timer_6; }
	inline void set_timer_6(Text_t356221433 * value)
	{
		___timer_6 = value;
		Il2CppCodeGenWriteBarrier((&___timer_6), value);
	}

	inline static int32_t get_offset_of_partsCollected_7() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___partsCollected_7)); }
	inline Text_t356221433 * get_partsCollected_7() const { return ___partsCollected_7; }
	inline Text_t356221433 ** get_address_of_partsCollected_7() { return &___partsCollected_7; }
	inline void set_partsCollected_7(Text_t356221433 * value)
	{
		___partsCollected_7 = value;
		Il2CppCodeGenWriteBarrier((&___partsCollected_7), value);
	}

	inline static int32_t get_offset_of_message_8() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___message_8)); }
	inline Text_t356221433 * get_message_8() const { return ___message_8; }
	inline Text_t356221433 ** get_address_of_message_8() { return &___message_8; }
	inline void set_message_8(Text_t356221433 * value)
	{
		___message_8 = value;
		Il2CppCodeGenWriteBarrier((&___message_8), value);
	}

	inline static int32_t get_offset_of_messageDescription_9() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___messageDescription_9)); }
	inline Text_t356221433 * get_messageDescription_9() const { return ___messageDescription_9; }
	inline Text_t356221433 ** get_address_of_messageDescription_9() { return &___messageDescription_9; }
	inline void set_messageDescription_9(Text_t356221433 * value)
	{
		___messageDescription_9 = value;
		Il2CppCodeGenWriteBarrier((&___messageDescription_9), value);
	}

	inline static int32_t get_offset_of_subMessage_10() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___subMessage_10)); }
	inline Text_t356221433 * get_subMessage_10() const { return ___subMessage_10; }
	inline Text_t356221433 ** get_address_of_subMessage_10() { return &___subMessage_10; }
	inline void set_subMessage_10(Text_t356221433 * value)
	{
		___subMessage_10 = value;
		Il2CppCodeGenWriteBarrier((&___subMessage_10), value);
	}

	inline static int32_t get_offset_of_partIcon_11() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___partIcon_11)); }
	inline GameObject_t1756533147 * get_partIcon_11() const { return ___partIcon_11; }
	inline GameObject_t1756533147 ** get_address_of_partIcon_11() { return &___partIcon_11; }
	inline void set_partIcon_11(GameObject_t1756533147 * value)
	{
		___partIcon_11 = value;
		Il2CppCodeGenWriteBarrier((&___partIcon_11), value);
	}

	inline static int32_t get_offset_of_deathCam_12() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___deathCam_12)); }
	inline GameObject_t1756533147 * get_deathCam_12() const { return ___deathCam_12; }
	inline GameObject_t1756533147 ** get_address_of_deathCam_12() { return &___deathCam_12; }
	inline void set_deathCam_12(GameObject_t1756533147 * value)
	{
		___deathCam_12 = value;
		Il2CppCodeGenWriteBarrier((&___deathCam_12), value);
	}

	inline static int32_t get_offset_of_shipAttackedDelay_13() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___shipAttackedDelay_13)); }
	inline float get_shipAttackedDelay_13() const { return ___shipAttackedDelay_13; }
	inline float* get_address_of_shipAttackedDelay_13() { return &___shipAttackedDelay_13; }
	inline void set_shipAttackedDelay_13(float value)
	{
		___shipAttackedDelay_13 = value;
	}

	inline static int32_t get_offset_of_freezeUI_14() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___freezeUI_14)); }
	inline bool get_freezeUI_14() const { return ___freezeUI_14; }
	inline bool* get_address_of_freezeUI_14() { return &___freezeUI_14; }
	inline void set_freezeUI_14(bool value)
	{
		___freezeUI_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONTROLLER_T2029583246_H
#ifndef RECTANGULARVERTEXCLIPPER_T3349113845_H
#define RECTANGULARVERTEXCLIPPER_T3349113845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RectangularVertexClipper
struct  RectangularVertexClipper_t3349113845  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_WorldCorners
	Vector3U5BU5D_t1172311765* ___m_WorldCorners_0;
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_CanvasCorners
	Vector3U5BU5D_t1172311765* ___m_CanvasCorners_1;

public:
	inline static int32_t get_offset_of_m_WorldCorners_0() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t3349113845, ___m_WorldCorners_0)); }
	inline Vector3U5BU5D_t1172311765* get_m_WorldCorners_0() const { return ___m_WorldCorners_0; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_WorldCorners_0() { return &___m_WorldCorners_0; }
	inline void set_m_WorldCorners_0(Vector3U5BU5D_t1172311765* value)
	{
		___m_WorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorldCorners_0), value);
	}

	inline static int32_t get_offset_of_m_CanvasCorners_1() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t3349113845, ___m_CanvasCorners_1)); }
	inline Vector3U5BU5D_t1172311765* get_m_CanvasCorners_1() const { return ___m_CanvasCorners_1; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_CanvasCorners_1() { return &___m_CanvasCorners_1; }
	inline void set_m_CanvasCorners_1(Vector3U5BU5D_t1172311765* value)
	{
		___m_CanvasCorners_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasCorners_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGULARVERTEXCLIPPER_T3349113845_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef LAYERMASK_T3188175821_H
#define LAYERMASK_T3188175821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3188175821 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3188175821, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3188175821_H
#ifndef U24ARRAYTYPEU3D12_T1568637717_H
#define U24ARRAYTYPEU3D12_T1568637717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t1568637717 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t1568637717__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T1568637717_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T154385424_H
#define DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t154385424 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef DIFFICULTYLEVEL_T2746910351_H
#define DIFFICULTYLEVEL_T2746910351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// difficultyLevel
struct  difficultyLevel_t2746910351 
{
public:
	// System.Int32 difficultyLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(difficultyLevel_t2746910351, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIFFICULTYLEVEL_T2746910351_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef WATERQUALITY_T3485013746_H
#define WATERQUALITY_T3485013746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterQuality
struct  WaterQuality_t3485013746 
{
public:
	// System.Int32 UnityStandardAssets.Water.WaterQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaterQuality_t3485013746, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERQUALITY_T3485013746_H
#ifndef AMMOTYPE_T4206898302_H
#define AMMOTYPE_T4206898302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ammoType
struct  ammoType_t4206898302 
{
public:
	// System.Int32 ammoType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ammoType_t4206898302, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMMOTYPE_T4206898302_H
#ifndef WATERMODE_T2303503718_H
#define WATERMODE_T2303503718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water/WaterMode
struct  WaterMode_t2303503718 
{
public:
	// System.Int32 UnityStandardAssets.Water.Water/WaterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaterMode_t2303503718, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERMODE_T2303503718_H
#ifndef RAYCASTHIT2D_T4063908774_H
#define RAYCASTHIT2D_T4063908774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t4063908774 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2243707579  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2243707579  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2243707579  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t646061738 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Centroid_0)); }
	inline Vector2_t2243707579  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2243707579 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2243707579  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Point_1)); }
	inline Vector2_t2243707579  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2243707579 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2243707579  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Normal_2)); }
	inline Vector2_t2243707579  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2243707579 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2243707579  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Collider_5)); }
	inline Collider2D_t646061738 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t646061738 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t646061738 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_pinvoke
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_com
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T4063908774_H
#ifndef BOMBTYPE_T3119779548_H
#define BOMBTYPE_T3119779548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bombType
struct  bombType_t3119779548 
{
public:
	// System.Int32 bombType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(bombType_t3119779548, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOMBTYPE_T3119779548_H
#ifndef RAYCASTHIT_T87180320_H
#define RAYCASTHIT_T87180320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t87180320 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t2243707580  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t2243707580  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2243707579  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t3497673348 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Point_0)); }
	inline Vector3_t2243707580  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t2243707580 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t2243707580  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Normal_1)); }
	inline Vector3_t2243707580  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t2243707580  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_UV_4)); }
	inline Vector2_t2243707579  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2243707579 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2243707579  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Collider_5)); }
	inline Collider_t3497673348 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t3497673348 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t3497673348 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_pinvoke
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_com
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T87180320_H
#ifndef RAY_T2469606224_H
#define RAY_T2469606224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t2469606224 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t2243707580  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t2243707580  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Origin_0)); }
	inline Vector3_t2243707580  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t2243707580 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t2243707580  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Direction_1)); }
	inline Vector3_t2243707580  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t2243707580 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t2243707580  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T2469606224_H
#ifndef TEXTANCHOR_T112990806_H
#define TEXTANCHOR_T112990806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t112990806 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t112990806, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T112990806_H
#ifndef CORNER_T1077473318_H
#define CORNER_T1077473318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Corner
struct  Corner_t1077473318 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Corner::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Corner_t1077473318, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_T1077473318_H
#ifndef CONSTRAINT_T3558160636_H
#define CONSTRAINT_T3558160636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Constraint
struct  Constraint_t3558160636 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Constraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Constraint_t3558160636, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T3558160636_H
#ifndef VERTEXHELPER_T385374196_H
#define VERTEXHELPER_T385374196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t385374196  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t1612828712 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t243638650 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t1612828711 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t1612828711 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t1612828711 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t1612828711 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t1612828712 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t1612828713 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t1440998580 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Positions_0)); }
	inline List_1_t1612828712 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t1612828712 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t1612828712 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Colors_1)); }
	inline List_1_t243638650 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t243638650 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t243638650 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv0S_2)); }
	inline List_1_t1612828711 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t1612828711 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t1612828711 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv1S_3)); }
	inline List_1_t1612828711 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t1612828711 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t1612828711 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv2S_4)); }
	inline List_1_t1612828711 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t1612828711 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t1612828711 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv3S_5)); }
	inline List_1_t1612828711 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t1612828711 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t1612828711 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Normals_6)); }
	inline List_1_t1612828712 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t1612828712 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t1612828712 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Tangents_7)); }
	inline List_1_t1612828713 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t1612828713 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t1612828713 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Indices_8)); }
	inline List_1_t1440998580 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t1440998580 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t1440998580 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t385374196_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t2243707581  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t2243707580  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t2243707581  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t2243707581 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t2243707581  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t2243707580  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t2243707580 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t2243707580  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T385374196_H
#ifndef FITMODE_T4030874534_H
#define FITMODE_T4030874534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter/FitMode
struct  FitMode_t4030874534 
{
public:
	// System.Int32 UnityEngine.UI.ContentSizeFitter/FitMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FitMode_t4030874534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FITMODE_T4030874534_H
#ifndef UNIT_T3220761768_H
#define UNIT_T3220761768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/Unit
struct  Unit_t3220761768 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/Unit::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Unit_t3220761768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T3220761768_H
#ifndef SCREENMATCHMODE_T1916789528_H
#define SCREENMATCHMODE_T1916789528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScreenMatchMode
struct  ScreenMatchMode_t1916789528 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScreenMatchMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenMatchMode_t1916789528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENMATCHMODE_T1916789528_H
#ifndef SCALEMODE_T987318053_H
#define SCALEMODE_T987318053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScaleMode
struct  ScaleMode_t987318053 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScaleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScaleMode_t987318053, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEMODE_T987318053_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305141_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305141  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t1568637717  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t1568637717  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t1568637717 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t1568637717  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305141_H
#ifndef ASPECTMODE_T1166448724_H
#define ASPECTMODE_T1166448724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter/AspectMode
struct  AspectMode_t1166448724 
{
public:
	// System.Int32 UnityEngine.UI.AspectRatioFitter/AspectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectMode_t1166448724, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTMODE_T1166448724_H
#ifndef AXIS_T1431825778_H
#define AXIS_T1431825778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Axis
struct  Axis_t1431825778 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t1431825778, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1431825778_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef SUBWEAPON_T1081861670_H
#define SUBWEAPON_T1081861670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubWeapon
struct  SubWeapon_t1081861670  : public RuntimeObject
{
public:
	// bombType SubWeapon::type
	int32_t ___type_0;
	// System.Int32 SubWeapon::damage
	int32_t ___damage_1;
	// System.Single SubWeapon::delay
	float ___delay_2;
	// System.Single SubWeapon::throwDelay
	float ___throwDelay_3;
	// System.Single SubWeapon::range
	float ___range_4;
	// System.Boolean SubWeapon::thrown
	bool ___thrown_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_damage_1() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___damage_1)); }
	inline int32_t get_damage_1() const { return ___damage_1; }
	inline int32_t* get_address_of_damage_1() { return &___damage_1; }
	inline void set_damage_1(int32_t value)
	{
		___damage_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_throwDelay_3() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___throwDelay_3)); }
	inline float get_throwDelay_3() const { return ___throwDelay_3; }
	inline float* get_address_of_throwDelay_3() { return &___throwDelay_3; }
	inline void set_throwDelay_3(float value)
	{
		___throwDelay_3 = value;
	}

	inline static int32_t get_offset_of_range_4() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___range_4)); }
	inline float get_range_4() const { return ___range_4; }
	inline float* get_address_of_range_4() { return &___range_4; }
	inline void set_range_4(float value)
	{
		___range_4 = value;
	}

	inline static int32_t get_offset_of_thrown_5() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___thrown_5)); }
	inline bool get_thrown_5() const { return ___thrown_5; }
	inline bool* get_address_of_thrown_5() { return &___thrown_5; }
	inline void set_thrown_5(bool value)
	{
		___thrown_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBWEAPON_T1081861670_H
#ifndef MAINWEAPON_T1214762455_H
#define MAINWEAPON_T1214762455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainWeapon
struct  MainWeapon_t1214762455  : public RuntimeObject
{
public:
	// ammoType MainWeapon::ammo
	int32_t ___ammo_0;
	// System.Single MainWeapon::range
	float ___range_1;
	// System.Int32 MainWeapon::damage
	int32_t ___damage_2;
	// System.Single MainWeapon::currentCharge
	float ___currentCharge_3;
	// System.Single MainWeapon::dischargeRate
	float ___dischargeRate_4;
	// System.Single MainWeapon::rechargeRate
	float ___rechargeRate_5;

public:
	inline static int32_t get_offset_of_ammo_0() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___ammo_0)); }
	inline int32_t get_ammo_0() const { return ___ammo_0; }
	inline int32_t* get_address_of_ammo_0() { return &___ammo_0; }
	inline void set_ammo_0(int32_t value)
	{
		___ammo_0 = value;
	}

	inline static int32_t get_offset_of_range_1() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___range_1)); }
	inline float get_range_1() const { return ___range_1; }
	inline float* get_address_of_range_1() { return &___range_1; }
	inline void set_range_1(float value)
	{
		___range_1 = value;
	}

	inline static int32_t get_offset_of_damage_2() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___damage_2)); }
	inline int32_t get_damage_2() const { return ___damage_2; }
	inline int32_t* get_address_of_damage_2() { return &___damage_2; }
	inline void set_damage_2(int32_t value)
	{
		___damage_2 = value;
	}

	inline static int32_t get_offset_of_currentCharge_3() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___currentCharge_3)); }
	inline float get_currentCharge_3() const { return ___currentCharge_3; }
	inline float* get_address_of_currentCharge_3() { return &___currentCharge_3; }
	inline void set_currentCharge_3(float value)
	{
		___currentCharge_3 = value;
	}

	inline static int32_t get_offset_of_dischargeRate_4() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___dischargeRate_4)); }
	inline float get_dischargeRate_4() const { return ___dischargeRate_4; }
	inline float* get_address_of_dischargeRate_4() { return &___dischargeRate_4; }
	inline void set_dischargeRate_4(float value)
	{
		___dischargeRate_4 = value;
	}

	inline static int32_t get_offset_of_rechargeRate_5() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___rechargeRate_5)); }
	inline float get_rechargeRate_5() const { return ___rechargeRate_5; }
	inline float* get_address_of_rechargeRate_5() { return &___rechargeRate_5; }
	inline void set_rechargeRate_5(float value)
	{
		___rechargeRate_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINWEAPON_T1214762455_H
#ifndef GAME_T1600141214_H
#define GAME_T1600141214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game
struct  Game_t1600141214  : public RuntimeObject
{
public:
	// difficultyLevel Game::difficulty
	int32_t ___difficulty_15;
	// System.Boolean Game::won
	bool ___won_16;
	// System.Int32 Game::wave
	int32_t ___wave_17;
	// System.Int32 Game::players
	int32_t ___players_18;
	// System.Int32 Game::partsReturned
	int32_t ___partsReturned_19;
	// System.Int32 Game::partsRequired
	int32_t ___partsRequired_20;
	// System.Single Game::timeRemaining
	float ___timeRemaining_21;
	// System.Single Game::betweenWaveDelay
	float ___betweenWaveDelay_22;
	// System.Single Game::partSpawnDelay
	float ___partSpawnDelay_23;
	// System.Single Game::partSpawnAmount
	float ___partSpawnAmount_24;
	// System.Single Game::enemySpawnDelay
	float ___enemySpawnDelay_25;
	// System.Int32 Game::enemySpawnAmount
	int32_t ___enemySpawnAmount_26;
	// System.Boolean Game::betweenWaves
	bool ___betweenWaves_27;

public:
	inline static int32_t get_offset_of_difficulty_15() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___difficulty_15)); }
	inline int32_t get_difficulty_15() const { return ___difficulty_15; }
	inline int32_t* get_address_of_difficulty_15() { return &___difficulty_15; }
	inline void set_difficulty_15(int32_t value)
	{
		___difficulty_15 = value;
	}

	inline static int32_t get_offset_of_won_16() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___won_16)); }
	inline bool get_won_16() const { return ___won_16; }
	inline bool* get_address_of_won_16() { return &___won_16; }
	inline void set_won_16(bool value)
	{
		___won_16 = value;
	}

	inline static int32_t get_offset_of_wave_17() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___wave_17)); }
	inline int32_t get_wave_17() const { return ___wave_17; }
	inline int32_t* get_address_of_wave_17() { return &___wave_17; }
	inline void set_wave_17(int32_t value)
	{
		___wave_17 = value;
	}

	inline static int32_t get_offset_of_players_18() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___players_18)); }
	inline int32_t get_players_18() const { return ___players_18; }
	inline int32_t* get_address_of_players_18() { return &___players_18; }
	inline void set_players_18(int32_t value)
	{
		___players_18 = value;
	}

	inline static int32_t get_offset_of_partsReturned_19() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___partsReturned_19)); }
	inline int32_t get_partsReturned_19() const { return ___partsReturned_19; }
	inline int32_t* get_address_of_partsReturned_19() { return &___partsReturned_19; }
	inline void set_partsReturned_19(int32_t value)
	{
		___partsReturned_19 = value;
	}

	inline static int32_t get_offset_of_partsRequired_20() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___partsRequired_20)); }
	inline int32_t get_partsRequired_20() const { return ___partsRequired_20; }
	inline int32_t* get_address_of_partsRequired_20() { return &___partsRequired_20; }
	inline void set_partsRequired_20(int32_t value)
	{
		___partsRequired_20 = value;
	}

	inline static int32_t get_offset_of_timeRemaining_21() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___timeRemaining_21)); }
	inline float get_timeRemaining_21() const { return ___timeRemaining_21; }
	inline float* get_address_of_timeRemaining_21() { return &___timeRemaining_21; }
	inline void set_timeRemaining_21(float value)
	{
		___timeRemaining_21 = value;
	}

	inline static int32_t get_offset_of_betweenWaveDelay_22() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___betweenWaveDelay_22)); }
	inline float get_betweenWaveDelay_22() const { return ___betweenWaveDelay_22; }
	inline float* get_address_of_betweenWaveDelay_22() { return &___betweenWaveDelay_22; }
	inline void set_betweenWaveDelay_22(float value)
	{
		___betweenWaveDelay_22 = value;
	}

	inline static int32_t get_offset_of_partSpawnDelay_23() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___partSpawnDelay_23)); }
	inline float get_partSpawnDelay_23() const { return ___partSpawnDelay_23; }
	inline float* get_address_of_partSpawnDelay_23() { return &___partSpawnDelay_23; }
	inline void set_partSpawnDelay_23(float value)
	{
		___partSpawnDelay_23 = value;
	}

	inline static int32_t get_offset_of_partSpawnAmount_24() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___partSpawnAmount_24)); }
	inline float get_partSpawnAmount_24() const { return ___partSpawnAmount_24; }
	inline float* get_address_of_partSpawnAmount_24() { return &___partSpawnAmount_24; }
	inline void set_partSpawnAmount_24(float value)
	{
		___partSpawnAmount_24 = value;
	}

	inline static int32_t get_offset_of_enemySpawnDelay_25() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___enemySpawnDelay_25)); }
	inline float get_enemySpawnDelay_25() const { return ___enemySpawnDelay_25; }
	inline float* get_address_of_enemySpawnDelay_25() { return &___enemySpawnDelay_25; }
	inline void set_enemySpawnDelay_25(float value)
	{
		___enemySpawnDelay_25 = value;
	}

	inline static int32_t get_offset_of_enemySpawnAmount_26() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___enemySpawnAmount_26)); }
	inline int32_t get_enemySpawnAmount_26() const { return ___enemySpawnAmount_26; }
	inline int32_t* get_address_of_enemySpawnAmount_26() { return &___enemySpawnAmount_26; }
	inline void set_enemySpawnAmount_26(int32_t value)
	{
		___enemySpawnAmount_26 = value;
	}

	inline static int32_t get_offset_of_betweenWaves_27() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___betweenWaves_27)); }
	inline bool get_betweenWaves_27() const { return ___betweenWaves_27; }
	inline bool* get_address_of_betweenWaves_27() { return &___betweenWaves_27; }
	inline void set_betweenWaves_27(bool value)
	{
		___betweenWaves_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAME_T1600141214_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef RAYCASTALLCALLBACK_T3435657708_H
#define RAYCASTALLCALLBACK_T3435657708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t3435657708  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T3435657708_H
#ifndef RAYCAST2DCALLBACK_T2260664863_H
#define RAYCAST2DCALLBACK_T2260664863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t2260664863  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T2260664863_H
#ifndef RAYCAST3DCALLBACK_T3928470916_H
#define RAYCAST3DCALLBACK_T3928470916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t3928470916  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T3928470916_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T2213949596_H
#define GETRAYINTERSECTIONALLCALLBACK_T2213949596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t2213949596  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T2213949596_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T3246763936_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T3246763936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t3246763936  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T3246763936_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T1074830945_H
#define GETRAYCASTNONALLOCCALLBACK_T1074830945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t1074830945  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T1074830945_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef SHIPCONTROLLER_T3277973446_H
#define SHIPCONTROLLER_T3277973446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipController
struct  ShipController_t3277973446  : public MonoBehaviour_t1158329972
{
public:
	// Ship ShipController::ship
	Ship_t1116303770 * ___ship_2;
	// UnityEngine.GameObject ShipController::explosiveEffect
	GameObject_t1756533147 * ___explosiveEffect_3;

public:
	inline static int32_t get_offset_of_ship_2() { return static_cast<int32_t>(offsetof(ShipController_t3277973446, ___ship_2)); }
	inline Ship_t1116303770 * get_ship_2() const { return ___ship_2; }
	inline Ship_t1116303770 ** get_address_of_ship_2() { return &___ship_2; }
	inline void set_ship_2(Ship_t1116303770 * value)
	{
		___ship_2 = value;
		Il2CppCodeGenWriteBarrier((&___ship_2), value);
	}

	inline static int32_t get_offset_of_explosiveEffect_3() { return static_cast<int32_t>(offsetof(ShipController_t3277973446, ___explosiveEffect_3)); }
	inline GameObject_t1756533147 * get_explosiveEffect_3() const { return ___explosiveEffect_3; }
	inline GameObject_t1756533147 ** get_address_of_explosiveEffect_3() { return &___explosiveEffect_3; }
	inline void set_explosiveEffect_3(GameObject_t1756533147 * value)
	{
		___explosiveEffect_3 = value;
		Il2CppCodeGenWriteBarrier((&___explosiveEffect_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPCONTROLLER_T3277973446_H
#ifndef BOMB_T2608481788_H
#define BOMB_T2608481788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bomb
struct  Bomb_t2608481788  : public MonoBehaviour_t1158329972
{
public:
	// SubWeapon Bomb::omNomBomb
	SubWeapon_t1081861670 * ___omNomBomb_2;
	// UnityEngine.GameObject Bomb::explosiveEffect
	GameObject_t1756533147 * ___explosiveEffect_3;
	// UnityEngine.GameObject Bomb::player
	GameObject_t1756533147 * ___player_4;
	// System.Single Bomb::explosionRadius
	float ___explosionRadius_5;

public:
	inline static int32_t get_offset_of_omNomBomb_2() { return static_cast<int32_t>(offsetof(Bomb_t2608481788, ___omNomBomb_2)); }
	inline SubWeapon_t1081861670 * get_omNomBomb_2() const { return ___omNomBomb_2; }
	inline SubWeapon_t1081861670 ** get_address_of_omNomBomb_2() { return &___omNomBomb_2; }
	inline void set_omNomBomb_2(SubWeapon_t1081861670 * value)
	{
		___omNomBomb_2 = value;
		Il2CppCodeGenWriteBarrier((&___omNomBomb_2), value);
	}

	inline static int32_t get_offset_of_explosiveEffect_3() { return static_cast<int32_t>(offsetof(Bomb_t2608481788, ___explosiveEffect_3)); }
	inline GameObject_t1756533147 * get_explosiveEffect_3() const { return ___explosiveEffect_3; }
	inline GameObject_t1756533147 ** get_address_of_explosiveEffect_3() { return &___explosiveEffect_3; }
	inline void set_explosiveEffect_3(GameObject_t1756533147 * value)
	{
		___explosiveEffect_3 = value;
		Il2CppCodeGenWriteBarrier((&___explosiveEffect_3), value);
	}

	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(Bomb_t2608481788, ___player_4)); }
	inline GameObject_t1756533147 * get_player_4() const { return ___player_4; }
	inline GameObject_t1756533147 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_t1756533147 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_explosionRadius_5() { return static_cast<int32_t>(offsetof(Bomb_t2608481788, ___explosionRadius_5)); }
	inline float get_explosionRadius_5() const { return ___explosionRadius_5; }
	inline float* get_address_of_explosionRadius_5() { return &___explosionRadius_5; }
	inline void set_explosionRadius_5(float value)
	{
		___explosionRadius_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOMB_T2608481788_H
#ifndef PLAYERCONTROLLER_T4148409433_H
#define PLAYERCONTROLLER_T4148409433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t4148409433  : public MonoBehaviour_t1158329972
{
public:
	// Player PlayerController::grubby
	Player_t1147783557 * ___grubby_2;
	// UnityEngine.CharacterController PlayerController::character
	CharacterController_t4094781467 * ___character_3;
	// UnityEngine.GameObject PlayerController::bombPrefab
	GameObject_t1756533147 * ___bombPrefab_4;
	// UnityEngine.GameObject PlayerController::bulletPrefab
	GameObject_t1756533147 * ___bulletPrefab_5;
	// UnityEngine.GameObject PlayerController::nozzle
	GameObject_t1756533147 * ___nozzle_6;
	// System.Single PlayerController::walkSpeed
	float ___walkSpeed_7;
	// System.Single PlayerController::sprintSpeed
	float ___sprintSpeed_8;
	// System.Single PlayerController::forwardSpeed
	float ___forwardSpeed_9;
	// System.Single PlayerController::sideSpeed
	float ___sideSpeed_10;
	// System.Single PlayerController::horizontalSensitivity
	float ___horizontalSensitivity_11;
	// System.Single PlayerController::verticalSensitivity
	float ___verticalSensitivity_12;
	// System.Single PlayerController::lookLimit
	float ___lookLimit_13;
	// System.Single PlayerController::pitch
	float ___pitch_14;
	// System.Single PlayerController::verticalVelocity
	float ___verticalVelocity_15;

public:
	inline static int32_t get_offset_of_grubby_2() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___grubby_2)); }
	inline Player_t1147783557 * get_grubby_2() const { return ___grubby_2; }
	inline Player_t1147783557 ** get_address_of_grubby_2() { return &___grubby_2; }
	inline void set_grubby_2(Player_t1147783557 * value)
	{
		___grubby_2 = value;
		Il2CppCodeGenWriteBarrier((&___grubby_2), value);
	}

	inline static int32_t get_offset_of_character_3() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___character_3)); }
	inline CharacterController_t4094781467 * get_character_3() const { return ___character_3; }
	inline CharacterController_t4094781467 ** get_address_of_character_3() { return &___character_3; }
	inline void set_character_3(CharacterController_t4094781467 * value)
	{
		___character_3 = value;
		Il2CppCodeGenWriteBarrier((&___character_3), value);
	}

	inline static int32_t get_offset_of_bombPrefab_4() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___bombPrefab_4)); }
	inline GameObject_t1756533147 * get_bombPrefab_4() const { return ___bombPrefab_4; }
	inline GameObject_t1756533147 ** get_address_of_bombPrefab_4() { return &___bombPrefab_4; }
	inline void set_bombPrefab_4(GameObject_t1756533147 * value)
	{
		___bombPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___bombPrefab_4), value);
	}

	inline static int32_t get_offset_of_bulletPrefab_5() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___bulletPrefab_5)); }
	inline GameObject_t1756533147 * get_bulletPrefab_5() const { return ___bulletPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_bulletPrefab_5() { return &___bulletPrefab_5; }
	inline void set_bulletPrefab_5(GameObject_t1756533147 * value)
	{
		___bulletPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_5), value);
	}

	inline static int32_t get_offset_of_nozzle_6() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___nozzle_6)); }
	inline GameObject_t1756533147 * get_nozzle_6() const { return ___nozzle_6; }
	inline GameObject_t1756533147 ** get_address_of_nozzle_6() { return &___nozzle_6; }
	inline void set_nozzle_6(GameObject_t1756533147 * value)
	{
		___nozzle_6 = value;
		Il2CppCodeGenWriteBarrier((&___nozzle_6), value);
	}

	inline static int32_t get_offset_of_walkSpeed_7() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___walkSpeed_7)); }
	inline float get_walkSpeed_7() const { return ___walkSpeed_7; }
	inline float* get_address_of_walkSpeed_7() { return &___walkSpeed_7; }
	inline void set_walkSpeed_7(float value)
	{
		___walkSpeed_7 = value;
	}

	inline static int32_t get_offset_of_sprintSpeed_8() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___sprintSpeed_8)); }
	inline float get_sprintSpeed_8() const { return ___sprintSpeed_8; }
	inline float* get_address_of_sprintSpeed_8() { return &___sprintSpeed_8; }
	inline void set_sprintSpeed_8(float value)
	{
		___sprintSpeed_8 = value;
	}

	inline static int32_t get_offset_of_forwardSpeed_9() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___forwardSpeed_9)); }
	inline float get_forwardSpeed_9() const { return ___forwardSpeed_9; }
	inline float* get_address_of_forwardSpeed_9() { return &___forwardSpeed_9; }
	inline void set_forwardSpeed_9(float value)
	{
		___forwardSpeed_9 = value;
	}

	inline static int32_t get_offset_of_sideSpeed_10() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___sideSpeed_10)); }
	inline float get_sideSpeed_10() const { return ___sideSpeed_10; }
	inline float* get_address_of_sideSpeed_10() { return &___sideSpeed_10; }
	inline void set_sideSpeed_10(float value)
	{
		___sideSpeed_10 = value;
	}

	inline static int32_t get_offset_of_horizontalSensitivity_11() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___horizontalSensitivity_11)); }
	inline float get_horizontalSensitivity_11() const { return ___horizontalSensitivity_11; }
	inline float* get_address_of_horizontalSensitivity_11() { return &___horizontalSensitivity_11; }
	inline void set_horizontalSensitivity_11(float value)
	{
		___horizontalSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_verticalSensitivity_12() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___verticalSensitivity_12)); }
	inline float get_verticalSensitivity_12() const { return ___verticalSensitivity_12; }
	inline float* get_address_of_verticalSensitivity_12() { return &___verticalSensitivity_12; }
	inline void set_verticalSensitivity_12(float value)
	{
		___verticalSensitivity_12 = value;
	}

	inline static int32_t get_offset_of_lookLimit_13() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___lookLimit_13)); }
	inline float get_lookLimit_13() const { return ___lookLimit_13; }
	inline float* get_address_of_lookLimit_13() { return &___lookLimit_13; }
	inline void set_lookLimit_13(float value)
	{
		___lookLimit_13 = value;
	}

	inline static int32_t get_offset_of_pitch_14() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___pitch_14)); }
	inline float get_pitch_14() const { return ___pitch_14; }
	inline float* get_address_of_pitch_14() { return &___pitch_14; }
	inline void set_pitch_14(float value)
	{
		___pitch_14 = value;
	}

	inline static int32_t get_offset_of_verticalVelocity_15() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___verticalVelocity_15)); }
	inline float get_verticalVelocity_15() const { return ___verticalVelocity_15; }
	inline float* get_address_of_verticalVelocity_15() { return &___verticalVelocity_15; }
	inline void set_verticalVelocity_15(float value)
	{
		___verticalVelocity_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T4148409433_H
#ifndef GAMEMANAGER_T2252321495_H
#define GAMEMANAGER_T2252321495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public MonoBehaviour_t1158329972
{
public:
	// Game GameManager::game
	Game_t1600141214 * ___game_2;
	// UIController GameManager::UI
	UIController_t2029583246 * ___UI_3;
	// UnityEngine.GameObject[] GameManager::spawnLocations
	GameObjectU5BU5D_t3057952154* ___spawnLocations_4;
	// UnityEngine.GameObject[] GameManager::shipPartSpawnLocations
	GameObjectU5BU5D_t3057952154* ___shipPartSpawnLocations_5;
	// UnityEngine.GameObject GameManager::player
	GameObject_t1756533147 * ___player_6;
	// UnityEngine.GameObject GameManager::ship
	GameObject_t1756533147 * ___ship_7;
	// UnityEngine.GameObject GameManager::enemyPrefab
	GameObject_t1756533147 * ___enemyPrefab_8;
	// UnityEngine.GameObject GameManager::shipPartPrefab
	GameObject_t1756533147 * ___shipPartPrefab_9;

public:
	inline static int32_t get_offset_of_game_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___game_2)); }
	inline Game_t1600141214 * get_game_2() const { return ___game_2; }
	inline Game_t1600141214 ** get_address_of_game_2() { return &___game_2; }
	inline void set_game_2(Game_t1600141214 * value)
	{
		___game_2 = value;
		Il2CppCodeGenWriteBarrier((&___game_2), value);
	}

	inline static int32_t get_offset_of_UI_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___UI_3)); }
	inline UIController_t2029583246 * get_UI_3() const { return ___UI_3; }
	inline UIController_t2029583246 ** get_address_of_UI_3() { return &___UI_3; }
	inline void set_UI_3(UIController_t2029583246 * value)
	{
		___UI_3 = value;
		Il2CppCodeGenWriteBarrier((&___UI_3), value);
	}

	inline static int32_t get_offset_of_spawnLocations_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___spawnLocations_4)); }
	inline GameObjectU5BU5D_t3057952154* get_spawnLocations_4() const { return ___spawnLocations_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_spawnLocations_4() { return &___spawnLocations_4; }
	inline void set_spawnLocations_4(GameObjectU5BU5D_t3057952154* value)
	{
		___spawnLocations_4 = value;
		Il2CppCodeGenWriteBarrier((&___spawnLocations_4), value);
	}

	inline static int32_t get_offset_of_shipPartSpawnLocations_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___shipPartSpawnLocations_5)); }
	inline GameObjectU5BU5D_t3057952154* get_shipPartSpawnLocations_5() const { return ___shipPartSpawnLocations_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_shipPartSpawnLocations_5() { return &___shipPartSpawnLocations_5; }
	inline void set_shipPartSpawnLocations_5(GameObjectU5BU5D_t3057952154* value)
	{
		___shipPartSpawnLocations_5 = value;
		Il2CppCodeGenWriteBarrier((&___shipPartSpawnLocations_5), value);
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___player_6)); }
	inline GameObject_t1756533147 * get_player_6() const { return ___player_6; }
	inline GameObject_t1756533147 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(GameObject_t1756533147 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}

	inline static int32_t get_offset_of_ship_7() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___ship_7)); }
	inline GameObject_t1756533147 * get_ship_7() const { return ___ship_7; }
	inline GameObject_t1756533147 ** get_address_of_ship_7() { return &___ship_7; }
	inline void set_ship_7(GameObject_t1756533147 * value)
	{
		___ship_7 = value;
		Il2CppCodeGenWriteBarrier((&___ship_7), value);
	}

	inline static int32_t get_offset_of_enemyPrefab_8() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___enemyPrefab_8)); }
	inline GameObject_t1756533147 * get_enemyPrefab_8() const { return ___enemyPrefab_8; }
	inline GameObject_t1756533147 ** get_address_of_enemyPrefab_8() { return &___enemyPrefab_8; }
	inline void set_enemyPrefab_8(GameObject_t1756533147 * value)
	{
		___enemyPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___enemyPrefab_8), value);
	}

	inline static int32_t get_offset_of_shipPartPrefab_9() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___shipPartPrefab_9)); }
	inline GameObject_t1756533147 * get_shipPartPrefab_9() const { return ___shipPartPrefab_9; }
	inline GameObject_t1756533147 ** get_address_of_shipPartPrefab_9() { return &___shipPartPrefab_9; }
	inline void set_shipPartPrefab_9(GameObject_t1756533147 * value)
	{
		___shipPartPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___shipPartPrefab_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T2252321495_H
#ifndef ENEMYCONTROLLER_T2146768720_H
#define ENEMYCONTROLLER_T2146768720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyController
struct  EnemyController_t2146768720  : public MonoBehaviour_t1158329972
{
public:
	// Enemy EnemyController::tasteless
	Enemy_t1088811588 * ___tasteless_2;
	// UnityEngine.GameObject EnemyController::player
	GameObject_t1756533147 * ___player_3;
	// UnityEngine.GameObject EnemyController::ship
	GameObject_t1756533147 * ___ship_4;
	// UnityEngine.AI.NavMeshAgent EnemyController::nav
	NavMeshAgent_t2761625415 * ___nav_5;
	// UnityEngine.Animator EnemyController::anim
	Animator_t69676727 * ___anim_6;
	// UnityEngine.Transform EnemyController::destination
	Transform_t3275118058 * ___destination_7;

public:
	inline static int32_t get_offset_of_tasteless_2() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___tasteless_2)); }
	inline Enemy_t1088811588 * get_tasteless_2() const { return ___tasteless_2; }
	inline Enemy_t1088811588 ** get_address_of_tasteless_2() { return &___tasteless_2; }
	inline void set_tasteless_2(Enemy_t1088811588 * value)
	{
		___tasteless_2 = value;
		Il2CppCodeGenWriteBarrier((&___tasteless_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___player_3)); }
	inline GameObject_t1756533147 * get_player_3() const { return ___player_3; }
	inline GameObject_t1756533147 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1756533147 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}

	inline static int32_t get_offset_of_ship_4() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___ship_4)); }
	inline GameObject_t1756533147 * get_ship_4() const { return ___ship_4; }
	inline GameObject_t1756533147 ** get_address_of_ship_4() { return &___ship_4; }
	inline void set_ship_4(GameObject_t1756533147 * value)
	{
		___ship_4 = value;
		Il2CppCodeGenWriteBarrier((&___ship_4), value);
	}

	inline static int32_t get_offset_of_nav_5() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___nav_5)); }
	inline NavMeshAgent_t2761625415 * get_nav_5() const { return ___nav_5; }
	inline NavMeshAgent_t2761625415 ** get_address_of_nav_5() { return &___nav_5; }
	inline void set_nav_5(NavMeshAgent_t2761625415 * value)
	{
		___nav_5 = value;
		Il2CppCodeGenWriteBarrier((&___nav_5), value);
	}

	inline static int32_t get_offset_of_anim_6() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___anim_6)); }
	inline Animator_t69676727 * get_anim_6() const { return ___anim_6; }
	inline Animator_t69676727 ** get_address_of_anim_6() { return &___anim_6; }
	inline void set_anim_6(Animator_t69676727 * value)
	{
		___anim_6 = value;
		Il2CppCodeGenWriteBarrier((&___anim_6), value);
	}

	inline static int32_t get_offset_of_destination_7() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___destination_7)); }
	inline Transform_t3275118058 * get_destination_7() const { return ___destination_7; }
	inline Transform_t3275118058 ** get_address_of_destination_7() { return &___destination_7; }
	inline void set_destination_7(Transform_t3275118058 * value)
	{
		___destination_7 = value;
		Il2CppCodeGenWriteBarrier((&___destination_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYCONTROLLER_T2146768720_H
#ifndef DEATHBARRIER_T1380906595_H
#define DEATHBARRIER_T1380906595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeathBarrier
struct  DeathBarrier_t1380906595  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEATHBARRIER_T1380906595_H
#ifndef COLLECTABLE_T2201227982_H
#define COLLECTABLE_T2201227982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Collectable
struct  Collectable_t2201227982  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Collectable::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Collectable_t2201227982, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTABLE_T2201227982_H
#ifndef BULLET_T2590115616_H
#define BULLET_T2590115616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bullet
struct  Bullet_t2590115616  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Bullet::splashEffect
	GameObject_t1756533147 * ___splashEffect_2;
	// UnityEngine.GameObject Bullet::player
	GameObject_t1756533147 * ___player_3;

public:
	inline static int32_t get_offset_of_splashEffect_2() { return static_cast<int32_t>(offsetof(Bullet_t2590115616, ___splashEffect_2)); }
	inline GameObject_t1756533147 * get_splashEffect_2() const { return ___splashEffect_2; }
	inline GameObject_t1756533147 ** get_address_of_splashEffect_2() { return &___splashEffect_2; }
	inline void set_splashEffect_2(GameObject_t1756533147 * value)
	{
		___splashEffect_2 = value;
		Il2CppCodeGenWriteBarrier((&___splashEffect_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(Bullet_t2590115616, ___player_3)); }
	inline GameObject_t1756533147 * get_player_3() const { return ___player_3; }
	inline GameObject_t1756533147 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1756533147 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLET_T2590115616_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef DISPLACE_T357053105_H
#define DISPLACE_T357053105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Displace
struct  Displace_t357053105  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLACE_T357053105_H
#ifndef PLANARREFLECTION_T2931543887_H
#define PLANARREFLECTION_T2931543887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.PlanarReflection
struct  PlanarReflection_t2931543887  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.LayerMask UnityStandardAssets.Water.PlanarReflection::reflectionMask
	LayerMask_t3188175821  ___reflectionMask_2;
	// System.Boolean UnityStandardAssets.Water.PlanarReflection::reflectSkybox
	bool ___reflectSkybox_3;
	// UnityEngine.Color UnityStandardAssets.Water.PlanarReflection::clearColor
	Color_t2020392075  ___clearColor_4;
	// System.String UnityStandardAssets.Water.PlanarReflection::reflectionSampler
	String_t* ___reflectionSampler_5;
	// System.Single UnityStandardAssets.Water.PlanarReflection::clipPlaneOffset
	float ___clipPlaneOffset_6;
	// UnityEngine.Vector3 UnityStandardAssets.Water.PlanarReflection::m_Oldpos
	Vector3_t2243707580  ___m_Oldpos_7;
	// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::m_ReflectionCamera
	Camera_t189460977 * ___m_ReflectionCamera_8;
	// UnityEngine.Material UnityStandardAssets.Water.PlanarReflection::m_SharedMaterial
	Material_t193706927 * ___m_SharedMaterial_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean> UnityStandardAssets.Water.PlanarReflection::m_HelperCameras
	Dictionary_2_t2882765492 * ___m_HelperCameras_10;

public:
	inline static int32_t get_offset_of_reflectionMask_2() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___reflectionMask_2)); }
	inline LayerMask_t3188175821  get_reflectionMask_2() const { return ___reflectionMask_2; }
	inline LayerMask_t3188175821 * get_address_of_reflectionMask_2() { return &___reflectionMask_2; }
	inline void set_reflectionMask_2(LayerMask_t3188175821  value)
	{
		___reflectionMask_2 = value;
	}

	inline static int32_t get_offset_of_reflectSkybox_3() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___reflectSkybox_3)); }
	inline bool get_reflectSkybox_3() const { return ___reflectSkybox_3; }
	inline bool* get_address_of_reflectSkybox_3() { return &___reflectSkybox_3; }
	inline void set_reflectSkybox_3(bool value)
	{
		___reflectSkybox_3 = value;
	}

	inline static int32_t get_offset_of_clearColor_4() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___clearColor_4)); }
	inline Color_t2020392075  get_clearColor_4() const { return ___clearColor_4; }
	inline Color_t2020392075 * get_address_of_clearColor_4() { return &___clearColor_4; }
	inline void set_clearColor_4(Color_t2020392075  value)
	{
		___clearColor_4 = value;
	}

	inline static int32_t get_offset_of_reflectionSampler_5() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___reflectionSampler_5)); }
	inline String_t* get_reflectionSampler_5() const { return ___reflectionSampler_5; }
	inline String_t** get_address_of_reflectionSampler_5() { return &___reflectionSampler_5; }
	inline void set_reflectionSampler_5(String_t* value)
	{
		___reflectionSampler_5 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionSampler_5), value);
	}

	inline static int32_t get_offset_of_clipPlaneOffset_6() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___clipPlaneOffset_6)); }
	inline float get_clipPlaneOffset_6() const { return ___clipPlaneOffset_6; }
	inline float* get_address_of_clipPlaneOffset_6() { return &___clipPlaneOffset_6; }
	inline void set_clipPlaneOffset_6(float value)
	{
		___clipPlaneOffset_6 = value;
	}

	inline static int32_t get_offset_of_m_Oldpos_7() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___m_Oldpos_7)); }
	inline Vector3_t2243707580  get_m_Oldpos_7() const { return ___m_Oldpos_7; }
	inline Vector3_t2243707580 * get_address_of_m_Oldpos_7() { return &___m_Oldpos_7; }
	inline void set_m_Oldpos_7(Vector3_t2243707580  value)
	{
		___m_Oldpos_7 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCamera_8() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___m_ReflectionCamera_8)); }
	inline Camera_t189460977 * get_m_ReflectionCamera_8() const { return ___m_ReflectionCamera_8; }
	inline Camera_t189460977 ** get_address_of_m_ReflectionCamera_8() { return &___m_ReflectionCamera_8; }
	inline void set_m_ReflectionCamera_8(Camera_t189460977 * value)
	{
		___m_ReflectionCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCamera_8), value);
	}

	inline static int32_t get_offset_of_m_SharedMaterial_9() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___m_SharedMaterial_9)); }
	inline Material_t193706927 * get_m_SharedMaterial_9() const { return ___m_SharedMaterial_9; }
	inline Material_t193706927 ** get_address_of_m_SharedMaterial_9() { return &___m_SharedMaterial_9; }
	inline void set_m_SharedMaterial_9(Material_t193706927 * value)
	{
		___m_SharedMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_SharedMaterial_9), value);
	}

	inline static int32_t get_offset_of_m_HelperCameras_10() { return static_cast<int32_t>(offsetof(PlanarReflection_t2931543887, ___m_HelperCameras_10)); }
	inline Dictionary_2_t2882765492 * get_m_HelperCameras_10() const { return ___m_HelperCameras_10; }
	inline Dictionary_2_t2882765492 ** get_address_of_m_HelperCameras_10() { return &___m_HelperCameras_10; }
	inline void set_m_HelperCameras_10(Dictionary_2_t2882765492 * value)
	{
		___m_HelperCameras_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_HelperCameras_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANARREFLECTION_T2931543887_H
#ifndef SPECULARLIGHTING_T2824122729_H
#define SPECULARLIGHTING_T2824122729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.SpecularLighting
struct  SpecularLighting_t2824122729  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform UnityStandardAssets.Water.SpecularLighting::specularLight
	Transform_t3275118058 * ___specularLight_2;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.SpecularLighting::m_WaterBase
	WaterBase_t4291487940 * ___m_WaterBase_3;

public:
	inline static int32_t get_offset_of_specularLight_2() { return static_cast<int32_t>(offsetof(SpecularLighting_t2824122729, ___specularLight_2)); }
	inline Transform_t3275118058 * get_specularLight_2() const { return ___specularLight_2; }
	inline Transform_t3275118058 ** get_address_of_specularLight_2() { return &___specularLight_2; }
	inline void set_specularLight_2(Transform_t3275118058 * value)
	{
		___specularLight_2 = value;
		Il2CppCodeGenWriteBarrier((&___specularLight_2), value);
	}

	inline static int32_t get_offset_of_m_WaterBase_3() { return static_cast<int32_t>(offsetof(SpecularLighting_t2824122729, ___m_WaterBase_3)); }
	inline WaterBase_t4291487940 * get_m_WaterBase_3() const { return ___m_WaterBase_3; }
	inline WaterBase_t4291487940 ** get_address_of_m_WaterBase_3() { return &___m_WaterBase_3; }
	inline void set_m_WaterBase_3(WaterBase_t4291487940 * value)
	{
		___m_WaterBase_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaterBase_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECULARLIGHTING_T2824122729_H
#ifndef WATER_T1562849653_H
#define WATER_T1562849653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.Water
struct  Water_t1562849653  : public MonoBehaviour_t1158329972
{
public:
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::waterMode
	int32_t ___waterMode_2;
	// System.Boolean UnityStandardAssets.Water.Water::disablePixelLights
	bool ___disablePixelLights_3;
	// System.Int32 UnityStandardAssets.Water.Water::textureSize
	int32_t ___textureSize_4;
	// System.Single UnityStandardAssets.Water.Water::clipPlaneOffset
	float ___clipPlaneOffset_5;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::reflectLayers
	LayerMask_t3188175821  ___reflectLayers_6;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::refractLayers
	LayerMask_t3188175821  ___refractLayers_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_ReflectionCameras
	Dictionary_2_t3541619047 * ___m_ReflectionCameras_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_RefractionCameras
	Dictionary_2_t3541619047 * ___m_RefractionCameras_9;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_ReflectionTexture
	RenderTexture_t2666733923 * ___m_ReflectionTexture_10;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_RefractionTexture
	RenderTexture_t2666733923 * ___m_RefractionTexture_11;
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::m_HardwareWaterSupport
	int32_t ___m_HardwareWaterSupport_12;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldReflectionTextureSize
	int32_t ___m_OldReflectionTextureSize_13;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldRefractionTextureSize
	int32_t ___m_OldRefractionTextureSize_14;

public:
	inline static int32_t get_offset_of_waterMode_2() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___waterMode_2)); }
	inline int32_t get_waterMode_2() const { return ___waterMode_2; }
	inline int32_t* get_address_of_waterMode_2() { return &___waterMode_2; }
	inline void set_waterMode_2(int32_t value)
	{
		___waterMode_2 = value;
	}

	inline static int32_t get_offset_of_disablePixelLights_3() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___disablePixelLights_3)); }
	inline bool get_disablePixelLights_3() const { return ___disablePixelLights_3; }
	inline bool* get_address_of_disablePixelLights_3() { return &___disablePixelLights_3; }
	inline void set_disablePixelLights_3(bool value)
	{
		___disablePixelLights_3 = value;
	}

	inline static int32_t get_offset_of_textureSize_4() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___textureSize_4)); }
	inline int32_t get_textureSize_4() const { return ___textureSize_4; }
	inline int32_t* get_address_of_textureSize_4() { return &___textureSize_4; }
	inline void set_textureSize_4(int32_t value)
	{
		___textureSize_4 = value;
	}

	inline static int32_t get_offset_of_clipPlaneOffset_5() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___clipPlaneOffset_5)); }
	inline float get_clipPlaneOffset_5() const { return ___clipPlaneOffset_5; }
	inline float* get_address_of_clipPlaneOffset_5() { return &___clipPlaneOffset_5; }
	inline void set_clipPlaneOffset_5(float value)
	{
		___clipPlaneOffset_5 = value;
	}

	inline static int32_t get_offset_of_reflectLayers_6() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___reflectLayers_6)); }
	inline LayerMask_t3188175821  get_reflectLayers_6() const { return ___reflectLayers_6; }
	inline LayerMask_t3188175821 * get_address_of_reflectLayers_6() { return &___reflectLayers_6; }
	inline void set_reflectLayers_6(LayerMask_t3188175821  value)
	{
		___reflectLayers_6 = value;
	}

	inline static int32_t get_offset_of_refractLayers_7() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___refractLayers_7)); }
	inline LayerMask_t3188175821  get_refractLayers_7() const { return ___refractLayers_7; }
	inline LayerMask_t3188175821 * get_address_of_refractLayers_7() { return &___refractLayers_7; }
	inline void set_refractLayers_7(LayerMask_t3188175821  value)
	{
		___refractLayers_7 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCameras_8() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_ReflectionCameras_8)); }
	inline Dictionary_2_t3541619047 * get_m_ReflectionCameras_8() const { return ___m_ReflectionCameras_8; }
	inline Dictionary_2_t3541619047 ** get_address_of_m_ReflectionCameras_8() { return &___m_ReflectionCameras_8; }
	inline void set_m_ReflectionCameras_8(Dictionary_2_t3541619047 * value)
	{
		___m_ReflectionCameras_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCameras_8), value);
	}

	inline static int32_t get_offset_of_m_RefractionCameras_9() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_RefractionCameras_9)); }
	inline Dictionary_2_t3541619047 * get_m_RefractionCameras_9() const { return ___m_RefractionCameras_9; }
	inline Dictionary_2_t3541619047 ** get_address_of_m_RefractionCameras_9() { return &___m_RefractionCameras_9; }
	inline void set_m_RefractionCameras_9(Dictionary_2_t3541619047 * value)
	{
		___m_RefractionCameras_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionCameras_9), value);
	}

	inline static int32_t get_offset_of_m_ReflectionTexture_10() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_ReflectionTexture_10)); }
	inline RenderTexture_t2666733923 * get_m_ReflectionTexture_10() const { return ___m_ReflectionTexture_10; }
	inline RenderTexture_t2666733923 ** get_address_of_m_ReflectionTexture_10() { return &___m_ReflectionTexture_10; }
	inline void set_m_ReflectionTexture_10(RenderTexture_t2666733923 * value)
	{
		___m_ReflectionTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionTexture_10), value);
	}

	inline static int32_t get_offset_of_m_RefractionTexture_11() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_RefractionTexture_11)); }
	inline RenderTexture_t2666733923 * get_m_RefractionTexture_11() const { return ___m_RefractionTexture_11; }
	inline RenderTexture_t2666733923 ** get_address_of_m_RefractionTexture_11() { return &___m_RefractionTexture_11; }
	inline void set_m_RefractionTexture_11(RenderTexture_t2666733923 * value)
	{
		___m_RefractionTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionTexture_11), value);
	}

	inline static int32_t get_offset_of_m_HardwareWaterSupport_12() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_HardwareWaterSupport_12)); }
	inline int32_t get_m_HardwareWaterSupport_12() const { return ___m_HardwareWaterSupport_12; }
	inline int32_t* get_address_of_m_HardwareWaterSupport_12() { return &___m_HardwareWaterSupport_12; }
	inline void set_m_HardwareWaterSupport_12(int32_t value)
	{
		___m_HardwareWaterSupport_12 = value;
	}

	inline static int32_t get_offset_of_m_OldReflectionTextureSize_13() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_OldReflectionTextureSize_13)); }
	inline int32_t get_m_OldReflectionTextureSize_13() const { return ___m_OldReflectionTextureSize_13; }
	inline int32_t* get_address_of_m_OldReflectionTextureSize_13() { return &___m_OldReflectionTextureSize_13; }
	inline void set_m_OldReflectionTextureSize_13(int32_t value)
	{
		___m_OldReflectionTextureSize_13 = value;
	}

	inline static int32_t get_offset_of_m_OldRefractionTextureSize_14() { return static_cast<int32_t>(offsetof(Water_t1562849653, ___m_OldRefractionTextureSize_14)); }
	inline int32_t get_m_OldRefractionTextureSize_14() const { return ___m_OldRefractionTextureSize_14; }
	inline int32_t* get_address_of_m_OldRefractionTextureSize_14() { return &___m_OldRefractionTextureSize_14; }
	inline void set_m_OldRefractionTextureSize_14(int32_t value)
	{
		___m_OldRefractionTextureSize_14 = value;
	}
};

struct Water_t1562849653_StaticFields
{
public:
	// System.Boolean UnityStandardAssets.Water.Water::s_InsideWater
	bool ___s_InsideWater_15;

public:
	inline static int32_t get_offset_of_s_InsideWater_15() { return static_cast<int32_t>(offsetof(Water_t1562849653_StaticFields, ___s_InsideWater_15)); }
	inline bool get_s_InsideWater_15() const { return ___s_InsideWater_15; }
	inline bool* get_address_of_s_InsideWater_15() { return &___s_InsideWater_15; }
	inline void set_s_InsideWater_15(bool value)
	{
		___s_InsideWater_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATER_T1562849653_H
#ifndef SHIPUI_T242265720_H
#define SHIPUI_T242265720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipUI
struct  ShipUI_t242265720  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera ShipUI::cameraToLookAt
	Camera_t189460977 * ___cameraToLookAt_2;
	// UnityEngine.GameObject ShipUI::bar
	GameObject_t1756533147 * ___bar_3;
	// UnityEngine.Vector3 ShipUI::barVector
	Vector3_t2243707580  ___barVector_4;

public:
	inline static int32_t get_offset_of_cameraToLookAt_2() { return static_cast<int32_t>(offsetof(ShipUI_t242265720, ___cameraToLookAt_2)); }
	inline Camera_t189460977 * get_cameraToLookAt_2() const { return ___cameraToLookAt_2; }
	inline Camera_t189460977 ** get_address_of_cameraToLookAt_2() { return &___cameraToLookAt_2; }
	inline void set_cameraToLookAt_2(Camera_t189460977 * value)
	{
		___cameraToLookAt_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraToLookAt_2), value);
	}

	inline static int32_t get_offset_of_bar_3() { return static_cast<int32_t>(offsetof(ShipUI_t242265720, ___bar_3)); }
	inline GameObject_t1756533147 * get_bar_3() const { return ___bar_3; }
	inline GameObject_t1756533147 ** get_address_of_bar_3() { return &___bar_3; }
	inline void set_bar_3(GameObject_t1756533147 * value)
	{
		___bar_3 = value;
		Il2CppCodeGenWriteBarrier((&___bar_3), value);
	}

	inline static int32_t get_offset_of_barVector_4() { return static_cast<int32_t>(offsetof(ShipUI_t242265720, ___barVector_4)); }
	inline Vector3_t2243707580  get_barVector_4() const { return ___barVector_4; }
	inline Vector3_t2243707580 * get_address_of_barVector_4() { return &___barVector_4; }
	inline void set_barVector_4(Vector3_t2243707580  value)
	{
		___barVector_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPUI_T242265720_H
#ifndef WATERBASE_T4291487940_H
#define WATERBASE_T4291487940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterBase
struct  WaterBase_t4291487940  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material UnityStandardAssets.Water.WaterBase::sharedMaterial
	Material_t193706927 * ___sharedMaterial_2;
	// UnityStandardAssets.Water.WaterQuality UnityStandardAssets.Water.WaterBase::waterQuality
	int32_t ___waterQuality_3;
	// System.Boolean UnityStandardAssets.Water.WaterBase::edgeBlend
	bool ___edgeBlend_4;

public:
	inline static int32_t get_offset_of_sharedMaterial_2() { return static_cast<int32_t>(offsetof(WaterBase_t4291487940, ___sharedMaterial_2)); }
	inline Material_t193706927 * get_sharedMaterial_2() const { return ___sharedMaterial_2; }
	inline Material_t193706927 ** get_address_of_sharedMaterial_2() { return &___sharedMaterial_2; }
	inline void set_sharedMaterial_2(Material_t193706927 * value)
	{
		___sharedMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterial_2), value);
	}

	inline static int32_t get_offset_of_waterQuality_3() { return static_cast<int32_t>(offsetof(WaterBase_t4291487940, ___waterQuality_3)); }
	inline int32_t get_waterQuality_3() const { return ___waterQuality_3; }
	inline int32_t* get_address_of_waterQuality_3() { return &___waterQuality_3; }
	inline void set_waterQuality_3(int32_t value)
	{
		___waterQuality_3 = value;
	}

	inline static int32_t get_offset_of_edgeBlend_4() { return static_cast<int32_t>(offsetof(WaterBase_t4291487940, ___edgeBlend_4)); }
	inline bool get_edgeBlend_4() const { return ___edgeBlend_4; }
	inline bool* get_address_of_edgeBlend_4() { return &___edgeBlend_4; }
	inline void set_edgeBlend_4(bool value)
	{
		___edgeBlend_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERBASE_T4291487940_H
#ifndef WATERTILE_T1797616825_H
#define WATERTILE_T1797616825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.WaterTile
struct  WaterTile_t1797616825  : public MonoBehaviour_t1158329972
{
public:
	// UnityStandardAssets.Water.PlanarReflection UnityStandardAssets.Water.WaterTile::reflection
	PlanarReflection_t2931543887 * ___reflection_2;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.WaterTile::waterBase
	WaterBase_t4291487940 * ___waterBase_3;

public:
	inline static int32_t get_offset_of_reflection_2() { return static_cast<int32_t>(offsetof(WaterTile_t1797616825, ___reflection_2)); }
	inline PlanarReflection_t2931543887 * get_reflection_2() const { return ___reflection_2; }
	inline PlanarReflection_t2931543887 ** get_address_of_reflection_2() { return &___reflection_2; }
	inline void set_reflection_2(PlanarReflection_t2931543887 * value)
	{
		___reflection_2 = value;
		Il2CppCodeGenWriteBarrier((&___reflection_2), value);
	}

	inline static int32_t get_offset_of_waterBase_3() { return static_cast<int32_t>(offsetof(WaterTile_t1797616825, ___waterBase_3)); }
	inline WaterBase_t4291487940 * get_waterBase_3() const { return ___waterBase_3; }
	inline WaterBase_t4291487940 ** get_address_of_waterBase_3() { return &___waterBase_3; }
	inline void set_waterBase_3(WaterBase_t4291487940 * value)
	{
		___waterBase_3 = value;
		Il2CppCodeGenWriteBarrier((&___waterBase_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERTILE_T1797616825_H
#ifndef LAYOUTGROUP_T3962498969_H
#define LAYOUTGROUP_T3962498969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t3962498969  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t3387826427 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2243707579  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2243707579  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2243707579  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t2719087314 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Padding_2)); }
	inline RectOffset_t3387826427 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t3387826427 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t3387826427 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalMinSize_6)); }
	inline Vector2_t2243707579  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2243707579 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2243707579  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2243707579  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2243707579 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2243707579  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2243707579  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2243707579 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2243707579  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_RectChildren_9)); }
	inline List_1_t2719087314 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t2719087314 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t2719087314 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T3962498969_H
#ifndef CANVASSCALER_T2574720772_H
#define CANVASSCALER_T2574720772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler
struct  CanvasScaler_t2574720772  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::m_UiScaleMode
	int32_t ___m_UiScaleMode_2;
	// System.Single UnityEngine.UI.CanvasScaler::m_ReferencePixelsPerUnit
	float ___m_ReferencePixelsPerUnit_3;
	// System.Single UnityEngine.UI.CanvasScaler::m_ScaleFactor
	float ___m_ScaleFactor_4;
	// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::m_ReferenceResolution
	Vector2_t2243707579  ___m_ReferenceResolution_5;
	// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::m_ScreenMatchMode
	int32_t ___m_ScreenMatchMode_6;
	// System.Single UnityEngine.UI.CanvasScaler::m_MatchWidthOrHeight
	float ___m_MatchWidthOrHeight_7;
	// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::m_PhysicalUnit
	int32_t ___m_PhysicalUnit_9;
	// System.Single UnityEngine.UI.CanvasScaler::m_FallbackScreenDPI
	float ___m_FallbackScreenDPI_10;
	// System.Single UnityEngine.UI.CanvasScaler::m_DefaultSpriteDPI
	float ___m_DefaultSpriteDPI_11;
	// System.Single UnityEngine.UI.CanvasScaler::m_DynamicPixelsPerUnit
	float ___m_DynamicPixelsPerUnit_12;
	// UnityEngine.Canvas UnityEngine.UI.CanvasScaler::m_Canvas
	Canvas_t209405766 * ___m_Canvas_13;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevScaleFactor
	float ___m_PrevScaleFactor_14;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevReferencePixelsPerUnit
	float ___m_PrevReferencePixelsPerUnit_15;

public:
	inline static int32_t get_offset_of_m_UiScaleMode_2() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_UiScaleMode_2)); }
	inline int32_t get_m_UiScaleMode_2() const { return ___m_UiScaleMode_2; }
	inline int32_t* get_address_of_m_UiScaleMode_2() { return &___m_UiScaleMode_2; }
	inline void set_m_UiScaleMode_2(int32_t value)
	{
		___m_UiScaleMode_2 = value;
	}

	inline static int32_t get_offset_of_m_ReferencePixelsPerUnit_3() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ReferencePixelsPerUnit_3)); }
	inline float get_m_ReferencePixelsPerUnit_3() const { return ___m_ReferencePixelsPerUnit_3; }
	inline float* get_address_of_m_ReferencePixelsPerUnit_3() { return &___m_ReferencePixelsPerUnit_3; }
	inline void set_m_ReferencePixelsPerUnit_3(float value)
	{
		___m_ReferencePixelsPerUnit_3 = value;
	}

	inline static int32_t get_offset_of_m_ScaleFactor_4() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ScaleFactor_4)); }
	inline float get_m_ScaleFactor_4() const { return ___m_ScaleFactor_4; }
	inline float* get_address_of_m_ScaleFactor_4() { return &___m_ScaleFactor_4; }
	inline void set_m_ScaleFactor_4(float value)
	{
		___m_ScaleFactor_4 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceResolution_5() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ReferenceResolution_5)); }
	inline Vector2_t2243707579  get_m_ReferenceResolution_5() const { return ___m_ReferenceResolution_5; }
	inline Vector2_t2243707579 * get_address_of_m_ReferenceResolution_5() { return &___m_ReferenceResolution_5; }
	inline void set_m_ReferenceResolution_5(Vector2_t2243707579  value)
	{
		___m_ReferenceResolution_5 = value;
	}

	inline static int32_t get_offset_of_m_ScreenMatchMode_6() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ScreenMatchMode_6)); }
	inline int32_t get_m_ScreenMatchMode_6() const { return ___m_ScreenMatchMode_6; }
	inline int32_t* get_address_of_m_ScreenMatchMode_6() { return &___m_ScreenMatchMode_6; }
	inline void set_m_ScreenMatchMode_6(int32_t value)
	{
		___m_ScreenMatchMode_6 = value;
	}

	inline static int32_t get_offset_of_m_MatchWidthOrHeight_7() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_MatchWidthOrHeight_7)); }
	inline float get_m_MatchWidthOrHeight_7() const { return ___m_MatchWidthOrHeight_7; }
	inline float* get_address_of_m_MatchWidthOrHeight_7() { return &___m_MatchWidthOrHeight_7; }
	inline void set_m_MatchWidthOrHeight_7(float value)
	{
		___m_MatchWidthOrHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_PhysicalUnit_9() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_PhysicalUnit_9)); }
	inline int32_t get_m_PhysicalUnit_9() const { return ___m_PhysicalUnit_9; }
	inline int32_t* get_address_of_m_PhysicalUnit_9() { return &___m_PhysicalUnit_9; }
	inline void set_m_PhysicalUnit_9(int32_t value)
	{
		___m_PhysicalUnit_9 = value;
	}

	inline static int32_t get_offset_of_m_FallbackScreenDPI_10() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_FallbackScreenDPI_10)); }
	inline float get_m_FallbackScreenDPI_10() const { return ___m_FallbackScreenDPI_10; }
	inline float* get_address_of_m_FallbackScreenDPI_10() { return &___m_FallbackScreenDPI_10; }
	inline void set_m_FallbackScreenDPI_10(float value)
	{
		___m_FallbackScreenDPI_10 = value;
	}

	inline static int32_t get_offset_of_m_DefaultSpriteDPI_11() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_DefaultSpriteDPI_11)); }
	inline float get_m_DefaultSpriteDPI_11() const { return ___m_DefaultSpriteDPI_11; }
	inline float* get_address_of_m_DefaultSpriteDPI_11() { return &___m_DefaultSpriteDPI_11; }
	inline void set_m_DefaultSpriteDPI_11(float value)
	{
		___m_DefaultSpriteDPI_11 = value;
	}

	inline static int32_t get_offset_of_m_DynamicPixelsPerUnit_12() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_DynamicPixelsPerUnit_12)); }
	inline float get_m_DynamicPixelsPerUnit_12() const { return ___m_DynamicPixelsPerUnit_12; }
	inline float* get_address_of_m_DynamicPixelsPerUnit_12() { return &___m_DynamicPixelsPerUnit_12; }
	inline void set_m_DynamicPixelsPerUnit_12(float value)
	{
		___m_DynamicPixelsPerUnit_12 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_Canvas_13)); }
	inline Canvas_t209405766 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_t209405766 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_13), value);
	}

	inline static int32_t get_offset_of_m_PrevScaleFactor_14() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_PrevScaleFactor_14)); }
	inline float get_m_PrevScaleFactor_14() const { return ___m_PrevScaleFactor_14; }
	inline float* get_address_of_m_PrevScaleFactor_14() { return &___m_PrevScaleFactor_14; }
	inline void set_m_PrevScaleFactor_14(float value)
	{
		___m_PrevScaleFactor_14 = value;
	}

	inline static int32_t get_offset_of_m_PrevReferencePixelsPerUnit_15() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_PrevReferencePixelsPerUnit_15)); }
	inline float get_m_PrevReferencePixelsPerUnit_15() const { return ___m_PrevReferencePixelsPerUnit_15; }
	inline float* get_address_of_m_PrevReferencePixelsPerUnit_15() { return &___m_PrevReferencePixelsPerUnit_15; }
	inline void set_m_PrevReferencePixelsPerUnit_15(float value)
	{
		___m_PrevReferencePixelsPerUnit_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSCALER_T2574720772_H
#ifndef LAYOUTELEMENT_T2808691390_H
#define LAYOUTELEMENT_T2808691390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutElement
struct  LayoutElement_t2808691390  : public UIBehaviour_t3960014691
{
public:
	// System.Boolean UnityEngine.UI.LayoutElement::m_IgnoreLayout
	bool ___m_IgnoreLayout_2;
	// System.Single UnityEngine.UI.LayoutElement::m_MinWidth
	float ___m_MinWidth_3;
	// System.Single UnityEngine.UI.LayoutElement::m_MinHeight
	float ___m_MinHeight_4;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredWidth
	float ___m_PreferredWidth_5;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredHeight
	float ___m_PreferredHeight_6;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleWidth
	float ___m_FlexibleWidth_7;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleHeight
	float ___m_FlexibleHeight_8;
	// System.Int32 UnityEngine.UI.LayoutElement::m_LayoutPriority
	int32_t ___m_LayoutPriority_9;

public:
	inline static int32_t get_offset_of_m_IgnoreLayout_2() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_IgnoreLayout_2)); }
	inline bool get_m_IgnoreLayout_2() const { return ___m_IgnoreLayout_2; }
	inline bool* get_address_of_m_IgnoreLayout_2() { return &___m_IgnoreLayout_2; }
	inline void set_m_IgnoreLayout_2(bool value)
	{
		___m_IgnoreLayout_2 = value;
	}

	inline static int32_t get_offset_of_m_MinWidth_3() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_MinWidth_3)); }
	inline float get_m_MinWidth_3() const { return ___m_MinWidth_3; }
	inline float* get_address_of_m_MinWidth_3() { return &___m_MinWidth_3; }
	inline void set_m_MinWidth_3(float value)
	{
		___m_MinWidth_3 = value;
	}

	inline static int32_t get_offset_of_m_MinHeight_4() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_MinHeight_4)); }
	inline float get_m_MinHeight_4() const { return ___m_MinHeight_4; }
	inline float* get_address_of_m_MinHeight_4() { return &___m_MinHeight_4; }
	inline void set_m_MinHeight_4(float value)
	{
		___m_MinHeight_4 = value;
	}

	inline static int32_t get_offset_of_m_PreferredWidth_5() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_PreferredWidth_5)); }
	inline float get_m_PreferredWidth_5() const { return ___m_PreferredWidth_5; }
	inline float* get_address_of_m_PreferredWidth_5() { return &___m_PreferredWidth_5; }
	inline void set_m_PreferredWidth_5(float value)
	{
		___m_PreferredWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_PreferredHeight_6() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_PreferredHeight_6)); }
	inline float get_m_PreferredHeight_6() const { return ___m_PreferredHeight_6; }
	inline float* get_address_of_m_PreferredHeight_6() { return &___m_PreferredHeight_6; }
	inline void set_m_PreferredHeight_6(float value)
	{
		___m_PreferredHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleWidth_7() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_FlexibleWidth_7)); }
	inline float get_m_FlexibleWidth_7() const { return ___m_FlexibleWidth_7; }
	inline float* get_address_of_m_FlexibleWidth_7() { return &___m_FlexibleWidth_7; }
	inline void set_m_FlexibleWidth_7(float value)
	{
		___m_FlexibleWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleHeight_8() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_FlexibleHeight_8)); }
	inline float get_m_FlexibleHeight_8() const { return ___m_FlexibleHeight_8; }
	inline float* get_address_of_m_FlexibleHeight_8() { return &___m_FlexibleHeight_8; }
	inline void set_m_FlexibleHeight_8(float value)
	{
		___m_FlexibleHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_LayoutPriority_9() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_LayoutPriority_9)); }
	inline int32_t get_m_LayoutPriority_9() const { return ___m_LayoutPriority_9; }
	inline int32_t* get_address_of_m_LayoutPriority_9() { return &___m_LayoutPriority_9; }
	inline void set_m_LayoutPriority_9(int32_t value)
	{
		___m_LayoutPriority_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENT_T2808691390_H
#ifndef GERSTNERDISPLACE_T4155590855_H
#define GERSTNERDISPLACE_T4155590855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Water.GerstnerDisplace
struct  GerstnerDisplace_t4155590855  : public Displace_t357053105
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GERSTNERDISPLACE_T4155590855_H
#ifndef CONTENTSIZEFITTER_T1325211874_H
#define CONTENTSIZEFITTER_T1325211874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter
struct  ContentSizeFitter_t1325211874  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_HorizontalFit
	int32_t ___m_HorizontalFit_2;
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_VerticalFit
	int32_t ___m_VerticalFit_3;
	// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ContentSizeFitter::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_HorizontalFit_2() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_HorizontalFit_2)); }
	inline int32_t get_m_HorizontalFit_2() const { return ___m_HorizontalFit_2; }
	inline int32_t* get_address_of_m_HorizontalFit_2() { return &___m_HorizontalFit_2; }
	inline void set_m_HorizontalFit_2(int32_t value)
	{
		___m_HorizontalFit_2 = value;
	}

	inline static int32_t get_offset_of_m_VerticalFit_3() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_VerticalFit_3)); }
	inline int32_t get_m_VerticalFit_3() const { return ___m_VerticalFit_3; }
	inline int32_t* get_address_of_m_VerticalFit_3() { return &___m_VerticalFit_3; }
	inline void set_m_VerticalFit_3(int32_t value)
	{
		___m_VerticalFit_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTSIZEFITTER_T1325211874_H
#ifndef BASEMESHEFFECT_T1728560551_H
#define BASEMESHEFFECT_T1728560551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t1728560551  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t2426225576 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t1728560551, ___m_Graphic_2)); }
	inline Graphic_t2426225576 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t2426225576 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t2426225576 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T1728560551_H
#ifndef ASPECTRATIOFITTER_T3114550109_H
#define ASPECTRATIOFITTER_T3114550109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter
struct  AspectRatioFitter_t3114550109  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::m_AspectMode
	int32_t ___m_AspectMode_2;
	// System.Single UnityEngine.UI.AspectRatioFitter::m_AspectRatio
	float ___m_AspectRatio_3;
	// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.AspectRatioFitter::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_AspectMode_2() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_AspectMode_2)); }
	inline int32_t get_m_AspectMode_2() const { return ___m_AspectMode_2; }
	inline int32_t* get_address_of_m_AspectMode_2() { return &___m_AspectMode_2; }
	inline void set_m_AspectMode_2(int32_t value)
	{
		___m_AspectMode_2 = value;
	}

	inline static int32_t get_offset_of_m_AspectRatio_3() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_AspectRatio_3)); }
	inline float get_m_AspectRatio_3() const { return ___m_AspectRatio_3; }
	inline float* get_address_of_m_AspectRatio_3() { return &___m_AspectRatio_3; }
	inline void set_m_AspectRatio_3(float value)
	{
		___m_AspectRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOFITTER_T3114550109_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t1968298610  : public LayoutGroup_t3962498969
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_10;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_11;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_14;

public:
	inline static int32_t get_offset_of_m_Spacing_10() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_Spacing_10)); }
	inline float get_m_Spacing_10() const { return ___m_Spacing_10; }
	inline float* get_address_of_m_Spacing_10() { return &___m_Spacing_10; }
	inline void set_m_Spacing_10(float value)
	{
		___m_Spacing_10 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_11() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildForceExpandWidth_11)); }
	inline bool get_m_ChildForceExpandWidth_11() const { return ___m_ChildForceExpandWidth_11; }
	inline bool* get_address_of_m_ChildForceExpandWidth_11() { return &___m_ChildForceExpandWidth_11; }
	inline void set_m_ChildForceExpandWidth_11(bool value)
	{
		___m_ChildForceExpandWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildForceExpandHeight_12)); }
	inline bool get_m_ChildForceExpandHeight_12() const { return ___m_ChildForceExpandHeight_12; }
	inline bool* get_address_of_m_ChildForceExpandHeight_12() { return &___m_ChildForceExpandHeight_12; }
	inline void set_m_ChildForceExpandHeight_12(bool value)
	{
		___m_ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildControlWidth_13)); }
	inline bool get_m_ChildControlWidth_13() const { return ___m_ChildControlWidth_13; }
	inline bool* get_address_of_m_ChildControlWidth_13() { return &___m_ChildControlWidth_13; }
	inline void set_m_ChildControlWidth_13(bool value)
	{
		___m_ChildControlWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildControlHeight_14)); }
	inline bool get_m_ChildControlHeight_14() const { return ___m_ChildControlHeight_14; }
	inline bool* get_address_of_m_ChildControlHeight_14() { return &___m_ChildControlHeight_14; }
	inline void set_m_ChildControlHeight_14(bool value)
	{
		___m_ChildControlHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#ifndef SHADOW_T4269599528_H
#define SHADOW_T4269599528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t4269599528  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2020392075  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2243707579  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectColor_3)); }
	inline Color_t2020392075  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2020392075 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2020392075  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectDistance_4)); }
	inline Vector2_t2243707579  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2243707579 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2243707579  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T4269599528_H
#ifndef POSITIONASUV1_T1102546563_H
#define POSITIONASUV1_T1102546563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t1102546563  : public BaseMeshEffect_t1728560551
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T1102546563_H
#ifndef GRIDLAYOUTGROUP_T1515633077_H
#define GRIDLAYOUTGROUP_T1515633077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup
struct  GridLayoutGroup_t1515633077  : public LayoutGroup_t3962498969
{
public:
	// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::m_StartCorner
	int32_t ___m_StartCorner_10;
	// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::m_StartAxis
	int32_t ___m_StartAxis_11;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_CellSize
	Vector2_t2243707579  ___m_CellSize_12;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_Spacing
	Vector2_t2243707579  ___m_Spacing_13;
	// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::m_Constraint
	int32_t ___m_Constraint_14;
	// System.Int32 UnityEngine.UI.GridLayoutGroup::m_ConstraintCount
	int32_t ___m_ConstraintCount_15;

public:
	inline static int32_t get_offset_of_m_StartCorner_10() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_StartCorner_10)); }
	inline int32_t get_m_StartCorner_10() const { return ___m_StartCorner_10; }
	inline int32_t* get_address_of_m_StartCorner_10() { return &___m_StartCorner_10; }
	inline void set_m_StartCorner_10(int32_t value)
	{
		___m_StartCorner_10 = value;
	}

	inline static int32_t get_offset_of_m_StartAxis_11() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_StartAxis_11)); }
	inline int32_t get_m_StartAxis_11() const { return ___m_StartAxis_11; }
	inline int32_t* get_address_of_m_StartAxis_11() { return &___m_StartAxis_11; }
	inline void set_m_StartAxis_11(int32_t value)
	{
		___m_StartAxis_11 = value;
	}

	inline static int32_t get_offset_of_m_CellSize_12() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_CellSize_12)); }
	inline Vector2_t2243707579  get_m_CellSize_12() const { return ___m_CellSize_12; }
	inline Vector2_t2243707579 * get_address_of_m_CellSize_12() { return &___m_CellSize_12; }
	inline void set_m_CellSize_12(Vector2_t2243707579  value)
	{
		___m_CellSize_12 = value;
	}

	inline static int32_t get_offset_of_m_Spacing_13() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_Spacing_13)); }
	inline Vector2_t2243707579  get_m_Spacing_13() const { return ___m_Spacing_13; }
	inline Vector2_t2243707579 * get_address_of_m_Spacing_13() { return &___m_Spacing_13; }
	inline void set_m_Spacing_13(Vector2_t2243707579  value)
	{
		___m_Spacing_13 = value;
	}

	inline static int32_t get_offset_of_m_Constraint_14() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_Constraint_14)); }
	inline int32_t get_m_Constraint_14() const { return ___m_Constraint_14; }
	inline int32_t* get_address_of_m_Constraint_14() { return &___m_Constraint_14; }
	inline void set_m_Constraint_14(int32_t value)
	{
		___m_Constraint_14 = value;
	}

	inline static int32_t get_offset_of_m_ConstraintCount_15() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_ConstraintCount_15)); }
	inline int32_t get_m_ConstraintCount_15() const { return ___m_ConstraintCount_15; }
	inline int32_t* get_address_of_m_ConstraintCount_15() { return &___m_ConstraintCount_15; }
	inline void set_m_ConstraintCount_15(int32_t value)
	{
		___m_ConstraintCount_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDLAYOUTGROUP_T1515633077_H
#ifndef VERTICALLAYOUTGROUP_T2468316403_H
#define VERTICALLAYOUTGROUP_T2468316403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_t2468316403  : public HorizontalOrVerticalLayoutGroup_t1968298610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_T2468316403_H
#ifndef OUTLINE_T1417504278_H
#define OUTLINE_T1417504278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t1417504278  : public Shadow_t4269599528
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T1417504278_H
#ifndef HORIZONTALLAYOUTGROUP_T2875670365_H
#define HORIZONTALLAYOUTGROUP_T2875670365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_t2875670365  : public HorizontalOrVerticalLayoutGroup_t1968298610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_T2875670365_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (AspectMode_t1166448724)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1704[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (ScaleMode_t987318053)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1706[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1707[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (Unit_t3220761768)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1708[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (FitMode_t4030874534)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1710[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (Corner_t1077473318)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1712[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (Axis_t1431825778)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1713[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (Constraint_t3558160636)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1714[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[8] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
	LayoutElement_t2808691390::get_offset_of_m_LayoutPriority_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1725[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1726[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1732[7] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (GetRayIntersectionAllNonAllocCallback_t3246763936), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (GetRaycastNonAllocCallback_t1074830945), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1739[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1747[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (U3CModuleU3E_t3783534233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (Bomb_t2608481788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[4] = 
{
	Bomb_t2608481788::get_offset_of_omNomBomb_2(),
	Bomb_t2608481788::get_offset_of_explosiveEffect_3(),
	Bomb_t2608481788::get_offset_of_player_4(),
	Bomb_t2608481788::get_offset_of_explosionRadius_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (Bullet_t2590115616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[2] = 
{
	Bullet_t2590115616::get_offset_of_splashEffect_2(),
	Bullet_t2590115616::get_offset_of_player_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (Collectable_t2201227982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[1] = 
{
	Collectable_t2201227982::get_offset_of_speed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (DeathBarrier_t1380906595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (Enemy_t1088811588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[4] = 
{
	Enemy_t1088811588::get_offset_of_health_0(),
	Enemy_t1088811588::get_offset_of_attackStrength_1(),
	Enemy_t1088811588::get_offset_of_attackDelay_2(),
	Enemy_t1088811588::get_offset_of_attacking_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (EnemyController_t2146768720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[6] = 
{
	EnemyController_t2146768720::get_offset_of_tasteless_2(),
	EnemyController_t2146768720::get_offset_of_player_3(),
	EnemyController_t2146768720::get_offset_of_ship_4(),
	EnemyController_t2146768720::get_offset_of_nav_5(),
	EnemyController_t2146768720::get_offset_of_anim_6(),
	EnemyController_t2146768720::get_offset_of_destination_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (difficultyLevel_t2746910351)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1756[4] = 
{
	difficultyLevel_t2746910351::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (Game_t1600141214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Game_t1600141214::get_offset_of_difficulty_15(),
	Game_t1600141214::get_offset_of_won_16(),
	Game_t1600141214::get_offset_of_wave_17(),
	Game_t1600141214::get_offset_of_players_18(),
	Game_t1600141214::get_offset_of_partsReturned_19(),
	Game_t1600141214::get_offset_of_partsRequired_20(),
	Game_t1600141214::get_offset_of_timeRemaining_21(),
	Game_t1600141214::get_offset_of_betweenWaveDelay_22(),
	Game_t1600141214::get_offset_of_partSpawnDelay_23(),
	Game_t1600141214::get_offset_of_partSpawnAmount_24(),
	Game_t1600141214::get_offset_of_enemySpawnDelay_25(),
	Game_t1600141214::get_offset_of_enemySpawnAmount_26(),
	Game_t1600141214::get_offset_of_betweenWaves_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (GameManager_t2252321495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[8] = 
{
	GameManager_t2252321495::get_offset_of_game_2(),
	GameManager_t2252321495::get_offset_of_UI_3(),
	GameManager_t2252321495::get_offset_of_spawnLocations_4(),
	GameManager_t2252321495::get_offset_of_shipPartSpawnLocations_5(),
	GameManager_t2252321495::get_offset_of_player_6(),
	GameManager_t2252321495::get_offset_of_ship_7(),
	GameManager_t2252321495::get_offset_of_enemyPrefab_8(),
	GameManager_t2252321495::get_offset_of_shipPartPrefab_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (ammoType_t4206898302)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1759[4] = 
{
	ammoType_t4206898302::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (MainWeapon_t1214762455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[6] = 
{
	MainWeapon_t1214762455::get_offset_of_ammo_0(),
	MainWeapon_t1214762455::get_offset_of_range_1(),
	MainWeapon_t1214762455::get_offset_of_damage_2(),
	MainWeapon_t1214762455::get_offset_of_currentCharge_3(),
	MainWeapon_t1214762455::get_offset_of_dischargeRate_4(),
	MainWeapon_t1214762455::get_offset_of_rechargeRate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (Player_t1147783557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[10] = 
{
	Player_t1147783557::get_offset_of_blenderBlaster_0(),
	Player_t1147783557::get_offset_of_omNomBomb_1(),
	Player_t1147783557::get_offset_of_speed_2(),
	Player_t1147783557::get_offset_of_walkingSpeed_3(),
	Player_t1147783557::get_offset_of_sprintingSpeed_4(),
	Player_t1147783557::get_offset_of_jumpHeight_5(),
	Player_t1147783557::get_offset_of_regenDelay_6(),
	Player_t1147783557::get_offset_of_health_7(),
	Player_t1147783557::get_offset_of_healthRegen_8(),
	Player_t1147783557::get_offset_of_partHeld_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (PlayerController_t4148409433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[14] = 
{
	PlayerController_t4148409433::get_offset_of_grubby_2(),
	PlayerController_t4148409433::get_offset_of_character_3(),
	PlayerController_t4148409433::get_offset_of_bombPrefab_4(),
	PlayerController_t4148409433::get_offset_of_bulletPrefab_5(),
	PlayerController_t4148409433::get_offset_of_nozzle_6(),
	PlayerController_t4148409433::get_offset_of_walkSpeed_7(),
	PlayerController_t4148409433::get_offset_of_sprintSpeed_8(),
	PlayerController_t4148409433::get_offset_of_forwardSpeed_9(),
	PlayerController_t4148409433::get_offset_of_sideSpeed_10(),
	PlayerController_t4148409433::get_offset_of_horizontalSensitivity_11(),
	PlayerController_t4148409433::get_offset_of_verticalSensitivity_12(),
	PlayerController_t4148409433::get_offset_of_lookLimit_13(),
	PlayerController_t4148409433::get_offset_of_pitch_14(),
	PlayerController_t4148409433::get_offset_of_verticalVelocity_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (Ship_t1116303770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[1] = 
{
	Ship_t1116303770::get_offset_of_health_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (ShipController_t3277973446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[2] = 
{
	ShipController_t3277973446::get_offset_of_ship_2(),
	ShipController_t3277973446::get_offset_of_explosiveEffect_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (ShipUI_t242265720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[3] = 
{
	ShipUI_t242265720::get_offset_of_cameraToLookAt_2(),
	ShipUI_t242265720::get_offset_of_bar_3(),
	ShipUI_t242265720::get_offset_of_barVector_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (bombType_t3119779548)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1766[4] = 
{
	bombType_t3119779548::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (SubWeapon_t1081861670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[6] = 
{
	SubWeapon_t1081861670::get_offset_of_type_0(),
	SubWeapon_t1081861670::get_offset_of_damage_1(),
	SubWeapon_t1081861670::get_offset_of_delay_2(),
	SubWeapon_t1081861670::get_offset_of_throwDelay_3(),
	SubWeapon_t1081861670::get_offset_of_range_4(),
	SubWeapon_t1081861670::get_offset_of_thrown_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (UIController_t2029583246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[15] = 
{
	0,
	UIController_t2029583246::get_offset_of_damageFrame_1(),
	UIController_t2029583246::get_offset_of_ammoBar_2(),
	UIController_t2029583246::get_offset_of_rechargingBar_3(),
	UIController_t2029583246::get_offset_of_omNombBombIcon_4(),
	UIController_t2029583246::get_offset_of_waveCounter_5(),
	UIController_t2029583246::get_offset_of_timer_6(),
	UIController_t2029583246::get_offset_of_partsCollected_7(),
	UIController_t2029583246::get_offset_of_message_8(),
	UIController_t2029583246::get_offset_of_messageDescription_9(),
	UIController_t2029583246::get_offset_of_subMessage_10(),
	UIController_t2029583246::get_offset_of_partIcon_11(),
	UIController_t2029583246::get_offset_of_deathCam_12(),
	UIController_t2029583246::get_offset_of_shipAttackedDelay_13(),
	UIController_t2029583246::get_offset_of_freezeUI_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (U3CModuleU3E_t3783534234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (Displace_t357053105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (GerstnerDisplace_t4155590855), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (MeshContainer_t299039902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[3] = 
{
	MeshContainer_t299039902::get_offset_of_mesh_0(),
	MeshContainer_t299039902::get_offset_of_vertices_1(),
	MeshContainer_t299039902::get_offset_of_normals_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (PlanarReflection_t2931543887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[9] = 
{
	PlanarReflection_t2931543887::get_offset_of_reflectionMask_2(),
	PlanarReflection_t2931543887::get_offset_of_reflectSkybox_3(),
	PlanarReflection_t2931543887::get_offset_of_clearColor_4(),
	PlanarReflection_t2931543887::get_offset_of_reflectionSampler_5(),
	PlanarReflection_t2931543887::get_offset_of_clipPlaneOffset_6(),
	PlanarReflection_t2931543887::get_offset_of_m_Oldpos_7(),
	PlanarReflection_t2931543887::get_offset_of_m_ReflectionCamera_8(),
	PlanarReflection_t2931543887::get_offset_of_m_SharedMaterial_9(),
	PlanarReflection_t2931543887::get_offset_of_m_HelperCameras_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (SpecularLighting_t2824122729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[2] = 
{
	SpecularLighting_t2824122729::get_offset_of_specularLight_2(),
	SpecularLighting_t2824122729::get_offset_of_m_WaterBase_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (Water_t1562849653), -1, sizeof(Water_t1562849653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1775[14] = 
{
	Water_t1562849653::get_offset_of_waterMode_2(),
	Water_t1562849653::get_offset_of_disablePixelLights_3(),
	Water_t1562849653::get_offset_of_textureSize_4(),
	Water_t1562849653::get_offset_of_clipPlaneOffset_5(),
	Water_t1562849653::get_offset_of_reflectLayers_6(),
	Water_t1562849653::get_offset_of_refractLayers_7(),
	Water_t1562849653::get_offset_of_m_ReflectionCameras_8(),
	Water_t1562849653::get_offset_of_m_RefractionCameras_9(),
	Water_t1562849653::get_offset_of_m_ReflectionTexture_10(),
	Water_t1562849653::get_offset_of_m_RefractionTexture_11(),
	Water_t1562849653::get_offset_of_m_HardwareWaterSupport_12(),
	Water_t1562849653::get_offset_of_m_OldReflectionTextureSize_13(),
	Water_t1562849653::get_offset_of_m_OldRefractionTextureSize_14(),
	Water_t1562849653_StaticFields::get_offset_of_s_InsideWater_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (WaterMode_t2303503718)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1776[4] = 
{
	WaterMode_t2303503718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (WaterQuality_t3485013746)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1777[4] = 
{
	WaterQuality_t3485013746::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (WaterBase_t4291487940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[3] = 
{
	WaterBase_t4291487940::get_offset_of_sharedMaterial_2(),
	WaterBase_t4291487940::get_offset_of_waterQuality_3(),
	WaterBase_t4291487940::get_offset_of_edgeBlend_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (WaterTile_t1797616825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[2] = 
{
	WaterTile_t1797616825::get_offset_of_reflection_2(),
	WaterTile_t1797616825::get_offset_of_waterBase_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
