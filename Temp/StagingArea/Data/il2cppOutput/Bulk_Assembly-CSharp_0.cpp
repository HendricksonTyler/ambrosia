﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Bomb
struct Bomb_t2608481788;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// PlayerController
struct PlayerController_t4148409433;
// UnityEngine.Object
struct Object_t1021602117;
// SubWeapon
struct SubWeapon_t1081861670;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// EnemyController
struct EnemyController_t2146768720;
// Enemy
struct Enemy_t1088811588;
// UnityEngine.Collider
struct Collider_t3497673348;
// Bullet
struct Bullet_t2590115616;
// UnityEngine.Collision
struct Collision_t2876846408;
// Collectable
struct Collectable_t2201227982;
// GameManager
struct GameManager_t2252321495;
// UIController
struct UIController_t2029583246;
// DeathBarrier
struct DeathBarrier_t1380906595;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t2761625415;
// UnityEngine.Animator
struct Animator_t69676727;
// Player
struct Player_t1147783557;
// ShipController
struct ShipController_t3277973446;
// Ship
struct Ship_t1116303770;
// Game
struct Game_t1600141214;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// MainWeapon
struct MainWeapon_t1214762455;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// ShipUI
struct ShipUI_t242265720;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t1084937515;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.UI.FontData
struct FontData_t2614388407;
// UnityEngine.TextGenerator
struct TextGenerator_t647235000;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;

extern const RuntimeMethod* GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t Bomb_Start_m3332595677_MetadataUsageId;
extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Bomb_Update_m1851208838_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisEnemyController_t2146768720_m1684476845_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1816890106;
extern const uint32_t Bomb_Explode_m1242553278_MetadataUsageId;
extern const uint32_t Bullet_Start_m1447617473_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisEnemyController_t2146768720_m543378449_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3318194134;
extern const uint32_t Bullet_OnCollisionEnter_m314193427_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2980400013;
extern const uint32_t Collectable_OnTriggerEnter_m1201684067_MetadataUsageId;
extern const uint32_t Collectable_OnTriggerExit_m1418933007_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral879651274;
extern const uint32_t DeathBarrier_OnTriggerEnter_m1529541608_MetadataUsageId;
extern RuntimeClass* Enemy_t1088811588_il2cpp_TypeInfo_var;
extern const uint32_t EnemyController__ctor_m1153179309_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisNavMeshAgent_t2761625415_m3828941945_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisAnimator_t69676727_m475627522_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1844382288;
extern const uint32_t EnemyController_Awake_m1186140764_MetadataUsageId;
extern const uint32_t EnemyController_Update_m3131616442_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisShipController_t3277973446_m2617641097_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral574095090;
extern const uint32_t EnemyController_OnTriggerStay_m3542192760_MetadataUsageId;
extern const uint32_t EnemyController_OnTriggerExit_m943580849_MetadataUsageId;
extern RuntimeClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t EnemyController_determineDestination_m3337669536_MetadataUsageId;
extern RuntimeClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern RuntimeClass* UIController_t2029583246_il2cpp_TypeInfo_var;
extern const uint32_t GameManager__ctor_m293624896_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2310044285;
extern Il2CppCodeGenString* _stringLiteral1255533420;
extern const uint32_t GameManager_InitGame_m4160192178_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3031669893;
extern const uint32_t GameManager_timeUp_m1204656286_MetadataUsageId;
extern const uint32_t GameManager_spawnEnemies_m1229942693_MetadataUsageId;
extern RuntimeClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1575516707;
extern const uint32_t GameManager_spawnShipParts_m906813607_MetadataUsageId;
extern const uint32_t GameManager_checkForDeath_m2230622135_MetadataUsageId;
extern const uint32_t GameManager_destroySpawnLocations_m3820108133_MetadataUsageId;
extern RuntimeClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t MainWeapon_discharge_m3402882019_MetadataUsageId;
extern const uint32_t MainWeapon_recharge_m3583788820_MetadataUsageId;
extern RuntimeClass* MainWeapon_t1214762455_il2cpp_TypeInfo_var;
extern RuntimeClass* SubWeapon_t1081861670_il2cpp_TypeInfo_var;
extern const uint32_t Player__ctor_m3620100752_MetadataUsageId;
extern RuntimeClass* Player_t1147783557_il2cpp_TypeInfo_var;
extern const uint32_t PlayerController__ctor_m3280132936_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCharacterController_t4094781467_m1582798737_RuntimeMethod_var;
extern const uint32_t PlayerController_Start_m3606284888_MetadataUsageId;
extern RuntimeClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern const uint32_t PlayerController_move_m1170970717_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1307534077;
extern Il2CppCodeGenString* _stringLiteral1307534078;
extern const uint32_t PlayerController_rotate_m1444289841_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral842948034;
extern const uint32_t PlayerController_jump_m563400686_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral372029369;
extern const uint32_t PlayerController_throwBomb_m3049288080_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2930725987;
extern Il2CppCodeGenString* _stringLiteral3645101709;
extern const uint32_t PlayerController_sprint_m4044474542_MetadataUsageId;
extern const uint32_t PlayerController_shoot_m9744895_MetadataUsageId;
extern const uint32_t PlayerController_regenerateHealth_m1430467426_MetadataUsageId;
extern const uint32_t PlayerController_die_m1474265594_MetadataUsageId;
extern RuntimeClass* Ship_t1116303770_il2cpp_TypeInfo_var;
extern const uint32_t ShipController__ctor_m2397218881_MetadataUsageId;
extern const uint32_t ShipController_Update_m1022057640_MetadataUsageId;
extern const uint32_t ShipController_OnTriggerEnter_m3928624133_MetadataUsageId;
extern const uint32_t ShipController_OnTriggerStay_m2544320122_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisCamera_t189460977_m4200645945_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral387931632;
extern Il2CppCodeGenString* _stringLiteral2051100417;
extern const uint32_t ShipUI_Start_m4254051481_MetadataUsageId;
extern const uint32_t ShipUI_Update_m1535098274_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisImage_t2042527209_m4162535761_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3200448484;
extern Il2CppCodeGenString* _stringLiteral1607060599;
extern Il2CppCodeGenString* _stringLiteral1711783022;
extern Il2CppCodeGenString* _stringLiteral1601101583;
extern Il2CppCodeGenString* _stringLiteral2328220233;
extern Il2CppCodeGenString* _stringLiteral3457520297;
extern Il2CppCodeGenString* _stringLiteral2933044483;
extern Il2CppCodeGenString* _stringLiteral259034962;
extern Il2CppCodeGenString* _stringLiteral360502915;
extern Il2CppCodeGenString* _stringLiteral91840137;
extern Il2CppCodeGenString* _stringLiteral3888774807;
extern Il2CppCodeGenString* _stringLiteral3141382987;
extern const uint32_t UIController_initializeUI_m1509084315_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisText_t356221433_m1342661039_RuntimeMethod_var;
extern const uint32_t UIController_countDown_m4243092691_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1609003072;
extern Il2CppCodeGenString* _stringLiteral2114188598;
extern const uint32_t UIController_healthDeathMessage_m2671790766_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2135999681;
extern Il2CppCodeGenString* _stringLiteral1989231835;
extern const uint32_t UIController_fallingDeathMessage_m2237346207_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1422551141;
extern Il2CppCodeGenString* _stringLiteral3872431501;
extern const uint32_t UIController_drowningDeathMessage_m3043734374_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral930836742;
extern const uint32_t UIController_shipUnderAttackMessage_m859257986_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3605215308;
extern Il2CppCodeGenString* _stringLiteral2017188972;
extern const uint32_t UIController_shipDestroyedMessage_m4291651381_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral220393588;
extern Il2CppCodeGenString* _stringLiteral3063152238;
extern const uint32_t UIController_partsNotCollectedMessage_m867535828_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2253030761;
extern const uint32_t UIController_partAlreadyHeldMessage_m1437698322_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4162974409;
extern Il2CppCodeGenString* _stringLiteral3049095124;
extern const uint32_t UIController_waveCompleteMessage_m112210205_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2515819241;
extern Il2CppCodeGenString* _stringLiteral1258241875;
extern Il2CppCodeGenString* _stringLiteral98844844;
extern Il2CppCodeGenString* _stringLiteral1623555996;
extern const uint32_t UIController_waveCountdownMessage_m2891189168_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral320178848;
extern Il2CppCodeGenString* _stringLiteral1627550058;
extern const uint32_t UIController_gameWonMessage_m284319100_MetadataUsageId;
extern const uint32_t UIController_updateTime_m340223966_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral57471863;
extern const uint32_t UIController_updatePartsReturned_m494708189_MetadataUsageId;
extern const uint32_t UIController_updateDamageFrame_m339031365_MetadataUsageId;
extern const uint32_t UIController_beginWave_m463379384_MetadataUsageId;
struct ContactPoint_t1376425630 ;

struct ColliderU5BU5D_t462843629;
struct GameObjectU5BU5D_t3057952154;
struct ObjectU5BU5D_t3614634134;


#ifndef U3CMODULEU3E_T3783534233_H
#define U3CMODULEU3E_T3783534233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534233 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534233_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef PLAYER_T1147783557_H
#define PLAYER_T1147783557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t1147783557  : public RuntimeObject
{
public:
	// MainWeapon Player::blenderBlaster
	MainWeapon_t1214762455 * ___blenderBlaster_0;
	// SubWeapon Player::omNomBomb
	SubWeapon_t1081861670 * ___omNomBomb_1;
	// System.Single Player::speed
	float ___speed_2;
	// System.Single Player::walkingSpeed
	float ___walkingSpeed_3;
	// System.Single Player::sprintingSpeed
	float ___sprintingSpeed_4;
	// System.Single Player::jumpHeight
	float ___jumpHeight_5;
	// System.Single Player::regenDelay
	float ___regenDelay_6;
	// System.Single Player::health
	float ___health_7;
	// System.Int32 Player::healthRegen
	int32_t ___healthRegen_8;
	// System.Boolean Player::partHeld
	bool ___partHeld_9;

public:
	inline static int32_t get_offset_of_blenderBlaster_0() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___blenderBlaster_0)); }
	inline MainWeapon_t1214762455 * get_blenderBlaster_0() const { return ___blenderBlaster_0; }
	inline MainWeapon_t1214762455 ** get_address_of_blenderBlaster_0() { return &___blenderBlaster_0; }
	inline void set_blenderBlaster_0(MainWeapon_t1214762455 * value)
	{
		___blenderBlaster_0 = value;
		Il2CppCodeGenWriteBarrier((&___blenderBlaster_0), value);
	}

	inline static int32_t get_offset_of_omNomBomb_1() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___omNomBomb_1)); }
	inline SubWeapon_t1081861670 * get_omNomBomb_1() const { return ___omNomBomb_1; }
	inline SubWeapon_t1081861670 ** get_address_of_omNomBomb_1() { return &___omNomBomb_1; }
	inline void set_omNomBomb_1(SubWeapon_t1081861670 * value)
	{
		___omNomBomb_1 = value;
		Il2CppCodeGenWriteBarrier((&___omNomBomb_1), value);
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_walkingSpeed_3() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___walkingSpeed_3)); }
	inline float get_walkingSpeed_3() const { return ___walkingSpeed_3; }
	inline float* get_address_of_walkingSpeed_3() { return &___walkingSpeed_3; }
	inline void set_walkingSpeed_3(float value)
	{
		___walkingSpeed_3 = value;
	}

	inline static int32_t get_offset_of_sprintingSpeed_4() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___sprintingSpeed_4)); }
	inline float get_sprintingSpeed_4() const { return ___sprintingSpeed_4; }
	inline float* get_address_of_sprintingSpeed_4() { return &___sprintingSpeed_4; }
	inline void set_sprintingSpeed_4(float value)
	{
		___sprintingSpeed_4 = value;
	}

	inline static int32_t get_offset_of_jumpHeight_5() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___jumpHeight_5)); }
	inline float get_jumpHeight_5() const { return ___jumpHeight_5; }
	inline float* get_address_of_jumpHeight_5() { return &___jumpHeight_5; }
	inline void set_jumpHeight_5(float value)
	{
		___jumpHeight_5 = value;
	}

	inline static int32_t get_offset_of_regenDelay_6() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___regenDelay_6)); }
	inline float get_regenDelay_6() const { return ___regenDelay_6; }
	inline float* get_address_of_regenDelay_6() { return &___regenDelay_6; }
	inline void set_regenDelay_6(float value)
	{
		___regenDelay_6 = value;
	}

	inline static int32_t get_offset_of_health_7() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___health_7)); }
	inline float get_health_7() const { return ___health_7; }
	inline float* get_address_of_health_7() { return &___health_7; }
	inline void set_health_7(float value)
	{
		___health_7 = value;
	}

	inline static int32_t get_offset_of_healthRegen_8() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___healthRegen_8)); }
	inline int32_t get_healthRegen_8() const { return ___healthRegen_8; }
	inline int32_t* get_address_of_healthRegen_8() { return &___healthRegen_8; }
	inline void set_healthRegen_8(int32_t value)
	{
		___healthRegen_8 = value;
	}

	inline static int32_t get_offset_of_partHeld_9() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___partHeld_9)); }
	inline bool get_partHeld_9() const { return ___partHeld_9; }
	inline bool* get_address_of_partHeld_9() { return &___partHeld_9; }
	inline void set_partHeld_9(bool value)
	{
		___partHeld_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T1147783557_H
#ifndef ENEMY_T1088811588_H
#define ENEMY_T1088811588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enemy
struct  Enemy_t1088811588  : public RuntimeObject
{
public:
	// System.Single Enemy::health
	float ___health_0;
	// System.Int32 Enemy::attackStrength
	int32_t ___attackStrength_1;
	// System.Single Enemy::attackDelay
	float ___attackDelay_2;
	// System.Boolean Enemy::attacking
	bool ___attacking_3;

public:
	inline static int32_t get_offset_of_health_0() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___health_0)); }
	inline float get_health_0() const { return ___health_0; }
	inline float* get_address_of_health_0() { return &___health_0; }
	inline void set_health_0(float value)
	{
		___health_0 = value;
	}

	inline static int32_t get_offset_of_attackStrength_1() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___attackStrength_1)); }
	inline int32_t get_attackStrength_1() const { return ___attackStrength_1; }
	inline int32_t* get_address_of_attackStrength_1() { return &___attackStrength_1; }
	inline void set_attackStrength_1(int32_t value)
	{
		___attackStrength_1 = value;
	}

	inline static int32_t get_offset_of_attackDelay_2() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___attackDelay_2)); }
	inline float get_attackDelay_2() const { return ___attackDelay_2; }
	inline float* get_address_of_attackDelay_2() { return &___attackDelay_2; }
	inline void set_attackDelay_2(float value)
	{
		___attackDelay_2 = value;
	}

	inline static int32_t get_offset_of_attacking_3() { return static_cast<int32_t>(offsetof(Enemy_t1088811588, ___attacking_3)); }
	inline bool get_attacking_3() const { return ___attacking_3; }
	inline bool* get_address_of_attacking_3() { return &___attacking_3; }
	inline void set_attacking_3(bool value)
	{
		___attacking_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMY_T1088811588_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef UICONTROLLER_T2029583246_H
#define UICONTROLLER_T2029583246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIController
struct  UIController_t2029583246  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image UIController::damageFrame
	Image_t2042527209 * ___damageFrame_1;
	// UnityEngine.UI.Image UIController::ammoBar
	Image_t2042527209 * ___ammoBar_2;
	// UnityEngine.UI.Image UIController::rechargingBar
	Image_t2042527209 * ___rechargingBar_3;
	// UnityEngine.GameObject UIController::omNombBombIcon
	GameObject_t1756533147 * ___omNombBombIcon_4;
	// UnityEngine.UI.Text UIController::waveCounter
	Text_t356221433 * ___waveCounter_5;
	// UnityEngine.UI.Text UIController::timer
	Text_t356221433 * ___timer_6;
	// UnityEngine.UI.Text UIController::partsCollected
	Text_t356221433 * ___partsCollected_7;
	// UnityEngine.UI.Text UIController::message
	Text_t356221433 * ___message_8;
	// UnityEngine.UI.Text UIController::messageDescription
	Text_t356221433 * ___messageDescription_9;
	// UnityEngine.UI.Text UIController::subMessage
	Text_t356221433 * ___subMessage_10;
	// UnityEngine.GameObject UIController::partIcon
	GameObject_t1756533147 * ___partIcon_11;
	// UnityEngine.GameObject UIController::deathCam
	GameObject_t1756533147 * ___deathCam_12;
	// System.Single UIController::shipAttackedDelay
	float ___shipAttackedDelay_13;
	// System.Boolean UIController::freezeUI
	bool ___freezeUI_14;

public:
	inline static int32_t get_offset_of_damageFrame_1() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___damageFrame_1)); }
	inline Image_t2042527209 * get_damageFrame_1() const { return ___damageFrame_1; }
	inline Image_t2042527209 ** get_address_of_damageFrame_1() { return &___damageFrame_1; }
	inline void set_damageFrame_1(Image_t2042527209 * value)
	{
		___damageFrame_1 = value;
		Il2CppCodeGenWriteBarrier((&___damageFrame_1), value);
	}

	inline static int32_t get_offset_of_ammoBar_2() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___ammoBar_2)); }
	inline Image_t2042527209 * get_ammoBar_2() const { return ___ammoBar_2; }
	inline Image_t2042527209 ** get_address_of_ammoBar_2() { return &___ammoBar_2; }
	inline void set_ammoBar_2(Image_t2042527209 * value)
	{
		___ammoBar_2 = value;
		Il2CppCodeGenWriteBarrier((&___ammoBar_2), value);
	}

	inline static int32_t get_offset_of_rechargingBar_3() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___rechargingBar_3)); }
	inline Image_t2042527209 * get_rechargingBar_3() const { return ___rechargingBar_3; }
	inline Image_t2042527209 ** get_address_of_rechargingBar_3() { return &___rechargingBar_3; }
	inline void set_rechargingBar_3(Image_t2042527209 * value)
	{
		___rechargingBar_3 = value;
		Il2CppCodeGenWriteBarrier((&___rechargingBar_3), value);
	}

	inline static int32_t get_offset_of_omNombBombIcon_4() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___omNombBombIcon_4)); }
	inline GameObject_t1756533147 * get_omNombBombIcon_4() const { return ___omNombBombIcon_4; }
	inline GameObject_t1756533147 ** get_address_of_omNombBombIcon_4() { return &___omNombBombIcon_4; }
	inline void set_omNombBombIcon_4(GameObject_t1756533147 * value)
	{
		___omNombBombIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&___omNombBombIcon_4), value);
	}

	inline static int32_t get_offset_of_waveCounter_5() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___waveCounter_5)); }
	inline Text_t356221433 * get_waveCounter_5() const { return ___waveCounter_5; }
	inline Text_t356221433 ** get_address_of_waveCounter_5() { return &___waveCounter_5; }
	inline void set_waveCounter_5(Text_t356221433 * value)
	{
		___waveCounter_5 = value;
		Il2CppCodeGenWriteBarrier((&___waveCounter_5), value);
	}

	inline static int32_t get_offset_of_timer_6() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___timer_6)); }
	inline Text_t356221433 * get_timer_6() const { return ___timer_6; }
	inline Text_t356221433 ** get_address_of_timer_6() { return &___timer_6; }
	inline void set_timer_6(Text_t356221433 * value)
	{
		___timer_6 = value;
		Il2CppCodeGenWriteBarrier((&___timer_6), value);
	}

	inline static int32_t get_offset_of_partsCollected_7() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___partsCollected_7)); }
	inline Text_t356221433 * get_partsCollected_7() const { return ___partsCollected_7; }
	inline Text_t356221433 ** get_address_of_partsCollected_7() { return &___partsCollected_7; }
	inline void set_partsCollected_7(Text_t356221433 * value)
	{
		___partsCollected_7 = value;
		Il2CppCodeGenWriteBarrier((&___partsCollected_7), value);
	}

	inline static int32_t get_offset_of_message_8() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___message_8)); }
	inline Text_t356221433 * get_message_8() const { return ___message_8; }
	inline Text_t356221433 ** get_address_of_message_8() { return &___message_8; }
	inline void set_message_8(Text_t356221433 * value)
	{
		___message_8 = value;
		Il2CppCodeGenWriteBarrier((&___message_8), value);
	}

	inline static int32_t get_offset_of_messageDescription_9() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___messageDescription_9)); }
	inline Text_t356221433 * get_messageDescription_9() const { return ___messageDescription_9; }
	inline Text_t356221433 ** get_address_of_messageDescription_9() { return &___messageDescription_9; }
	inline void set_messageDescription_9(Text_t356221433 * value)
	{
		___messageDescription_9 = value;
		Il2CppCodeGenWriteBarrier((&___messageDescription_9), value);
	}

	inline static int32_t get_offset_of_subMessage_10() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___subMessage_10)); }
	inline Text_t356221433 * get_subMessage_10() const { return ___subMessage_10; }
	inline Text_t356221433 ** get_address_of_subMessage_10() { return &___subMessage_10; }
	inline void set_subMessage_10(Text_t356221433 * value)
	{
		___subMessage_10 = value;
		Il2CppCodeGenWriteBarrier((&___subMessage_10), value);
	}

	inline static int32_t get_offset_of_partIcon_11() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___partIcon_11)); }
	inline GameObject_t1756533147 * get_partIcon_11() const { return ___partIcon_11; }
	inline GameObject_t1756533147 ** get_address_of_partIcon_11() { return &___partIcon_11; }
	inline void set_partIcon_11(GameObject_t1756533147 * value)
	{
		___partIcon_11 = value;
		Il2CppCodeGenWriteBarrier((&___partIcon_11), value);
	}

	inline static int32_t get_offset_of_deathCam_12() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___deathCam_12)); }
	inline GameObject_t1756533147 * get_deathCam_12() const { return ___deathCam_12; }
	inline GameObject_t1756533147 ** get_address_of_deathCam_12() { return &___deathCam_12; }
	inline void set_deathCam_12(GameObject_t1756533147 * value)
	{
		___deathCam_12 = value;
		Il2CppCodeGenWriteBarrier((&___deathCam_12), value);
	}

	inline static int32_t get_offset_of_shipAttackedDelay_13() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___shipAttackedDelay_13)); }
	inline float get_shipAttackedDelay_13() const { return ___shipAttackedDelay_13; }
	inline float* get_address_of_shipAttackedDelay_13() { return &___shipAttackedDelay_13; }
	inline void set_shipAttackedDelay_13(float value)
	{
		___shipAttackedDelay_13 = value;
	}

	inline static int32_t get_offset_of_freezeUI_14() { return static_cast<int32_t>(offsetof(UIController_t2029583246, ___freezeUI_14)); }
	inline bool get_freezeUI_14() const { return ___freezeUI_14; }
	inline bool* get_address_of_freezeUI_14() { return &___freezeUI_14; }
	inline void set_freezeUI_14(bool value)
	{
		___freezeUI_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICONTROLLER_T2029583246_H
#ifndef SHIP_T1116303770_H
#define SHIP_T1116303770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ship
struct  Ship_t1116303770  : public RuntimeObject
{
public:
	// System.Int32 Ship::health
	int32_t ___health_0;

public:
	inline static int32_t get_offset_of_health_0() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___health_0)); }
	inline int32_t get_health_0() const { return ___health_0; }
	inline int32_t* get_address_of_health_0() { return &___health_0; }
	inline void set_health_0(int32_t value)
	{
		___health_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIP_T1116303770_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef COLLISION_T2876846408_H
#define COLLISION_T2876846408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision
struct  Collision_t2876846408  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t2243707580  ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t2243707580  ___m_RelativeVelocity_1;
	// UnityEngine.Rigidbody UnityEngine.Collision::m_Rigidbody
	Rigidbody_t4233889191 * ___m_Rigidbody_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t3497673348 * ___m_Collider_3;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_Contacts
	ContactPointU5BU5D_t1084937515* ___m_Contacts_4;

public:
	inline static int32_t get_offset_of_m_Impulse_0() { return static_cast<int32_t>(offsetof(Collision_t2876846408, ___m_Impulse_0)); }
	inline Vector3_t2243707580  get_m_Impulse_0() const { return ___m_Impulse_0; }
	inline Vector3_t2243707580 * get_address_of_m_Impulse_0() { return &___m_Impulse_0; }
	inline void set_m_Impulse_0(Vector3_t2243707580  value)
	{
		___m_Impulse_0 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_1() { return static_cast<int32_t>(offsetof(Collision_t2876846408, ___m_RelativeVelocity_1)); }
	inline Vector3_t2243707580  get_m_RelativeVelocity_1() const { return ___m_RelativeVelocity_1; }
	inline Vector3_t2243707580 * get_address_of_m_RelativeVelocity_1() { return &___m_RelativeVelocity_1; }
	inline void set_m_RelativeVelocity_1(Vector3_t2243707580  value)
	{
		___m_RelativeVelocity_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision_t2876846408, ___m_Rigidbody_2)); }
	inline Rigidbody_t4233889191 * get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline Rigidbody_t4233889191 ** get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(Rigidbody_t4233889191 * value)
	{
		___m_Rigidbody_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_2), value);
	}

	inline static int32_t get_offset_of_m_Collider_3() { return static_cast<int32_t>(offsetof(Collision_t2876846408, ___m_Collider_3)); }
	inline Collider_t3497673348 * get_m_Collider_3() const { return ___m_Collider_3; }
	inline Collider_t3497673348 ** get_address_of_m_Collider_3() { return &___m_Collider_3; }
	inline void set_m_Collider_3(Collider_t3497673348 * value)
	{
		___m_Collider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_3), value);
	}

	inline static int32_t get_offset_of_m_Contacts_4() { return static_cast<int32_t>(offsetof(Collision_t2876846408, ___m_Contacts_4)); }
	inline ContactPointU5BU5D_t1084937515* get_m_Contacts_4() const { return ___m_Contacts_4; }
	inline ContactPointU5BU5D_t1084937515** get_address_of_m_Contacts_4() { return &___m_Contacts_4; }
	inline void set_m_Contacts_4(ContactPointU5BU5D_t1084937515* value)
	{
		___m_Contacts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Contacts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_t2876846408_marshaled_pinvoke
{
	Vector3_t2243707580  ___m_Impulse_0;
	Vector3_t2243707580  ___m_RelativeVelocity_1;
	Rigidbody_t4233889191 * ___m_Rigidbody_2;
	Collider_t3497673348 * ___m_Collider_3;
	ContactPoint_t1376425630 * ___m_Contacts_4;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_t2876846408_marshaled_com
{
	Vector3_t2243707580  ___m_Impulse_0;
	Vector3_t2243707580  ___m_RelativeVelocity_1;
	Rigidbody_t4233889191 * ___m_Rigidbody_2;
	Collider_t3497673348 * ___m_Collider_3;
	ContactPoint_t1376425630 * ___m_Contacts_4;
};
#endif // COLLISION_T2876846408_H
#ifndef CURSORLOCKMODE_T3372615096_H
#define CURSORLOCKMODE_T3372615096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CursorLockMode
struct  CursorLockMode_t3372615096 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CursorLockMode_t3372615096, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORLOCKMODE_T3372615096_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef AMMOTYPE_T4206898302_H
#define AMMOTYPE_T4206898302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ammoType
struct  ammoType_t4206898302 
{
public:
	// System.Int32 ammoType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ammoType_t4206898302, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMMOTYPE_T4206898302_H
#ifndef FILLMETHOD_T1640962579_H
#define FILLMETHOD_T1640962579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1640962579 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1640962579, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1640962579_H
#ifndef DIFFICULTYLEVEL_T2746910351_H
#define DIFFICULTYLEVEL_T2746910351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// difficultyLevel
struct  difficultyLevel_t2746910351 
{
public:
	// System.Int32 difficultyLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(difficultyLevel_t2746910351, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIFFICULTYLEVEL_T2746910351_H
#ifndef COLLISIONFLAGS_T4046947985_H
#define COLLISIONFLAGS_T4046947985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CollisionFlags
struct  CollisionFlags_t4046947985 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionFlags_t4046947985, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONFLAGS_T4046947985_H
#ifndef FORCEMODE_T1856518252_H
#define FORCEMODE_T1856518252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode
struct  ForceMode_t1856518252 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode_t1856518252, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE_T1856518252_H
#ifndef TYPE_T3352948571_H
#define TYPE_T3352948571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t3352948571 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t3352948571, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3352948571_H
#ifndef BOMBTYPE_T3119779548_H
#define BOMBTYPE_T3119779548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bombType
struct  bombType_t3119779548 
{
public:
	// System.Int32 bombType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(bombType_t3119779548, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOMBTYPE_T3119779548_H
#ifndef GAMEOBJECT_T1756533147_H
#define GAMEOBJECT_T1756533147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1756533147  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1756533147_H
#ifndef MAINWEAPON_T1214762455_H
#define MAINWEAPON_T1214762455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainWeapon
struct  MainWeapon_t1214762455  : public RuntimeObject
{
public:
	// ammoType MainWeapon::ammo
	int32_t ___ammo_0;
	// System.Single MainWeapon::range
	float ___range_1;
	// System.Int32 MainWeapon::damage
	int32_t ___damage_2;
	// System.Single MainWeapon::currentCharge
	float ___currentCharge_3;
	// System.Single MainWeapon::dischargeRate
	float ___dischargeRate_4;
	// System.Single MainWeapon::rechargeRate
	float ___rechargeRate_5;

public:
	inline static int32_t get_offset_of_ammo_0() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___ammo_0)); }
	inline int32_t get_ammo_0() const { return ___ammo_0; }
	inline int32_t* get_address_of_ammo_0() { return &___ammo_0; }
	inline void set_ammo_0(int32_t value)
	{
		___ammo_0 = value;
	}

	inline static int32_t get_offset_of_range_1() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___range_1)); }
	inline float get_range_1() const { return ___range_1; }
	inline float* get_address_of_range_1() { return &___range_1; }
	inline void set_range_1(float value)
	{
		___range_1 = value;
	}

	inline static int32_t get_offset_of_damage_2() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___damage_2)); }
	inline int32_t get_damage_2() const { return ___damage_2; }
	inline int32_t* get_address_of_damage_2() { return &___damage_2; }
	inline void set_damage_2(int32_t value)
	{
		___damage_2 = value;
	}

	inline static int32_t get_offset_of_currentCharge_3() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___currentCharge_3)); }
	inline float get_currentCharge_3() const { return ___currentCharge_3; }
	inline float* get_address_of_currentCharge_3() { return &___currentCharge_3; }
	inline void set_currentCharge_3(float value)
	{
		___currentCharge_3 = value;
	}

	inline static int32_t get_offset_of_dischargeRate_4() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___dischargeRate_4)); }
	inline float get_dischargeRate_4() const { return ___dischargeRate_4; }
	inline float* get_address_of_dischargeRate_4() { return &___dischargeRate_4; }
	inline void set_dischargeRate_4(float value)
	{
		___dischargeRate_4 = value;
	}

	inline static int32_t get_offset_of_rechargeRate_5() { return static_cast<int32_t>(offsetof(MainWeapon_t1214762455, ___rechargeRate_5)); }
	inline float get_rechargeRate_5() const { return ___rechargeRate_5; }
	inline float* get_address_of_rechargeRate_5() { return &___rechargeRate_5; }
	inline void set_rechargeRate_5(float value)
	{
		___rechargeRate_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINWEAPON_T1214762455_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef GAME_T1600141214_H
#define GAME_T1600141214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game
struct  Game_t1600141214  : public RuntimeObject
{
public:
	// difficultyLevel Game::difficulty
	int32_t ___difficulty_15;
	// System.Boolean Game::won
	bool ___won_16;
	// System.Int32 Game::wave
	int32_t ___wave_17;
	// System.Int32 Game::players
	int32_t ___players_18;
	// System.Int32 Game::partsReturned
	int32_t ___partsReturned_19;
	// System.Int32 Game::partsRequired
	int32_t ___partsRequired_20;
	// System.Single Game::timeRemaining
	float ___timeRemaining_21;
	// System.Single Game::betweenWaveDelay
	float ___betweenWaveDelay_22;
	// System.Single Game::partSpawnDelay
	float ___partSpawnDelay_23;
	// System.Single Game::partSpawnAmount
	float ___partSpawnAmount_24;
	// System.Single Game::enemySpawnDelay
	float ___enemySpawnDelay_25;
	// System.Int32 Game::enemySpawnAmount
	int32_t ___enemySpawnAmount_26;
	// System.Boolean Game::betweenWaves
	bool ___betweenWaves_27;

public:
	inline static int32_t get_offset_of_difficulty_15() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___difficulty_15)); }
	inline int32_t get_difficulty_15() const { return ___difficulty_15; }
	inline int32_t* get_address_of_difficulty_15() { return &___difficulty_15; }
	inline void set_difficulty_15(int32_t value)
	{
		___difficulty_15 = value;
	}

	inline static int32_t get_offset_of_won_16() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___won_16)); }
	inline bool get_won_16() const { return ___won_16; }
	inline bool* get_address_of_won_16() { return &___won_16; }
	inline void set_won_16(bool value)
	{
		___won_16 = value;
	}

	inline static int32_t get_offset_of_wave_17() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___wave_17)); }
	inline int32_t get_wave_17() const { return ___wave_17; }
	inline int32_t* get_address_of_wave_17() { return &___wave_17; }
	inline void set_wave_17(int32_t value)
	{
		___wave_17 = value;
	}

	inline static int32_t get_offset_of_players_18() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___players_18)); }
	inline int32_t get_players_18() const { return ___players_18; }
	inline int32_t* get_address_of_players_18() { return &___players_18; }
	inline void set_players_18(int32_t value)
	{
		___players_18 = value;
	}

	inline static int32_t get_offset_of_partsReturned_19() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___partsReturned_19)); }
	inline int32_t get_partsReturned_19() const { return ___partsReturned_19; }
	inline int32_t* get_address_of_partsReturned_19() { return &___partsReturned_19; }
	inline void set_partsReturned_19(int32_t value)
	{
		___partsReturned_19 = value;
	}

	inline static int32_t get_offset_of_partsRequired_20() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___partsRequired_20)); }
	inline int32_t get_partsRequired_20() const { return ___partsRequired_20; }
	inline int32_t* get_address_of_partsRequired_20() { return &___partsRequired_20; }
	inline void set_partsRequired_20(int32_t value)
	{
		___partsRequired_20 = value;
	}

	inline static int32_t get_offset_of_timeRemaining_21() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___timeRemaining_21)); }
	inline float get_timeRemaining_21() const { return ___timeRemaining_21; }
	inline float* get_address_of_timeRemaining_21() { return &___timeRemaining_21; }
	inline void set_timeRemaining_21(float value)
	{
		___timeRemaining_21 = value;
	}

	inline static int32_t get_offset_of_betweenWaveDelay_22() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___betweenWaveDelay_22)); }
	inline float get_betweenWaveDelay_22() const { return ___betweenWaveDelay_22; }
	inline float* get_address_of_betweenWaveDelay_22() { return &___betweenWaveDelay_22; }
	inline void set_betweenWaveDelay_22(float value)
	{
		___betweenWaveDelay_22 = value;
	}

	inline static int32_t get_offset_of_partSpawnDelay_23() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___partSpawnDelay_23)); }
	inline float get_partSpawnDelay_23() const { return ___partSpawnDelay_23; }
	inline float* get_address_of_partSpawnDelay_23() { return &___partSpawnDelay_23; }
	inline void set_partSpawnDelay_23(float value)
	{
		___partSpawnDelay_23 = value;
	}

	inline static int32_t get_offset_of_partSpawnAmount_24() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___partSpawnAmount_24)); }
	inline float get_partSpawnAmount_24() const { return ___partSpawnAmount_24; }
	inline float* get_address_of_partSpawnAmount_24() { return &___partSpawnAmount_24; }
	inline void set_partSpawnAmount_24(float value)
	{
		___partSpawnAmount_24 = value;
	}

	inline static int32_t get_offset_of_enemySpawnDelay_25() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___enemySpawnDelay_25)); }
	inline float get_enemySpawnDelay_25() const { return ___enemySpawnDelay_25; }
	inline float* get_address_of_enemySpawnDelay_25() { return &___enemySpawnDelay_25; }
	inline void set_enemySpawnDelay_25(float value)
	{
		___enemySpawnDelay_25 = value;
	}

	inline static int32_t get_offset_of_enemySpawnAmount_26() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___enemySpawnAmount_26)); }
	inline int32_t get_enemySpawnAmount_26() const { return ___enemySpawnAmount_26; }
	inline int32_t* get_address_of_enemySpawnAmount_26() { return &___enemySpawnAmount_26; }
	inline void set_enemySpawnAmount_26(int32_t value)
	{
		___enemySpawnAmount_26 = value;
	}

	inline static int32_t get_offset_of_betweenWaves_27() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___betweenWaves_27)); }
	inline bool get_betweenWaves_27() const { return ___betweenWaves_27; }
	inline bool* get_address_of_betweenWaves_27() { return &___betweenWaves_27; }
	inline void set_betweenWaves_27(bool value)
	{
		___betweenWaves_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAME_T1600141214_H
#ifndef SUBWEAPON_T1081861670_H
#define SUBWEAPON_T1081861670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubWeapon
struct  SubWeapon_t1081861670  : public RuntimeObject
{
public:
	// bombType SubWeapon::type
	int32_t ___type_0;
	// System.Int32 SubWeapon::damage
	int32_t ___damage_1;
	// System.Single SubWeapon::delay
	float ___delay_2;
	// System.Single SubWeapon::throwDelay
	float ___throwDelay_3;
	// System.Single SubWeapon::range
	float ___range_4;
	// System.Boolean SubWeapon::thrown
	bool ___thrown_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_damage_1() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___damage_1)); }
	inline int32_t get_damage_1() const { return ___damage_1; }
	inline int32_t* get_address_of_damage_1() { return &___damage_1; }
	inline void set_damage_1(int32_t value)
	{
		___damage_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_throwDelay_3() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___throwDelay_3)); }
	inline float get_throwDelay_3() const { return ___throwDelay_3; }
	inline float* get_address_of_throwDelay_3() { return &___throwDelay_3; }
	inline void set_throwDelay_3(float value)
	{
		___throwDelay_3 = value;
	}

	inline static int32_t get_offset_of_range_4() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___range_4)); }
	inline float get_range_4() const { return ___range_4; }
	inline float* get_address_of_range_4() { return &___range_4; }
	inline void set_range_4(float value)
	{
		___range_4 = value;
	}

	inline static int32_t get_offset_of_thrown_5() { return static_cast<int32_t>(offsetof(SubWeapon_t1081861670, ___thrown_5)); }
	inline bool get_thrown_5() const { return ___thrown_5; }
	inline bool* get_address_of_thrown_5() { return &___thrown_5; }
	inline void set_thrown_5(bool value)
	{
		___thrown_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBWEAPON_T1081861670_H
#ifndef RIGIDBODY_T4233889191_H
#define RIGIDBODY_T4233889191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t4233889191  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T4233889191_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef COLLIDER_T3497673348_H
#define COLLIDER_T3497673348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t3497673348  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T3497673348_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef CAMERA_T189460977_H
#define CAMERA_T189460977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t189460977  : public Behaviour_t955675639
{
public:

public:
};

struct Camera_t189460977_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t834278767 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t834278767 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t834278767 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t834278767 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t834278767 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t834278767 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t834278767 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t834278767 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t834278767 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t834278767 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t834278767 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t834278767 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T189460977_H
#ifndef CHARACTERCONTROLLER_T4094781467_H
#define CHARACTERCONTROLLER_T4094781467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CharacterController
struct  CharacterController_t4094781467  : public Collider_t3497673348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCONTROLLER_T4094781467_H
#ifndef NAVMESHAGENT_T2761625415_H
#define NAVMESHAGENT_T2761625415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshAgent
struct  NavMeshAgent_t2761625415  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHAGENT_T2761625415_H
#ifndef ANIMATOR_T69676727_H
#define ANIMATOR_T69676727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t69676727  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T69676727_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef SHIPUI_T242265720_H
#define SHIPUI_T242265720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipUI
struct  ShipUI_t242265720  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera ShipUI::cameraToLookAt
	Camera_t189460977 * ___cameraToLookAt_2;
	// UnityEngine.GameObject ShipUI::bar
	GameObject_t1756533147 * ___bar_3;
	// UnityEngine.Vector3 ShipUI::barVector
	Vector3_t2243707580  ___barVector_4;

public:
	inline static int32_t get_offset_of_cameraToLookAt_2() { return static_cast<int32_t>(offsetof(ShipUI_t242265720, ___cameraToLookAt_2)); }
	inline Camera_t189460977 * get_cameraToLookAt_2() const { return ___cameraToLookAt_2; }
	inline Camera_t189460977 ** get_address_of_cameraToLookAt_2() { return &___cameraToLookAt_2; }
	inline void set_cameraToLookAt_2(Camera_t189460977 * value)
	{
		___cameraToLookAt_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraToLookAt_2), value);
	}

	inline static int32_t get_offset_of_bar_3() { return static_cast<int32_t>(offsetof(ShipUI_t242265720, ___bar_3)); }
	inline GameObject_t1756533147 * get_bar_3() const { return ___bar_3; }
	inline GameObject_t1756533147 ** get_address_of_bar_3() { return &___bar_3; }
	inline void set_bar_3(GameObject_t1756533147 * value)
	{
		___bar_3 = value;
		Il2CppCodeGenWriteBarrier((&___bar_3), value);
	}

	inline static int32_t get_offset_of_barVector_4() { return static_cast<int32_t>(offsetof(ShipUI_t242265720, ___barVector_4)); }
	inline Vector3_t2243707580  get_barVector_4() const { return ___barVector_4; }
	inline Vector3_t2243707580 * get_address_of_barVector_4() { return &___barVector_4; }
	inline void set_barVector_4(Vector3_t2243707580  value)
	{
		___barVector_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPUI_T242265720_H
#ifndef BOMB_T2608481788_H
#define BOMB_T2608481788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bomb
struct  Bomb_t2608481788  : public MonoBehaviour_t1158329972
{
public:
	// SubWeapon Bomb::omNomBomb
	SubWeapon_t1081861670 * ___omNomBomb_2;
	// UnityEngine.GameObject Bomb::explosiveEffect
	GameObject_t1756533147 * ___explosiveEffect_3;
	// UnityEngine.GameObject Bomb::player
	GameObject_t1756533147 * ___player_4;
	// System.Single Bomb::explosionRadius
	float ___explosionRadius_5;

public:
	inline static int32_t get_offset_of_omNomBomb_2() { return static_cast<int32_t>(offsetof(Bomb_t2608481788, ___omNomBomb_2)); }
	inline SubWeapon_t1081861670 * get_omNomBomb_2() const { return ___omNomBomb_2; }
	inline SubWeapon_t1081861670 ** get_address_of_omNomBomb_2() { return &___omNomBomb_2; }
	inline void set_omNomBomb_2(SubWeapon_t1081861670 * value)
	{
		___omNomBomb_2 = value;
		Il2CppCodeGenWriteBarrier((&___omNomBomb_2), value);
	}

	inline static int32_t get_offset_of_explosiveEffect_3() { return static_cast<int32_t>(offsetof(Bomb_t2608481788, ___explosiveEffect_3)); }
	inline GameObject_t1756533147 * get_explosiveEffect_3() const { return ___explosiveEffect_3; }
	inline GameObject_t1756533147 ** get_address_of_explosiveEffect_3() { return &___explosiveEffect_3; }
	inline void set_explosiveEffect_3(GameObject_t1756533147 * value)
	{
		___explosiveEffect_3 = value;
		Il2CppCodeGenWriteBarrier((&___explosiveEffect_3), value);
	}

	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(Bomb_t2608481788, ___player_4)); }
	inline GameObject_t1756533147 * get_player_4() const { return ___player_4; }
	inline GameObject_t1756533147 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_t1756533147 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_explosionRadius_5() { return static_cast<int32_t>(offsetof(Bomb_t2608481788, ___explosionRadius_5)); }
	inline float get_explosionRadius_5() const { return ___explosionRadius_5; }
	inline float* get_address_of_explosionRadius_5() { return &___explosionRadius_5; }
	inline void set_explosionRadius_5(float value)
	{
		___explosionRadius_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOMB_T2608481788_H
#ifndef PLAYERCONTROLLER_T4148409433_H
#define PLAYERCONTROLLER_T4148409433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t4148409433  : public MonoBehaviour_t1158329972
{
public:
	// Player PlayerController::grubby
	Player_t1147783557 * ___grubby_2;
	// UnityEngine.CharacterController PlayerController::character
	CharacterController_t4094781467 * ___character_3;
	// UnityEngine.GameObject PlayerController::bombPrefab
	GameObject_t1756533147 * ___bombPrefab_4;
	// UnityEngine.GameObject PlayerController::bulletPrefab
	GameObject_t1756533147 * ___bulletPrefab_5;
	// UnityEngine.GameObject PlayerController::nozzle
	GameObject_t1756533147 * ___nozzle_6;
	// System.Single PlayerController::walkSpeed
	float ___walkSpeed_7;
	// System.Single PlayerController::sprintSpeed
	float ___sprintSpeed_8;
	// System.Single PlayerController::forwardSpeed
	float ___forwardSpeed_9;
	// System.Single PlayerController::sideSpeed
	float ___sideSpeed_10;
	// System.Single PlayerController::horizontalSensitivity
	float ___horizontalSensitivity_11;
	// System.Single PlayerController::verticalSensitivity
	float ___verticalSensitivity_12;
	// System.Single PlayerController::lookLimit
	float ___lookLimit_13;
	// System.Single PlayerController::pitch
	float ___pitch_14;
	// System.Single PlayerController::verticalVelocity
	float ___verticalVelocity_15;

public:
	inline static int32_t get_offset_of_grubby_2() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___grubby_2)); }
	inline Player_t1147783557 * get_grubby_2() const { return ___grubby_2; }
	inline Player_t1147783557 ** get_address_of_grubby_2() { return &___grubby_2; }
	inline void set_grubby_2(Player_t1147783557 * value)
	{
		___grubby_2 = value;
		Il2CppCodeGenWriteBarrier((&___grubby_2), value);
	}

	inline static int32_t get_offset_of_character_3() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___character_3)); }
	inline CharacterController_t4094781467 * get_character_3() const { return ___character_3; }
	inline CharacterController_t4094781467 ** get_address_of_character_3() { return &___character_3; }
	inline void set_character_3(CharacterController_t4094781467 * value)
	{
		___character_3 = value;
		Il2CppCodeGenWriteBarrier((&___character_3), value);
	}

	inline static int32_t get_offset_of_bombPrefab_4() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___bombPrefab_4)); }
	inline GameObject_t1756533147 * get_bombPrefab_4() const { return ___bombPrefab_4; }
	inline GameObject_t1756533147 ** get_address_of_bombPrefab_4() { return &___bombPrefab_4; }
	inline void set_bombPrefab_4(GameObject_t1756533147 * value)
	{
		___bombPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___bombPrefab_4), value);
	}

	inline static int32_t get_offset_of_bulletPrefab_5() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___bulletPrefab_5)); }
	inline GameObject_t1756533147 * get_bulletPrefab_5() const { return ___bulletPrefab_5; }
	inline GameObject_t1756533147 ** get_address_of_bulletPrefab_5() { return &___bulletPrefab_5; }
	inline void set_bulletPrefab_5(GameObject_t1756533147 * value)
	{
		___bulletPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_5), value);
	}

	inline static int32_t get_offset_of_nozzle_6() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___nozzle_6)); }
	inline GameObject_t1756533147 * get_nozzle_6() const { return ___nozzle_6; }
	inline GameObject_t1756533147 ** get_address_of_nozzle_6() { return &___nozzle_6; }
	inline void set_nozzle_6(GameObject_t1756533147 * value)
	{
		___nozzle_6 = value;
		Il2CppCodeGenWriteBarrier((&___nozzle_6), value);
	}

	inline static int32_t get_offset_of_walkSpeed_7() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___walkSpeed_7)); }
	inline float get_walkSpeed_7() const { return ___walkSpeed_7; }
	inline float* get_address_of_walkSpeed_7() { return &___walkSpeed_7; }
	inline void set_walkSpeed_7(float value)
	{
		___walkSpeed_7 = value;
	}

	inline static int32_t get_offset_of_sprintSpeed_8() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___sprintSpeed_8)); }
	inline float get_sprintSpeed_8() const { return ___sprintSpeed_8; }
	inline float* get_address_of_sprintSpeed_8() { return &___sprintSpeed_8; }
	inline void set_sprintSpeed_8(float value)
	{
		___sprintSpeed_8 = value;
	}

	inline static int32_t get_offset_of_forwardSpeed_9() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___forwardSpeed_9)); }
	inline float get_forwardSpeed_9() const { return ___forwardSpeed_9; }
	inline float* get_address_of_forwardSpeed_9() { return &___forwardSpeed_9; }
	inline void set_forwardSpeed_9(float value)
	{
		___forwardSpeed_9 = value;
	}

	inline static int32_t get_offset_of_sideSpeed_10() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___sideSpeed_10)); }
	inline float get_sideSpeed_10() const { return ___sideSpeed_10; }
	inline float* get_address_of_sideSpeed_10() { return &___sideSpeed_10; }
	inline void set_sideSpeed_10(float value)
	{
		___sideSpeed_10 = value;
	}

	inline static int32_t get_offset_of_horizontalSensitivity_11() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___horizontalSensitivity_11)); }
	inline float get_horizontalSensitivity_11() const { return ___horizontalSensitivity_11; }
	inline float* get_address_of_horizontalSensitivity_11() { return &___horizontalSensitivity_11; }
	inline void set_horizontalSensitivity_11(float value)
	{
		___horizontalSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_verticalSensitivity_12() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___verticalSensitivity_12)); }
	inline float get_verticalSensitivity_12() const { return ___verticalSensitivity_12; }
	inline float* get_address_of_verticalSensitivity_12() { return &___verticalSensitivity_12; }
	inline void set_verticalSensitivity_12(float value)
	{
		___verticalSensitivity_12 = value;
	}

	inline static int32_t get_offset_of_lookLimit_13() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___lookLimit_13)); }
	inline float get_lookLimit_13() const { return ___lookLimit_13; }
	inline float* get_address_of_lookLimit_13() { return &___lookLimit_13; }
	inline void set_lookLimit_13(float value)
	{
		___lookLimit_13 = value;
	}

	inline static int32_t get_offset_of_pitch_14() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___pitch_14)); }
	inline float get_pitch_14() const { return ___pitch_14; }
	inline float* get_address_of_pitch_14() { return &___pitch_14; }
	inline void set_pitch_14(float value)
	{
		___pitch_14 = value;
	}

	inline static int32_t get_offset_of_verticalVelocity_15() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___verticalVelocity_15)); }
	inline float get_verticalVelocity_15() const { return ___verticalVelocity_15; }
	inline float* get_address_of_verticalVelocity_15() { return &___verticalVelocity_15; }
	inline void set_verticalVelocity_15(float value)
	{
		___verticalVelocity_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T4148409433_H
#ifndef ENEMYCONTROLLER_T2146768720_H
#define ENEMYCONTROLLER_T2146768720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyController
struct  EnemyController_t2146768720  : public MonoBehaviour_t1158329972
{
public:
	// Enemy EnemyController::tasteless
	Enemy_t1088811588 * ___tasteless_2;
	// UnityEngine.GameObject EnemyController::player
	GameObject_t1756533147 * ___player_3;
	// UnityEngine.GameObject EnemyController::ship
	GameObject_t1756533147 * ___ship_4;
	// UnityEngine.AI.NavMeshAgent EnemyController::nav
	NavMeshAgent_t2761625415 * ___nav_5;
	// UnityEngine.Animator EnemyController::anim
	Animator_t69676727 * ___anim_6;
	// UnityEngine.Transform EnemyController::destination
	Transform_t3275118058 * ___destination_7;

public:
	inline static int32_t get_offset_of_tasteless_2() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___tasteless_2)); }
	inline Enemy_t1088811588 * get_tasteless_2() const { return ___tasteless_2; }
	inline Enemy_t1088811588 ** get_address_of_tasteless_2() { return &___tasteless_2; }
	inline void set_tasteless_2(Enemy_t1088811588 * value)
	{
		___tasteless_2 = value;
		Il2CppCodeGenWriteBarrier((&___tasteless_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___player_3)); }
	inline GameObject_t1756533147 * get_player_3() const { return ___player_3; }
	inline GameObject_t1756533147 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1756533147 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}

	inline static int32_t get_offset_of_ship_4() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___ship_4)); }
	inline GameObject_t1756533147 * get_ship_4() const { return ___ship_4; }
	inline GameObject_t1756533147 ** get_address_of_ship_4() { return &___ship_4; }
	inline void set_ship_4(GameObject_t1756533147 * value)
	{
		___ship_4 = value;
		Il2CppCodeGenWriteBarrier((&___ship_4), value);
	}

	inline static int32_t get_offset_of_nav_5() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___nav_5)); }
	inline NavMeshAgent_t2761625415 * get_nav_5() const { return ___nav_5; }
	inline NavMeshAgent_t2761625415 ** get_address_of_nav_5() { return &___nav_5; }
	inline void set_nav_5(NavMeshAgent_t2761625415 * value)
	{
		___nav_5 = value;
		Il2CppCodeGenWriteBarrier((&___nav_5), value);
	}

	inline static int32_t get_offset_of_anim_6() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___anim_6)); }
	inline Animator_t69676727 * get_anim_6() const { return ___anim_6; }
	inline Animator_t69676727 ** get_address_of_anim_6() { return &___anim_6; }
	inline void set_anim_6(Animator_t69676727 * value)
	{
		___anim_6 = value;
		Il2CppCodeGenWriteBarrier((&___anim_6), value);
	}

	inline static int32_t get_offset_of_destination_7() { return static_cast<int32_t>(offsetof(EnemyController_t2146768720, ___destination_7)); }
	inline Transform_t3275118058 * get_destination_7() const { return ___destination_7; }
	inline Transform_t3275118058 ** get_address_of_destination_7() { return &___destination_7; }
	inline void set_destination_7(Transform_t3275118058 * value)
	{
		___destination_7 = value;
		Il2CppCodeGenWriteBarrier((&___destination_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYCONTROLLER_T2146768720_H
#ifndef BULLET_T2590115616_H
#define BULLET_T2590115616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bullet
struct  Bullet_t2590115616  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Bullet::splashEffect
	GameObject_t1756533147 * ___splashEffect_2;
	// UnityEngine.GameObject Bullet::player
	GameObject_t1756533147 * ___player_3;

public:
	inline static int32_t get_offset_of_splashEffect_2() { return static_cast<int32_t>(offsetof(Bullet_t2590115616, ___splashEffect_2)); }
	inline GameObject_t1756533147 * get_splashEffect_2() const { return ___splashEffect_2; }
	inline GameObject_t1756533147 ** get_address_of_splashEffect_2() { return &___splashEffect_2; }
	inline void set_splashEffect_2(GameObject_t1756533147 * value)
	{
		___splashEffect_2 = value;
		Il2CppCodeGenWriteBarrier((&___splashEffect_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(Bullet_t2590115616, ___player_3)); }
	inline GameObject_t1756533147 * get_player_3() const { return ___player_3; }
	inline GameObject_t1756533147 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1756533147 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLET_T2590115616_H
#ifndef COLLECTABLE_T2201227982_H
#define COLLECTABLE_T2201227982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Collectable
struct  Collectable_t2201227982  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Collectable::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Collectable_t2201227982, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTABLE_T2201227982_H
#ifndef GAMEMANAGER_T2252321495_H
#define GAMEMANAGER_T2252321495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public MonoBehaviour_t1158329972
{
public:
	// Game GameManager::game
	Game_t1600141214 * ___game_2;
	// UIController GameManager::UI
	UIController_t2029583246 * ___UI_3;
	// UnityEngine.GameObject[] GameManager::spawnLocations
	GameObjectU5BU5D_t3057952154* ___spawnLocations_4;
	// UnityEngine.GameObject[] GameManager::shipPartSpawnLocations
	GameObjectU5BU5D_t3057952154* ___shipPartSpawnLocations_5;
	// UnityEngine.GameObject GameManager::player
	GameObject_t1756533147 * ___player_6;
	// UnityEngine.GameObject GameManager::ship
	GameObject_t1756533147 * ___ship_7;
	// UnityEngine.GameObject GameManager::enemyPrefab
	GameObject_t1756533147 * ___enemyPrefab_8;
	// UnityEngine.GameObject GameManager::shipPartPrefab
	GameObject_t1756533147 * ___shipPartPrefab_9;

public:
	inline static int32_t get_offset_of_game_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___game_2)); }
	inline Game_t1600141214 * get_game_2() const { return ___game_2; }
	inline Game_t1600141214 ** get_address_of_game_2() { return &___game_2; }
	inline void set_game_2(Game_t1600141214 * value)
	{
		___game_2 = value;
		Il2CppCodeGenWriteBarrier((&___game_2), value);
	}

	inline static int32_t get_offset_of_UI_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___UI_3)); }
	inline UIController_t2029583246 * get_UI_3() const { return ___UI_3; }
	inline UIController_t2029583246 ** get_address_of_UI_3() { return &___UI_3; }
	inline void set_UI_3(UIController_t2029583246 * value)
	{
		___UI_3 = value;
		Il2CppCodeGenWriteBarrier((&___UI_3), value);
	}

	inline static int32_t get_offset_of_spawnLocations_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___spawnLocations_4)); }
	inline GameObjectU5BU5D_t3057952154* get_spawnLocations_4() const { return ___spawnLocations_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_spawnLocations_4() { return &___spawnLocations_4; }
	inline void set_spawnLocations_4(GameObjectU5BU5D_t3057952154* value)
	{
		___spawnLocations_4 = value;
		Il2CppCodeGenWriteBarrier((&___spawnLocations_4), value);
	}

	inline static int32_t get_offset_of_shipPartSpawnLocations_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___shipPartSpawnLocations_5)); }
	inline GameObjectU5BU5D_t3057952154* get_shipPartSpawnLocations_5() const { return ___shipPartSpawnLocations_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_shipPartSpawnLocations_5() { return &___shipPartSpawnLocations_5; }
	inline void set_shipPartSpawnLocations_5(GameObjectU5BU5D_t3057952154* value)
	{
		___shipPartSpawnLocations_5 = value;
		Il2CppCodeGenWriteBarrier((&___shipPartSpawnLocations_5), value);
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___player_6)); }
	inline GameObject_t1756533147 * get_player_6() const { return ___player_6; }
	inline GameObject_t1756533147 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(GameObject_t1756533147 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}

	inline static int32_t get_offset_of_ship_7() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___ship_7)); }
	inline GameObject_t1756533147 * get_ship_7() const { return ___ship_7; }
	inline GameObject_t1756533147 ** get_address_of_ship_7() { return &___ship_7; }
	inline void set_ship_7(GameObject_t1756533147 * value)
	{
		___ship_7 = value;
		Il2CppCodeGenWriteBarrier((&___ship_7), value);
	}

	inline static int32_t get_offset_of_enemyPrefab_8() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___enemyPrefab_8)); }
	inline GameObject_t1756533147 * get_enemyPrefab_8() const { return ___enemyPrefab_8; }
	inline GameObject_t1756533147 ** get_address_of_enemyPrefab_8() { return &___enemyPrefab_8; }
	inline void set_enemyPrefab_8(GameObject_t1756533147 * value)
	{
		___enemyPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___enemyPrefab_8), value);
	}

	inline static int32_t get_offset_of_shipPartPrefab_9() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___shipPartPrefab_9)); }
	inline GameObject_t1756533147 * get_shipPartPrefab_9() const { return ___shipPartPrefab_9; }
	inline GameObject_t1756533147 ** get_address_of_shipPartPrefab_9() { return &___shipPartPrefab_9; }
	inline void set_shipPartPrefab_9(GameObject_t1756533147 * value)
	{
		___shipPartPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___shipPartPrefab_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T2252321495_H
#ifndef SHIPCONTROLLER_T3277973446_H
#define SHIPCONTROLLER_T3277973446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipController
struct  ShipController_t3277973446  : public MonoBehaviour_t1158329972
{
public:
	// Ship ShipController::ship
	Ship_t1116303770 * ___ship_2;
	// UnityEngine.GameObject ShipController::explosiveEffect
	GameObject_t1756533147 * ___explosiveEffect_3;

public:
	inline static int32_t get_offset_of_ship_2() { return static_cast<int32_t>(offsetof(ShipController_t3277973446, ___ship_2)); }
	inline Ship_t1116303770 * get_ship_2() const { return ___ship_2; }
	inline Ship_t1116303770 ** get_address_of_ship_2() { return &___ship_2; }
	inline void set_ship_2(Ship_t1116303770 * value)
	{
		___ship_2 = value;
		Il2CppCodeGenWriteBarrier((&___ship_2), value);
	}

	inline static int32_t get_offset_of_explosiveEffect_3() { return static_cast<int32_t>(offsetof(ShipController_t3277973446, ___explosiveEffect_3)); }
	inline GameObject_t1756533147 * get_explosiveEffect_3() const { return ___explosiveEffect_3; }
	inline GameObject_t1756533147 ** get_address_of_explosiveEffect_3() { return &___explosiveEffect_3; }
	inline void set_explosiveEffect_3(GameObject_t1756533147 * value)
	{
		___explosiveEffect_3 = value;
		Il2CppCodeGenWriteBarrier((&___explosiveEffect_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPCONTROLLER_T3277973446_H
#ifndef DEATHBARRIER_T1380906595_H
#define DEATHBARRIER_T1380906595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeathBarrier
struct  DeathBarrier_t1380906595  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEATHBARRIER_T1380906595_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef TEXT_T356221433_H
#define TEXT_T356221433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t356221433  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t2614388407 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t647235000 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t647235000 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t3048644023* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_FontData_28)); }
	inline FontData_t2614388407 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t2614388407 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t2614388407 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCache_30)); }
	inline TextGenerator_t647235000 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t647235000 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t647235000 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t647235000 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t3048644023* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t3048644023* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t356221433_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t193706927 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t356221433_StaticFields, ___s_DefaultText_32)); }
	inline Material_t193706927 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t193706927 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t193706927 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T356221433_H
#ifndef IMAGE_T2042527209_H
#define IMAGE_T2042527209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2042527209  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t309593783 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t309593783 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_Sprite_29)); }
	inline Sprite_t309593783 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t309593783 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t309593783 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_OverrideSprite_30)); }
	inline Sprite_t309593783 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t309593783 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t309593783 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2042527209_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t193706927 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t686124026* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t686124026* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1172311765* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1172311765* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t193706927 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t193706927 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t193706927 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t686124026* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t686124026** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t686124026* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t686124026* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t686124026** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t686124026* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1172311765* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1172311765** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1172311765* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1172311765* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1172311765** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1172311765* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2042527209_H
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t3497673348 * m_Items[1];

public:
	inline Collider_t3497673348 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t3497673348 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t3497673348 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider_t3497673348 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t3497673348 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t3497673348 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1756533147 * m_Items[1];

public:
	inline GameObject_t1756533147 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1756533147 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m3829784634_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2724124387_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1825328214 (MonoBehaviour_t1158329972 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C"  GameObject_t1756533147 * GameObject_FindGameObjectWithTag_m1433464258 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<PlayerController>()
#define GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(__this, method) ((  PlayerController_t4148409433 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m3768854296 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m3925508629 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SubWeapon::tick(System.Single)
extern "C"  void SubWeapon_tick_m860919181 (SubWeapon_t1081861670 * __this, float ___time0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Bomb::Explode()
extern "C"  void Bomb_Explode_m1242553278 (Bomb_t2608481788 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m2159020946 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m3490276752 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m2304215762 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m2617026815 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m682534386 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m3374354972 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
extern "C"  ColliderU5BU5D_t462843629* Physics_OverlapSphere_m1592863987 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m124558427 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<EnemyController>()
#define Component_GetComponent_TisEnemyController_t2146768720_m1684476845(__this, method) ((  EnemyController_t2146768720 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void Enemy::takeDamage(System.Int32)
extern "C"  void Enemy_takeDamage_m3649204152 (Enemy_t1088811588 * __this, int32_t ___damage0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m3959286051 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern "C"  GameObject_t1756533147 * Collision_get_gameObject_m1090148537 (Collision_t2876846408 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GameObject::get_tag()
extern "C"  String_t* GameObject_get_tag_m3359901967 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m304203149 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<EnemyController>()
#define GameObject_GetComponent_TisEnemyController_t2146768720_m543378449(__this, method) ((  EnemyController_t2146768720 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C"  void Transform_Rotate_m542935446 (Transform_t3275118058 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m279709565 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<GameManager>()
#define GameObject_GetComponent_TisGameManager_t2252321495_m190008436(__this, method) ((  GameManager_t2252321495 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UIController::updatePartIcon(System.Boolean)
extern "C"  void UIController_updatePartIcon_m265678373 (UIController_t2029583246 * __this, bool ___partHeld0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::partAlreadyHeldMessage()
extern "C"  void UIController_partAlreadyHeldMessage_m1437698322 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::clearSubMessage()
extern "C"  void UIController_clearSubMessage_m1922647077 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::die()
extern "C"  void PlayerController_die_m1474265594 (PlayerController_t4148409433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m3369637820 (Object_t1021602117 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::fallingDeathMessage()
extern "C"  void UIController_fallingDeathMessage_m2237346207 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::drowningDeathMessage()
extern "C"  void UIController_drowningDeathMessage_m3043734374 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::.ctor()
extern "C"  void Enemy__ctor_m2474411757 (Enemy_t1088811588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AI.NavMeshAgent>()
#define Component_GetComponent_TisNavMeshAgent_t2761625415_m3828941945(__this, method) ((  NavMeshAgent_t2761625415 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void EnemyController::determineDestination()
extern "C"  void EnemyController_determineDestination_m3337669536 (EnemyController_t2146768720 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Player::takeDamage(System.Int32)
extern "C"  void Player_takeDamage_m3178363385 (Player_t1147783557 * __this, int32_t ___damage0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::updateDamageFrame(System.Single)
extern "C"  void UIController_updateDamageFrame_m339031365 (UIController_t2029583246 * __this, float ___health0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C"  void Animator_SetBool_m312734517 (Animator_t69676727 * __this, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<ShipController>()
#define GameObject_GetComponent_TisShipController_t3277973446_m2617641097(__this, method) ((  ShipController_t3277973446 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void Ship::takeDamage(System.Int32)
extern "C"  void Ship_takeDamage_m936844050 (Ship_t1116303770 * __this, int32_t ___damage0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m2960507181 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern "C"  bool NavMeshAgent_SetDestination_m679113415 (NavMeshAgent_t2761625415 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2516226135 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Game::.ctor(difficultyLevel)
extern "C"  void Game__ctor_m946268338 (Game_t1600141214 * __this, int32_t ___theDifficulty0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::.ctor()
extern "C"  void UIController__ctor_m4279849967 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::InitGame()
extern "C"  void GameManager_InitGame_m4160192178 (GameManager_t2252321495 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::initializeUI()
extern "C"  void UIController_initializeUI_m1509084315 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern "C"  void Cursor_set_visible_m339443736 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
extern "C"  void Cursor_set_lockState_m1427485448 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C"  GameObjectU5BU5D_t3057952154* GameObject_FindGameObjectsWithTag_m3720927271 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::updateTime()
extern "C"  void GameManager_updateTime_m860194674 (GameManager_t2252321495 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::checkForDeath()
extern "C"  void GameManager_checkForDeath_m2230622135 (GameManager_t2252321495 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::updateTime(System.Single)
extern "C"  void UIController_updateTime_m340223966 (UIController_t2029583246 * __this, float ___time0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::countDown(System.Single)
extern "C"  void UIController_countDown_m4243092691 (UIController_t2029583246 * __this, float ___time0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::waveCountdownMessage(System.Int32,System.Single)
extern "C"  void UIController_waveCountdownMessage_m2891189168 (UIController_t2029583246 * __this, int32_t ___waveNumber0, float ___roundDelay1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::beginWave(System.Int32)
extern "C"  void UIController_beginWave_m463379384 (UIController_t2029583246 * __this, int32_t ___waveNumber0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Game::resetTimeRemaining()
extern "C"  void Game_resetTimeRemaining_m3671941965 (Game_t1600141214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::updatePartsReturned(System.Int32,System.Int32)
extern "C"  void UIController_updatePartsReturned_m494708189 (UIController_t2029583246 * __this, int32_t ___returned0, int32_t ___required1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::timeUp()
extern "C"  void GameManager_timeUp_m1204656286 (GameManager_t2252321495 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::spawnEnemies()
extern "C"  void GameManager_spawnEnemies_m1229942693 (GameManager_t2252321495 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::spawnShipParts()
extern "C"  void GameManager_spawnShipParts_m906813607 (GameManager_t2252321495 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::gameWonMessage()
extern "C"  void UIController_gameWonMessage_m284319100 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::waveCompleteMessage(System.Int32)
extern "C"  void UIController_waveCompleteMessage_m112210205 (UIController_t2029583246 * __this, int32_t ___waveNumber0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Game::advanceWave()
extern "C"  void Game_advanceWave_m3558657402 (Game_t1600141214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::partsNotCollectedMessage()
extern "C"  void UIController_partsNotCollectedMessage_m867535828 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m3327624272 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Game::resetSpawnDelay()
extern "C"  void Game_resetSpawnDelay_m3164128188 (Game_t1600141214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m2923680153 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Game::resetShipPartSpawnDelay()
extern "C"  void Game_resetShipPartSpawnDelay_m2707294655 (Game_t1600141214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::healthDeathMessage()
extern "C"  void UIController_healthDeathMessage_m2671790766 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::destroySpawnLocations()
extern "C"  void GameManager_destroySpawnLocations_m3820108133 (GameManager_t2252321495 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::shipDestroyedMessage()
extern "C"  void UIController_shipDestroyedMessage_m4291651381 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m1779415170 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MainWeapon::.ctor(ammoType)
extern "C"  void MainWeapon__ctor_m47867042 (MainWeapon_t1214762455 * __this, int32_t ___theAmmo0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SubWeapon::.ctor(bombType)
extern "C"  void SubWeapon__ctor_m954666591 (SubWeapon_t1081861670 * __this, int32_t ___theType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Player::.ctor(ammoType,bombType)
extern "C"  void Player__ctor_m3620100752 (Player_t1147783557 * __this, int32_t ___theAmmoType0, int32_t ___theBombtype1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CharacterController>()
#define Component_GetComponent_TisCharacterController_t4094781467_m1582798737(__this, method) ((  CharacterController_t4094781467 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void PlayerController::rotate()
extern "C"  void PlayerController_rotate_m1444289841 (PlayerController_t4148409433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::move()
extern "C"  void PlayerController_move_m1170970717 (PlayerController_t4148409433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::sprint()
extern "C"  void PlayerController_sprint_m4044474542 (PlayerController_t4148409433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::jump()
extern "C"  void PlayerController_jump_m563400686 (PlayerController_t4148409433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::shoot()
extern "C"  void PlayerController_shoot_m9744895 (PlayerController_t4148409433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::throwBomb()
extern "C"  void PlayerController_throwBomb_m3049288080 (PlayerController_t4148409433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::regenerateHealth()
extern "C"  void PlayerController_regenerateHealth_m1430467426 (PlayerController_t4148409433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::checkForDeath()
extern "C"  void PlayerController_checkForDeath_m1722688921 (PlayerController_t4148409433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C"  float Input_GetAxis_m948504553 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::get_isGrounded()
extern "C"  bool CharacterController_get_isGrounded_m172388764 (CharacterController_t4094781467 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C"  Vector3_t2243707580  Physics_get_gravity_m1035197364 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1555724485 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_TransformDirection_m505001666 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m2498445460 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C"  int32_t CharacterController_Move_m388312410 (CharacterController_t4094781467 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m881971336 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_localEulerAngles_m233727872 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C"  bool Input_GetButtonDown_m717298472 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
extern "C"  bool Input_GetKeyDown_m3563899393 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyUp(System.String)
extern "C"  bool Input_GetKeyUp_m682874492 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
#define GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(__this, method) ((  Rigidbody_t4233889191 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t2243707580  Transform_get_forward_m2144220796 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m3639708911 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SubWeapon::resetBomb()
extern "C"  void SubWeapon_resetBomb_m1651505034 (SubWeapon_t1081861670 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKey(System.String)
extern "C"  bool Input_GetKey_m2374355427 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButton(System.String)
extern "C"  bool Input_GetButton_m1292624864 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MainWeapon::discharge(System.Single)
extern "C"  void MainWeapon_discharge_m3402882019 (MainWeapon_t1214762455 * __this, float ___time0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MainWeapon::recharge(System.Single)
extern "C"  void MainWeapon_recharge_m3583788820 (MainWeapon_t1214762455 * __this, float ___time0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Player::regenHealth(System.Single)
extern "C"  void Player_regenHealth_m1787165012 (Player_t1147783557 * __this, float ___time0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Ship::.ctor()
extern "C"  void Ship__ctor_m4202732153 (Ship_t1116303770 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Game::returnPart()
extern "C"  void Game_returnPart_m2055149738 (Game_t1600141214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UIController::shipUnderAttackMessage()
extern "C"  void UIController_shipUnderAttackMessage_m859257986 (UIController_t2029583246 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t189460977_m4200645945(__this, method) ((  Camera_t189460977 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t2243707580  Transform_get_localScale_m46214814 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m1442831667 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Subtraction_m4046047256 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m1970949065 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2042527209_m4162535761(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.UI.Image::set_fillAmount(System.Single)
extern "C"  void Image_set_fillAmount_m2220966753 (Image_t2042527209 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2693135142 (GameObject_t1756533147 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2020392075  Color_get_white_m4252342900 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t2020392075  Color_get_red_m3374418718 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m2000667605 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString(System.String)
extern "C"  String_t* Single_ToString_m2359963436 (float* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3881798623 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Bomb::.ctor()
extern "C"  void Bomb__ctor_m1894427141 (Bomb_t2608481788 * __this, const RuntimeMethod* method)
{
	{
		__this->set_explosionRadius_5((3.0f));
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Bomb::Start()
extern "C"  void Bomb_Start_m3332595677 (Bomb_t2608481788 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bomb_Start_m3332595677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		__this->set_player_4(L_0);
		GameObject_t1756533147 * L_1 = __this->get_player_4();
		PlayerController_t4148409433 * L_2 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_1, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_3 = L_2->get_grubby_2();
		SubWeapon_t1081861670 * L_4 = L_3->get_omNomBomb_1();
		__this->set_omNomBomb_2(L_4);
		return;
	}
}
// System.Void Bomb::Update()
extern "C"  void Bomb_Update_m1851208838 (Bomb_t2608481788 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bomb_Update_m1851208838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_player_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005f;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_player_4();
		PlayerController_t4148409433 * L_3 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_2, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_4 = L_3->get_grubby_2();
		SubWeapon_t1081861670 * L_5 = L_4->get_omNomBomb_1();
		float L_6 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		SubWeapon_tick_m860919181(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_player_4();
		PlayerController_t4148409433 * L_8 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_7, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_9 = L_8->get_grubby_2();
		SubWeapon_t1081861670 * L_10 = L_9->get_omNomBomb_1();
		float L_11 = L_10->get_delay_2();
		if ((!(((float)L_11) <= ((float)(0.0f)))))
		{
			goto IL_005a;
		}
	}
	{
		Bomb_Explode_m1242553278(__this, /*hidden argument*/NULL);
	}

IL_005a:
	{
		goto IL_0065;
	}

IL_005f:
	{
		Bomb_Explode_m1242553278(__this, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void Bomb::Explode()
extern "C"  void Bomb_Explode_m1242553278 (Bomb_t2608481788 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bomb_Explode_m1242553278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ColliderU5BU5D_t462843629* V_1 = NULL;
	int32_t V_2 = 0;
	Collider_t3497673348 * V_3 = NULL;
	ColliderU5BU5D_t462843629* V_4 = NULL;
	int32_t V_5 = 0;
	{
		GameObject_t1756533147 * L_0 = __this->get_explosiveEffect_3();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m3490276752(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Transform_get_position_m2304215762(L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m3490276752(L_4, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_6 = Transform_get_rotation_m2617026815(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_7 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_0, L_3, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		Object_Destroy_m682534386(NULL /*static, unused*/, L_7, (4.0f), /*hidden argument*/NULL);
		Transform_t3275118058 * L_8 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Transform_get_position_m2304215762(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Vector3_t2243707580  L_10 = V_0;
		float L_11 = __this->get_explosionRadius_5();
		ColliderU5BU5D_t462843629* L_12 = Physics_OverlapSphere_m1592863987(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		V_2 = 0;
		ColliderU5BU5D_t462843629* L_13 = V_1;
		V_4 = L_13;
		V_5 = 0;
		goto IL_00bb;
	}

IL_005b:
	{
		ColliderU5BU5D_t462843629* L_14 = V_4;
		int32_t L_15 = V_5;
		int32_t L_16 = L_15;
		Collider_t3497673348 * L_17 = (L_14)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_16));
		V_3 = L_17;
		Collider_t3497673348 * L_18 = V_3;
		String_t* L_19 = Component_get_tag_m124558427(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_19, _stringLiteral1816890106, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b5;
		}
	}
	{
		GameObject_t1756533147 * L_21 = __this->get_player_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00b5;
		}
	}
	{
		Collider_t3497673348 * L_23 = V_3;
		EnemyController_t2146768720 * L_24 = Component_GetComponent_TisEnemyController_t2146768720_m1684476845(L_23, /*hidden argument*/Component_GetComponent_TisEnemyController_t2146768720_m1684476845_RuntimeMethod_var);
		Enemy_t1088811588 * L_25 = L_24->get_tasteless_2();
		GameObject_t1756533147 * L_26 = __this->get_player_4();
		PlayerController_t4148409433 * L_27 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_26, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_28 = L_27->get_grubby_2();
		SubWeapon_t1081861670 * L_29 = L_28->get_omNomBomb_1();
		int32_t L_30 = L_29->get_damage_1();
		Enemy_takeDamage_m3649204152(L_25, L_30, /*hidden argument*/NULL);
		int32_t L_31 = V_2;
		V_2 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_00b5:
	{
		int32_t L_32 = V_5;
		V_5 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00bb:
	{
		int32_t L_33 = V_5;
		ColliderU5BU5D_t462843629* L_34 = V_4;
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_34)->max_length)))))))
		{
			goto IL_005b;
		}
	}
	{
		GameObject_t1756533147 * L_35 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Bullet::.ctor()
extern "C"  void Bullet__ctor_m2895089121 (Bullet_t2590115616 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Bullet::Start()
extern "C"  void Bullet_Start_m1447617473 (Bullet_t2590115616 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bullet_Start_m1447617473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		__this->set_player_3(L_0);
		return;
	}
}
// System.Void Bullet::Update()
extern "C"  void Bullet_Update_m524047786 (Bullet_t2590115616 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Bullet::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Bullet_OnCollisionEnter_m314193427 (Bullet_t2590115616 * __this, Collision_t2876846408 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bullet_OnCollisionEnter_m314193427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collision_t2876846408 * L_0 = ___other0;
		GameObject_t1756533147 * L_1 = Collision_get_gameObject_m1090148537(L_0, /*hidden argument*/NULL);
		String_t* L_2 = GameObject_get_tag_m3359901967(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_2, _stringLiteral3318194134, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00b4;
		}
	}
	{
		Collision_t2876846408 * L_4 = ___other0;
		GameObject_t1756533147 * L_5 = Collision_get_gameObject_m1090148537(L_4, /*hidden argument*/NULL);
		String_t* L_6 = GameObject_get_tag_m3359901967(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral1816890106, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0074;
		}
	}
	{
		GameObject_t1756533147 * L_8 = __this->get_player_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_8, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0074;
		}
	}
	{
		Collision_t2876846408 * L_10 = ___other0;
		GameObject_t1756533147 * L_11 = Collision_get_gameObject_m1090148537(L_10, /*hidden argument*/NULL);
		EnemyController_t2146768720 * L_12 = GameObject_GetComponent_TisEnemyController_t2146768720_m543378449(L_11, /*hidden argument*/GameObject_GetComponent_TisEnemyController_t2146768720_m543378449_RuntimeMethod_var);
		Enemy_t1088811588 * L_13 = L_12->get_tasteless_2();
		GameObject_t1756533147 * L_14 = __this->get_player_3();
		PlayerController_t4148409433 * L_15 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_14, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_16 = L_15->get_grubby_2();
		MainWeapon_t1214762455 * L_17 = L_16->get_blenderBlaster_0();
		int32_t L_18 = L_17->get_damage_2();
		Enemy_takeDamage_m3649204152(L_13, L_18, /*hidden argument*/NULL);
	}

IL_0074:
	{
		GameObject_t1756533147 * L_19 = __this->get_splashEffect_2();
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m3490276752(L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = Transform_get_position_m2304215762(L_21, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m3490276752(L_23, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_25 = Transform_get_rotation_m2617026815(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_26 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_19, L_22, L_25, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		Object_Destroy_m682534386(NULL /*static, unused*/, L_26, (2.0f), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		return;
	}
}
// System.Void Collectable::.ctor()
extern "C"  void Collectable__ctor_m1539653007 (Collectable_t2201227982 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_2((2.0f));
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Collectable::Update()
extern "C"  void Collectable_Update_m1042715772 (Collectable_t2201227982 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_speed_2();
		Transform_Rotate_m542935446(L_0, (0.0f), L_1, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Collectable::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Collectable_OnTriggerEnter_m1201684067 (Collectable_t2201227982 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collectable_OnTriggerEnter_m1201684067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(L_0, /*hidden argument*/NULL);
		String_t* L_2 = GameObject_get_tag_m3359901967(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0088;
		}
	}
	{
		Collider_t3497673348 * L_4 = ___other0;
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m2159020946(L_4, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_6 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_5, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_7 = L_6->get_grubby_2();
		bool L_8 = L_7->get_partHeld_9();
		if (L_8)
		{
			goto IL_0088;
		}
	}
	{
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Collider_t3497673348 * L_10 = ___other0;
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m2159020946(L_10, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_12 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_11, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_13 = L_12->get_grubby_2();
		L_13->set_partHeld_9((bool)1);
		GameObject_t1756533147 * L_14 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_15 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_14, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_16 = L_15->get_UI_3();
		Collider_t3497673348 * L_17 = ___other0;
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m2159020946(L_17, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_19 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_18, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_20 = L_19->get_grubby_2();
		bool L_21 = L_20->get_partHeld_9();
		UIController_updatePartIcon_m265678373(L_16, L_21, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_0088:
	{
		Collider_t3497673348 * L_22 = ___other0;
		GameObject_t1756533147 * L_23 = Component_get_gameObject_m2159020946(L_22, /*hidden argument*/NULL);
		String_t* L_24 = GameObject_get_tag_m3359901967(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_24, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d5;
		}
	}
	{
		Collider_t3497673348 * L_26 = ___other0;
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m2159020946(L_26, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_28 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_27, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_29 = L_28->get_grubby_2();
		bool L_30 = L_29->get_partHeld_9();
		if (!L_30)
		{
			goto IL_00d5;
		}
	}
	{
		GameObject_t1756533147 * L_31 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_32 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_31, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_33 = L_32->get_UI_3();
		UIController_partAlreadyHeldMessage_m1437698322(L_33, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		return;
	}
}
// System.Void Collectable::OnTriggerExit(UnityEngine.Collider)
extern "C"  void Collectable_OnTriggerExit_m1418933007 (Collectable_t2201227982 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collectable_OnTriggerExit_m1418933007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_1 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_0, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_2 = L_1->get_UI_3();
		UIController_clearSubMessage_m1922647077(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DeathBarrier::.ctor()
extern "C"  void DeathBarrier__ctor_m2592197156 (DeathBarrier_t1380906595 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DeathBarrier::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void DeathBarrier_OnTriggerEnter_m1529541608 (DeathBarrier_t1380906595 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeathBarrier_OnTriggerEnter_m1529541608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(L_0, /*hidden argument*/NULL);
		String_t* L_2 = GameObject_get_tag_m3359901967(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0095;
		}
	}
	{
		Collider_t3497673348 * L_4 = ___other0;
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m2159020946(L_4, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_6 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_5, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		PlayerController_die_m1474265594(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_8 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_7, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		Game_t1600141214 * L_9 = L_8->get_game_2();
		L_9->set_won_16((bool)0);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		String_t* L_11 = Object_get_name_m3369637820(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral879651274, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007c;
		}
	}
	{
		GameObject_t1756533147 * L_13 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_14 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_13, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_15 = L_14->get_UI_3();
		UIController_fallingDeathMessage_m2237346207(L_15, /*hidden argument*/NULL);
		goto IL_0095;
	}

IL_007c:
	{
		GameObject_t1756533147 * L_16 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_17 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_16, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_18 = L_17->get_UI_3();
		UIController_drowningDeathMessage_m3043734374(L_18, /*hidden argument*/NULL);
	}

IL_0095:
	{
		Collider_t3497673348 * L_19 = ___other0;
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m2159020946(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Enemy::.ctor()
extern "C"  void Enemy__ctor_m2474411757 (Enemy_t1088811588 * __this, const RuntimeMethod* method)
{
	{
		__this->set_health_0((100.0f));
		__this->set_attackStrength_1(((int32_t)15));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Enemy::takeDamage(System.Int32)
extern "C"  void Enemy_takeDamage_m3649204152 (Enemy_t1088811588 * __this, int32_t ___damage0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_health_0();
		int32_t L_1 = ___damage0;
		__this->set_health_0(((float)((float)L_0-(float)(((float)((float)L_1))))));
		return;
	}
}
// System.Void EnemyController::.ctor()
extern "C"  void EnemyController__ctor_m1153179309 (EnemyController_t2146768720 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController__ctor_m1153179309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enemy_t1088811588 * L_0 = (Enemy_t1088811588 *)il2cpp_codegen_object_new(Enemy_t1088811588_il2cpp_TypeInfo_var);
		Enemy__ctor_m2474411757(L_0, /*hidden argument*/NULL);
		__this->set_tasteless_2(L_0);
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyController::Awake()
extern "C"  void EnemyController_Awake_m1186140764 (EnemyController_t2146768720 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_Awake_m1186140764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		__this->set_player_3(L_0);
		GameObject_t1756533147 * L_1 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1844382288, /*hidden argument*/NULL);
		__this->set_ship_4(L_1);
		NavMeshAgent_t2761625415 * L_2 = Component_GetComponent_TisNavMeshAgent_t2761625415_m3828941945(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t2761625415_m3828941945_RuntimeMethod_var);
		__this->set_nav_5(L_2);
		Animator_t69676727 * L_3 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_RuntimeMethod_var);
		__this->set_anim_6(L_3);
		return;
	}
}
// System.Void EnemyController::Update()
extern "C"  void EnemyController_Update_m3131616442 (EnemyController_t2146768720 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_Update_m3131616442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EnemyController_determineDestination_m3337669536(__this, /*hidden argument*/NULL);
		Enemy_t1088811588 * L_0 = __this->get_tasteless_2();
		Enemy_t1088811588 * L_1 = L_0;
		float L_2 = L_1->get_attackDelay_2();
		float L_3 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_1->set_attackDelay_2(((float)((float)L_2-(float)L_3)));
		Enemy_t1088811588 * L_4 = __this->get_tasteless_2();
		float L_5 = L_4->get_health_0();
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			goto IL_003d;
		}
	}
	{
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void EnemyController::OnTriggerStay(UnityEngine.Collider)
extern "C"  void EnemyController_OnTriggerStay_m3542192760 (EnemyController_t2146768720 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_OnTriggerStay_m3542192760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		String_t* L_1 = Component_get_tag_m124558427(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b4;
		}
	}
	{
		Enemy_t1088811588 * L_3 = __this->get_tasteless_2();
		float L_4 = L_3->get_attackDelay_2();
		if ((!(((float)L_4) <= ((float)(0.0f)))))
		{
			goto IL_00b4;
		}
	}
	{
		Collider_t3497673348 * L_5 = ___other0;
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m2159020946(L_5, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_7 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_6, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_8 = L_7->get_grubby_2();
		Enemy_t1088811588 * L_9 = __this->get_tasteless_2();
		int32_t L_10 = L_9->get_attackStrength_1();
		Player_takeDamage_m3178363385(L_8, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_12 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_11, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_13 = L_12->get_UI_3();
		Collider_t3497673348 * L_14 = ___other0;
		GameObject_t1756533147 * L_15 = Component_get_gameObject_m2159020946(L_14, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_16 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_15, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_17 = L_16->get_grubby_2();
		float L_18 = L_17->get_health_7();
		UIController_updateDamageFrame_m339031365(L_13, L_18, /*hidden argument*/NULL);
		Enemy_t1088811588 * L_19 = __this->get_tasteless_2();
		L_19->set_attackDelay_2((1.5f));
		Enemy_t1088811588 * L_20 = __this->get_tasteless_2();
		L_20->set_attacking_3((bool)1);
		Animator_t69676727 * L_21 = __this->get_anim_6();
		Enemy_t1088811588 * L_22 = __this->get_tasteless_2();
		bool L_23 = L_22->get_attacking_3();
		Animator_SetBool_m312734517(L_21, _stringLiteral574095090, L_23, /*hidden argument*/NULL);
		goto IL_0135;
	}

IL_00b4:
	{
		Collider_t3497673348 * L_24 = ___other0;
		String_t* L_25 = Object_get_name_m3369637820(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_25, _stringLiteral1844382288, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0135;
		}
	}
	{
		Enemy_t1088811588 * L_27 = __this->get_tasteless_2();
		float L_28 = L_27->get_attackDelay_2();
		if ((!(((float)L_28) <= ((float)(0.0f)))))
		{
			goto IL_0135;
		}
	}
	{
		Collider_t3497673348 * L_29 = ___other0;
		GameObject_t1756533147 * L_30 = Component_get_gameObject_m2159020946(L_29, /*hidden argument*/NULL);
		ShipController_t3277973446 * L_31 = GameObject_GetComponent_TisShipController_t3277973446_m2617641097(L_30, /*hidden argument*/GameObject_GetComponent_TisShipController_t3277973446_m2617641097_RuntimeMethod_var);
		Ship_t1116303770 * L_32 = L_31->get_ship_2();
		Enemy_t1088811588 * L_33 = __this->get_tasteless_2();
		int32_t L_34 = L_33->get_attackStrength_1();
		Ship_takeDamage_m936844050(L_32, L_34, /*hidden argument*/NULL);
		Enemy_t1088811588 * L_35 = __this->get_tasteless_2();
		L_35->set_attackDelay_2((1.5f));
		Enemy_t1088811588 * L_36 = __this->get_tasteless_2();
		L_36->set_attacking_3((bool)1);
		Animator_t69676727 * L_37 = __this->get_anim_6();
		Enemy_t1088811588 * L_38 = __this->get_tasteless_2();
		bool L_39 = L_38->get_attacking_3();
		Animator_SetBool_m312734517(L_37, _stringLiteral574095090, L_39, /*hidden argument*/NULL);
	}

IL_0135:
	{
		return;
	}
}
// System.Void EnemyController::OnTriggerExit(UnityEngine.Collider)
extern "C"  void EnemyController_OnTriggerExit_m943580849 (EnemyController_t2146768720 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_OnTriggerExit_m943580849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enemy_t1088811588 * L_0 = __this->get_tasteless_2();
		L_0->set_attacking_3((bool)0);
		Animator_t69676727 * L_1 = __this->get_anim_6();
		Enemy_t1088811588 * L_2 = __this->get_tasteless_2();
		bool L_3 = L_2->get_attacking_3();
		Animator_SetBool_m312734517(L_1, _stringLiteral574095090, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyController::determineDestination()
extern "C"  void EnemyController_determineDestination_m3337669536 (EnemyController_t2146768720 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_determineDestination_m3337669536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		GameObject_t1756533147 * L_0 = __this->get_player_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d9;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_ship_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00d9;
		}
	}
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Transform_get_position_m2304215762(L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_player_3();
		Transform_t3275118058 * L_7 = GameObject_get_transform_m3490276752(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Transform_get_position_m2304215762(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		float L_9 = Vector3_Distance_m2960507181(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Transform_t3275118058 * L_10 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Transform_get_position_m2304215762(L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = __this->get_ship_4();
		Transform_t3275118058 * L_13 = GameObject_get_transform_m3490276752(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Transform_get_position_m2304215762(L_13, /*hidden argument*/NULL);
		float L_15 = Vector3_Distance_m2960507181(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = V_0;
		float L_17 = V_1;
		if ((!(((float)L_16) <= ((float)L_17))))
		{
			goto IL_008c;
		}
	}
	{
		NavMeshAgent_t2761625415 * L_18 = __this->get_nav_5();
		GameObject_t1756533147 * L_19 = __this->get_player_3();
		Transform_t3275118058 * L_20 = GameObject_get_transform_m3490276752(L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Transform_get_position_m2304215762(L_20, /*hidden argument*/NULL);
		NavMeshAgent_SetDestination_m679113415(L_18, L_21, /*hidden argument*/NULL);
		goto IL_00a8;
	}

IL_008c:
	{
		NavMeshAgent_t2761625415 * L_22 = __this->get_nav_5();
		GameObject_t1756533147 * L_23 = __this->get_ship_4();
		Transform_t3275118058 * L_24 = GameObject_get_transform_m3490276752(L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Transform_get_position_m2304215762(L_24, /*hidden argument*/NULL);
		NavMeshAgent_SetDestination_m679113415(L_22, L_25, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		Enemy_t1088811588 * L_26 = __this->get_tasteless_2();
		bool L_27 = L_26->get_attacking_3();
		if (!L_27)
		{
			goto IL_00d4;
		}
	}
	{
		NavMeshAgent_t2761625415 * L_28 = __this->get_nav_5();
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_30 = GameObject_get_transform_m3490276752(L_29, /*hidden argument*/NULL);
		Vector3_t2243707580  L_31 = Transform_get_position_m2304215762(L_30, /*hidden argument*/NULL);
		NavMeshAgent_SetDestination_m679113415(L_28, L_31, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		goto IL_015a;
	}

IL_00d9:
	{
		GameObject_t1756533147 * L_32 = __this->get_ship_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_32, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_011c;
		}
	}
	{
		GameObject_t1756533147 * L_34 = __this->get_player_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_35 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_34, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_011c;
		}
	}
	{
		NavMeshAgent_t2761625415 * L_36 = __this->get_nav_5();
		GameObject_t1756533147 * L_37 = __this->get_player_3();
		Transform_t3275118058 * L_38 = GameObject_get_transform_m3490276752(L_37, /*hidden argument*/NULL);
		Vector3_t2243707580  L_39 = Transform_get_position_m2304215762(L_38, /*hidden argument*/NULL);
		NavMeshAgent_SetDestination_m679113415(L_36, L_39, /*hidden argument*/NULL);
		goto IL_015a;
	}

IL_011c:
	{
		GameObject_t1756533147 * L_40 = __this->get_ship_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_40, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_015a;
		}
	}
	{
		GameObject_t1756533147 * L_42 = __this->get_player_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_43 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_42, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_015a;
		}
	}
	{
		NavMeshAgent_t2761625415 * L_44 = __this->get_nav_5();
		GameObject_t1756533147 * L_45 = __this->get_ship_4();
		Transform_t3275118058 * L_46 = GameObject_get_transform_m3490276752(L_45, /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = Transform_get_position_m2304215762(L_46, /*hidden argument*/NULL);
		NavMeshAgent_SetDestination_m679113415(L_44, L_47, /*hidden argument*/NULL);
	}

IL_015a:
	{
		return;
	}
}
// System.Void Game::.ctor(difficultyLevel)
extern "C"  void Game__ctor_m946268338 (Game_t1600141214 * __this, int32_t ___theDifficulty0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		__this->set_wave_17(1);
		__this->set_players_18(1);
		__this->set_betweenWaveDelay_22((8.0f));
		__this->set_partSpawnAmount_24((1.0f));
		__this->set_betweenWaves_27((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___theDifficulty0;
		__this->set_difficulty_15(L_0);
		int32_t L_1 = __this->get_difficulty_15();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0093;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_00ce;
		}
	}
	{
		goto IL_0109;
	}

IL_0058:
	{
		__this->set_partsRequired_20(2);
		__this->set_timeRemaining_21((90.0f));
		__this->set_partSpawnDelay_23((22.5f));
		__this->set_enemySpawnDelay_25((15.0f));
		int32_t L_5 = __this->get_players_18();
		__this->set_enemySpawnAmount_26(((int32_t)((int32_t)3*(int32_t)L_5)));
		goto IL_0109;
	}

IL_0093:
	{
		__this->set_partsRequired_20(5);
		__this->set_timeRemaining_21((120.0f));
		__this->set_partSpawnDelay_23((15.0f));
		__this->set_enemySpawnDelay_25((8.0f));
		int32_t L_6 = __this->get_players_18();
		__this->set_enemySpawnAmount_26(((int32_t)((int32_t)4*(int32_t)L_6)));
		goto IL_0109;
	}

IL_00ce:
	{
		__this->set_partsRequired_20(8);
		__this->set_timeRemaining_21((120.0f));
		__this->set_partSpawnDelay_23((8.5f));
		__this->set_enemySpawnDelay_25((4.0f));
		int32_t L_7 = __this->get_players_18();
		__this->set_enemySpawnAmount_26(((int32_t)((int32_t)4*(int32_t)L_7)));
		goto IL_0109;
	}

IL_0109:
	{
		return;
	}
}
// System.Void Game::advanceWave()
extern "C"  void Game_advanceWave_m3558657402 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_wave_17();
		__this->set_wave_17(((int32_t)((int32_t)L_0+(int32_t)1)));
		return;
	}
}
// System.Void Game::returnPart()
extern "C"  void Game_returnPart_m2055149738 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_partsReturned_19();
		__this->set_partsReturned_19(((int32_t)((int32_t)L_0+(int32_t)1)));
		return;
	}
}
// System.Void Game::resetSpawnDelay()
extern "C"  void Game_resetSpawnDelay_m3164128188 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_difficulty_15();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0040;
		}
	}
	{
		goto IL_0050;
	}

IL_0020:
	{
		__this->set_enemySpawnDelay_25((15.0f));
		goto IL_0050;
	}

IL_0030:
	{
		__this->set_enemySpawnDelay_25((8.0f));
		goto IL_0050;
	}

IL_0040:
	{
		__this->set_enemySpawnDelay_25((4.0f));
		goto IL_0050;
	}

IL_0050:
	{
		return;
	}
}
// System.Void Game::resetShipPartSpawnDelay()
extern "C"  void Game_resetShipPartSpawnDelay_m2707294655 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_difficulty_15();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0040;
		}
	}
	{
		goto IL_0050;
	}

IL_0020:
	{
		__this->set_partSpawnDelay_23((22.5f));
		goto IL_0050;
	}

IL_0030:
	{
		__this->set_partSpawnDelay_23((15.0f));
		goto IL_0050;
	}

IL_0040:
	{
		__this->set_partSpawnDelay_23((8.5f));
		goto IL_0050;
	}

IL_0050:
	{
		return;
	}
}
// System.Void Game::resetTimeRemaining()
extern "C"  void Game_resetTimeRemaining_m3671941965 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_difficulty_15();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0040;
		}
	}
	{
		goto IL_0050;
	}

IL_0020:
	{
		__this->set_timeRemaining_21((90.0f));
		goto IL_0050;
	}

IL_0030:
	{
		__this->set_timeRemaining_21((120.0f));
		goto IL_0050;
	}

IL_0040:
	{
		__this->set_timeRemaining_21((120.0f));
		goto IL_0050;
	}

IL_0050:
	{
		return;
	}
}
// System.Void GameManager::.ctor()
extern "C"  void GameManager__ctor_m293624896 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager__ctor_m293624896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_t1600141214 * L_0 = (Game_t1600141214 *)il2cpp_codegen_object_new(Game_t1600141214_il2cpp_TypeInfo_var);
		Game__ctor_m946268338(L_0, 0, /*hidden argument*/NULL);
		__this->set_game_2(L_0);
		UIController_t2029583246 * L_1 = (UIController_t2029583246 *)il2cpp_codegen_object_new(UIController_t2029583246_il2cpp_TypeInfo_var);
		UIController__ctor_m4279849967(L_1, /*hidden argument*/NULL);
		__this->set_UI_3(L_1);
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Awake()
extern "C"  void GameManager_Awake_m99497495 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	{
		GameManager_InitGame_m4160192178(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::InitGame()
extern "C"  void GameManager_InitGame_m4160192178 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_InitGame_m4160192178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIController_t2029583246 * L_0 = __this->get_UI_3();
		UIController_initializeUI_m1509084315(L_0, /*hidden argument*/NULL);
		Cursor_set_visible_m339443736(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		Cursor_set_lockState_m1427485448(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_1 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral2310044285, /*hidden argument*/NULL);
		__this->set_spawnLocations_4(L_1);
		GameObjectU5BU5D_t3057952154* L_2 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral1255533420, /*hidden argument*/NULL);
		__this->set_shipPartSpawnLocations_5(L_2);
		GameObject_t1756533147 * L_3 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		__this->set_player_6(L_3);
		GameObject_t1756533147 * L_4 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1844382288, /*hidden argument*/NULL);
		__this->set_ship_7(L_4);
		return;
	}
}
// System.Void GameManager::Update()
extern "C"  void GameManager_Update_m969954595 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	{
		GameManager_updateTime_m860194674(__this, /*hidden argument*/NULL);
		GameManager_checkForDeath_m2230622135(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::updateTime()
extern "C"  void GameManager_updateTime_m860194674 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	{
		UIController_t2029583246 * L_0 = __this->get_UI_3();
		Game_t1600141214 * L_1 = __this->get_game_2();
		float L_2 = L_1->get_timeRemaining_21();
		UIController_updateTime_m340223966(L_0, L_2, /*hidden argument*/NULL);
		UIController_t2029583246 * L_3 = __this->get_UI_3();
		float L_4 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		UIController_countDown_m4243092691(L_3, L_4, /*hidden argument*/NULL);
		Game_t1600141214 * L_5 = __this->get_game_2();
		bool L_6 = L_5->get_betweenWaves_27();
		if (!L_6)
		{
			goto IL_00f2;
		}
	}
	{
		Game_t1600141214 * L_7 = __this->get_game_2();
		Game_t1600141214 * L_8 = L_7;
		float L_9 = L_8->get_betweenWaveDelay_22();
		float L_10 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_8->set_betweenWaveDelay_22(((float)((float)L_9-(float)L_10)));
		UIController_t2029583246 * L_11 = __this->get_UI_3();
		Game_t1600141214 * L_12 = __this->get_game_2();
		int32_t L_13 = L_12->get_wave_17();
		Game_t1600141214 * L_14 = __this->get_game_2();
		float L_15 = L_14->get_betweenWaveDelay_22();
		UIController_waveCountdownMessage_m2891189168(L_11, L_13, L_15, /*hidden argument*/NULL);
		Game_t1600141214 * L_16 = __this->get_game_2();
		float L_17 = L_16->get_betweenWaveDelay_22();
		if ((!(((float)L_17) <= ((float)(0.0f)))))
		{
			goto IL_00ed;
		}
	}
	{
		Game_t1600141214 * L_18 = __this->get_game_2();
		bool L_19 = L_18->get_won_16();
		if (L_19)
		{
			goto IL_00ed;
		}
	}
	{
		UIController_t2029583246 * L_20 = __this->get_UI_3();
		Game_t1600141214 * L_21 = __this->get_game_2();
		int32_t L_22 = L_21->get_wave_17();
		UIController_beginWave_m463379384(L_20, L_22, /*hidden argument*/NULL);
		Game_t1600141214 * L_23 = __this->get_game_2();
		L_23->set_betweenWaves_27((bool)0);
		Game_t1600141214 * L_24 = __this->get_game_2();
		Game_resetTimeRemaining_m3671941965(L_24, /*hidden argument*/NULL);
		Game_t1600141214 * L_25 = __this->get_game_2();
		L_25->set_partsReturned_19(0);
		UIController_t2029583246 * L_26 = __this->get_UI_3();
		Game_t1600141214 * L_27 = __this->get_game_2();
		int32_t L_28 = L_27->get_partsReturned_19();
		Game_t1600141214 * L_29 = __this->get_game_2();
		int32_t L_30 = L_29->get_partsRequired_20();
		UIController_updatePartsReturned_m494708189(L_26, L_28, L_30, /*hidden argument*/NULL);
	}

IL_00ed:
	{
		goto IL_01a2;
	}

IL_00f2:
	{
		Game_t1600141214 * L_31 = __this->get_game_2();
		float L_32 = L_31->get_timeRemaining_21();
		if ((!(((float)L_32) > ((float)(0.0f)))))
		{
			goto IL_0151;
		}
	}
	{
		Game_t1600141214 * L_33 = __this->get_game_2();
		Game_t1600141214 * L_34 = L_33;
		float L_35 = L_34->get_timeRemaining_21();
		float L_36 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_34->set_timeRemaining_21(((float)((float)L_35-(float)L_36)));
		Game_t1600141214 * L_37 = __this->get_game_2();
		Game_t1600141214 * L_38 = L_37;
		float L_39 = L_38->get_enemySpawnDelay_25();
		float L_40 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_38->set_enemySpawnDelay_25(((float)((float)L_39-(float)L_40)));
		Game_t1600141214 * L_41 = __this->get_game_2();
		Game_t1600141214 * L_42 = L_41;
		float L_43 = L_42->get_partSpawnDelay_23();
		float L_44 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_42->set_partSpawnDelay_23(((float)((float)L_43-(float)L_44)));
		goto IL_016c;
	}

IL_0151:
	{
		Game_t1600141214 * L_45 = __this->get_game_2();
		float L_46 = L_45->get_timeRemaining_21();
		if ((!(((float)L_46) <= ((float)(0.0f)))))
		{
			goto IL_016c;
		}
	}
	{
		GameManager_timeUp_m1204656286(__this, /*hidden argument*/NULL);
	}

IL_016c:
	{
		Game_t1600141214 * L_47 = __this->get_game_2();
		float L_48 = L_47->get_enemySpawnDelay_25();
		if ((!(((float)L_48) <= ((float)(0.0f)))))
		{
			goto IL_0187;
		}
	}
	{
		GameManager_spawnEnemies_m1229942693(__this, /*hidden argument*/NULL);
	}

IL_0187:
	{
		Game_t1600141214 * L_49 = __this->get_game_2();
		float L_50 = L_49->get_partSpawnDelay_23();
		if ((!(((float)L_50) <= ((float)(0.0f)))))
		{
			goto IL_01a2;
		}
	}
	{
		GameManager_spawnShipParts_m906813607(__this, /*hidden argument*/NULL);
	}

IL_01a2:
	{
		return;
	}
}
// System.Void GameManager::timeUp()
extern "C"  void GameManager_timeUp_m1204656286 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_timeUp_m1204656286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3057952154* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObjectU5BU5D_t3057952154* V_2 = NULL;
	int32_t V_3 = 0;
	GameObjectU5BU5D_t3057952154* V_4 = NULL;
	GameObject_t1756533147 * V_5 = NULL;
	GameObjectU5BU5D_t3057952154* V_6 = NULL;
	int32_t V_7 = 0;
	{
		Game_t1600141214 * L_0 = __this->get_game_2();
		int32_t L_1 = L_0->get_partsReturned_19();
		Game_t1600141214 * L_2 = __this->get_game_2();
		int32_t L_3 = L_2->get_partsRequired_20();
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_00f7;
		}
	}
	{
		Game_t1600141214 * L_4 = __this->get_game_2();
		bool L_5 = L_4->get_betweenWaves_27();
		if (L_5)
		{
			goto IL_00f7;
		}
	}
	{
		Game_t1600141214 * L_6 = __this->get_game_2();
		int32_t L_7 = L_6->get_wave_17();
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0053;
		}
	}
	{
		Game_t1600141214 * L_8 = __this->get_game_2();
		L_8->set_won_16((bool)1);
		UIController_t2029583246 * L_9 = __this->get_UI_3();
		UIController_gameWonMessage_m284319100(L_9, /*hidden argument*/NULL);
	}

IL_0053:
	{
		Game_t1600141214 * L_10 = __this->get_game_2();
		L_10->set_betweenWaves_27((bool)1);
		UIController_t2029583246 * L_11 = __this->get_UI_3();
		Game_t1600141214 * L_12 = __this->get_game_2();
		int32_t L_13 = L_12->get_wave_17();
		UIController_waveCompleteMessage_m112210205(L_11, L_13, /*hidden argument*/NULL);
		Game_t1600141214 * L_14 = __this->get_game_2();
		Game_advanceWave_m3558657402(L_14, /*hidden argument*/NULL);
		Game_t1600141214 * L_15 = __this->get_game_2();
		L_15->set_betweenWaveDelay_22((8.0f));
		GameObjectU5BU5D_t3057952154* L_16 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral1816890106, /*hidden argument*/NULL);
		V_0 = L_16;
		GameObjectU5BU5D_t3057952154* L_17 = V_0;
		V_2 = L_17;
		V_3 = 0;
		goto IL_00b2;
	}

IL_00a4:
	{
		GameObjectU5BU5D_t3057952154* L_18 = V_2;
		int32_t L_19 = V_3;
		int32_t L_20 = L_19;
		GameObject_t1756533147 * L_21 = (L_18)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_20));
		V_1 = L_21;
		GameObject_t1756533147 * L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		V_3 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00b2:
	{
		int32_t L_24 = V_3;
		GameObjectU5BU5D_t3057952154* L_25 = V_2;
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_25)->max_length)))))))
		{
			goto IL_00a4;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_26 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral3031669893, /*hidden argument*/NULL);
		V_4 = L_26;
		GameObjectU5BU5D_t3057952154* L_27 = V_4;
		V_6 = L_27;
		V_7 = 0;
		goto IL_00e7;
	}

IL_00d3:
	{
		GameObjectU5BU5D_t3057952154* L_28 = V_6;
		int32_t L_29 = V_7;
		int32_t L_30 = L_29;
		GameObject_t1756533147 * L_31 = (L_28)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_30));
		V_5 = L_31;
		GameObject_t1756533147 * L_32 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		int32_t L_33 = V_7;
		V_7 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00e7:
	{
		int32_t L_34 = V_7;
		GameObjectU5BU5D_t3057952154* L_35 = V_6;
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))))))
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_0139;
	}

IL_00f7:
	{
		Game_t1600141214 * L_36 = __this->get_game_2();
		int32_t L_37 = L_36->get_partsReturned_19();
		Game_t1600141214 * L_38 = __this->get_game_2();
		int32_t L_39 = L_38->get_partsRequired_20();
		if ((((int32_t)L_37) > ((int32_t)L_39)))
		{
			goto IL_0139;
		}
	}
	{
		Game_t1600141214 * L_40 = __this->get_game_2();
		bool L_41 = L_40->get_betweenWaves_27();
		if (L_41)
		{
			goto IL_0139;
		}
	}
	{
		UIController_t2029583246 * L_42 = __this->get_UI_3();
		UIController_partsNotCollectedMessage_m867535828(L_42, /*hidden argument*/NULL);
		Game_t1600141214 * L_43 = __this->get_game_2();
		L_43->set_won_16((bool)0);
	}

IL_0139:
	{
		return;
	}
}
// System.Void GameManager::spawnEnemies()
extern "C"  void GameManager_spawnEnemies_m1229942693 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_spawnEnemies_m1229942693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_spawnLocations_4();
		int32_t L_1 = 0;
		GameObject_t1756533147 * L_2 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_1));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0079;
		}
	}
	{
		V_0 = 0;
		goto IL_005d;
	}

IL_001a:
	{
		GameObjectU5BU5D_t3057952154* L_4 = __this->get_spawnLocations_4();
		int32_t L_5 = Random_Range_m3327624272(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length)))), /*hidden argument*/NULL);
		V_1 = L_5;
		GameObject_t1756533147 * L_6 = __this->get_enemyPrefab_8();
		GameObjectU5BU5D_t3057952154* L_7 = __this->get_spawnLocations_4();
		int32_t L_8 = V_1;
		int32_t L_9 = L_8;
		GameObject_t1756533147 * L_10 = (L_7)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_9));
		Transform_t3275118058 * L_11 = GameObject_get_transform_m3490276752(L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Transform_get_position_m2304215762(L_11, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_13 = __this->get_spawnLocations_4();
		int32_t L_14 = V_1;
		int32_t L_15 = L_14;
		GameObject_t1756533147 * L_16 = (L_13)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_15));
		Transform_t3275118058 * L_17 = GameObject_get_transform_m3490276752(L_16, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_18 = Transform_get_rotation_m2617026815(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_6, L_12, L_18, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		int32_t L_19 = V_0;
		V_0 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_20 = V_0;
		Game_t1600141214 * L_21 = __this->get_game_2();
		int32_t L_22 = L_21->get_enemySpawnAmount_26();
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_001a;
		}
	}
	{
		Game_t1600141214 * L_23 = __this->get_game_2();
		Game_resetSpawnDelay_m3164128188(L_23, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void GameManager::spawnShipParts()
extern "C"  void GameManager_spawnShipParts_m906813607 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_spawnShipParts_m906813607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_shipPartSpawnLocations_5();
		int32_t L_1 = 0;
		GameObject_t1756533147 * L_2 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_1));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, _stringLiteral1575516707, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_4 = __this->get_shipPartSpawnLocations_5();
		int32_t L_5 = (((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length))));
		RuntimeObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		Debug_Log_m2923680153(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_7 = __this->get_shipPartSpawnLocations_5();
		int32_t L_8 = Random_Range_m3327624272(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_8;
		GameObject_t1756533147 * L_9 = __this->get_shipPartPrefab_9();
		GameObjectU5BU5D_t3057952154* L_10 = __this->get_shipPartSpawnLocations_5();
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		GameObject_t1756533147 * L_13 = (L_10)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_12));
		Transform_t3275118058 * L_14 = GameObject_get_transform_m3490276752(L_13, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Transform_get_position_m2304215762(L_14, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_16 = __this->get_shipPartSpawnLocations_5();
		int32_t L_17 = V_0;
		int32_t L_18 = L_17;
		GameObject_t1756533147 * L_19 = (L_16)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_18));
		Transform_t3275118058 * L_20 = GameObject_get_transform_m3490276752(L_19, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_21 = Transform_get_rotation_m2617026815(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_9, L_15, L_21, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		Game_t1600141214 * L_22 = __this->get_game_2();
		Game_resetShipPartSpawnDelay_m2707294655(L_22, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void GameManager::checkForDeath()
extern "C"  void GameManager_checkForDeath_m2230622135 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_checkForDeath_m2230622135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_player_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0046;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_player_6();
		PlayerController_t4148409433 * L_3 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_2, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_4 = L_3->get_grubby_2();
		float L_5 = L_4->get_health_7();
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			goto IL_0046;
		}
	}
	{
		UIController_t2029583246 * L_6 = __this->get_UI_3();
		UIController_healthDeathMessage_m2671790766(L_6, /*hidden argument*/NULL);
		GameManager_destroySpawnLocations_m3820108133(__this, /*hidden argument*/NULL);
		goto IL_0093;
	}

IL_0046:
	{
		GameObject_t1756533147 * L_7 = __this->get_ship_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0093;
		}
	}
	{
		GameObject_t1756533147 * L_9 = __this->get_ship_7();
		ShipController_t3277973446 * L_10 = GameObject_GetComponent_TisShipController_t3277973446_m2617641097(L_9, /*hidden argument*/GameObject_GetComponent_TisShipController_t3277973446_m2617641097_RuntimeMethod_var);
		Ship_t1116303770 * L_11 = L_10->get_ship_2();
		int32_t L_12 = L_11->get_health_0();
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_0093;
		}
	}
	{
		Game_t1600141214 * L_13 = __this->get_game_2();
		L_13->set_timeRemaining_21((0.0f));
		UIController_t2029583246 * L_14 = __this->get_UI_3();
		UIController_shipDestroyedMessage_m4291651381(L_14, /*hidden argument*/NULL);
		GameManager_destroySpawnLocations_m3820108133(__this, /*hidden argument*/NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Void GameManager::destroySpawnLocations()
extern "C"  void GameManager_destroySpawnLocations_m3820108133 (GameManager_t2252321495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_destroySpawnLocations_m3820108133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObjectU5BU5D_t3057952154* V_1 = NULL;
	int32_t V_2 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_spawnLocations_4();
		V_1 = L_0;
		V_2 = 0;
		goto IL_001c;
	}

IL_000e:
	{
		GameObjectU5BU5D_t3057952154* L_1 = V_1;
		int32_t L_2 = V_2;
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_7 = V_2;
		GameObjectU5BU5D_t3057952154* L_8 = V_1;
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void MainWeapon::.ctor(ammoType)
extern "C"  void MainWeapon__ctor_m47867042 (MainWeapon_t1214762455 * __this, int32_t ___theAmmo0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		__this->set_currentCharge_3((100.0f));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___theAmmo0;
		__this->set_ammo_0(L_0);
		int32_t L_1 = __this->get_ammo_0();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_0093;
		}
	}
	{
		goto IL_00c0;
	}

IL_0038:
	{
		__this->set_damage_2(((int32_t)20));
		__this->set_range_1((7.5f));
		__this->set_dischargeRate_4((15.0f));
		__this->set_rechargeRate_5((8.0f));
		goto IL_00c0;
	}

IL_0066:
	{
		__this->set_damage_2(5);
		__this->set_range_1((15.0f));
		__this->set_dischargeRate_4((10.5f));
		__this->set_rechargeRate_5((8.0f));
		goto IL_00c0;
	}

IL_0093:
	{
		__this->set_damage_2(2);
		__this->set_range_1((25.0f));
		__this->set_dischargeRate_4((2.5f));
		__this->set_rechargeRate_5((8.0f));
		goto IL_00c0;
	}

IL_00c0:
	{
		return;
	}
}
// System.Void MainWeapon::discharge(System.Single)
extern "C"  void MainWeapon_discharge_m3402882019 (MainWeapon_t1214762455 * __this, float ___time0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainWeapon_discharge_m3402882019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_currentCharge_3();
		float L_1 = ___time0;
		float L_2 = __this->get_dischargeRate_4();
		__this->set_currentCharge_3(((float)((float)L_0-(float)((float)((float)L_1*(float)L_2)))));
		float L_3 = __this->get_currentCharge_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp_m1779415170(NULL /*static, unused*/, L_3, (0.0f), (100.0f), /*hidden argument*/NULL);
		__this->set_currentCharge_3(L_4);
		return;
	}
}
// System.Void MainWeapon::recharge(System.Single)
extern "C"  void MainWeapon_recharge_m3583788820 (MainWeapon_t1214762455 * __this, float ___time0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainWeapon_recharge_m3583788820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_currentCharge_3();
		float L_1 = ___time0;
		float L_2 = __this->get_rechargeRate_5();
		__this->set_currentCharge_3(((float)((float)L_0+(float)((float)((float)L_1*(float)L_2)))));
		float L_3 = __this->get_currentCharge_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp_m1779415170(NULL /*static, unused*/, L_3, (0.0f), (100.0f), /*hidden argument*/NULL);
		__this->set_currentCharge_3(L_4);
		return;
	}
}
// System.Void Player::.ctor(ammoType,bombType)
extern "C"  void Player__ctor_m3620100752 (Player_t1147783557 * __this, int32_t ___theAmmoType0, int32_t ___theBombtype1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player__ctor_m3620100752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_speed_2((15.0f));
		__this->set_walkingSpeed_3((15.0f));
		__this->set_sprintingSpeed_4((30.0f));
		__this->set_jumpHeight_5((5.0f));
		__this->set_regenDelay_6((4.0f));
		__this->set_health_7((100.0f));
		__this->set_healthRegen_8(1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___theAmmoType0;
		MainWeapon_t1214762455 * L_1 = (MainWeapon_t1214762455 *)il2cpp_codegen_object_new(MainWeapon_t1214762455_il2cpp_TypeInfo_var);
		MainWeapon__ctor_m47867042(L_1, L_0, /*hidden argument*/NULL);
		__this->set_blenderBlaster_0(L_1);
		int32_t L_2 = ___theBombtype1;
		SubWeapon_t1081861670 * L_3 = (SubWeapon_t1081861670 *)il2cpp_codegen_object_new(SubWeapon_t1081861670_il2cpp_TypeInfo_var);
		SubWeapon__ctor_m954666591(L_3, L_2, /*hidden argument*/NULL);
		__this->set_omNomBomb_1(L_3);
		return;
	}
}
// System.Void Player::takeDamage(System.Int32)
extern "C"  void Player_takeDamage_m3178363385 (Player_t1147783557 * __this, int32_t ___damage0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_health_7();
		int32_t L_1 = ___damage0;
		__this->set_health_7(((float)((float)L_0-(float)(((float)((float)L_1))))));
		__this->set_regenDelay_6((4.0f));
		return;
	}
}
// System.Void Player::regenHealth(System.Single)
extern "C"  void Player_regenHealth_m1787165012 (Player_t1147783557 * __this, float ___time0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_health_7();
		if ((!(((float)L_0) < ((float)(100.0f)))))
		{
			goto IL_0024;
		}
	}
	{
		float L_1 = __this->get_health_7();
		int32_t L_2 = __this->get_healthRegen_8();
		__this->set_health_7(((float)((float)L_1+(float)(((float)((float)L_2))))));
	}

IL_0024:
	{
		return;
	}
}
// System.Void PlayerController::.ctor()
extern "C"  void PlayerController__ctor_m3280132936 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController__ctor_m3280132936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Player_t1147783557 * L_0 = (Player_t1147783557 *)il2cpp_codegen_object_new(Player_t1147783557_il2cpp_TypeInfo_var);
		Player__ctor_m3620100752(L_0, 1, 1, /*hidden argument*/NULL);
		__this->set_grubby_2(L_0);
		__this->set_walkSpeed_7((5.0f));
		__this->set_sprintSpeed_8((10.0f));
		__this->set_horizontalSensitivity_11((5.0f));
		__this->set_verticalSensitivity_12((5.0f));
		__this->set_lookLimit_13((80.0f));
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::Start()
extern "C"  void PlayerController_Start_m3606284888 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Start_m3606284888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharacterController_t4094781467 * L_0 = Component_GetComponent_TisCharacterController_t4094781467_m1582798737(__this, /*hidden argument*/Component_GetComponent_TisCharacterController_t4094781467_m1582798737_RuntimeMethod_var);
		__this->set_character_3(L_0);
		return;
	}
}
// System.Void PlayerController::Update()
extern "C"  void PlayerController_Update_m4228472513 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	{
		PlayerController_rotate_m1444289841(__this, /*hidden argument*/NULL);
		PlayerController_move_m1170970717(__this, /*hidden argument*/NULL);
		PlayerController_sprint_m4044474542(__this, /*hidden argument*/NULL);
		PlayerController_jump_m563400686(__this, /*hidden argument*/NULL);
		PlayerController_shoot_m9744895(__this, /*hidden argument*/NULL);
		PlayerController_throwBomb_m3049288080(__this, /*hidden argument*/NULL);
		PlayerController_regenerateHealth_m1430467426(__this, /*hidden argument*/NULL);
		PlayerController_checkForDeath_m1722688921(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::move()
extern "C"  void PlayerController_move_m1170970717 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_move_m1170970717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m948504553(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		Player_t1147783557 * L_1 = __this->get_grubby_2();
		float L_2 = L_1->get_speed_2();
		__this->set_sideSpeed_10(((float)((float)L_0*(float)L_2)));
		float L_3 = Input_GetAxis_m948504553(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		Player_t1147783557 * L_4 = __this->get_grubby_2();
		float L_5 = L_4->get_speed_2();
		__this->set_forwardSpeed_9(((float)((float)L_3*(float)L_5)));
		CharacterController_t4094781467 * L_6 = __this->get_character_3();
		bool L_7 = CharacterController_get_isGrounded_m172388764(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0068;
		}
	}
	{
		float L_8 = __this->get_verticalVelocity_15();
		Vector3_t2243707580  L_9 = Physics_get_gravity_m1035197364(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = (&V_0)->get_y_2();
		float L_11 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_verticalVelocity_15(((float)((float)L_8+(float)((float)((float)L_10*(float)L_11)))));
	}

IL_0068:
	{
		float L_12 = __this->get_sideSpeed_10();
		float L_13 = __this->get_verticalVelocity_15();
		float L_14 = __this->get_forwardSpeed_9();
		Vector3__ctor_m1555724485((&V_1), L_12, L_13, L_14, /*hidden argument*/NULL);
		Transform_t3275118058 * L_15 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = V_1;
		Vector3_t2243707580  L_17 = Transform_TransformDirection_m505001666(L_15, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		CharacterController_t4094781467 * L_18 = __this->get_character_3();
		Vector3_t2243707580  L_19 = V_1;
		float L_20 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_21 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		CharacterController_Move_m388312410(L_18, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::rotate()
extern "C"  void PlayerController_rotate_m1444289841 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_rotate_m1444289841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m948504553(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		float L_1 = __this->get_horizontalSensitivity_11();
		V_0 = ((float)((float)L_0*(float)L_1));
		Transform_t3275118058 * L_2 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		float L_3 = V_0;
		Transform_Rotate_m542935446(L_2, (0.0f), L_3, (0.0f), /*hidden argument*/NULL);
		float L_4 = __this->get_pitch_14();
		float L_5 = Input_GetAxis_m948504553(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		float L_6 = __this->get_verticalSensitivity_12();
		__this->set_pitch_14(((float)((float)L_4-(float)((float)((float)L_5*(float)L_6)))));
		float L_7 = __this->get_pitch_14();
		float L_8 = __this->get_lookLimit_13();
		float L_9 = __this->get_lookLimit_13();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Clamp_m1779415170(NULL /*static, unused*/, L_7, ((-L_8)), L_9, /*hidden argument*/NULL);
		__this->set_pitch_14(L_10);
		Camera_t189460977 * L_11 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Component_get_transform_m3374354972(L_11, /*hidden argument*/NULL);
		float L_13 = __this->get_pitch_14();
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m1555724485((&L_14), L_13, (0.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_localEulerAngles_m233727872(L_12, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::jump()
extern "C"  void PlayerController_jump_m563400686 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_jump_m563400686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButtonDown_m717298472(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		CharacterController_t4094781467 * L_1 = __this->get_character_3();
		bool L_2 = CharacterController_get_isGrounded_m172388764(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		Player_t1147783557 * L_3 = __this->get_grubby_2();
		float L_4 = L_3->get_jumpHeight_5();
		__this->set_verticalVelocity_15(L_4);
	}

IL_0030:
	{
		return;
	}
}
// System.Void PlayerController::throwBomb()
extern "C"  void PlayerController_throwBomb_m3049288080 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_throwBomb_m3049288080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m3563899393(NULL /*static, unused*/, _stringLiteral372029369, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		Player_t1147783557 * L_1 = __this->get_grubby_2();
		SubWeapon_t1081861670 * L_2 = L_1->get_omNomBomb_1();
		bool L_3 = L_2->get_thrown_5();
		if (L_3)
		{
			goto IL_0024;
		}
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKeyUp_m682874492(NULL /*static, unused*/, _stringLiteral372029369, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00ba;
		}
	}
	{
		Player_t1147783557 * L_5 = __this->get_grubby_2();
		SubWeapon_t1081861670 * L_6 = L_5->get_omNomBomb_1();
		bool L_7 = L_6->get_thrown_5();
		if (L_7)
		{
			goto IL_00ba;
		}
	}
	{
		GameObject_t1756533147 * L_8 = __this->get_bombPrefab_4();
		Camera_t189460977 * L_9 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Component_get_transform_m3374354972(L_9, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Transform_get_position_m2304215762(L_10, /*hidden argument*/NULL);
		Camera_t189460977 * L_12 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = Component_get_transform_m3374354972(L_12, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_14 = Transform_get_rotation_m2617026815(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_15 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_8, L_11, L_14, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		V_0 = L_15;
		GameObject_t1756533147 * L_16 = V_0;
		Rigidbody_t4233889191 * L_17 = GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(L_16, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_RuntimeMethod_var);
		Camera_t189460977 * L_18 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_19 = Component_get_transform_m3374354972(L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = Transform_get_forward_m2144220796(L_19, /*hidden argument*/NULL);
		Player_t1147783557 * L_21 = __this->get_grubby_2();
		SubWeapon_t1081861670 * L_22 = L_21->get_omNomBomb_1();
		float L_23 = L_22->get_range_4();
		float L_24 = __this->get_forwardSpeed_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_25 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_20, ((float)((float)L_23+(float)L_24)), /*hidden argument*/NULL);
		Rigidbody_AddForce_m3639708911(L_17, L_25, 1, /*hidden argument*/NULL);
		Player_t1147783557 * L_26 = __this->get_grubby_2();
		SubWeapon_t1081861670 * L_27 = L_26->get_omNomBomb_1();
		L_27->set_thrown_5((bool)1);
	}

IL_00ba:
	{
		Player_t1147783557 * L_28 = __this->get_grubby_2();
		SubWeapon_t1081861670 * L_29 = L_28->get_omNomBomb_1();
		bool L_30 = L_29->get_thrown_5();
		if (!L_30)
		{
			goto IL_0115;
		}
	}
	{
		Player_t1147783557 * L_31 = __this->get_grubby_2();
		SubWeapon_t1081861670 * L_32 = L_31->get_omNomBomb_1();
		SubWeapon_t1081861670 * L_33 = L_32;
		float L_34 = L_33->get_throwDelay_3();
		float L_35 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_33->set_throwDelay_3(((float)((float)L_34-(float)L_35)));
		Player_t1147783557 * L_36 = __this->get_grubby_2();
		SubWeapon_t1081861670 * L_37 = L_36->get_omNomBomb_1();
		float L_38 = L_37->get_throwDelay_3();
		if ((!(((float)L_38) <= ((float)(0.0f)))))
		{
			goto IL_0115;
		}
	}
	{
		Player_t1147783557 * L_39 = __this->get_grubby_2();
		SubWeapon_t1081861670 * L_40 = L_39->get_omNomBomb_1();
		SubWeapon_resetBomb_m1651505034(L_40, /*hidden argument*/NULL);
	}

IL_0115:
	{
		return;
	}
}
// System.Void PlayerController::sprint()
extern "C"  void PlayerController_sprint_m4044474542 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_sprint_m4044474542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharacterController_t4094781467 * L_0 = __this->get_character_3();
		bool L_1 = CharacterController_get_isGrounded_m172388764(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0055;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m2374355427(NULL /*static, unused*/, _stringLiteral2930725987, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetButton_m1292624864(NULL /*static, unused*/, _stringLiteral3645101709, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		Player_t1147783557 * L_4 = __this->get_grubby_2();
		float L_5 = __this->get_sprintSpeed_8();
		L_4->set_speed_2(L_5);
		goto IL_0055;
	}

IL_0044:
	{
		Player_t1147783557 * L_6 = __this->get_grubby_2();
		float L_7 = __this->get_walkSpeed_7();
		L_6->set_speed_2(L_7);
	}

IL_0055:
	{
		return;
	}
}
// System.Void PlayerController::shoot()
extern "C"  void PlayerController_shoot_m9744895 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_shoot_m9744895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButton_m1292624864(NULL /*static, unused*/, _stringLiteral3645101709, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00a7;
		}
	}
	{
		Player_t1147783557 * L_1 = __this->get_grubby_2();
		MainWeapon_t1214762455 * L_2 = L_1->get_blenderBlaster_0();
		float L_3 = L_2->get_currentCharge_3();
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			goto IL_00a7;
		}
	}
	{
		GameObject_t1756533147 * L_4 = __this->get_bulletPrefab_5();
		GameObject_t1756533147 * L_5 = __this->get_nozzle_6();
		Transform_t3275118058 * L_6 = GameObject_get_transform_m3490276752(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Transform_get_position_m2304215762(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_nozzle_6();
		Transform_t3275118058 * L_9 = GameObject_get_transform_m3490276752(L_8, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m2617026815(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_11 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_4, L_7, L_10, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		V_0 = L_11;
		GameObject_t1756533147 * L_12 = V_0;
		Rigidbody_t4233889191 * L_13 = GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193(L_12, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t4233889191_m1060888193_RuntimeMethod_var);
		GameObject_t1756533147 * L_14 = __this->get_nozzle_6();
		Transform_t3275118058 * L_15 = GameObject_get_transform_m3490276752(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Transform_get_forward_m2144220796(L_15, /*hidden argument*/NULL);
		Player_t1147783557 * L_17 = __this->get_grubby_2();
		MainWeapon_t1214762455 * L_18 = L_17->get_blenderBlaster_0();
		float L_19 = L_18->get_range_1();
		float L_20 = __this->get_forwardSpeed_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_21 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_16, ((float)((float)L_19+(float)L_20)), /*hidden argument*/NULL);
		Rigidbody_AddForce_m3639708911(L_13, L_21, 1, /*hidden argument*/NULL);
		Player_t1147783557 * L_22 = __this->get_grubby_2();
		MainWeapon_t1214762455 * L_23 = L_22->get_blenderBlaster_0();
		float L_24 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		MainWeapon_discharge_m3402882019(L_23, L_24, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_00a7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_25 = Input_GetButton_m1292624864(NULL /*static, unused*/, _stringLiteral3645101709, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00cb;
		}
	}
	{
		Player_t1147783557 * L_26 = __this->get_grubby_2();
		MainWeapon_t1214762455 * L_27 = L_26->get_blenderBlaster_0();
		float L_28 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		MainWeapon_recharge_m3583788820(L_27, L_28, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		return;
	}
}
// System.Void PlayerController::regenerateHealth()
extern "C"  void PlayerController_regenerateHealth_m1430467426 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_regenerateHealth_m1430467426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Player_t1147783557 * L_0 = __this->get_grubby_2();
		Player_t1147783557 * L_1 = L_0;
		float L_2 = L_1->get_regenDelay_6();
		float L_3 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_1->set_regenDelay_6(((float)((float)L_2-(float)L_3)));
		Player_t1147783557 * L_4 = __this->get_grubby_2();
		float L_5 = L_4->get_regenDelay_6();
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			goto IL_0060;
		}
	}
	{
		Player_t1147783557 * L_6 = __this->get_grubby_2();
		float L_7 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		Player_regenHealth_m1787165012(L_6, L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_9 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_8, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_10 = L_9->get_UI_3();
		Player_t1147783557 * L_11 = __this->get_grubby_2();
		float L_12 = L_11->get_health_7();
		UIController_updateDamageFrame_m339031365(L_10, L_12, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void PlayerController::checkForDeath()
extern "C"  void PlayerController_checkForDeath_m1722688921 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	{
		Player_t1147783557 * L_0 = __this->get_grubby_2();
		float L_1 = L_0->get_health_7();
		if ((!(((float)L_1) <= ((float)(0.0f)))))
		{
			goto IL_001b;
		}
	}
	{
		PlayerController_die_m1474265594(__this, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void PlayerController::die()
extern "C"  void PlayerController_die_m1474265594 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_die_m1474265594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Player_t1147783557 * L_0 = __this->get_grubby_2();
		L_0->set_health_7((0.0f));
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_3 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_2, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		Game_t1600141214 * L_4 = L_3->get_game_2();
		L_4->set_won_16((bool)0);
		return;
	}
}
// System.Void Ship::.ctor()
extern "C"  void Ship__ctor_m4202732153 (Ship_t1116303770 * __this, const RuntimeMethod* method)
{
	{
		__this->set_health_0(((int32_t)1000));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ship::takeDamage(System.Int32)
extern "C"  void Ship_takeDamage_m936844050 (Ship_t1116303770 * __this, int32_t ___damage0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_health_0();
		int32_t L_1 = ___damage0;
		__this->set_health_0(((int32_t)((int32_t)L_0-(int32_t)L_1)));
		return;
	}
}
// System.Void ShipController::.ctor()
extern "C"  void ShipController__ctor_m2397218881 (ShipController_t3277973446 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipController__ctor_m2397218881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Ship_t1116303770 * L_0 = (Ship_t1116303770 *)il2cpp_codegen_object_new(Ship_t1116303770_il2cpp_TypeInfo_var);
		Ship__ctor_m4202732153(L_0, /*hidden argument*/NULL);
		__this->set_ship_2(L_0);
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipController::Start()
extern "C"  void ShipController_Start_m952825521 (ShipController_t3277973446 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ShipController::Update()
extern "C"  void ShipController_Update_m1022057640 (ShipController_t3277973446 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipController_Update_m1022057640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Ship_t1116303770 * L_0 = __this->get_ship_2();
		int32_t L_1 = L_0->get_health_0();
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_009d;
		}
	}
	{
		GameObject_t1756533147 * L_2 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_3 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_2, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_4 = L_3->get_UI_3();
		UIController_clearSubMessage_m1922647077(L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_6 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_5, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_7 = L_6->get_UI_3();
		UIController_shipDestroyedMessage_m4291651381(L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_9 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_8, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		Game_t1600141214 * L_10 = L_9->get_game_2();
		L_10->set_won_16((bool)0);
		GameObject_t1756533147 * L_11 = __this->get_explosiveEffect_3();
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = GameObject_get_transform_m3490276752(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Transform_get_position_m2304215762(L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m3490276752(L_15, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_17 = Transform_get_rotation_m2617026815(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_18 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_11, L_14, L_17, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		Object_Destroy_m682534386(NULL /*static, unused*/, L_18, (4.0f), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_009d:
	{
		return;
	}
}
// System.Void ShipController::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void ShipController_OnTriggerEnter_m3928624133 (ShipController_t3277973446 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipController_OnTriggerEnter_m3928624133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(L_0, /*hidden argument*/NULL);
		String_t* L_2 = GameObject_get_tag_m3359901967(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00dc;
		}
	}
	{
		Collider_t3497673348 * L_4 = ___other0;
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m2159020946(L_4, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_6 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_5, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_7 = L_6->get_grubby_2();
		bool L_8 = L_7->get_partHeld_9();
		if (!L_8)
		{
			goto IL_00dc;
		}
	}
	{
		GameObject_t1756533147 * L_9 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_10 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_9, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		Game_t1600141214 * L_11 = L_10->get_game_2();
		Game_returnPart_m2055149738(L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_13 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_12, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_14 = L_13->get_UI_3();
		GameObject_t1756533147 * L_15 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_16 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_15, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		Game_t1600141214 * L_17 = L_16->get_game_2();
		int32_t L_18 = L_17->get_partsReturned_19();
		GameObject_t1756533147 * L_19 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_20 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_19, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		Game_t1600141214 * L_21 = L_20->get_game_2();
		int32_t L_22 = L_21->get_partsRequired_20();
		UIController_updatePartsReturned_m494708189(L_14, L_18, L_22, /*hidden argument*/NULL);
		Collider_t3497673348 * L_23 = ___other0;
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m2159020946(L_23, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_25 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_24, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_26 = L_25->get_grubby_2();
		L_26->set_partHeld_9((bool)0);
		GameObject_t1756533147 * L_27 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_28 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_27, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_29 = L_28->get_UI_3();
		Collider_t3497673348 * L_30 = ___other0;
		GameObject_t1756533147 * L_31 = Component_get_gameObject_m2159020946(L_30, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_32 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_31, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_33 = L_32->get_grubby_2();
		bool L_34 = L_33->get_partHeld_9();
		UIController_updatePartIcon_m265678373(L_29, L_34, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		return;
	}
}
// System.Void ShipController::OnTriggerStay(UnityEngine.Collider)
extern "C"  void ShipController_OnTriggerStay_m2544320122 (ShipController_t3277973446 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipController_OnTriggerStay_m2544320122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(L_0, /*hidden argument*/NULL);
		String_t* L_2 = GameObject_get_tag_m3359901967(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral1816890106, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t1756533147 * L_4 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2980400013, /*hidden argument*/NULL);
		GameManager_t2252321495 * L_5 = GameObject_GetComponent_TisGameManager_t2252321495_m190008436(L_4, /*hidden argument*/GameObject_GetComponent_TisGameManager_t2252321495_m190008436_RuntimeMethod_var);
		UIController_t2029583246 * L_6 = L_5->get_UI_3();
		UIController_shipUnderAttackMessage_m859257986(L_6, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void ShipUI::.ctor()
extern "C"  void ShipUI__ctor_m166092689 (ShipUI_t242265720 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipUI::Start()
extern "C"  void ShipUI_Start_m4254051481 (ShipUI_t242265720 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipUI_Start_m4254051481_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral387931632, /*hidden argument*/NULL);
		Camera_t189460977 * L_1 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_0, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_RuntimeMethod_var);
		__this->set_cameraToLookAt_2(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2051100417, /*hidden argument*/NULL);
		__this->set_bar_3(L_2);
		GameObject_t1756533147 * L_3 = __this->get_bar_3();
		Transform_t3275118058 * L_4 = GameObject_get_transform_m3490276752(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Transform_get_localScale_m46214814(L_4, /*hidden argument*/NULL);
		__this->set_barVector_4(L_5);
		return;
	}
}
// System.Void ShipUI::Update()
extern "C"  void ShipUI_Update_m1535098274 (ShipUI_t242265720 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipUI_Update_m1535098274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Camera_t189460977 * L_0 = __this->get_cameraToLookAt_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00c9;
		}
	}
	{
		Vector3_t2243707580 * L_2 = __this->get_address_of_barVector_4();
		GameObject_t1756533147 * L_3 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1844382288, /*hidden argument*/NULL);
		ShipController_t3277973446 * L_4 = GameObject_GetComponent_TisShipController_t3277973446_m2617641097(L_3, /*hidden argument*/GameObject_GetComponent_TisShipController_t3277973446_m2617641097_RuntimeMethod_var);
		Ship_t1116303770 * L_5 = L_4->get_ship_2();
		int32_t L_6 = L_5->get_health_0();
		L_2->set_x_1(((float)((float)(((float)((float)L_6)))/(float)(1000.0f))));
		GameObject_t1756533147 * L_7 = __this->get_bar_3();
		Transform_t3275118058 * L_8 = GameObject_get_transform_m3490276752(L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = __this->get_barVector_4();
		Transform_set_localScale_m1442831667(L_8, L_9, /*hidden argument*/NULL);
		Camera_t189460977 * L_10 = __this->get_cameraToLookAt_2();
		Transform_t3275118058 * L_11 = Component_get_transform_m3374354972(L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Transform_get_position_m2304215762(L_11, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Transform_get_position_m2304215762(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_15 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		float L_16 = (0.0f);
		V_1 = L_16;
		(&V_0)->set_z_3(L_16);
		float L_17 = V_1;
		(&V_0)->set_x_1(L_17);
		Transform_t3275118058 * L_18 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Camera_t189460977 * L_19 = __this->get_cameraToLookAt_2();
		Transform_t3275118058 * L_20 = Component_get_transform_m3374354972(L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Transform_get_position_m2304215762(L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = V_0;
		Vector3_t2243707580  L_23 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		Transform_LookAt_m1970949065(L_18, L_23, /*hidden argument*/NULL);
		Transform_t3275118058 * L_24 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_Rotate_m542935446(L_24, (0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_00c9:
	{
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		return;
	}
}
// System.Void SubWeapon::.ctor(bombType)
extern "C"  void SubWeapon__ctor_m954666591 (SubWeapon_t1081861670 * __this, int32_t ___theType0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		__this->set_type_0(1);
		__this->set_delay_2((2.0f));
		__this->set_throwDelay_3((4.0f));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___theType0;
		__this->set_type_0(L_0);
		int32_t L_1 = __this->get_type_0();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_0067;
		}
	}
	{
		goto IL_006c;
	}

IL_004a:
	{
		goto IL_006c;
	}

IL_004f:
	{
		__this->set_damage_1(((int32_t)60));
		__this->set_range_4((15.0f));
		goto IL_006c;
	}

IL_0067:
	{
		goto IL_006c;
	}

IL_006c:
	{
		return;
	}
}
// System.Void SubWeapon::tick(System.Single)
extern "C"  void SubWeapon_tick_m860919181 (SubWeapon_t1081861670 * __this, float ___time0, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_delay_2();
		float L_1 = ___time0;
		__this->set_delay_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
// System.Void SubWeapon::resetBomb()
extern "C"  void SubWeapon_resetBomb_m1651505034 (SubWeapon_t1081861670 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0047;
		}
	}
	{
		goto IL_004c;
	}

IL_0020:
	{
		goto IL_004c;
	}

IL_0025:
	{
		__this->set_delay_2((2.0f));
		__this->set_throwDelay_3((4.0f));
		__this->set_thrown_5((bool)0);
		goto IL_004c;
	}

IL_0047:
	{
		goto IL_004c;
	}

IL_004c:
	{
		return;
	}
}
// System.Void UIController::.ctor()
extern "C"  void UIController__ctor_m4279849967 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIController::initializeUI()
extern "C"  void UIController_initializeUI_m1509084315 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_initializeUI_m1509084315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral3200448484, /*hidden argument*/NULL);
		Image_t2042527209 * L_1 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_RuntimeMethod_var);
		__this->set_damageFrame_1(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1607060599, /*hidden argument*/NULL);
		Image_t2042527209 * L_3 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_2, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_RuntimeMethod_var);
		__this->set_ammoBar_2(L_3);
		GameObject_t1756533147 * L_4 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1711783022, /*hidden argument*/NULL);
		Image_t2042527209 * L_5 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_4, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_RuntimeMethod_var);
		__this->set_rechargingBar_3(L_5);
		GameObject_t1756533147 * L_6 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1601101583, /*hidden argument*/NULL);
		__this->set_omNombBombIcon_4(L_6);
		GameObject_t1756533147 * L_7 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2328220233, /*hidden argument*/NULL);
		Text_t356221433 * L_8 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_7, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		__this->set_waveCounter_5(L_8);
		GameObject_t1756533147 * L_9 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral3457520297, /*hidden argument*/NULL);
		Text_t356221433 * L_10 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_9, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		__this->set_timer_6(L_10);
		GameObject_t1756533147 * L_11 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2933044483, /*hidden argument*/NULL);
		Text_t356221433 * L_12 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_11, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		__this->set_partsCollected_7(L_12);
		GameObject_t1756533147 * L_13 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral259034962, /*hidden argument*/NULL);
		__this->set_partIcon_11(L_13);
		GameObject_t1756533147 * L_14 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral360502915, /*hidden argument*/NULL);
		Text_t356221433 * L_15 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_14, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		__this->set_message_8(L_15);
		GameObject_t1756533147 * L_16 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral91840137, /*hidden argument*/NULL);
		Text_t356221433 * L_17 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_16, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		__this->set_messageDescription_9(L_17);
		GameObject_t1756533147 * L_18 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral3888774807, /*hidden argument*/NULL);
		Text_t356221433 * L_19 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_18, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		__this->set_subMessage_10(L_19);
		GameObject_t1756533147 * L_20 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral3141382987, /*hidden argument*/NULL);
		__this->set_deathCam_12(L_20);
		Image_t2042527209 * L_21 = __this->get_damageFrame_1();
		Color_t2020392075  L_22 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_21);
		V_0 = L_22;
		(&V_0)->set_a_3((0.0f));
		Image_t2042527209 * L_23 = __this->get_damageFrame_1();
		Color_t2020392075  L_24 = V_0;
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_23, L_24);
		Image_t2042527209 * L_25 = __this->get_ammoBar_2();
		Image_set_fillAmount_m2220966753(L_25, (0.0f), /*hidden argument*/NULL);
		Image_t2042527209 * L_26 = __this->get_rechargingBar_3();
		Image_set_fillAmount_m2220966753(L_26, (0.0f), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = __this->get_omNombBombIcon_4();
		GameObject_SetActive_m2693135142(L_27, (bool)0, /*hidden argument*/NULL);
		Text_t356221433 * L_28 = __this->get_message_8();
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m2159020946(L_28, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_29, (bool)0, /*hidden argument*/NULL);
		Text_t356221433 * L_30 = __this->get_messageDescription_9();
		GameObject_t1756533147 * L_31 = Component_get_gameObject_m2159020946(L_30, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_31, (bool)0, /*hidden argument*/NULL);
		Text_t356221433 * L_32 = __this->get_subMessage_10();
		GameObject_t1756533147 * L_33 = Component_get_gameObject_m2159020946(L_32, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_33, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_34 = __this->get_partIcon_11();
		GameObject_SetActive_m2693135142(L_34, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = __this->get_deathCam_12();
		GameObject_SetActive_m2693135142(L_35, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIController::countDown(System.Single)
extern "C"  void UIController_countDown_m4243092691 (UIController_t2029583246 * __this, float ___time0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_countDown_m4243092691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_shipAttackedDelay_13();
		if ((!(((float)L_0) <= ((float)(10.0f)))))
		{
			goto IL_001e;
		}
	}
	{
		float L_1 = __this->get_shipAttackedDelay_13();
		float L_2 = ___time0;
		__this->set_shipAttackedDelay_13(((float)((float)L_1-(float)L_2)));
	}

IL_001e:
	{
		float L_3 = __this->get_shipAttackedDelay_13();
		if ((!(((float)L_3) <= ((float)(0.0f)))))
		{
			goto IL_0054;
		}
	}
	{
		__this->set_shipAttackedDelay_13((20.0f));
		UIController_clearSubMessage_m1922647077(__this, /*hidden argument*/NULL);
		Text_t356221433 * L_4 = __this->get_subMessage_10();
		Text_t356221433 * L_5 = Component_GetComponent_TisText_t356221433_m1342661039(L_4, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_RuntimeMethod_var);
		Color_t2020392075  L_6 = Color_get_white_m4252342900(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_6);
	}

IL_0054:
	{
		GameObject_t1756533147 * L_7 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00c5;
		}
	}
	{
		GameObject_t1756533147 * L_9 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_10 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_9, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_11 = L_10->get_grubby_2();
		MainWeapon_t1214762455 * L_12 = L_11->get_blenderBlaster_0();
		float L_13 = L_12->get_currentCharge_3();
		if ((!(((float)L_13) < ((float)(100.0f)))))
		{
			goto IL_00c5;
		}
	}
	{
		Image_t2042527209 * L_14 = __this->get_ammoBar_2();
		GameObject_t1756533147 * L_15 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_16 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_15, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_17 = L_16->get_grubby_2();
		MainWeapon_t1214762455 * L_18 = L_17->get_blenderBlaster_0();
		float L_19 = L_18->get_currentCharge_3();
		Image_set_fillAmount_m2220966753(L_14, ((float)((float)L_19/(float)(100.0f))), /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_00c5:
	{
		Image_t2042527209 * L_20 = __this->get_ammoBar_2();
		Image_set_fillAmount_m2220966753(L_20, (0.0f), /*hidden argument*/NULL);
	}

IL_00d5:
	{
		GameObject_t1756533147 * L_21 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0152;
		}
	}
	{
		GameObject_t1756533147 * L_23 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_24 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_23, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_25 = L_24->get_grubby_2();
		SubWeapon_t1081861670 * L_26 = L_25->get_omNomBomb_1();
		float L_27 = L_26->get_throwDelay_3();
		if ((((float)L_27) == ((float)(4.0f))))
		{
			goto IL_0152;
		}
	}
	{
		GameObject_t1756533147 * L_28 = __this->get_omNombBombIcon_4();
		GameObject_SetActive_m2693135142(L_28, (bool)1, /*hidden argument*/NULL);
		Image_t2042527209 * L_29 = __this->get_rechargingBar_3();
		GameObject_t1756533147 * L_30 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		PlayerController_t4148409433 * L_31 = GameObject_GetComponent_TisPlayerController_t4148409433_m607205852(L_30, /*hidden argument*/GameObject_GetComponent_TisPlayerController_t4148409433_m607205852_RuntimeMethod_var);
		Player_t1147783557 * L_32 = L_31->get_grubby_2();
		SubWeapon_t1081861670 * L_33 = L_32->get_omNomBomb_1();
		float L_34 = L_33->get_throwDelay_3();
		Image_set_fillAmount_m2220966753(L_29, ((float)((float)L_34/(float)(4.0f))), /*hidden argument*/NULL);
		goto IL_016e;
	}

IL_0152:
	{
		GameObject_t1756533147 * L_35 = __this->get_omNombBombIcon_4();
		GameObject_SetActive_m2693135142(L_35, (bool)0, /*hidden argument*/NULL);
		Image_t2042527209 * L_36 = __this->get_rechargingBar_3();
		Image_set_fillAmount_m2220966753(L_36, (0.0f), /*hidden argument*/NULL);
	}

IL_016e:
	{
		return;
	}
}
// System.Void UIController::healthDeathMessage()
extern "C"  void UIController_healthDeathMessage_m2671790766 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_healthDeathMessage_m2671790766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_0054;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_message_8();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteral1609003072);
		Text_t356221433 * L_2 = __this->get_message_8();
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m2159020946(L_2, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_3, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_4 = __this->get_messageDescription_9();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral2114188598);
		Text_t356221433 * L_5 = __this->get_messageDescription_9();
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m2159020946(L_5, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_6, (bool)1, /*hidden argument*/NULL);
		__this->set_freezeUI_14((bool)1);
	}

IL_0054:
	{
		GameObject_t1756533147 * L_7 = __this->get_deathCam_12();
		GameObject_SetActive_m2693135142(L_7, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIController::fallingDeathMessage()
extern "C"  void UIController_fallingDeathMessage_m2237346207 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_fallingDeathMessage_m2237346207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_007d;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_message_8();
		Text_t356221433 * L_2 = Component_GetComponent_TisText_t356221433_m1342661039(L_1, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_RuntimeMethod_var);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral2135999681);
		Text_t356221433 * L_3 = __this->get_message_8();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m2159020946(L_3, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_4, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_5 = __this->get_messageDescription_9();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral1989231835);
		Text_t356221433 * L_6 = __this->get_messageDescription_9();
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m2159020946(L_6, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_7, (bool)1, /*hidden argument*/NULL);
		Image_t2042527209 * L_8 = __this->get_damageFrame_1();
		Color_t2020392075  L_9 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_8);
		V_0 = L_9;
		(&V_0)->set_a_3((1.0f));
		Image_t2042527209 * L_10 = __this->get_damageFrame_1();
		Color_t2020392075  L_11 = V_0;
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_10, L_11);
		__this->set_freezeUI_14((bool)1);
	}

IL_007d:
	{
		GameObject_t1756533147 * L_12 = __this->get_deathCam_12();
		GameObject_SetActive_m2693135142(L_12, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIController::drowningDeathMessage()
extern "C"  void UIController_drowningDeathMessage_m3043734374 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_drowningDeathMessage_m3043734374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_007d;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_message_8();
		Text_t356221433 * L_2 = Component_GetComponent_TisText_t356221433_m1342661039(L_1, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_RuntimeMethod_var);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral1422551141);
		Text_t356221433 * L_3 = __this->get_message_8();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m2159020946(L_3, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_4, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_5 = __this->get_messageDescription_9();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral3872431501);
		Text_t356221433 * L_6 = __this->get_messageDescription_9();
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m2159020946(L_6, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_7, (bool)1, /*hidden argument*/NULL);
		Image_t2042527209 * L_8 = __this->get_damageFrame_1();
		Color_t2020392075  L_9 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_8);
		V_0 = L_9;
		(&V_0)->set_a_3((1.0f));
		Image_t2042527209 * L_10 = __this->get_damageFrame_1();
		Color_t2020392075  L_11 = V_0;
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_10, L_11);
		__this->set_freezeUI_14((bool)1);
	}

IL_007d:
	{
		GameObject_t1756533147 * L_12 = __this->get_deathCam_12();
		GameObject_SetActive_m2693135142(L_12, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIController::shipUnderAttackMessage()
extern "C"  void UIController_shipUnderAttackMessage_m859257986 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_shipUnderAttackMessage_m859257986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_shipAttackedDelay_13((1.0f));
		Text_t356221433 * L_1 = __this->get_subMessage_10();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteral930836742);
		Text_t356221433 * L_2 = __this->get_subMessage_10();
		Color_t2020392075  L_3 = Color_get_red_m3374418718(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_2, L_3);
		Text_t356221433 * L_4 = __this->get_subMessage_10();
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m2159020946(L_4, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_5, (bool)1, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// System.Void UIController::shipDestroyedMessage()
extern "C"  void UIController_shipDestroyedMessage_m4291651381 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_shipDestroyedMessage_m4291651381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_0078;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_message_8();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteral3605215308);
		Text_t356221433 * L_2 = __this->get_message_8();
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m2159020946(L_2, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_3, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_4 = __this->get_messageDescription_9();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral2017188972);
		Text_t356221433 * L_5 = __this->get_messageDescription_9();
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m2159020946(L_5, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_6, (bool)1, /*hidden argument*/NULL);
		Image_t2042527209 * L_7 = __this->get_damageFrame_1();
		Color_t2020392075  L_8 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_7);
		V_0 = L_8;
		(&V_0)->set_a_3((1.0f));
		Image_t2042527209 * L_9 = __this->get_damageFrame_1();
		Color_t2020392075  L_10 = V_0;
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		__this->set_freezeUI_14((bool)1);
	}

IL_0078:
	{
		return;
	}
}
// System.Void UIController::partsNotCollectedMessage()
extern "C"  void UIController_partsNotCollectedMessage_m867535828 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_partsNotCollectedMessage_m867535828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_0054;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_message_8();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteral220393588);
		Text_t356221433 * L_2 = __this->get_message_8();
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m2159020946(L_2, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_3, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_4 = __this->get_messageDescription_9();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral3063152238);
		Text_t356221433 * L_5 = __this->get_messageDescription_9();
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m2159020946(L_5, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_6, (bool)1, /*hidden argument*/NULL);
		__this->set_freezeUI_14((bool)1);
	}

IL_0054:
	{
		return;
	}
}
// System.Void UIController::partAlreadyHeldMessage()
extern "C"  void UIController_partAlreadyHeldMessage_m1437698322 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_partAlreadyHeldMessage_m1437698322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_subMessage_10();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral2253030761);
		Text_t356221433 * L_1 = __this->get_subMessage_10();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m2159020946(L_1, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIController::waveCompleteMessage(System.Int32)
extern "C"  void UIController_waveCompleteMessage_m112210205 (UIController_t2029583246 * __this, int32_t ___waveNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_waveCompleteMessage_m112210205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_003c;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_message_8();
		int32_t L_2 = ___waveNumber0;
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral4162974409, L_4, _stringLiteral3049095124, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_5);
		Text_t356221433 * L_6 = __this->get_message_8();
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m2159020946(L_6, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_7, (bool)1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void UIController::waveCountdownMessage(System.Int32,System.Single)
extern "C"  void UIController_waveCountdownMessage_m2891189168 (UIController_t2029583246 * __this, int32_t ___waveNumber0, float ___roundDelay1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_waveCountdownMessage_m2891189168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_0062;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_subMessage_10();
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		ArrayElementTypeCheck (L_2, _stringLiteral2515819241);
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2515819241);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		int32_t L_4 = ___waveNumber0;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_6);
		ObjectU5BU5D_t3614634134* L_7 = L_3;
		ArrayElementTypeCheck (L_7, _stringLiteral1258241875);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1258241875);
		ObjectU5BU5D_t3614634134* L_8 = L_7;
		String_t* L_9 = Single_ToString_m2359963436((&___roundDelay1), _stringLiteral98844844, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_8;
		ArrayElementTypeCheck (L_10, _stringLiteral1623555996);
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1623555996);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3881798623(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_11);
		Text_t356221433 * L_12 = __this->get_subMessage_10();
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m2159020946(L_12, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_13, (bool)1, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UIController::gameWonMessage()
extern "C"  void UIController_gameWonMessage_m284319100 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_gameWonMessage_m284319100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_0054;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_message_8();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteral320178848);
		Text_t356221433 * L_2 = __this->get_message_8();
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m2159020946(L_2, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_3, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_4 = __this->get_messageDescription_9();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral1627550058);
		Text_t356221433 * L_5 = __this->get_messageDescription_9();
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m2159020946(L_5, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_6, (bool)1, /*hidden argument*/NULL);
		__this->set_freezeUI_14((bool)1);
	}

IL_0054:
	{
		return;
	}
}
// System.Void UIController::updateTime(System.Single)
extern "C"  void UIController_updateTime_m340223966 (UIController_t2029583246 * __this, float ___time0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_updateTime_m340223966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_freezeUI_14();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		Text_t356221433 * L_1 = __this->get_timer_6();
		String_t* L_2 = Single_ToString_m2359963436((&___time0), _stringLiteral98844844, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UIController::updatePartsReturned(System.Int32,System.Int32)
extern "C"  void UIController_updatePartsReturned_m494708189 (UIController_t2029583246 * __this, int32_t ___returned0, int32_t ___required1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_updatePartsReturned_m494708189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_partsCollected_7();
		int32_t L_1 = ___returned0;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = ___required1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2000667605(NULL /*static, unused*/, L_3, _stringLiteral57471863, L_6, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_7);
		return;
	}
}
// System.Void UIController::updatePartIcon(System.Boolean)
extern "C"  void UIController_updatePartIcon_m265678373 (UIController_t2029583246 * __this, bool ___partHeld0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___partHeld0;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_partIcon_11();
		GameObject_SetActive_m2693135142(L_1, (bool)1, /*hidden argument*/NULL);
		goto IL_0023;
	}

IL_0017:
	{
		GameObject_t1756533147 * L_2 = __this->get_partIcon_11();
		GameObject_SetActive_m2693135142(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void UIController::updateDamageFrame(System.Single)
extern "C"  void UIController_updateDamageFrame_m339031365 (UIController_t2029583246 * __this, float ___health0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_updateDamageFrame_m339031365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		bool L_2 = __this->get_freezeUI_14();
		if (L_2)
		{
			goto IL_0051;
		}
	}
	{
		Image_t2042527209 * L_3 = __this->get_damageFrame_1();
		Color_t2020392075  L_4 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_3);
		V_0 = L_4;
		float L_5 = ___health0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = fabsf(((float)((float)L_5-(float)(100.0f))));
		(&V_0)->set_a_3(((float)((float)L_6/(float)(100.0f))));
		Image_t2042527209 * L_7 = __this->get_damageFrame_1();
		Color_t2020392075  L_8 = V_0;
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_7, L_8);
	}

IL_0051:
	{
		return;
	}
}
// System.Void UIController::beginWave(System.Int32)
extern "C"  void UIController_beginWave_m463379384 (UIController_t2029583246 * __this, int32_t ___waveNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIController_beginWave_m463379384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_waveCounter_5();
		int32_t L_1 = ___waveNumber0;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral4162974409, L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		Text_t356221433 * L_5 = __this->get_message_8();
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m2159020946(L_5, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_6, (bool)0, /*hidden argument*/NULL);
		Text_t356221433 * L_7 = __this->get_subMessage_10();
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m2159020946(L_7, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_8, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIController::clearSubMessage()
extern "C"  void UIController_clearSubMessage_m1922647077 (UIController_t2029583246 * __this, const RuntimeMethod* method)
{
	{
		Text_t356221433 * L_0 = __this->get_subMessage_10();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
